import { base_url } from "./config.js"

class HttpRequest {
    request( url, method, params ) {//相当于es5 HttpRequest.prototype.request=function(){}
        uni.showLoading({
            title: '正在加载中...'
        });
        return new Promise((resolve, reject) => {
            uni.request({
                url: base_url + url,//仅为示例，并非真实接口地址。
                data: params,
                method,
                // header: {
                //     'custom-header': 'hello' //自定义请求头信息
                // },
                success: (res) => {
                    if (res.statusCode === 200) {
                        resolve(res.data)
                        uni.hideLoading();
                    }

                },
                fail: (err) => {
                    reject(err);
                    uni.hideLoading();
                    uni.showToast({
                        title: '请求失败',
                        duration: 2000
                    });
                },
                complete: () => {//调用成功、失败都会执行
                    uni.hideLoading();
                }
            });
        })
    }
    get(url, params) {
		console.log(params)
        return this.request( url, 'get', params )
    }
    post(url, params) {
        return this.request( url, 'POST', params )
    }
}

export default new HttpRequest();