import {login} from '../../api/login'

const user={
    namespaced: true,//开启命名空间
    state:()=>{
        return {
            token:uni.getStorageSync('token')||'',
            userInfo:uni.getStorageSync('userInfo')||{},
        };
    },
    mutations: {
        changeInfo(state,payload){
            state.token=payload.token;
            state.userInfo=payload.userInfo
            this.commit('user/savestoreage')
        },
        savestoreage(state){
            uni.setStorageSync('token',state.token)
            uni.setStorageSync('userInfo',state.userInfo)
        }
    },
    actions: {
       async getToken({state,commit},{code,userInfo}){
            let res=await login({
                principal:code,
            });
            console.log(res);
            commit('changeInfo',{token:res.access_token,userInfo})
        },
        async islogin ({state}){
            if(state.token){
                
            }
        }
    },
 }

 export default user