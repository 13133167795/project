import Vue from 'vue'
import Vuex from 'vuex'
import total from './modules/total'
import user from './modules/user'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules:{
    total,
    user
  }
})
export default store