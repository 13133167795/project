import httprequest from "../utils/httprequest";

//首页轮播图接口
export const indexImgs = (params) => httprequest.get("/indexImgs", params)
//首页数据接口
export const indexlist = (params) => httprequest.get("/prod/tagProdList", params)
//首页更多数据接口
export const indexnotice = (params) => httprequest.get("/shop/notice/noticeList", params)
//首页更多跳转
export const indexlastedprodpage = (params) => httprequest.get("/prod/prodListByTagId", params)