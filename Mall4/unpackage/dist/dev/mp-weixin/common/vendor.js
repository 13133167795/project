(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["common/vendor"],[
/* 0 */,
/* 1 */
/*!************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {Object.defineProperty(exports, "__esModule", { value: true });exports.createApp = createApp;exports.createComponent = createComponent;exports.createPage = createPage;exports.createPlugin = createPlugin;exports.createSubpackageApp = createSubpackageApp;exports.default = void 0;var _uniI18n = __webpack_require__(/*! @dcloudio/uni-i18n */ 3);
var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 4));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}function ownKeys(object, enumerableOnly) {var keys = Object.keys(object);if (Object.getOwnPropertySymbols) {var symbols = Object.getOwnPropertySymbols(object);if (enumerableOnly) symbols = symbols.filter(function (sym) {return Object.getOwnPropertyDescriptor(object, sym).enumerable;});keys.push.apply(keys, symbols);}return keys;}function _objectSpread(target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i] != null ? arguments[i] : {};if (i % 2) {ownKeys(Object(source), true).forEach(function (key) {_defineProperty(target, key, source[key]);});} else if (Object.getOwnPropertyDescriptors) {Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));} else {ownKeys(Object(source)).forEach(function (key) {Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));});}}return target;}function _slicedToArray(arr, i) {return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();}function _nonIterableRest() {throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _iterableToArrayLimit(arr, i) {if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;var _arr = [];var _n = true;var _d = false;var _e = undefined;try {for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {_arr.push(_s.value);if (i && _arr.length === i) break;}} catch (err) {_d = true;_e = err;} finally {try {if (!_n && _i["return"] != null) _i["return"]();} finally {if (_d) throw _e;}}return _arr;}function _arrayWithHoles(arr) {if (Array.isArray(arr)) return arr;}function _defineProperty(obj, key, value) {if (key in obj) {Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });} else {obj[key] = value;}return obj;}function _toConsumableArray(arr) {return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();}function _nonIterableSpread() {throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _unsupportedIterableToArray(o, minLen) {if (!o) return;if (typeof o === "string") return _arrayLikeToArray(o, minLen);var n = Object.prototype.toString.call(o).slice(8, -1);if (n === "Object" && o.constructor) n = o.constructor.name;if (n === "Map" || n === "Set") return Array.from(o);if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);}function _iterableToArray(iter) {if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);}function _arrayWithoutHoles(arr) {if (Array.isArray(arr)) return _arrayLikeToArray(arr);}function _arrayLikeToArray(arr, len) {if (len == null || len > arr.length) len = arr.length;for (var i = 0, arr2 = new Array(len); i < len; i++) {arr2[i] = arr[i];}return arr2;}

var realAtob;

var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
var b64re = /^(?:[A-Za-z\d+/]{4})*?(?:[A-Za-z\d+/]{2}(?:==)?|[A-Za-z\d+/]{3}=?)?$/;

if (typeof atob !== 'function') {
  realAtob = function realAtob(str) {
    str = String(str).replace(/[\t\n\f\r ]+/g, '');
    if (!b64re.test(str)) {throw new Error("Failed to execute 'atob' on 'Window': The string to be decoded is not correctly encoded.");}

    // Adding the padding if missing, for semplicity
    str += '=='.slice(2 - (str.length & 3));
    var bitmap;var result = '';var r1;var r2;var i = 0;
    for (; i < str.length;) {
      bitmap = b64.indexOf(str.charAt(i++)) << 18 | b64.indexOf(str.charAt(i++)) << 12 |
      (r1 = b64.indexOf(str.charAt(i++))) << 6 | (r2 = b64.indexOf(str.charAt(i++)));

      result += r1 === 64 ? String.fromCharCode(bitmap >> 16 & 255) :
      r2 === 64 ? String.fromCharCode(bitmap >> 16 & 255, bitmap >> 8 & 255) :
      String.fromCharCode(bitmap >> 16 & 255, bitmap >> 8 & 255, bitmap & 255);
    }
    return result;
  };
} else {
  // 注意atob只能在全局对象上调用，例如：`const Base64 = {atob};Base64.atob('xxxx')`是错误的用法
  realAtob = atob;
}

function b64DecodeUnicode(str) {
  return decodeURIComponent(realAtob(str).split('').map(function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
}

function getCurrentUserInfo() {
  var token = wx.getStorageSync('uni_id_token') || '';
  var tokenArr = token.split('.');
  if (!token || tokenArr.length !== 3) {
    return {
      uid: null,
      role: [],
      permission: [],
      tokenExpired: 0 };

  }
  var userInfo;
  try {
    userInfo = JSON.parse(b64DecodeUnicode(tokenArr[1]));
  } catch (error) {
    throw new Error('获取当前用户信息出错，详细错误信息为：' + error.message);
  }
  userInfo.tokenExpired = userInfo.exp * 1000;
  delete userInfo.exp;
  delete userInfo.iat;
  return userInfo;
}

function uniIdMixin(Vue) {
  Vue.prototype.uniIDHasRole = function (roleId) {var _getCurrentUserInfo =


    getCurrentUserInfo(),role = _getCurrentUserInfo.role;
    return role.indexOf(roleId) > -1;
  };
  Vue.prototype.uniIDHasPermission = function (permissionId) {var _getCurrentUserInfo2 =


    getCurrentUserInfo(),permission = _getCurrentUserInfo2.permission;
    return this.uniIDHasRole('admin') || permission.indexOf(permissionId) > -1;
  };
  Vue.prototype.uniIDTokenValid = function () {var _getCurrentUserInfo3 =


    getCurrentUserInfo(),tokenExpired = _getCurrentUserInfo3.tokenExpired;
    return tokenExpired > Date.now();
  };
}

var _toString = Object.prototype.toString;
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isFn(fn) {
  return typeof fn === 'function';
}

function isStr(str) {
  return typeof str === 'string';
}

function isPlainObject(obj) {
  return _toString.call(obj) === '[object Object]';
}

function hasOwn(obj, key) {
  return hasOwnProperty.call(obj, key);
}

function noop() {}

/**
                    * Create a cached version of a pure function.
                    */
function cached(fn) {
  var cache = Object.create(null);
  return function cachedFn(str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str));
  };
}

/**
   * Camelize a hyphen-delimited string.
   */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) {return c ? c.toUpperCase() : '';});
});

function sortObject(obj) {
  var sortObj = {};
  if (isPlainObject(obj)) {
    Object.keys(obj).sort().forEach(function (key) {
      sortObj[key] = obj[key];
    });
  }
  return !Object.keys(sortObj) ? obj : sortObj;
}

var HOOKS = [
'invoke',
'success',
'fail',
'complete',
'returnValue'];


var globalInterceptors = {};
var scopedInterceptors = {};

function mergeHook(parentVal, childVal) {
  var res = childVal ?
  parentVal ?
  parentVal.concat(childVal) :
  Array.isArray(childVal) ?
  childVal : [childVal] :
  parentVal;
  return res ?
  dedupeHooks(res) :
  res;
}

function dedupeHooks(hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res;
}

function removeHook(hooks, hook) {
  var index = hooks.indexOf(hook);
  if (index !== -1) {
    hooks.splice(index, 1);
  }
}

function mergeInterceptorHook(interceptor, option) {
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      interceptor[hook] = mergeHook(interceptor[hook], option[hook]);
    }
  });
}

function removeInterceptorHook(interceptor, option) {
  if (!interceptor || !option) {
    return;
  }
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      removeHook(interceptor[hook], option[hook]);
    }
  });
}

function addInterceptor(method, option) {
  if (typeof method === 'string' && isPlainObject(option)) {
    mergeInterceptorHook(scopedInterceptors[method] || (scopedInterceptors[method] = {}), option);
  } else if (isPlainObject(method)) {
    mergeInterceptorHook(globalInterceptors, method);
  }
}

function removeInterceptor(method, option) {
  if (typeof method === 'string') {
    if (isPlainObject(option)) {
      removeInterceptorHook(scopedInterceptors[method], option);
    } else {
      delete scopedInterceptors[method];
    }
  } else if (isPlainObject(method)) {
    removeInterceptorHook(globalInterceptors, method);
  }
}

function wrapperHook(hook) {
  return function (data) {
    return hook(data) || data;
  };
}

function isPromise(obj) {
  return !!obj && (typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function';
}

function queue(hooks, data) {
  var promise = false;
  for (var i = 0; i < hooks.length; i++) {
    var hook = hooks[i];
    if (promise) {
      promise = Promise.resolve(wrapperHook(hook));
    } else {
      var res = hook(data);
      if (isPromise(res)) {
        promise = Promise.resolve(res);
      }
      if (res === false) {
        return {
          then: function then() {} };

      }
    }
  }
  return promise || {
    then: function then(callback) {
      return callback(data);
    } };

}

function wrapperOptions(interceptor) {var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  ['success', 'fail', 'complete'].forEach(function (name) {
    if (Array.isArray(interceptor[name])) {
      var oldCallback = options[name];
      options[name] = function callbackInterceptor(res) {
        queue(interceptor[name], res).then(function (res) {
          /* eslint-disable no-mixed-operators */
          return isFn(oldCallback) && oldCallback(res) || res;
        });
      };
    }
  });
  return options;
}

function wrapperReturnValue(method, returnValue) {
  var returnValueHooks = [];
  if (Array.isArray(globalInterceptors.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, _toConsumableArray(globalInterceptors.returnValue));
  }
  var interceptor = scopedInterceptors[method];
  if (interceptor && Array.isArray(interceptor.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, _toConsumableArray(interceptor.returnValue));
  }
  returnValueHooks.forEach(function (hook) {
    returnValue = hook(returnValue) || returnValue;
  });
  return returnValue;
}

function getApiInterceptorHooks(method) {
  var interceptor = Object.create(null);
  Object.keys(globalInterceptors).forEach(function (hook) {
    if (hook !== 'returnValue') {
      interceptor[hook] = globalInterceptors[hook].slice();
    }
  });
  var scopedInterceptor = scopedInterceptors[method];
  if (scopedInterceptor) {
    Object.keys(scopedInterceptor).forEach(function (hook) {
      if (hook !== 'returnValue') {
        interceptor[hook] = (interceptor[hook] || []).concat(scopedInterceptor[hook]);
      }
    });
  }
  return interceptor;
}

function invokeApi(method, api, options) {for (var _len = arguments.length, params = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {params[_key - 3] = arguments[_key];}
  var interceptor = getApiInterceptorHooks(method);
  if (interceptor && Object.keys(interceptor).length) {
    if (Array.isArray(interceptor.invoke)) {
      var res = queue(interceptor.invoke, options);
      return res.then(function (options) {
        return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
      });
    } else {
      return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
    }
  }
  return api.apply(void 0, [options].concat(params));
}

var promiseInterceptor = {
  returnValue: function returnValue(res) {
    if (!isPromise(res)) {
      return res;
    }
    return new Promise(function (resolve, reject) {
      res.then(function (res) {
        if (res[0]) {
          reject(res[0]);
        } else {
          resolve(res[1]);
        }
      });
    });
  } };


var SYNC_API_RE =
/^\$|Window$|WindowStyle$|sendHostEvent|sendNativeEvent|restoreGlobal|requireGlobal|getCurrentSubNVue|getMenuButtonBoundingClientRect|^report|interceptors|Interceptor$|getSubNVueById|requireNativePlugin|upx2px|hideKeyboard|canIUse|^create|Sync$|Manager$|base64ToArrayBuffer|arrayBufferToBase64|getLocale|setLocale|invokePushCallback|getWindowInfo|getDeviceInfo|getAppBaseInfo|getSystemSetting|getAppAuthorizeSetting/;

var CONTEXT_API_RE = /^create|Manager$/;

// Context例外情况
var CONTEXT_API_RE_EXC = ['createBLEConnection'];

// 同步例外情况
var ASYNC_API = ['createBLEConnection', 'createPushMessage'];

var CALLBACK_API_RE = /^on|^off/;

function isContextApi(name) {
  return CONTEXT_API_RE.test(name) && CONTEXT_API_RE_EXC.indexOf(name) === -1;
}
function isSyncApi(name) {
  return SYNC_API_RE.test(name) && ASYNC_API.indexOf(name) === -1;
}

function isCallbackApi(name) {
  return CALLBACK_API_RE.test(name) && name !== 'onPush';
}

function handlePromise(promise) {
  return promise.then(function (data) {
    return [null, data];
  }).
  catch(function (err) {return [err];});
}

function shouldPromise(name) {
  if (
  isContextApi(name) ||
  isSyncApi(name) ||
  isCallbackApi(name))
  {
    return false;
  }
  return true;
}

/* eslint-disable no-extend-native */
if (!Promise.prototype.finally) {
  Promise.prototype.finally = function (callback) {
    var promise = this.constructor;
    return this.then(
    function (value) {return promise.resolve(callback()).then(function () {return value;});},
    function (reason) {return promise.resolve(callback()).then(function () {
        throw reason;
      });});

  };
}

function promisify(name, api) {
  if (!shouldPromise(name)) {
    return api;
  }
  return function promiseApi() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};for (var _len2 = arguments.length, params = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {params[_key2 - 1] = arguments[_key2];}
    if (isFn(options.success) || isFn(options.fail) || isFn(options.complete)) {
      return wrapperReturnValue(name, invokeApi.apply(void 0, [name, api, options].concat(params)));
    }
    return wrapperReturnValue(name, handlePromise(new Promise(function (resolve, reject) {
      invokeApi.apply(void 0, [name, api, Object.assign({}, options, {
        success: resolve,
        fail: reject })].concat(
      params));
    })));
  };
}

var EPS = 1e-4;
var BASE_DEVICE_WIDTH = 750;
var isIOS = false;
var deviceWidth = 0;
var deviceDPR = 0;

function checkDeviceWidth() {var _wx$getSystemInfoSync =




  wx.getSystemInfoSync(),platform = _wx$getSystemInfoSync.platform,pixelRatio = _wx$getSystemInfoSync.pixelRatio,windowWidth = _wx$getSystemInfoSync.windowWidth; // uni=>wx runtime 编译目标是 uni 对象，内部不允许直接使用 uni

  deviceWidth = windowWidth;
  deviceDPR = pixelRatio;
  isIOS = platform === 'ios';
}

function upx2px(number, newDeviceWidth) {
  if (deviceWidth === 0) {
    checkDeviceWidth();
  }

  number = Number(number);
  if (number === 0) {
    return 0;
  }
  var result = number / BASE_DEVICE_WIDTH * (newDeviceWidth || deviceWidth);
  if (result < 0) {
    result = -result;
  }
  result = Math.floor(result + EPS);
  if (result === 0) {
    if (deviceDPR === 1 || !isIOS) {
      result = 1;
    } else {
      result = 0.5;
    }
  }
  return number < 0 ? -result : result;
}

var LOCALE_ZH_HANS = 'zh-Hans';
var LOCALE_ZH_HANT = 'zh-Hant';
var LOCALE_EN = 'en';
var LOCALE_FR = 'fr';
var LOCALE_ES = 'es';

var messages = {};

var locale;

{
  locale = normalizeLocale(wx.getSystemInfoSync().language) || LOCALE_EN;
}

function initI18nMessages() {
  if (!isEnableLocale()) {
    return;
  }
  var localeKeys = Object.keys(__uniConfig.locales);
  if (localeKeys.length) {
    localeKeys.forEach(function (locale) {
      var curMessages = messages[locale];
      var userMessages = __uniConfig.locales[locale];
      if (curMessages) {
        Object.assign(curMessages, userMessages);
      } else {
        messages[locale] = userMessages;
      }
    });
  }
}

initI18nMessages();

var i18n = (0, _uniI18n.initVueI18n)(
locale,
{});

var t = i18n.t;
var i18nMixin = i18n.mixin = {
  beforeCreate: function beforeCreate() {var _this = this;
    var unwatch = i18n.i18n.watchLocale(function () {
      _this.$forceUpdate();
    });
    this.$once('hook:beforeDestroy', function () {
      unwatch();
    });
  },
  methods: {
    $$t: function $$t(key, values) {
      return t(key, values);
    } } };


var setLocale = i18n.setLocale;
var getLocale = i18n.getLocale;

function initAppLocale(Vue, appVm, locale) {
  var state = Vue.observable({
    locale: locale || i18n.getLocale() });

  var localeWatchers = [];
  appVm.$watchLocale = function (fn) {
    localeWatchers.push(fn);
  };
  Object.defineProperty(appVm, '$locale', {
    get: function get() {
      return state.locale;
    },
    set: function set(v) {
      state.locale = v;
      localeWatchers.forEach(function (watch) {return watch(v);});
    } });

}

function isEnableLocale() {
  return typeof __uniConfig !== 'undefined' && __uniConfig.locales && !!Object.keys(__uniConfig.locales).length;
}

function include(str, parts) {
  return !!parts.find(function (part) {return str.indexOf(part) !== -1;});
}

function startsWith(str, parts) {
  return parts.find(function (part) {return str.indexOf(part) === 0;});
}

function normalizeLocale(locale, messages) {
  if (!locale) {
    return;
  }
  locale = locale.trim().replace(/_/g, '-');
  if (messages && messages[locale]) {
    return locale;
  }
  locale = locale.toLowerCase();
  if (locale === 'chinese') {
    // 支付宝
    return LOCALE_ZH_HANS;
  }
  if (locale.indexOf('zh') === 0) {
    if (locale.indexOf('-hans') > -1) {
      return LOCALE_ZH_HANS;
    }
    if (locale.indexOf('-hant') > -1) {
      return LOCALE_ZH_HANT;
    }
    if (include(locale, ['-tw', '-hk', '-mo', '-cht'])) {
      return LOCALE_ZH_HANT;
    }
    return LOCALE_ZH_HANS;
  }
  var lang = startsWith(locale, [LOCALE_EN, LOCALE_FR, LOCALE_ES]);
  if (lang) {
    return lang;
  }
}
// export function initI18n() {
//   const localeKeys = Object.keys(__uniConfig.locales || {})
//   if (localeKeys.length) {
//     localeKeys.forEach((locale) =>
//       i18n.add(locale, __uniConfig.locales[locale])
//     )
//   }
// }

function getLocale$1() {
  // 优先使用 $locale
  var app = getApp({
    allowDefault: true });

  if (app && app.$vm) {
    return app.$vm.$locale;
  }
  return normalizeLocale(wx.getSystemInfoSync().language) || LOCALE_EN;
}

function setLocale$1(locale) {
  var app = getApp();
  if (!app) {
    return false;
  }
  var oldLocale = app.$vm.$locale;
  if (oldLocale !== locale) {
    app.$vm.$locale = locale;
    onLocaleChangeCallbacks.forEach(function (fn) {return fn({
        locale: locale });});

    return true;
  }
  return false;
}

var onLocaleChangeCallbacks = [];
function onLocaleChange(fn) {
  if (onLocaleChangeCallbacks.indexOf(fn) === -1) {
    onLocaleChangeCallbacks.push(fn);
  }
}

if (typeof global !== 'undefined') {
  global.getLocale = getLocale$1;
}

var interceptors = {
  promiseInterceptor: promiseInterceptor };


var baseApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  upx2px: upx2px,
  getLocale: getLocale$1,
  setLocale: setLocale$1,
  onLocaleChange: onLocaleChange,
  addInterceptor: addInterceptor,
  removeInterceptor: removeInterceptor,
  interceptors: interceptors });


function findExistsPageIndex(url) {
  var pages = getCurrentPages();
  var len = pages.length;
  while (len--) {
    var page = pages[len];
    if (page.$page && page.$page.fullPath === url) {
      return len;
    }
  }
  return -1;
}

var redirectTo = {
  name: function name(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.delta) {
      return 'navigateBack';
    }
    return 'redirectTo';
  },
  args: function args(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.url) {
      var existsPageIndex = findExistsPageIndex(fromArgs.url);
      if (existsPageIndex !== -1) {
        var delta = getCurrentPages().length - 1 - existsPageIndex;
        if (delta > 0) {
          fromArgs.delta = delta;
        }
      }
    }
  } };


var previewImage = {
  args: function args(fromArgs) {
    var currentIndex = parseInt(fromArgs.current);
    if (isNaN(currentIndex)) {
      return;
    }
    var urls = fromArgs.urls;
    if (!Array.isArray(urls)) {
      return;
    }
    var len = urls.length;
    if (!len) {
      return;
    }
    if (currentIndex < 0) {
      currentIndex = 0;
    } else if (currentIndex >= len) {
      currentIndex = len - 1;
    }
    if (currentIndex > 0) {
      fromArgs.current = urls[currentIndex];
      fromArgs.urls = urls.filter(
      function (item, index) {return index < currentIndex ? item !== urls[currentIndex] : true;});

    } else {
      fromArgs.current = urls[0];
    }
    return {
      indicator: false,
      loop: false };

  } };


var UUID_KEY = '__DC_STAT_UUID';
var deviceId;
function useDeviceId(result) {
  deviceId = deviceId || wx.getStorageSync(UUID_KEY);
  if (!deviceId) {
    deviceId = Date.now() + '' + Math.floor(Math.random() * 1e7);
    wx.setStorage({
      key: UUID_KEY,
      data: deviceId });

  }
  result.deviceId = deviceId;
}

function addSafeAreaInsets(result) {
  if (result.safeArea) {
    var safeArea = result.safeArea;
    result.safeAreaInsets = {
      top: safeArea.top,
      left: safeArea.left,
      right: result.windowWidth - safeArea.right,
      bottom: result.screenHeight - safeArea.bottom };

  }
}

function populateParameters(result) {var _result$brand =





  result.brand,brand = _result$brand === void 0 ? '' : _result$brand,_result$model = result.model,model = _result$model === void 0 ? '' : _result$model,_result$system = result.system,system = _result$system === void 0 ? '' : _result$system,_result$language = result.language,language = _result$language === void 0 ? '' : _result$language,theme = result.theme,version = result.version,platform = result.platform,fontSizeSetting = result.fontSizeSetting,SDKVersion = result.SDKVersion,pixelRatio = result.pixelRatio,deviceOrientation = result.deviceOrientation;
  // const isQuickApp = "mp-weixin".indexOf('quickapp-webview') !== -1

  // osName osVersion
  var osName = '';
  var osVersion = '';
  {
    osName = system.split(' ')[0] || '';
    osVersion = system.split(' ')[1] || '';
  }
  var hostVersion = version;

  // deviceType
  var deviceType = getGetDeviceType(result, model);

  // deviceModel
  var deviceBrand = getDeviceBrand(brand);

  // hostName
  var _hostName = getHostName(result);

  // deviceOrientation
  var _deviceOrientation = deviceOrientation; // 仅 微信 百度 支持

  // devicePixelRatio
  var _devicePixelRatio = pixelRatio;

  // SDKVersion
  var _SDKVersion = SDKVersion;

  // hostLanguage
  var hostLanguage = language.replace(/_/g, '-');

  // wx.getAccountInfoSync

  var parameters = {
    appId: "__UNI__CFB204B",
    appName: "Mall4",
    appVersion: "1.0.0",
    appVersionCode: "100",
    appLanguage: getAppLanguage(hostLanguage),
    uniCompileVersion: "3.6.4",
    uniRuntimeVersion: "3.6.4",
    uniPlatform: undefined || "mp-weixin",
    deviceBrand: deviceBrand,
    deviceModel: model,
    deviceType: deviceType,
    devicePixelRatio: _devicePixelRatio,
    deviceOrientation: _deviceOrientation,
    osName: osName.toLocaleLowerCase(),
    osVersion: osVersion,
    hostTheme: theme,
    hostVersion: hostVersion,
    hostLanguage: hostLanguage,
    hostName: _hostName,
    hostSDKVersion: _SDKVersion,
    hostFontSizeSetting: fontSizeSetting,
    windowTop: 0,
    windowBottom: 0,
    // TODO
    osLanguage: undefined,
    osTheme: undefined,
    ua: undefined,
    hostPackageName: undefined,
    browserName: undefined,
    browserVersion: undefined };


  Object.assign(result, parameters);
}

function getGetDeviceType(result, model) {
  var deviceType = result.deviceType || 'phone';
  {
    var deviceTypeMaps = {
      ipad: 'pad',
      windows: 'pc',
      mac: 'pc' };

    var deviceTypeMapsKeys = Object.keys(deviceTypeMaps);
    var _model = model.toLocaleLowerCase();
    for (var index = 0; index < deviceTypeMapsKeys.length; index++) {
      var _m = deviceTypeMapsKeys[index];
      if (_model.indexOf(_m) !== -1) {
        deviceType = deviceTypeMaps[_m];
        break;
      }
    }
  }
  return deviceType;
}

function getDeviceBrand(brand) {
  var deviceBrand = brand;
  if (deviceBrand) {
    deviceBrand = brand.toLocaleLowerCase();
  }
  return deviceBrand;
}

function getAppLanguage(defaultLanguage) {
  return getLocale$1 ?
  getLocale$1() :
  defaultLanguage;
}

function getHostName(result) {
  var _platform = 'WeChat';
  var _hostName = result.hostName || _platform; // mp-jd
  {
    if (result.environment) {
      _hostName = result.environment;
    } else if (result.host && result.host.env) {
      _hostName = result.host.env;
    }
  }

  return _hostName;
}

var getSystemInfo = {
  returnValue: function returnValue(result) {
    useDeviceId(result);
    addSafeAreaInsets(result);
    populateParameters(result);
  } };


var showActionSheet = {
  args: function args(fromArgs) {
    if (typeof fromArgs === 'object') {
      fromArgs.alertText = fromArgs.title;
    }
  } };


var getAppBaseInfo = {
  returnValue: function returnValue(result) {var _result =
    result,version = _result.version,language = _result.language,SDKVersion = _result.SDKVersion,theme = _result.theme;

    var _hostName = getHostName(result);

    var hostLanguage = language.replace('_', '-');

    result = sortObject(Object.assign(result, {
      appId: "__UNI__CFB204B",
      appName: "Mall4",
      appVersion: "1.0.0",
      appVersionCode: "100",
      appLanguage: getAppLanguage(hostLanguage),
      hostVersion: version,
      hostLanguage: hostLanguage,
      hostName: _hostName,
      hostSDKVersion: SDKVersion,
      hostTheme: theme }));

  } };


var getDeviceInfo = {
  returnValue: function returnValue(result) {var _result2 =
    result,brand = _result2.brand,model = _result2.model;
    var deviceType = getGetDeviceType(result, model);
    var deviceBrand = getDeviceBrand(brand);
    useDeviceId(result);

    result = sortObject(Object.assign(result, {
      deviceType: deviceType,
      deviceBrand: deviceBrand,
      deviceModel: model }));

  } };


var getWindowInfo = {
  returnValue: function returnValue(result) {
    addSafeAreaInsets(result);

    result = sortObject(Object.assign(result, {
      windowTop: 0,
      windowBottom: 0 }));

  } };


var getAppAuthorizeSetting = {
  returnValue: function returnValue(result) {var
    locationReducedAccuracy = result.locationReducedAccuracy;

    result.locationAccuracy = 'unsupported';
    if (locationReducedAccuracy === true) {
      result.locationAccuracy = 'reduced';
    } else if (locationReducedAccuracy === false) {
      result.locationAccuracy = 'full';
    }
  } };


// import navigateTo from 'uni-helpers/navigate-to'

var protocols = {
  redirectTo: redirectTo,
  // navigateTo,  // 由于在微信开发者工具的页面参数，会显示__id__参数，因此暂时关闭mp-weixin对于navigateTo的AOP
  previewImage: previewImage,
  getSystemInfo: getSystemInfo,
  getSystemInfoSync: getSystemInfo,
  showActionSheet: showActionSheet,
  getAppBaseInfo: getAppBaseInfo,
  getDeviceInfo: getDeviceInfo,
  getWindowInfo: getWindowInfo,
  getAppAuthorizeSetting: getAppAuthorizeSetting };

var todos = [
'vibrate',
'preloadPage',
'unPreloadPage',
'loadSubPackage'];

var canIUses = [];

var CALLBACKS = ['success', 'fail', 'cancel', 'complete'];

function processCallback(methodName, method, returnValue) {
  return function (res) {
    return method(processReturnValue(methodName, res, returnValue));
  };
}

function processArgs(methodName, fromArgs) {var argsOption = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};var returnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};var keepFromArgs = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
  if (isPlainObject(fromArgs)) {// 一般 api 的参数解析
    var toArgs = keepFromArgs === true ? fromArgs : {}; // returnValue 为 false 时，说明是格式化返回值，直接在返回值对象上修改赋值
    if (isFn(argsOption)) {
      argsOption = argsOption(fromArgs, toArgs) || {};
    }
    for (var key in fromArgs) {
      if (hasOwn(argsOption, key)) {
        var keyOption = argsOption[key];
        if (isFn(keyOption)) {
          keyOption = keyOption(fromArgs[key], fromArgs, toArgs);
        }
        if (!keyOption) {// 不支持的参数
          console.warn("The '".concat(methodName, "' method of platform '\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F' does not support option '").concat(key, "'"));
        } else if (isStr(keyOption)) {// 重写参数 key
          toArgs[keyOption] = fromArgs[key];
        } else if (isPlainObject(keyOption)) {// {name:newName,value:value}可重新指定参数 key:value
          toArgs[keyOption.name ? keyOption.name : key] = keyOption.value;
        }
      } else if (CALLBACKS.indexOf(key) !== -1) {
        if (isFn(fromArgs[key])) {
          toArgs[key] = processCallback(methodName, fromArgs[key], returnValue);
        }
      } else {
        if (!keepFromArgs) {
          toArgs[key] = fromArgs[key];
        }
      }
    }
    return toArgs;
  } else if (isFn(fromArgs)) {
    fromArgs = processCallback(methodName, fromArgs, returnValue);
  }
  return fromArgs;
}

function processReturnValue(methodName, res, returnValue) {var keepReturnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  if (isFn(protocols.returnValue)) {// 处理通用 returnValue
    res = protocols.returnValue(methodName, res);
  }
  return processArgs(methodName, res, returnValue, {}, keepReturnValue);
}

function wrapper(methodName, method) {
  if (hasOwn(protocols, methodName)) {
    var protocol = protocols[methodName];
    if (!protocol) {// 暂不支持的 api
      return function () {
        console.error("Platform '\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F' does not support '".concat(methodName, "'."));
      };
    }
    return function (arg1, arg2) {// 目前 api 最多两个参数
      var options = protocol;
      if (isFn(protocol)) {
        options = protocol(arg1);
      }

      arg1 = processArgs(methodName, arg1, options.args, options.returnValue);

      var args = [arg1];
      if (typeof arg2 !== 'undefined') {
        args.push(arg2);
      }
      if (isFn(options.name)) {
        methodName = options.name(arg1);
      } else if (isStr(options.name)) {
        methodName = options.name;
      }
      var returnValue = wx[methodName].apply(wx, args);
      if (isSyncApi(methodName)) {// 同步 api
        return processReturnValue(methodName, returnValue, options.returnValue, isContextApi(methodName));
      }
      return returnValue;
    };
  }
  return method;
}

var todoApis = Object.create(null);

var TODOS = [
'onTabBarMidButtonTap',
'subscribePush',
'unsubscribePush',
'onPush',
'offPush',
'share'];


function createTodoApi(name) {
  return function todoApi(_ref)


  {var fail = _ref.fail,complete = _ref.complete;
    var res = {
      errMsg: "".concat(name, ":fail method '").concat(name, "' not supported") };

    isFn(fail) && fail(res);
    isFn(complete) && complete(res);
  };
}

TODOS.forEach(function (name) {
  todoApis[name] = createTodoApi(name);
});

var providers = {
  oauth: ['weixin'],
  share: ['weixin'],
  payment: ['wxpay'],
  push: ['weixin'] };


function getProvider(_ref2)




{var service = _ref2.service,success = _ref2.success,fail = _ref2.fail,complete = _ref2.complete;
  var res = false;
  if (providers[service]) {
    res = {
      errMsg: 'getProvider:ok',
      service: service,
      provider: providers[service] };

    isFn(success) && success(res);
  } else {
    res = {
      errMsg: 'getProvider:fail service not found' };

    isFn(fail) && fail(res);
  }
  isFn(complete) && complete(res);
}

var extraApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  getProvider: getProvider });


var getEmitter = function () {
  var Emitter;
  return function getUniEmitter() {
    if (!Emitter) {
      Emitter = new _vue.default();
    }
    return Emitter;
  };
}();

function apply(ctx, method, args) {
  return ctx[method].apply(ctx, args);
}

function $on() {
  return apply(getEmitter(), '$on', Array.prototype.slice.call(arguments));
}
function $off() {
  return apply(getEmitter(), '$off', Array.prototype.slice.call(arguments));
}
function $once() {
  return apply(getEmitter(), '$once', Array.prototype.slice.call(arguments));
}
function $emit() {
  return apply(getEmitter(), '$emit', Array.prototype.slice.call(arguments));
}

var eventApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  $on: $on,
  $off: $off,
  $once: $once,
  $emit: $emit });


/**
                    * 框架内 try-catch
                    */
/**
                        * 开发者 try-catch
                        */
function tryCatch(fn) {
  return function () {
    try {
      return fn.apply(fn, arguments);
    } catch (e) {
      // TODO
      console.error(e);
    }
  };
}

function getApiCallbacks(params) {
  var apiCallbacks = {};
  for (var name in params) {
    var param = params[name];
    if (isFn(param)) {
      apiCallbacks[name] = tryCatch(param);
      delete params[name];
    }
  }
  return apiCallbacks;
}

var cid;
var cidErrMsg;
var enabled;

function normalizePushMessage(message) {
  try {
    return JSON.parse(message);
  } catch (e) {}
  return message;
}

function invokePushCallback(
args)
{
  if (args.type === 'enabled') {
    enabled = true;
  } else if (args.type === 'clientId') {
    cid = args.cid;
    cidErrMsg = args.errMsg;
    invokeGetPushCidCallbacks(cid, args.errMsg);
  } else if (args.type === 'pushMsg') {
    var message = {
      type: 'receive',
      data: normalizePushMessage(args.message) };

    for (var i = 0; i < onPushMessageCallbacks.length; i++) {
      var callback = onPushMessageCallbacks[i];
      callback(message);
      // 该消息已被阻止
      if (message.stopped) {
        break;
      }
    }
  } else if (args.type === 'click') {
    onPushMessageCallbacks.forEach(function (callback) {
      callback({
        type: 'click',
        data: normalizePushMessage(args.message) });

    });
  }
}

var getPushCidCallbacks = [];

function invokeGetPushCidCallbacks(cid, errMsg) {
  getPushCidCallbacks.forEach(function (callback) {
    callback(cid, errMsg);
  });
  getPushCidCallbacks.length = 0;
}

function getPushClientId(args) {
  if (!isPlainObject(args)) {
    args = {};
  }var _getApiCallbacks =




  getApiCallbacks(args),success = _getApiCallbacks.success,fail = _getApiCallbacks.fail,complete = _getApiCallbacks.complete;
  var hasSuccess = isFn(success);
  var hasFail = isFn(fail);
  var hasComplete = isFn(complete);

  Promise.resolve().then(function () {
    if (typeof enabled === 'undefined') {
      enabled = false;
      cid = '';
      cidErrMsg = 'uniPush is not enabled';
    }
    getPushCidCallbacks.push(function (cid, errMsg) {
      var res;
      if (cid) {
        res = {
          errMsg: 'getPushClientId:ok',
          cid: cid };

        hasSuccess && success(res);
      } else {
        res = {
          errMsg: 'getPushClientId:fail' + (errMsg ? ' ' + errMsg : '') };

        hasFail && fail(res);
      }
      hasComplete && complete(res);
    });
    if (typeof cid !== 'undefined') {
      invokeGetPushCidCallbacks(cid, cidErrMsg);
    }
  });
}

var onPushMessageCallbacks = [];
// 不使用 defineOnApi 实现，是因为 defineOnApi 依赖 UniServiceJSBridge ，该对象目前在小程序上未提供，故简单实现
var onPushMessage = function onPushMessage(fn) {
  if (onPushMessageCallbacks.indexOf(fn) === -1) {
    onPushMessageCallbacks.push(fn);
  }
};

var offPushMessage = function offPushMessage(fn) {
  if (!fn) {
    onPushMessageCallbacks.length = 0;
  } else {
    var index = onPushMessageCallbacks.indexOf(fn);
    if (index > -1) {
      onPushMessageCallbacks.splice(index, 1);
    }
  }
};

var api = /*#__PURE__*/Object.freeze({
  __proto__: null,
  getPushClientId: getPushClientId,
  onPushMessage: onPushMessage,
  offPushMessage: offPushMessage,
  invokePushCallback: invokePushCallback });


var MPPage = Page;
var MPComponent = Component;

var customizeRE = /:/g;

var customize = cached(function (str) {
  return camelize(str.replace(customizeRE, '-'));
});

function initTriggerEvent(mpInstance) {
  var oldTriggerEvent = mpInstance.triggerEvent;
  var newTriggerEvent = function newTriggerEvent(event) {for (var _len3 = arguments.length, args = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {args[_key3 - 1] = arguments[_key3];}
    // 事件名统一转驼峰格式，仅处理：当前组件为 vue 组件、当前组件为 vue 组件子组件
    if (this.$vm || this.dataset && this.dataset.comType) {
      event = customize(event);
    } else {
      // 针对微信/QQ小程序单独补充驼峰格式事件，以兼容历史项目
      var newEvent = customize(event);
      if (newEvent !== event) {
        oldTriggerEvent.apply(this, [newEvent].concat(args));
      }
    }
    return oldTriggerEvent.apply(this, [event].concat(args));
  };
  try {
    // 京东小程序 triggerEvent 为只读
    mpInstance.triggerEvent = newTriggerEvent;
  } catch (error) {
    mpInstance._triggerEvent = newTriggerEvent;
  }
}

function initHook(name, options, isComponent) {
  var oldHook = options[name];
  if (!oldHook) {
    options[name] = function () {
      initTriggerEvent(this);
    };
  } else {
    options[name] = function () {
      initTriggerEvent(this);for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {args[_key4] = arguments[_key4];}
      return oldHook.apply(this, args);
    };
  }
}
if (!MPPage.__$wrappered) {
  MPPage.__$wrappered = true;
  Page = function Page() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    initHook('onLoad', options);
    return MPPage(options);
  };
  Page.after = MPPage.after;

  Component = function Component() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    initHook('created', options);
    return MPComponent(options);
  };
}

var PAGE_EVENT_HOOKS = [
'onPullDownRefresh',
'onReachBottom',
'onAddToFavorites',
'onShareTimeline',
'onShareAppMessage',
'onPageScroll',
'onResize',
'onTabItemTap'];


function initMocks(vm, mocks) {
  var mpInstance = vm.$mp[vm.mpType];
  mocks.forEach(function (mock) {
    if (hasOwn(mpInstance, mock)) {
      vm[mock] = mpInstance[mock];
    }
  });
}

function hasHook(hook, vueOptions) {
  if (!vueOptions) {
    return true;
  }

  if (_vue.default.options && Array.isArray(_vue.default.options[hook])) {
    return true;
  }

  vueOptions = vueOptions.default || vueOptions;

  if (isFn(vueOptions)) {
    if (isFn(vueOptions.extendOptions[hook])) {
      return true;
    }
    if (vueOptions.super &&
    vueOptions.super.options &&
    Array.isArray(vueOptions.super.options[hook])) {
      return true;
    }
    return false;
  }

  if (isFn(vueOptions[hook])) {
    return true;
  }
  var mixins = vueOptions.mixins;
  if (Array.isArray(mixins)) {
    return !!mixins.find(function (mixin) {return hasHook(hook, mixin);});
  }
}

function initHooks(mpOptions, hooks, vueOptions) {
  hooks.forEach(function (hook) {
    if (hasHook(hook, vueOptions)) {
      mpOptions[hook] = function (args) {
        return this.$vm && this.$vm.__call_hook(hook, args);
      };
    }
  });
}

function initUnknownHooks(mpOptions, vueOptions) {var excludes = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
  findHooks(vueOptions).forEach(function (hook) {return initHook$1(mpOptions, hook, excludes);});
}

function findHooks(vueOptions) {var hooks = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  if (vueOptions) {
    Object.keys(vueOptions).forEach(function (name) {
      if (name.indexOf('on') === 0 && isFn(vueOptions[name])) {
        hooks.push(name);
      }
    });
  }
  return hooks;
}

function initHook$1(mpOptions, hook, excludes) {
  if (excludes.indexOf(hook) === -1 && !hasOwn(mpOptions, hook)) {
    mpOptions[hook] = function (args) {
      return this.$vm && this.$vm.__call_hook(hook, args);
    };
  }
}

function initVueComponent(Vue, vueOptions) {
  vueOptions = vueOptions.default || vueOptions;
  var VueComponent;
  if (isFn(vueOptions)) {
    VueComponent = vueOptions;
  } else {
    VueComponent = Vue.extend(vueOptions);
  }
  vueOptions = VueComponent.options;
  return [VueComponent, vueOptions];
}

function initSlots(vm, vueSlots) {
  if (Array.isArray(vueSlots) && vueSlots.length) {
    var $slots = Object.create(null);
    vueSlots.forEach(function (slotName) {
      $slots[slotName] = true;
    });
    vm.$scopedSlots = vm.$slots = $slots;
  }
}

function initVueIds(vueIds, mpInstance) {
  vueIds = (vueIds || '').split(',');
  var len = vueIds.length;

  if (len === 1) {
    mpInstance._$vueId = vueIds[0];
  } else if (len === 2) {
    mpInstance._$vueId = vueIds[0];
    mpInstance._$vuePid = vueIds[1];
  }
}

function initData(vueOptions, context) {
  var data = vueOptions.data || {};
  var methods = vueOptions.methods || {};

  if (typeof data === 'function') {
    try {
      data = data.call(context); // 支持 Vue.prototype 上挂的数据
    } catch (e) {
      if (Object({"VUE_APP_NAME":"Mall4","VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.warn('根据 Vue 的 data 函数初始化小程序 data 失败，请尽量确保 data 函数中不访问 vm 对象，否则可能影响首次数据渲染速度。', data);
      }
    }
  } else {
    try {
      // 对 data 格式化
      data = JSON.parse(JSON.stringify(data));
    } catch (e) {}
  }

  if (!isPlainObject(data)) {
    data = {};
  }

  Object.keys(methods).forEach(function (methodName) {
    if (context.__lifecycle_hooks__.indexOf(methodName) === -1 && !hasOwn(data, methodName)) {
      data[methodName] = methods[methodName];
    }
  });

  return data;
}

var PROP_TYPES = [String, Number, Boolean, Object, Array, null];

function createObserver(name) {
  return function observer(newVal, oldVal) {
    if (this.$vm) {
      this.$vm[name] = newVal; // 为了触发其他非 render watcher
    }
  };
}

function initBehaviors(vueOptions, initBehavior) {
  var vueBehaviors = vueOptions.behaviors;
  var vueExtends = vueOptions.extends;
  var vueMixins = vueOptions.mixins;

  var vueProps = vueOptions.props;

  if (!vueProps) {
    vueOptions.props = vueProps = [];
  }

  var behaviors = [];
  if (Array.isArray(vueBehaviors)) {
    vueBehaviors.forEach(function (behavior) {
      behaviors.push(behavior.replace('uni://', "wx".concat("://")));
      if (behavior === 'uni://form-field') {
        if (Array.isArray(vueProps)) {
          vueProps.push('name');
          vueProps.push('value');
        } else {
          vueProps.name = {
            type: String,
            default: '' };

          vueProps.value = {
            type: [String, Number, Boolean, Array, Object, Date],
            default: '' };

        }
      }
    });
  }
  if (isPlainObject(vueExtends) && vueExtends.props) {
    behaviors.push(
    initBehavior({
      properties: initProperties(vueExtends.props, true) }));


  }
  if (Array.isArray(vueMixins)) {
    vueMixins.forEach(function (vueMixin) {
      if (isPlainObject(vueMixin) && vueMixin.props) {
        behaviors.push(
        initBehavior({
          properties: initProperties(vueMixin.props, true) }));


      }
    });
  }
  return behaviors;
}

function parsePropType(key, type, defaultValue, file) {
  // [String]=>String
  if (Array.isArray(type) && type.length === 1) {
    return type[0];
  }
  return type;
}

function initProperties(props) {var isBehavior = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;var file = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';var options = arguments.length > 3 ? arguments[3] : undefined;
  var properties = {};
  if (!isBehavior) {
    properties.vueId = {
      type: String,
      value: '' };

    {
      if (options.virtualHost) {
        properties.virtualHostStyle = {
          type: null,
          value: '' };

        properties.virtualHostClass = {
          type: null,
          value: '' };

      }
    }
    // scopedSlotsCompiler auto
    properties.scopedSlotsCompiler = {
      type: String,
      value: '' };

    properties.vueSlots = { // 小程序不能直接定义 $slots 的 props，所以通过 vueSlots 转换到 $slots
      type: null,
      value: [],
      observer: function observer(newVal, oldVal) {
        var $slots = Object.create(null);
        newVal.forEach(function (slotName) {
          $slots[slotName] = true;
        });
        this.setData({
          $slots: $slots });

      } };

  }
  if (Array.isArray(props)) {// ['title']
    props.forEach(function (key) {
      properties[key] = {
        type: null,
        observer: createObserver(key) };

    });
  } else if (isPlainObject(props)) {// {title:{type:String,default:''},content:String}
    Object.keys(props).forEach(function (key) {
      var opts = props[key];
      if (isPlainObject(opts)) {// title:{type:String,default:''}
        var value = opts.default;
        if (isFn(value)) {
          value = value();
        }

        opts.type = parsePropType(key, opts.type);

        properties[key] = {
          type: PROP_TYPES.indexOf(opts.type) !== -1 ? opts.type : null,
          value: value,
          observer: createObserver(key) };

      } else {// content:String
        var type = parsePropType(key, opts);
        properties[key] = {
          type: PROP_TYPES.indexOf(type) !== -1 ? type : null,
          observer: createObserver(key) };

      }
    });
  }
  return properties;
}

function wrapper$1(event) {
  // TODO 又得兼容 mpvue 的 mp 对象
  try {
    event.mp = JSON.parse(JSON.stringify(event));
  } catch (e) {}

  event.stopPropagation = noop;
  event.preventDefault = noop;

  event.target = event.target || {};

  if (!hasOwn(event, 'detail')) {
    event.detail = {};
  }

  if (hasOwn(event, 'markerId')) {
    event.detail = typeof event.detail === 'object' ? event.detail : {};
    event.detail.markerId = event.markerId;
  }

  if (isPlainObject(event.detail)) {
    event.target = Object.assign({}, event.target, event.detail);
  }

  return event;
}

function getExtraValue(vm, dataPathsArray) {
  var context = vm;
  dataPathsArray.forEach(function (dataPathArray) {
    var dataPath = dataPathArray[0];
    var value = dataPathArray[2];
    if (dataPath || typeof value !== 'undefined') {// ['','',index,'disable']
      var propPath = dataPathArray[1];
      var valuePath = dataPathArray[3];

      var vFor;
      if (Number.isInteger(dataPath)) {
        vFor = dataPath;
      } else if (!dataPath) {
        vFor = context;
      } else if (typeof dataPath === 'string' && dataPath) {
        if (dataPath.indexOf('#s#') === 0) {
          vFor = dataPath.substr(3);
        } else {
          vFor = vm.__get_value(dataPath, context);
        }
      }

      if (Number.isInteger(vFor)) {
        context = value;
      } else if (!propPath) {
        context = vFor[value];
      } else {
        if (Array.isArray(vFor)) {
          context = vFor.find(function (vForItem) {
            return vm.__get_value(propPath, vForItem) === value;
          });
        } else if (isPlainObject(vFor)) {
          context = Object.keys(vFor).find(function (vForKey) {
            return vm.__get_value(propPath, vFor[vForKey]) === value;
          });
        } else {
          console.error('v-for 暂不支持循环数据：', vFor);
        }
      }

      if (valuePath) {
        context = vm.__get_value(valuePath, context);
      }
    }
  });
  return context;
}

function processEventExtra(vm, extra, event, __args__) {
  var extraObj = {};

  if (Array.isArray(extra) && extra.length) {
    /**
                                              *[
                                              *    ['data.items', 'data.id', item.data.id],
                                              *    ['metas', 'id', meta.id]
                                              *],
                                              *[
                                              *    ['data.items', 'data.id', item.data.id],
                                              *    ['metas', 'id', meta.id]
                                              *],
                                              *'test'
                                              */
    extra.forEach(function (dataPath, index) {
      if (typeof dataPath === 'string') {
        if (!dataPath) {// model,prop.sync
          extraObj['$' + index] = vm;
        } else {
          if (dataPath === '$event') {// $event
            extraObj['$' + index] = event;
          } else if (dataPath === 'arguments') {
            extraObj['$' + index] = event.detail ? event.detail.__args__ || __args__ : __args__;
          } else if (dataPath.indexOf('$event.') === 0) {// $event.target.value
            extraObj['$' + index] = vm.__get_value(dataPath.replace('$event.', ''), event);
          } else {
            extraObj['$' + index] = vm.__get_value(dataPath);
          }
        }
      } else {
        extraObj['$' + index] = getExtraValue(vm, dataPath);
      }
    });
  }

  return extraObj;
}

function getObjByArray(arr) {
  var obj = {};
  for (var i = 1; i < arr.length; i++) {
    var element = arr[i];
    obj[element[0]] = element[1];
  }
  return obj;
}

function processEventArgs(vm, event) {var args = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];var extra = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];var isCustom = arguments.length > 4 ? arguments[4] : undefined;var methodName = arguments.length > 5 ? arguments[5] : undefined;
  var isCustomMPEvent = false; // wxcomponent 组件，传递原始 event 对象

  // fixed 用户直接触发 mpInstance.triggerEvent
  var __args__ = isPlainObject(event.detail) ?
  event.detail.__args__ || [event.detail] :
  [event.detail];

  if (isCustom) {// 自定义事件
    isCustomMPEvent = event.currentTarget &&
    event.currentTarget.dataset &&
    event.currentTarget.dataset.comType === 'wx';
    if (!args.length) {// 无参数，直接传入 event 或 detail 数组
      if (isCustomMPEvent) {
        return [event];
      }
      return __args__;
    }
  }

  var extraObj = processEventExtra(vm, extra, event, __args__);

  var ret = [];
  args.forEach(function (arg) {
    if (arg === '$event') {
      if (methodName === '__set_model' && !isCustom) {// input v-model value
        ret.push(event.target.value);
      } else {
        if (isCustom && !isCustomMPEvent) {
          ret.push(__args__[0]);
        } else {// wxcomponent 组件或内置组件
          ret.push(event);
        }
      }
    } else {
      if (Array.isArray(arg) && arg[0] === 'o') {
        ret.push(getObjByArray(arg));
      } else if (typeof arg === 'string' && hasOwn(extraObj, arg)) {
        ret.push(extraObj[arg]);
      } else {
        ret.push(arg);
      }
    }
  });

  return ret;
}

var ONCE = '~';
var CUSTOM = '^';

function isMatchEventType(eventType, optType) {
  return eventType === optType ||

  optType === 'regionchange' && (

  eventType === 'begin' ||
  eventType === 'end');


}

function getContextVm(vm) {
  var $parent = vm.$parent;
  // 父组件是 scoped slots 或者其他自定义组件时继续查找
  while ($parent && $parent.$parent && ($parent.$options.generic || $parent.$parent.$options.generic || $parent.$scope._$vuePid)) {
    $parent = $parent.$parent;
  }
  return $parent && $parent.$parent;
}

function handleEvent(event) {var _this2 = this;
  event = wrapper$1(event);

  // [['tap',[['handle',[1,2,a]],['handle1',[1,2,a]]]]]
  var dataset = (event.currentTarget || event.target).dataset;
  if (!dataset) {
    return console.warn('事件信息不存在');
  }
  var eventOpts = dataset.eventOpts || dataset['event-opts']; // 支付宝 web-view 组件 dataset 非驼峰
  if (!eventOpts) {
    return console.warn('事件信息不存在');
  }

  // [['handle',[1,2,a]],['handle1',[1,2,a]]]
  var eventType = event.type;

  var ret = [];

  eventOpts.forEach(function (eventOpt) {
    var type = eventOpt[0];
    var eventsArray = eventOpt[1];

    var isCustom = type.charAt(0) === CUSTOM;
    type = isCustom ? type.slice(1) : type;
    var isOnce = type.charAt(0) === ONCE;
    type = isOnce ? type.slice(1) : type;

    if (eventsArray && isMatchEventType(eventType, type)) {
      eventsArray.forEach(function (eventArray) {
        var methodName = eventArray[0];
        if (methodName) {
          var handlerCtx = _this2.$vm;
          if (handlerCtx.$options.generic) {// mp-weixin,mp-toutiao 抽象节点模拟 scoped slots
            handlerCtx = getContextVm(handlerCtx) || handlerCtx;
          }
          if (methodName === '$emit') {
            handlerCtx.$emit.apply(handlerCtx,
            processEventArgs(
            _this2.$vm,
            event,
            eventArray[1],
            eventArray[2],
            isCustom,
            methodName));

            return;
          }
          var handler = handlerCtx[methodName];
          if (!isFn(handler)) {
            var _type = _this2.$vm.mpType === 'page' ? 'Page' : 'Component';
            var path = _this2.route || _this2.is;
            throw new Error("".concat(_type, " \"").concat(path, "\" does not have a method \"").concat(methodName, "\""));
          }
          if (isOnce) {
            if (handler.once) {
              return;
            }
            handler.once = true;
          }
          var params = processEventArgs(
          _this2.$vm,
          event,
          eventArray[1],
          eventArray[2],
          isCustom,
          methodName);

          params = Array.isArray(params) ? params : [];
          // 参数尾部增加原始事件对象用于复杂表达式内获取额外数据
          if (/=\s*\S+\.eventParams\s*\|\|\s*\S+\[['"]event-params['"]\]/.test(handler.toString())) {
            // eslint-disable-next-line no-sparse-arrays
            params = params.concat([,,,,,,,,,, event]);
          }
          ret.push(handler.apply(handlerCtx, params));
        }
      });
    }
  });

  if (
  eventType === 'input' &&
  ret.length === 1 &&
  typeof ret[0] !== 'undefined')
  {
    return ret[0];
  }
}

var eventChannels = {};

var eventChannelStack = [];

function getEventChannel(id) {
  if (id) {
    var eventChannel = eventChannels[id];
    delete eventChannels[id];
    return eventChannel;
  }
  return eventChannelStack.shift();
}

var hooks = [
'onShow',
'onHide',
'onError',
'onPageNotFound',
'onThemeChange',
'onUnhandledRejection'];


function initEventChannel() {
  _vue.default.prototype.getOpenerEventChannel = function () {
    // 微信小程序使用自身getOpenerEventChannel
    {
      return this.$scope.getOpenerEventChannel();
    }
  };
  var callHook = _vue.default.prototype.__call_hook;
  _vue.default.prototype.__call_hook = function (hook, args) {
    if (hook === 'onLoad' && args && args.__id__) {
      this.__eventChannel__ = getEventChannel(args.__id__);
      delete args.__id__;
    }
    return callHook.call(this, hook, args);
  };
}

function initScopedSlotsParams() {
  var center = {};
  var parents = {};

  _vue.default.prototype.$hasScopedSlotsParams = function (vueId) {
    var has = center[vueId];
    if (!has) {
      parents[vueId] = this;
      this.$on('hook:destroyed', function () {
        delete parents[vueId];
      });
    }
    return has;
  };

  _vue.default.prototype.$getScopedSlotsParams = function (vueId, name, key) {
    var data = center[vueId];
    if (data) {
      var object = data[name] || {};
      return key ? object[key] : object;
    } else {
      parents[vueId] = this;
      this.$on('hook:destroyed', function () {
        delete parents[vueId];
      });
    }
  };

  _vue.default.prototype.$setScopedSlotsParams = function (name, value) {
    var vueIds = this.$options.propsData.vueId;
    if (vueIds) {
      var vueId = vueIds.split(',')[0];
      var object = center[vueId] = center[vueId] || {};
      object[name] = value;
      if (parents[vueId]) {
        parents[vueId].$forceUpdate();
      }
    }
  };

  _vue.default.mixin({
    destroyed: function destroyed() {
      var propsData = this.$options.propsData;
      var vueId = propsData && propsData.vueId;
      if (vueId) {
        delete center[vueId];
        delete parents[vueId];
      }
    } });

}

function parseBaseApp(vm, _ref3)


{var mocks = _ref3.mocks,initRefs = _ref3.initRefs;
  initEventChannel();
  {
    initScopedSlotsParams();
  }
  if (vm.$options.store) {
    _vue.default.prototype.$store = vm.$options.store;
  }
  uniIdMixin(_vue.default);

  _vue.default.prototype.mpHost = "mp-weixin";

  _vue.default.mixin({
    beforeCreate: function beforeCreate() {
      if (!this.$options.mpType) {
        return;
      }

      this.mpType = this.$options.mpType;

      this.$mp = _defineProperty({
        data: {} },
      this.mpType, this.$options.mpInstance);


      this.$scope = this.$options.mpInstance;

      delete this.$options.mpType;
      delete this.$options.mpInstance;
      if (this.mpType === 'page' && typeof getApp === 'function') {// hack vue-i18n
        var app = getApp();
        if (app.$vm && app.$vm.$i18n) {
          this._i18n = app.$vm.$i18n;
        }
      }
      if (this.mpType !== 'app') {
        initRefs(this);
        initMocks(this, mocks);
      }
    } });


  var appOptions = {
    onLaunch: function onLaunch(args) {
      if (this.$vm) {// 已经初始化过了，主要是为了百度，百度 onShow 在 onLaunch 之前
        return;
      }
      {
        if (wx.canIUse && !wx.canIUse('nextTick')) {// 事实 上2.2.3 即可，简单使用 2.3.0 的 nextTick 判断
          console.error('当前微信基础库版本过低，请将 微信开发者工具-详情-项目设置-调试基础库版本 更换为`2.3.0`以上');
        }
      }

      this.$vm = vm;

      this.$vm.$mp = {
        app: this };


      this.$vm.$scope = this;
      // vm 上也挂载 globalData
      this.$vm.globalData = this.globalData;

      this.$vm._isMounted = true;
      this.$vm.__call_hook('mounted', args);

      this.$vm.__call_hook('onLaunch', args);
    } };


  // 兼容旧版本 globalData
  appOptions.globalData = vm.$options.globalData || {};
  // 将 methods 中的方法挂在 getApp() 中
  var methods = vm.$options.methods;
  if (methods) {
    Object.keys(methods).forEach(function (name) {
      appOptions[name] = methods[name];
    });
  }

  initAppLocale(_vue.default, vm, normalizeLocale(wx.getSystemInfoSync().language) || LOCALE_EN);

  initHooks(appOptions, hooks);
  initUnknownHooks(appOptions, vm.$options);

  return appOptions;
}

var mocks = ['__route__', '__wxExparserNodeId__', '__wxWebviewId__'];

function findVmByVueId(vm, vuePid) {
  var $children = vm.$children;
  // 优先查找直属(反向查找:https://github.com/dcloudio/uni-app/issues/1200)
  for (var i = $children.length - 1; i >= 0; i--) {
    var childVm = $children[i];
    if (childVm.$scope._$vueId === vuePid) {
      return childVm;
    }
  }
  // 反向递归查找
  var parentVm;
  for (var _i = $children.length - 1; _i >= 0; _i--) {
    parentVm = findVmByVueId($children[_i], vuePid);
    if (parentVm) {
      return parentVm;
    }
  }
}

function initBehavior(options) {
  return Behavior(options);
}

function isPage() {
  return !!this.route;
}

function initRelation(detail) {
  this.triggerEvent('__l', detail);
}

function selectAllComponents(mpInstance, selector, $refs) {
  var components = mpInstance.selectAllComponents(selector);
  components.forEach(function (component) {
    var ref = component.dataset.ref;
    $refs[ref] = component.$vm || component;
    {
      if (component.dataset.vueGeneric === 'scoped') {
        component.selectAllComponents('.scoped-ref').forEach(function (scopedComponent) {
          selectAllComponents(scopedComponent, selector, $refs);
        });
      }
    }
  });
}

function initRefs(vm) {
  var mpInstance = vm.$scope;
  Object.defineProperty(vm, '$refs', {
    get: function get() {
      var $refs = {};
      selectAllComponents(mpInstance, '.vue-ref', $refs);
      // TODO 暂不考虑 for 中的 scoped
      var forComponents = mpInstance.selectAllComponents('.vue-ref-in-for');
      forComponents.forEach(function (component) {
        var ref = component.dataset.ref;
        if (!$refs[ref]) {
          $refs[ref] = [];
        }
        $refs[ref].push(component.$vm || component);
      });
      return $refs;
    } });

}

function handleLink(event) {var _ref4 =



  event.detail || event.value,vuePid = _ref4.vuePid,vueOptions = _ref4.vueOptions; // detail 是微信,value 是百度(dipatch)

  var parentVm;

  if (vuePid) {
    parentVm = findVmByVueId(this.$vm, vuePid);
  }

  if (!parentVm) {
    parentVm = this.$vm;
  }

  vueOptions.parent = parentVm;
}

function parseApp(vm) {
  return parseBaseApp(vm, {
    mocks: mocks,
    initRefs: initRefs });

}

function createApp(vm) {
  App(parseApp(vm));
  return vm;
}

var encodeReserveRE = /[!'()*]/g;
var encodeReserveReplacer = function encodeReserveReplacer(c) {return '%' + c.charCodeAt(0).toString(16);};
var commaRE = /%2C/g;

// fixed encodeURIComponent which is more conformant to RFC3986:
// - escapes [!'()*]
// - preserve commas
var encode = function encode(str) {return encodeURIComponent(str).
  replace(encodeReserveRE, encodeReserveReplacer).
  replace(commaRE, ',');};

function stringifyQuery(obj) {var encodeStr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : encode;
  var res = obj ? Object.keys(obj).map(function (key) {
    var val = obj[key];

    if (val === undefined) {
      return '';
    }

    if (val === null) {
      return encodeStr(key);
    }

    if (Array.isArray(val)) {
      var result = [];
      val.forEach(function (val2) {
        if (val2 === undefined) {
          return;
        }
        if (val2 === null) {
          result.push(encodeStr(key));
        } else {
          result.push(encodeStr(key) + '=' + encodeStr(val2));
        }
      });
      return result.join('&');
    }

    return encodeStr(key) + '=' + encodeStr(val);
  }).filter(function (x) {return x.length > 0;}).join('&') : null;
  return res ? "?".concat(res) : '';
}

function parseBaseComponent(vueComponentOptions)


{var _ref5 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},isPage = _ref5.isPage,initRelation = _ref5.initRelation;var _initVueComponent =
  initVueComponent(_vue.default, vueComponentOptions),_initVueComponent2 = _slicedToArray(_initVueComponent, 2),VueComponent = _initVueComponent2[0],vueOptions = _initVueComponent2[1];

  var options = _objectSpread({
    multipleSlots: true,
    addGlobalClass: true },
  vueOptions.options || {});


  {
    // 微信 multipleSlots 部分情况有 bug，导致内容顺序错乱 如 u-list，提供覆盖选项
    if (vueOptions['mp-weixin'] && vueOptions['mp-weixin'].options) {
      Object.assign(options, vueOptions['mp-weixin'].options);
    }
  }

  var componentOptions = {
    options: options,
    data: initData(vueOptions, _vue.default.prototype),
    behaviors: initBehaviors(vueOptions, initBehavior),
    properties: initProperties(vueOptions.props, false, vueOptions.__file, options),
    lifetimes: {
      attached: function attached() {
        var properties = this.properties;

        var options = {
          mpType: isPage.call(this) ? 'page' : 'component',
          mpInstance: this,
          propsData: properties };


        initVueIds(properties.vueId, this);

        // 处理父子关系
        initRelation.call(this, {
          vuePid: this._$vuePid,
          vueOptions: options });


        // 初始化 vue 实例
        this.$vm = new VueComponent(options);

        // 处理$slots,$scopedSlots（暂不支持动态变化$slots）
        initSlots(this.$vm, properties.vueSlots);

        // 触发首次 setData
        this.$vm.$mount();
      },
      ready: function ready() {
        // 当组件 props 默认值为 true，初始化时传入 false 会导致 created,ready 触发, 但 attached 不触发
        // https://developers.weixin.qq.com/community/develop/doc/00066ae2844cc0f8eb883e2a557800
        if (this.$vm) {
          this.$vm._isMounted = true;
          this.$vm.__call_hook('mounted');
          this.$vm.__call_hook('onReady');
        }
      },
      detached: function detached() {
        this.$vm && this.$vm.$destroy();
      } },

    pageLifetimes: {
      show: function show(args) {
        this.$vm && this.$vm.__call_hook('onPageShow', args);
      },
      hide: function hide() {
        this.$vm && this.$vm.__call_hook('onPageHide');
      },
      resize: function resize(size) {
        this.$vm && this.$vm.__call_hook('onPageResize', size);
      } },

    methods: {
      __l: handleLink,
      __e: handleEvent } };


  // externalClasses
  if (vueOptions.externalClasses) {
    componentOptions.externalClasses = vueOptions.externalClasses;
  }

  if (Array.isArray(vueOptions.wxsCallMethods)) {
    vueOptions.wxsCallMethods.forEach(function (callMethod) {
      componentOptions.methods[callMethod] = function (args) {
        return this.$vm[callMethod](args);
      };
    });
  }

  if (isPage) {
    return componentOptions;
  }
  return [componentOptions, VueComponent];
}

function parseComponent(vueComponentOptions) {
  return parseBaseComponent(vueComponentOptions, {
    isPage: isPage,
    initRelation: initRelation });

}

var hooks$1 = [
'onShow',
'onHide',
'onUnload'];


hooks$1.push.apply(hooks$1, PAGE_EVENT_HOOKS);

function parseBasePage(vuePageOptions, _ref6)


{var isPage = _ref6.isPage,initRelation = _ref6.initRelation;
  var pageOptions = parseComponent(vuePageOptions);

  initHooks(pageOptions.methods, hooks$1, vuePageOptions);

  pageOptions.methods.onLoad = function (query) {
    this.options = query;
    var copyQuery = Object.assign({}, query);
    delete copyQuery.__id__;
    this.$page = {
      fullPath: '/' + (this.route || this.is) + stringifyQuery(copyQuery) };

    this.$vm.$mp.query = query; // 兼容 mpvue
    this.$vm.__call_hook('onLoad', query);
  };
  initUnknownHooks(pageOptions.methods, vuePageOptions, ['onReady']);

  return pageOptions;
}

function parsePage(vuePageOptions) {
  return parseBasePage(vuePageOptions, {
    isPage: isPage,
    initRelation: initRelation });

}

function createPage(vuePageOptions) {
  {
    return Component(parsePage(vuePageOptions));
  }
}

function createComponent(vueOptions) {
  {
    return Component(parseComponent(vueOptions));
  }
}

function createSubpackageApp(vm) {
  var appOptions = parseApp(vm);
  var app = getApp({
    allowDefault: true });

  vm.$scope = app;
  var globalData = app.globalData;
  if (globalData) {
    Object.keys(appOptions.globalData).forEach(function (name) {
      if (!hasOwn(globalData, name)) {
        globalData[name] = appOptions.globalData[name];
      }
    });
  }
  Object.keys(appOptions).forEach(function (name) {
    if (!hasOwn(app, name)) {
      app[name] = appOptions[name];
    }
  });
  if (isFn(appOptions.onShow) && wx.onAppShow) {
    wx.onAppShow(function () {for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {args[_key5] = arguments[_key5];}
      vm.__call_hook('onShow', args);
    });
  }
  if (isFn(appOptions.onHide) && wx.onAppHide) {
    wx.onAppHide(function () {for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {args[_key6] = arguments[_key6];}
      vm.__call_hook('onHide', args);
    });
  }
  if (isFn(appOptions.onLaunch)) {
    var args = wx.getLaunchOptionsSync && wx.getLaunchOptionsSync();
    vm.__call_hook('onLaunch', args);
  }
  return vm;
}

function createPlugin(vm) {
  var appOptions = parseApp(vm);
  if (isFn(appOptions.onShow) && wx.onAppShow) {
    wx.onAppShow(function () {for (var _len7 = arguments.length, args = new Array(_len7), _key7 = 0; _key7 < _len7; _key7++) {args[_key7] = arguments[_key7];}
      vm.__call_hook('onShow', args);
    });
  }
  if (isFn(appOptions.onHide) && wx.onAppHide) {
    wx.onAppHide(function () {for (var _len8 = arguments.length, args = new Array(_len8), _key8 = 0; _key8 < _len8; _key8++) {args[_key8] = arguments[_key8];}
      vm.__call_hook('onHide', args);
    });
  }
  if (isFn(appOptions.onLaunch)) {
    var args = wx.getLaunchOptionsSync && wx.getLaunchOptionsSync();
    vm.__call_hook('onLaunch', args);
  }
  return vm;
}

todos.forEach(function (todoApi) {
  protocols[todoApi] = false;
});

canIUses.forEach(function (canIUseApi) {
  var apiName = protocols[canIUseApi] && protocols[canIUseApi].name ? protocols[canIUseApi].name :
  canIUseApi;
  if (!wx.canIUse(apiName)) {
    protocols[canIUseApi] = false;
  }
});

var uni = {};

if (typeof Proxy !== 'undefined' && "mp-weixin" !== 'app-plus') {
  uni = new Proxy({}, {
    get: function get(target, name) {
      if (hasOwn(target, name)) {
        return target[name];
      }
      if (baseApi[name]) {
        return baseApi[name];
      }
      if (api[name]) {
        return promisify(name, api[name]);
      }
      {
        if (extraApi[name]) {
          return promisify(name, extraApi[name]);
        }
        if (todoApis[name]) {
          return promisify(name, todoApis[name]);
        }
      }
      if (eventApi[name]) {
        return eventApi[name];
      }
      if (!hasOwn(wx, name) && !hasOwn(protocols, name)) {
        return;
      }
      return promisify(name, wrapper(name, wx[name]));
    },
    set: function set(target, name, value) {
      target[name] = value;
      return true;
    } });

} else {
  Object.keys(baseApi).forEach(function (name) {
    uni[name] = baseApi[name];
  });

  {
    Object.keys(todoApis).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
    Object.keys(extraApi).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
  }

  Object.keys(eventApi).forEach(function (name) {
    uni[name] = eventApi[name];
  });

  Object.keys(api).forEach(function (name) {
    uni[name] = promisify(name, api[name]);
  });

  Object.keys(wx).forEach(function (name) {
    if (hasOwn(wx, name) || hasOwn(protocols, name)) {
      uni[name] = promisify(name, wrapper(name, wx[name]));
    }
  });
}

wx.createApp = createApp;
wx.createPage = createPage;
wx.createComponent = createComponent;
wx.createSubpackageApp = createSubpackageApp;
wx.createPlugin = createPlugin;

var uni$1 = uni;var _default =

uni$1;exports.default = _default;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../webpack/buildin/global.js */ 2)))

/***/ }),
/* 2 */
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 3 */
/*!*************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-i18n/dist/uni-i18n.es.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni, global) {Object.defineProperty(exports, "__esModule", { value: true });exports.compileI18nJsonStr = compileI18nJsonStr;exports.hasI18nJson = hasI18nJson;exports.initVueI18n = initVueI18n;exports.isI18nStr = isI18nStr;exports.normalizeLocale = normalizeLocale;exports.parseI18nJson = parseI18nJson;exports.resolveLocale = resolveLocale;exports.isString = exports.LOCALE_ZH_HANT = exports.LOCALE_ZH_HANS = exports.LOCALE_FR = exports.LOCALE_ES = exports.LOCALE_EN = exports.I18n = exports.Formatter = void 0;function _slicedToArray(arr, i) {return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();}function _nonIterableRest() {throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _unsupportedIterableToArray(o, minLen) {if (!o) return;if (typeof o === "string") return _arrayLikeToArray(o, minLen);var n = Object.prototype.toString.call(o).slice(8, -1);if (n === "Object" && o.constructor) n = o.constructor.name;if (n === "Map" || n === "Set") return Array.from(o);if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);}function _arrayLikeToArray(arr, len) {if (len == null || len > arr.length) len = arr.length;for (var i = 0, arr2 = new Array(len); i < len; i++) {arr2[i] = arr[i];}return arr2;}function _iterableToArrayLimit(arr, i) {if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;var _arr = [];var _n = true;var _d = false;var _e = undefined;try {for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {_arr.push(_s.value);if (i && _arr.length === i) break;}} catch (err) {_d = true;_e = err;} finally {try {if (!_n && _i["return"] != null) _i["return"]();} finally {if (_d) throw _e;}}return _arr;}function _arrayWithHoles(arr) {if (Array.isArray(arr)) return arr;}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}var isArray = Array.isArray;
var isObject = function isObject(val) {return val !== null && typeof val === 'object';};
var defaultDelimiters = ['{', '}'];var
BaseFormatter = /*#__PURE__*/function () {
  function BaseFormatter() {_classCallCheck(this, BaseFormatter);
    this._caches = Object.create(null);
  }_createClass(BaseFormatter, [{ key: "interpolate", value: function interpolate(
    message, values) {var delimiters = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : defaultDelimiters;
      if (!values) {
        return [message];
      }
      var tokens = this._caches[message];
      if (!tokens) {
        tokens = parse(message, delimiters);
        this._caches[message] = tokens;
      }
      return compile(tokens, values);
    } }]);return BaseFormatter;}();exports.Formatter = BaseFormatter;

var RE_TOKEN_LIST_VALUE = /^(?:\d)+/;
var RE_TOKEN_NAMED_VALUE = /^(?:\w)+/;
function parse(format, _ref) {var _ref2 = _slicedToArray(_ref, 2),startDelimiter = _ref2[0],endDelimiter = _ref2[1];
  var tokens = [];
  var position = 0;
  var text = '';
  while (position < format.length) {
    var char = format[position++];
    if (char === startDelimiter) {
      if (text) {
        tokens.push({ type: 'text', value: text });
      }
      text = '';
      var sub = '';
      char = format[position++];
      while (char !== undefined && char !== endDelimiter) {
        sub += char;
        char = format[position++];
      }
      var isClosed = char === endDelimiter;
      var type = RE_TOKEN_LIST_VALUE.test(sub) ?
      'list' :
      isClosed && RE_TOKEN_NAMED_VALUE.test(sub) ?
      'named' :
      'unknown';
      tokens.push({ value: sub, type: type });
    }
    //  else if (char === '%') {
    //   // when found rails i18n syntax, skip text capture
    //   if (format[position] !== '{') {
    //     text += char
    //   }
    // }
    else {
        text += char;
      }
  }
  text && tokens.push({ type: 'text', value: text });
  return tokens;
}
function compile(tokens, values) {
  var compiled = [];
  var index = 0;
  var mode = isArray(values) ?
  'list' :
  isObject(values) ?
  'named' :
  'unknown';
  if (mode === 'unknown') {
    return compiled;
  }
  while (index < tokens.length) {
    var token = tokens[index];
    switch (token.type) {
      case 'text':
        compiled.push(token.value);
        break;
      case 'list':
        compiled.push(values[parseInt(token.value, 10)]);
        break;
      case 'named':
        if (mode === 'named') {
          compiled.push(values[token.value]);
        } else
        {
          if (true) {
            console.warn("Type of token '".concat(token.type, "' and format of value '").concat(mode, "' don't match!"));
          }
        }
        break;
      case 'unknown':
        if (true) {
          console.warn("Detect 'unknown' type of token!");
        }
        break;}

    index++;
  }
  return compiled;
}

var LOCALE_ZH_HANS = 'zh-Hans';exports.LOCALE_ZH_HANS = LOCALE_ZH_HANS;
var LOCALE_ZH_HANT = 'zh-Hant';exports.LOCALE_ZH_HANT = LOCALE_ZH_HANT;
var LOCALE_EN = 'en';exports.LOCALE_EN = LOCALE_EN;
var LOCALE_FR = 'fr';exports.LOCALE_FR = LOCALE_FR;
var LOCALE_ES = 'es';exports.LOCALE_ES = LOCALE_ES;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var hasOwn = function hasOwn(val, key) {return hasOwnProperty.call(val, key);};
var defaultFormatter = new BaseFormatter();
function include(str, parts) {
  return !!parts.find(function (part) {return str.indexOf(part) !== -1;});
}
function startsWith(str, parts) {
  return parts.find(function (part) {return str.indexOf(part) === 0;});
}
function normalizeLocale(locale, messages) {
  if (!locale) {
    return;
  }
  locale = locale.trim().replace(/_/g, '-');
  if (messages && messages[locale]) {
    return locale;
  }
  locale = locale.toLowerCase();
  if (locale.indexOf('zh') === 0) {
    if (locale.indexOf('-hans') > -1) {
      return LOCALE_ZH_HANS;
    }
    if (locale.indexOf('-hant') > -1) {
      return LOCALE_ZH_HANT;
    }
    if (include(locale, ['-tw', '-hk', '-mo', '-cht'])) {
      return LOCALE_ZH_HANT;
    }
    return LOCALE_ZH_HANS;
  }
  var lang = startsWith(locale, [LOCALE_EN, LOCALE_FR, LOCALE_ES]);
  if (lang) {
    return lang;
  }
}var
I18n = /*#__PURE__*/function () {
  function I18n(_ref3) {var locale = _ref3.locale,fallbackLocale = _ref3.fallbackLocale,messages = _ref3.messages,watcher = _ref3.watcher,formater = _ref3.formater;_classCallCheck(this, I18n);
    this.locale = LOCALE_EN;
    this.fallbackLocale = LOCALE_EN;
    this.message = {};
    this.messages = {};
    this.watchers = [];
    if (fallbackLocale) {
      this.fallbackLocale = fallbackLocale;
    }
    this.formater = formater || defaultFormatter;
    this.messages = messages || {};
    this.setLocale(locale || LOCALE_EN);
    if (watcher) {
      this.watchLocale(watcher);
    }
  }_createClass(I18n, [{ key: "setLocale", value: function setLocale(
    locale) {var _this = this;
      var oldLocale = this.locale;
      this.locale = normalizeLocale(locale, this.messages) || this.fallbackLocale;
      if (!this.messages[this.locale]) {
        // 可能初始化时不存在
        this.messages[this.locale] = {};
      }
      this.message = this.messages[this.locale];
      // 仅发生变化时，通知
      if (oldLocale !== this.locale) {
        this.watchers.forEach(function (watcher) {
          watcher(_this.locale, oldLocale);
        });
      }
    } }, { key: "getLocale", value: function getLocale()
    {
      return this.locale;
    } }, { key: "watchLocale", value: function watchLocale(
    fn) {var _this2 = this;
      var index = this.watchers.push(fn) - 1;
      return function () {
        _this2.watchers.splice(index, 1);
      };
    } }, { key: "add", value: function add(
    locale, message) {var override = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      var curMessages = this.messages[locale];
      if (curMessages) {
        if (override) {
          Object.assign(curMessages, message);
        } else
        {
          Object.keys(message).forEach(function (key) {
            if (!hasOwn(curMessages, key)) {
              curMessages[key] = message[key];
            }
          });
        }
      } else
      {
        this.messages[locale] = message;
      }
    } }, { key: "f", value: function f(
    message, values, delimiters) {
      return this.formater.interpolate(message, values, delimiters).join('');
    } }, { key: "t", value: function t(
    key, locale, values) {
      var message = this.message;
      if (typeof locale === 'string') {
        locale = normalizeLocale(locale, this.messages);
        locale && (message = this.messages[locale]);
      } else
      {
        values = locale;
      }
      if (!hasOwn(message, key)) {
        console.warn("Cannot translate the value of keypath ".concat(key, ". Use the value of keypath as default."));
        return key;
      }
      return this.formater.interpolate(message[key], values).join('');
    } }]);return I18n;}();exports.I18n = I18n;


function watchAppLocale(appVm, i18n) {
  // 需要保证 watch 的触发在组件渲染之前
  if (appVm.$watchLocale) {
    // vue2
    appVm.$watchLocale(function (newLocale) {
      i18n.setLocale(newLocale);
    });
  } else
  {
    appVm.$watch(function () {return appVm.$locale;}, function (newLocale) {
      i18n.setLocale(newLocale);
    });
  }
}
function getDefaultLocale() {
  if (typeof uni !== 'undefined' && uni.getLocale) {
    return uni.getLocale();
  }
  // 小程序平台，uni 和 uni-i18n 互相引用，导致访问不到 uni，故在 global 上挂了 getLocale
  if (typeof global !== 'undefined' && global.getLocale) {
    return global.getLocale();
  }
  return LOCALE_EN;
}
function initVueI18n(locale) {var messages = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};var fallbackLocale = arguments.length > 2 ? arguments[2] : undefined;var watcher = arguments.length > 3 ? arguments[3] : undefined;
  // 兼容旧版本入参
  if (typeof locale !== 'string') {var _ref4 =
    [
    messages,
    locale];locale = _ref4[0];messages = _ref4[1];

  }
  if (typeof locale !== 'string') {
    // 因为小程序平台，uni-i18n 和 uni 互相引用，导致此时访问 uni 时，为 undefined
    locale = getDefaultLocale();
  }
  if (typeof fallbackLocale !== 'string') {
    fallbackLocale =
    typeof __uniConfig !== 'undefined' && __uniConfig.fallbackLocale ||
    LOCALE_EN;
  }
  var i18n = new I18n({
    locale: locale,
    fallbackLocale: fallbackLocale,
    messages: messages,
    watcher: watcher });

  var _t = function t(key, values) {
    if (typeof getApp !== 'function') {
      // app view
      /* eslint-disable no-func-assign */
      _t = function t(key, values) {
        return i18n.t(key, values);
      };
    } else
    {
      var isWatchedAppLocale = false;
      _t = function t(key, values) {
        var appVm = getApp().$vm;
        // 可能$vm还不存在，比如在支付宝小程序中，组件定义较早，在props的default里使用了t()函数（如uni-goods-nav），此时app还未初始化
        // options: {
        // 	type: Array,
        // 	default () {
        // 		return [{
        // 			icon: 'shop',
        // 			text: t("uni-goods-nav.options.shop"),
        // 		}, {
        // 			icon: 'cart',
        // 			text: t("uni-goods-nav.options.cart")
        // 		}]
        // 	}
        // },
        if (appVm) {
          // 触发响应式
          appVm.$locale;
          if (!isWatchedAppLocale) {
            isWatchedAppLocale = true;
            watchAppLocale(appVm, i18n);
          }
        }
        return i18n.t(key, values);
      };
    }
    return _t(key, values);
  };
  return {
    i18n: i18n,
    f: function f(message, values, delimiters) {
      return i18n.f(message, values, delimiters);
    },
    t: function t(key, values) {
      return _t(key, values);
    },
    add: function add(locale, message) {var override = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      return i18n.add(locale, message, override);
    },
    watch: function watch(fn) {
      return i18n.watchLocale(fn);
    },
    getLocale: function getLocale() {
      return i18n.getLocale();
    },
    setLocale: function setLocale(newLocale) {
      return i18n.setLocale(newLocale);
    } };

}

var isString = function isString(val) {return typeof val === 'string';};exports.isString = isString;
var formater;
function hasI18nJson(jsonObj, delimiters) {
  if (!formater) {
    formater = new BaseFormatter();
  }
  return walkJsonObj(jsonObj, function (jsonObj, key) {
    var value = jsonObj[key];
    if (isString(value)) {
      if (isI18nStr(value, delimiters)) {
        return true;
      }
    } else
    {
      return hasI18nJson(value, delimiters);
    }
  });
}
function parseI18nJson(jsonObj, values, delimiters) {
  if (!formater) {
    formater = new BaseFormatter();
  }
  walkJsonObj(jsonObj, function (jsonObj, key) {
    var value = jsonObj[key];
    if (isString(value)) {
      if (isI18nStr(value, delimiters)) {
        jsonObj[key] = compileStr(value, values, delimiters);
      }
    } else
    {
      parseI18nJson(value, values, delimiters);
    }
  });
  return jsonObj;
}
function compileI18nJsonStr(jsonStr, _ref5) {var locale = _ref5.locale,locales = _ref5.locales,delimiters = _ref5.delimiters;
  if (!isI18nStr(jsonStr, delimiters)) {
    return jsonStr;
  }
  if (!formater) {
    formater = new BaseFormatter();
  }
  var localeValues = [];
  Object.keys(locales).forEach(function (name) {
    if (name !== locale) {
      localeValues.push({
        locale: name,
        values: locales[name] });

    }
  });
  localeValues.unshift({ locale: locale, values: locales[locale] });
  try {
    return JSON.stringify(compileJsonObj(JSON.parse(jsonStr), localeValues, delimiters), null, 2);
  }
  catch (e) {}
  return jsonStr;
}
function isI18nStr(value, delimiters) {
  return value.indexOf(delimiters[0]) > -1;
}
function compileStr(value, values, delimiters) {
  return formater.interpolate(value, values, delimiters).join('');
}
function compileValue(jsonObj, key, localeValues, delimiters) {
  var value = jsonObj[key];
  if (isString(value)) {
    // 存在国际化
    if (isI18nStr(value, delimiters)) {
      jsonObj[key] = compileStr(value, localeValues[0].values, delimiters);
      if (localeValues.length > 1) {
        // 格式化国际化语言
        var valueLocales = jsonObj[key + 'Locales'] = {};
        localeValues.forEach(function (localValue) {
          valueLocales[localValue.locale] = compileStr(value, localValue.values, delimiters);
        });
      }
    }
  } else
  {
    compileJsonObj(value, localeValues, delimiters);
  }
}
function compileJsonObj(jsonObj, localeValues, delimiters) {
  walkJsonObj(jsonObj, function (jsonObj, key) {
    compileValue(jsonObj, key, localeValues, delimiters);
  });
  return jsonObj;
}
function walkJsonObj(jsonObj, walk) {
  if (isArray(jsonObj)) {
    for (var i = 0; i < jsonObj.length; i++) {
      if (walk(jsonObj, i)) {
        return true;
      }
    }
  } else
  if (isObject(jsonObj)) {
    for (var key in jsonObj) {
      if (walk(jsonObj, key)) {
        return true;
      }
    }
  }
  return false;
}

function resolveLocale(locales) {
  return function (locale) {
    if (!locale) {
      return locale;
    }
    locale = normalizeLocale(locale) || locale;
    return resolveLocaleChain(locale).find(function (locale) {return locales.indexOf(locale) > -1;});
  };
}
function resolveLocaleChain(locale) {
  var chain = [];
  var tokens = locale.split('-');
  while (tokens.length) {
    chain.push(tokens.join('-'));
    tokens.pop();
  }
  return chain;
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"], __webpack_require__(/*! ./../../../webpack/buildin/global.js */ 2)))

/***/ }),
/* 4 */
/*!******************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/mp-vue/dist/mp.runtime.esm.js ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * Vue.js v2.6.11
 * (c) 2014-2022 Evan You
 * Released under the MIT License.
 */
/*  */

var emptyObject = Object.freeze({});

// These helpers produce better VM code in JS engines due to their
// explicitness and function inlining.
function isUndef (v) {
  return v === undefined || v === null
}

function isDef (v) {
  return v !== undefined && v !== null
}

function isTrue (v) {
  return v === true
}

function isFalse (v) {
  return v === false
}

/**
 * Check if value is primitive.
 */
function isPrimitive (value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    // $flow-disable-line
    typeof value === 'symbol' ||
    typeof value === 'boolean'
  )
}

/**
 * Quick object check - this is primarily used to tell
 * Objects from primitive values when we know the value
 * is a JSON-compliant type.
 */
function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

/**
 * Get the raw type string of a value, e.g., [object Object].
 */
var _toString = Object.prototype.toString;

function toRawType (value) {
  return _toString.call(value).slice(8, -1)
}

/**
 * Strict object type check. Only returns true
 * for plain JavaScript objects.
 */
function isPlainObject (obj) {
  return _toString.call(obj) === '[object Object]'
}

function isRegExp (v) {
  return _toString.call(v) === '[object RegExp]'
}

/**
 * Check if val is a valid array index.
 */
function isValidArrayIndex (val) {
  var n = parseFloat(String(val));
  return n >= 0 && Math.floor(n) === n && isFinite(val)
}

function isPromise (val) {
  return (
    isDef(val) &&
    typeof val.then === 'function' &&
    typeof val.catch === 'function'
  )
}

/**
 * Convert a value to a string that is actually rendered.
 */
function toString (val) {
  return val == null
    ? ''
    : Array.isArray(val) || (isPlainObject(val) && val.toString === _toString)
      ? JSON.stringify(val, null, 2)
      : String(val)
}

/**
 * Convert an input value to a number for persistence.
 * If the conversion fails, return original string.
 */
function toNumber (val) {
  var n = parseFloat(val);
  return isNaN(n) ? val : n
}

/**
 * Make a map and return a function for checking if a key
 * is in that map.
 */
function makeMap (
  str,
  expectsLowerCase
) {
  var map = Object.create(null);
  var list = str.split(',');
  for (var i = 0; i < list.length; i++) {
    map[list[i]] = true;
  }
  return expectsLowerCase
    ? function (val) { return map[val.toLowerCase()]; }
    : function (val) { return map[val]; }
}

/**
 * Check if a tag is a built-in tag.
 */
var isBuiltInTag = makeMap('slot,component', true);

/**
 * Check if an attribute is a reserved attribute.
 */
var isReservedAttribute = makeMap('key,ref,slot,slot-scope,is');

/**
 * Remove an item from an array.
 */
function remove (arr, item) {
  if (arr.length) {
    var index = arr.indexOf(item);
    if (index > -1) {
      return arr.splice(index, 1)
    }
  }
}

/**
 * Check whether an object has the property.
 */
var hasOwnProperty = Object.prototype.hasOwnProperty;
function hasOwn (obj, key) {
  return hasOwnProperty.call(obj, key)
}

/**
 * Create a cached version of a pure function.
 */
function cached (fn) {
  var cache = Object.create(null);
  return (function cachedFn (str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str))
  })
}

/**
 * Camelize a hyphen-delimited string.
 */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) { return c ? c.toUpperCase() : ''; })
});

/**
 * Capitalize a string.
 */
var capitalize = cached(function (str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
});

/**
 * Hyphenate a camelCase string.
 */
var hyphenateRE = /\B([A-Z])/g;
var hyphenate = cached(function (str) {
  return str.replace(hyphenateRE, '-$1').toLowerCase()
});

/**
 * Simple bind polyfill for environments that do not support it,
 * e.g., PhantomJS 1.x. Technically, we don't need this anymore
 * since native bind is now performant enough in most browsers.
 * But removing it would mean breaking code that was able to run in
 * PhantomJS 1.x, so this must be kept for backward compatibility.
 */

/* istanbul ignore next */
function polyfillBind (fn, ctx) {
  function boundFn (a) {
    var l = arguments.length;
    return l
      ? l > 1
        ? fn.apply(ctx, arguments)
        : fn.call(ctx, a)
      : fn.call(ctx)
  }

  boundFn._length = fn.length;
  return boundFn
}

function nativeBind (fn, ctx) {
  return fn.bind(ctx)
}

var bind = Function.prototype.bind
  ? nativeBind
  : polyfillBind;

/**
 * Convert an Array-like object to a real Array.
 */
function toArray (list, start) {
  start = start || 0;
  var i = list.length - start;
  var ret = new Array(i);
  while (i--) {
    ret[i] = list[i + start];
  }
  return ret
}

/**
 * Mix properties into target object.
 */
function extend (to, _from) {
  for (var key in _from) {
    to[key] = _from[key];
  }
  return to
}

/**
 * Merge an Array of Objects into a single Object.
 */
function toObject (arr) {
  var res = {};
  for (var i = 0; i < arr.length; i++) {
    if (arr[i]) {
      extend(res, arr[i]);
    }
  }
  return res
}

/* eslint-disable no-unused-vars */

/**
 * Perform no operation.
 * Stubbing args to make Flow happy without leaving useless transpiled code
 * with ...rest (https://flow.org/blog/2017/05/07/Strict-Function-Call-Arity/).
 */
function noop (a, b, c) {}

/**
 * Always return false.
 */
var no = function (a, b, c) { return false; };

/* eslint-enable no-unused-vars */

/**
 * Return the same value.
 */
var identity = function (_) { return _; };

/**
 * Check if two values are loosely equal - that is,
 * if they are plain objects, do they have the same shape?
 */
function looseEqual (a, b) {
  if (a === b) { return true }
  var isObjectA = isObject(a);
  var isObjectB = isObject(b);
  if (isObjectA && isObjectB) {
    try {
      var isArrayA = Array.isArray(a);
      var isArrayB = Array.isArray(b);
      if (isArrayA && isArrayB) {
        return a.length === b.length && a.every(function (e, i) {
          return looseEqual(e, b[i])
        })
      } else if (a instanceof Date && b instanceof Date) {
        return a.getTime() === b.getTime()
      } else if (!isArrayA && !isArrayB) {
        var keysA = Object.keys(a);
        var keysB = Object.keys(b);
        return keysA.length === keysB.length && keysA.every(function (key) {
          return looseEqual(a[key], b[key])
        })
      } else {
        /* istanbul ignore next */
        return false
      }
    } catch (e) {
      /* istanbul ignore next */
      return false
    }
  } else if (!isObjectA && !isObjectB) {
    return String(a) === String(b)
  } else {
    return false
  }
}

/**
 * Return the first index at which a loosely equal value can be
 * found in the array (if value is a plain object, the array must
 * contain an object of the same shape), or -1 if it is not present.
 */
function looseIndexOf (arr, val) {
  for (var i = 0; i < arr.length; i++) {
    if (looseEqual(arr[i], val)) { return i }
  }
  return -1
}

/**
 * Ensure a function is called only once.
 */
function once (fn) {
  var called = false;
  return function () {
    if (!called) {
      called = true;
      fn.apply(this, arguments);
    }
  }
}

var ASSET_TYPES = [
  'component',
  'directive',
  'filter'
];

var LIFECYCLE_HOOKS = [
  'beforeCreate',
  'created',
  'beforeMount',
  'mounted',
  'beforeUpdate',
  'updated',
  'beforeDestroy',
  'destroyed',
  'activated',
  'deactivated',
  'errorCaptured',
  'serverPrefetch'
];

/*  */



var config = ({
  /**
   * Option merge strategies (used in core/util/options)
   */
  // $flow-disable-line
  optionMergeStrategies: Object.create(null),

  /**
   * Whether to suppress warnings.
   */
  silent: false,

  /**
   * Show production mode tip message on boot?
   */
  productionTip: "development" !== 'production',

  /**
   * Whether to enable devtools
   */
  devtools: "development" !== 'production',

  /**
   * Whether to record perf
   */
  performance: false,

  /**
   * Error handler for watcher errors
   */
  errorHandler: null,

  /**
   * Warn handler for watcher warns
   */
  warnHandler: null,

  /**
   * Ignore certain custom elements
   */
  ignoredElements: [],

  /**
   * Custom user key aliases for v-on
   */
  // $flow-disable-line
  keyCodes: Object.create(null),

  /**
   * Check if a tag is reserved so that it cannot be registered as a
   * component. This is platform-dependent and may be overwritten.
   */
  isReservedTag: no,

  /**
   * Check if an attribute is reserved so that it cannot be used as a component
   * prop. This is platform-dependent and may be overwritten.
   */
  isReservedAttr: no,

  /**
   * Check if a tag is an unknown element.
   * Platform-dependent.
   */
  isUnknownElement: no,

  /**
   * Get the namespace of an element
   */
  getTagNamespace: noop,

  /**
   * Parse the real tag name for the specific platform.
   */
  parsePlatformTagName: identity,

  /**
   * Check if an attribute must be bound using property, e.g. value
   * Platform-dependent.
   */
  mustUseProp: no,

  /**
   * Perform updates asynchronously. Intended to be used by Vue Test Utils
   * This will significantly reduce performance if set to false.
   */
  async: true,

  /**
   * Exposed for legacy reasons
   */
  _lifecycleHooks: LIFECYCLE_HOOKS
});

/*  */

/**
 * unicode letters used for parsing html tags, component names and property paths.
 * using https://www.w3.org/TR/html53/semantics-scripting.html#potentialcustomelementname
 * skipping \u10000-\uEFFFF due to it freezing up PhantomJS
 */
var unicodeRegExp = /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/;

/**
 * Check if a string starts with $ or _
 */
function isReserved (str) {
  var c = (str + '').charCodeAt(0);
  return c === 0x24 || c === 0x5F
}

/**
 * Define a property.
 */
function def (obj, key, val, enumerable) {
  Object.defineProperty(obj, key, {
    value: val,
    enumerable: !!enumerable,
    writable: true,
    configurable: true
  });
}

/**
 * Parse simple path.
 */
var bailRE = new RegExp(("[^" + (unicodeRegExp.source) + ".$_\\d]"));
function parsePath (path) {
  if (bailRE.test(path)) {
    return
  }
  var segments = path.split('.');
  return function (obj) {
    for (var i = 0; i < segments.length; i++) {
      if (!obj) { return }
      obj = obj[segments[i]];
    }
    return obj
  }
}

/*  */

// can we use __proto__?
var hasProto = '__proto__' in {};

// Browser environment sniffing
var inBrowser = typeof window !== 'undefined';
var inWeex = typeof WXEnvironment !== 'undefined' && !!WXEnvironment.platform;
var weexPlatform = inWeex && WXEnvironment.platform.toLowerCase();
var UA = inBrowser && window.navigator.userAgent.toLowerCase();
var isIE = UA && /msie|trident/.test(UA);
var isIE9 = UA && UA.indexOf('msie 9.0') > 0;
var isEdge = UA && UA.indexOf('edge/') > 0;
var isAndroid = (UA && UA.indexOf('android') > 0) || (weexPlatform === 'android');
var isIOS = (UA && /iphone|ipad|ipod|ios/.test(UA)) || (weexPlatform === 'ios');
var isChrome = UA && /chrome\/\d+/.test(UA) && !isEdge;
var isPhantomJS = UA && /phantomjs/.test(UA);
var isFF = UA && UA.match(/firefox\/(\d+)/);

// Firefox has a "watch" function on Object.prototype...
var nativeWatch = ({}).watch;
if (inBrowser) {
  try {
    var opts = {};
    Object.defineProperty(opts, 'passive', ({
      get: function get () {
      }
    })); // https://github.com/facebook/flow/issues/285
    window.addEventListener('test-passive', null, opts);
  } catch (e) {}
}

// this needs to be lazy-evaled because vue may be required before
// vue-server-renderer can set VUE_ENV
var _isServer;
var isServerRendering = function () {
  if (_isServer === undefined) {
    /* istanbul ignore if */
    if (!inBrowser && !inWeex && typeof global !== 'undefined') {
      // detect presence of vue-server-renderer and avoid
      // Webpack shimming the process
      _isServer = global['process'] && global['process'].env.VUE_ENV === 'server';
    } else {
      _isServer = false;
    }
  }
  return _isServer
};

// detect devtools
var devtools = inBrowser && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

/* istanbul ignore next */
function isNative (Ctor) {
  return typeof Ctor === 'function' && /native code/.test(Ctor.toString())
}

var hasSymbol =
  typeof Symbol !== 'undefined' && isNative(Symbol) &&
  typeof Reflect !== 'undefined' && isNative(Reflect.ownKeys);

var _Set;
/* istanbul ignore if */ // $flow-disable-line
if (typeof Set !== 'undefined' && isNative(Set)) {
  // use native Set when available.
  _Set = Set;
} else {
  // a non-standard Set polyfill that only works with primitive keys.
  _Set = /*@__PURE__*/(function () {
    function Set () {
      this.set = Object.create(null);
    }
    Set.prototype.has = function has (key) {
      return this.set[key] === true
    };
    Set.prototype.add = function add (key) {
      this.set[key] = true;
    };
    Set.prototype.clear = function clear () {
      this.set = Object.create(null);
    };

    return Set;
  }());
}

/*  */

var warn = noop;
var tip = noop;
var generateComponentTrace = (noop); // work around flow check
var formatComponentName = (noop);

if (true) {
  var hasConsole = typeof console !== 'undefined';
  var classifyRE = /(?:^|[-_])(\w)/g;
  var classify = function (str) { return str
    .replace(classifyRE, function (c) { return c.toUpperCase(); })
    .replace(/[-_]/g, ''); };

  warn = function (msg, vm) {
    var trace = vm ? generateComponentTrace(vm) : '';

    if (config.warnHandler) {
      config.warnHandler.call(null, msg, vm, trace);
    } else if (hasConsole && (!config.silent)) {
      console.error(("[Vue warn]: " + msg + trace));
    }
  };

  tip = function (msg, vm) {
    if (hasConsole && (!config.silent)) {
      console.warn("[Vue tip]: " + msg + (
        vm ? generateComponentTrace(vm) : ''
      ));
    }
  };

  formatComponentName = function (vm, includeFile) {
    if (vm.$root === vm) {
      if (vm.$options && vm.$options.__file) { // fixed by xxxxxx
        return ('') + vm.$options.__file
      }
      return '<Root>'
    }
    var options = typeof vm === 'function' && vm.cid != null
      ? vm.options
      : vm._isVue
        ? vm.$options || vm.constructor.options
        : vm;
    var name = options.name || options._componentTag;
    var file = options.__file;
    if (!name && file) {
      var match = file.match(/([^/\\]+)\.vue$/);
      name = match && match[1];
    }

    return (
      (name ? ("<" + (classify(name)) + ">") : "<Anonymous>") +
      (file && includeFile !== false ? (" at " + file) : '')
    )
  };

  var repeat = function (str, n) {
    var res = '';
    while (n) {
      if (n % 2 === 1) { res += str; }
      if (n > 1) { str += str; }
      n >>= 1;
    }
    return res
  };

  generateComponentTrace = function (vm) {
    if (vm._isVue && vm.$parent) {
      var tree = [];
      var currentRecursiveSequence = 0;
      while (vm && vm.$options.name !== 'PageBody') {
        if (tree.length > 0) {
          var last = tree[tree.length - 1];
          if (last.constructor === vm.constructor) {
            currentRecursiveSequence++;
            vm = vm.$parent;
            continue
          } else if (currentRecursiveSequence > 0) {
            tree[tree.length - 1] = [last, currentRecursiveSequence];
            currentRecursiveSequence = 0;
          }
        }
        !vm.$options.isReserved && tree.push(vm);
        vm = vm.$parent;
      }
      return '\n\nfound in\n\n' + tree
        .map(function (vm, i) { return ("" + (i === 0 ? '---> ' : repeat(' ', 5 + i * 2)) + (Array.isArray(vm)
            ? ((formatComponentName(vm[0])) + "... (" + (vm[1]) + " recursive calls)")
            : formatComponentName(vm))); })
        .join('\n')
    } else {
      return ("\n\n(found in " + (formatComponentName(vm)) + ")")
    }
  };
}

/*  */

var uid = 0;

/**
 * A dep is an observable that can have multiple
 * directives subscribing to it.
 */
var Dep = function Dep () {
  this.id = uid++;
  this.subs = [];
};

Dep.prototype.addSub = function addSub (sub) {
  this.subs.push(sub);
};

Dep.prototype.removeSub = function removeSub (sub) {
  remove(this.subs, sub);
};

Dep.prototype.depend = function depend () {
  if (Dep.SharedObject.target) {
    Dep.SharedObject.target.addDep(this);
  }
};

Dep.prototype.notify = function notify () {
  // stabilize the subscriber list first
  var subs = this.subs.slice();
  if ( true && !config.async) {
    // subs aren't sorted in scheduler if not running async
    // we need to sort them now to make sure they fire in correct
    // order
    subs.sort(function (a, b) { return a.id - b.id; });
  }
  for (var i = 0, l = subs.length; i < l; i++) {
    subs[i].update();
  }
};

// The current target watcher being evaluated.
// This is globally unique because only one watcher
// can be evaluated at a time.
// fixed by xxxxxx (nvue shared vuex)
/* eslint-disable no-undef */
Dep.SharedObject = {};
Dep.SharedObject.target = null;
Dep.SharedObject.targetStack = [];

function pushTarget (target) {
  Dep.SharedObject.targetStack.push(target);
  Dep.SharedObject.target = target;
  Dep.target = target;
}

function popTarget () {
  Dep.SharedObject.targetStack.pop();
  Dep.SharedObject.target = Dep.SharedObject.targetStack[Dep.SharedObject.targetStack.length - 1];
  Dep.target = Dep.SharedObject.target;
}

/*  */

var VNode = function VNode (
  tag,
  data,
  children,
  text,
  elm,
  context,
  componentOptions,
  asyncFactory
) {
  this.tag = tag;
  this.data = data;
  this.children = children;
  this.text = text;
  this.elm = elm;
  this.ns = undefined;
  this.context = context;
  this.fnContext = undefined;
  this.fnOptions = undefined;
  this.fnScopeId = undefined;
  this.key = data && data.key;
  this.componentOptions = componentOptions;
  this.componentInstance = undefined;
  this.parent = undefined;
  this.raw = false;
  this.isStatic = false;
  this.isRootInsert = true;
  this.isComment = false;
  this.isCloned = false;
  this.isOnce = false;
  this.asyncFactory = asyncFactory;
  this.asyncMeta = undefined;
  this.isAsyncPlaceholder = false;
};

var prototypeAccessors = { child: { configurable: true } };

// DEPRECATED: alias for componentInstance for backwards compat.
/* istanbul ignore next */
prototypeAccessors.child.get = function () {
  return this.componentInstance
};

Object.defineProperties( VNode.prototype, prototypeAccessors );

var createEmptyVNode = function (text) {
  if ( text === void 0 ) text = '';

  var node = new VNode();
  node.text = text;
  node.isComment = true;
  return node
};

function createTextVNode (val) {
  return new VNode(undefined, undefined, undefined, String(val))
}

// optimized shallow clone
// used for static nodes and slot nodes because they may be reused across
// multiple renders, cloning them avoids errors when DOM manipulations rely
// on their elm reference.
function cloneVNode (vnode) {
  var cloned = new VNode(
    vnode.tag,
    vnode.data,
    // #7975
    // clone children array to avoid mutating original in case of cloning
    // a child.
    vnode.children && vnode.children.slice(),
    vnode.text,
    vnode.elm,
    vnode.context,
    vnode.componentOptions,
    vnode.asyncFactory
  );
  cloned.ns = vnode.ns;
  cloned.isStatic = vnode.isStatic;
  cloned.key = vnode.key;
  cloned.isComment = vnode.isComment;
  cloned.fnContext = vnode.fnContext;
  cloned.fnOptions = vnode.fnOptions;
  cloned.fnScopeId = vnode.fnScopeId;
  cloned.asyncMeta = vnode.asyncMeta;
  cloned.isCloned = true;
  return cloned
}

/*
 * not type checking this file because flow doesn't play well with
 * dynamically accessing methods on Array prototype
 */

var arrayProto = Array.prototype;
var arrayMethods = Object.create(arrayProto);

var methodsToPatch = [
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'sort',
  'reverse'
];

/**
 * Intercept mutating methods and emit events
 */
methodsToPatch.forEach(function (method) {
  // cache original method
  var original = arrayProto[method];
  def(arrayMethods, method, function mutator () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    var result = original.apply(this, args);
    var ob = this.__ob__;
    var inserted;
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args;
        break
      case 'splice':
        inserted = args.slice(2);
        break
    }
    if (inserted) { ob.observeArray(inserted); }
    // notify change
    ob.dep.notify();
    return result
  });
});

/*  */

var arrayKeys = Object.getOwnPropertyNames(arrayMethods);

/**
 * In some cases we may want to disable observation inside a component's
 * update computation.
 */
var shouldObserve = true;

function toggleObserving (value) {
  shouldObserve = value;
}

/**
 * Observer class that is attached to each observed
 * object. Once attached, the observer converts the target
 * object's property keys into getter/setters that
 * collect dependencies and dispatch updates.
 */
var Observer = function Observer (value) {
  this.value = value;
  this.dep = new Dep();
  this.vmCount = 0;
  def(value, '__ob__', this);
  if (Array.isArray(value)) {
    if (hasProto) {
      {// fixed by xxxxxx 微信小程序使用 plugins 之后，数组方法被直接挂载到了数组对象上，需要执行 copyAugment 逻辑
        if(value.push !== value.__proto__.push){
          copyAugment(value, arrayMethods, arrayKeys);
        } else {
          protoAugment(value, arrayMethods);
        }
      }
    } else {
      copyAugment(value, arrayMethods, arrayKeys);
    }
    this.observeArray(value);
  } else {
    this.walk(value);
  }
};

/**
 * Walk through all properties and convert them into
 * getter/setters. This method should only be called when
 * value type is Object.
 */
Observer.prototype.walk = function walk (obj) {
  var keys = Object.keys(obj);
  for (var i = 0; i < keys.length; i++) {
    defineReactive$$1(obj, keys[i]);
  }
};

/**
 * Observe a list of Array items.
 */
Observer.prototype.observeArray = function observeArray (items) {
  for (var i = 0, l = items.length; i < l; i++) {
    observe(items[i]);
  }
};

// helpers

/**
 * Augment a target Object or Array by intercepting
 * the prototype chain using __proto__
 */
function protoAugment (target, src) {
  /* eslint-disable no-proto */
  target.__proto__ = src;
  /* eslint-enable no-proto */
}

/**
 * Augment a target Object or Array by defining
 * hidden properties.
 */
/* istanbul ignore next */
function copyAugment (target, src, keys) {
  for (var i = 0, l = keys.length; i < l; i++) {
    var key = keys[i];
    def(target, key, src[key]);
  }
}

/**
 * Attempt to create an observer instance for a value,
 * returns the new observer if successfully observed,
 * or the existing observer if the value already has one.
 */
function observe (value, asRootData) {
  if (!isObject(value) || value instanceof VNode) {
    return
  }
  var ob;
  if (hasOwn(value, '__ob__') && value.__ob__ instanceof Observer) {
    ob = value.__ob__;
  } else if (
    shouldObserve &&
    !isServerRendering() &&
    (Array.isArray(value) || isPlainObject(value)) &&
    Object.isExtensible(value) &&
    !value._isVue
  ) {
    ob = new Observer(value);
  }
  if (asRootData && ob) {
    ob.vmCount++;
  }
  return ob
}

/**
 * Define a reactive property on an Object.
 */
function defineReactive$$1 (
  obj,
  key,
  val,
  customSetter,
  shallow
) {
  var dep = new Dep();

  var property = Object.getOwnPropertyDescriptor(obj, key);
  if (property && property.configurable === false) {
    return
  }

  // cater for pre-defined getter/setters
  var getter = property && property.get;
  var setter = property && property.set;
  if ((!getter || setter) && arguments.length === 2) {
    val = obj[key];
  }

  var childOb = !shallow && observe(val);
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get: function reactiveGetter () {
      var value = getter ? getter.call(obj) : val;
      if (Dep.SharedObject.target) { // fixed by xxxxxx
        dep.depend();
        if (childOb) {
          childOb.dep.depend();
          if (Array.isArray(value)) {
            dependArray(value);
          }
        }
      }
      return value
    },
    set: function reactiveSetter (newVal) {
      var value = getter ? getter.call(obj) : val;
      /* eslint-disable no-self-compare */
      if (newVal === value || (newVal !== newVal && value !== value)) {
        return
      }
      /* eslint-enable no-self-compare */
      if ( true && customSetter) {
        customSetter();
      }
      // #7981: for accessor properties without setter
      if (getter && !setter) { return }
      if (setter) {
        setter.call(obj, newVal);
      } else {
        val = newVal;
      }
      childOb = !shallow && observe(newVal);
      dep.notify();
    }
  });
}

/**
 * Set a property on an object. Adds the new property and
 * triggers change notification if the property doesn't
 * already exist.
 */
function set (target, key, val) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot set reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.length = Math.max(target.length, key);
    target.splice(key, 1, val);
    return val
  }
  if (key in target && !(key in Object.prototype)) {
    target[key] = val;
    return val
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid adding reactive properties to a Vue instance or its root $data ' +
      'at runtime - declare it upfront in the data option.'
    );
    return val
  }
  if (!ob) {
    target[key] = val;
    return val
  }
  defineReactive$$1(ob.value, key, val);
  ob.dep.notify();
  return val
}

/**
 * Delete a property and trigger change if necessary.
 */
function del (target, key) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot delete reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.splice(key, 1);
    return
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid deleting properties on a Vue instance or its root $data ' +
      '- just set it to null.'
    );
    return
  }
  if (!hasOwn(target, key)) {
    return
  }
  delete target[key];
  if (!ob) {
    return
  }
  ob.dep.notify();
}

/**
 * Collect dependencies on array elements when the array is touched, since
 * we cannot intercept array element access like property getters.
 */
function dependArray (value) {
  for (var e = (void 0), i = 0, l = value.length; i < l; i++) {
    e = value[i];
    e && e.__ob__ && e.__ob__.dep.depend();
    if (Array.isArray(e)) {
      dependArray(e);
    }
  }
}

/*  */

/**
 * Option overwriting strategies are functions that handle
 * how to merge a parent option value and a child option
 * value into the final value.
 */
var strats = config.optionMergeStrategies;

/**
 * Options with restrictions
 */
if (true) {
  strats.el = strats.propsData = function (parent, child, vm, key) {
    if (!vm) {
      warn(
        "option \"" + key + "\" can only be used during instance " +
        'creation with the `new` keyword.'
      );
    }
    return defaultStrat(parent, child)
  };
}

/**
 * Helper that recursively merges two data objects together.
 */
function mergeData (to, from) {
  if (!from) { return to }
  var key, toVal, fromVal;

  var keys = hasSymbol
    ? Reflect.ownKeys(from)
    : Object.keys(from);

  for (var i = 0; i < keys.length; i++) {
    key = keys[i];
    // in case the object is already observed...
    if (key === '__ob__') { continue }
    toVal = to[key];
    fromVal = from[key];
    if (!hasOwn(to, key)) {
      set(to, key, fromVal);
    } else if (
      toVal !== fromVal &&
      isPlainObject(toVal) &&
      isPlainObject(fromVal)
    ) {
      mergeData(toVal, fromVal);
    }
  }
  return to
}

/**
 * Data
 */
function mergeDataOrFn (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    // in a Vue.extend merge, both should be functions
    if (!childVal) {
      return parentVal
    }
    if (!parentVal) {
      return childVal
    }
    // when parentVal & childVal are both present,
    // we need to return a function that returns the
    // merged result of both functions... no need to
    // check if parentVal is a function here because
    // it has to be a function to pass previous merges.
    return function mergedDataFn () {
      return mergeData(
        typeof childVal === 'function' ? childVal.call(this, this) : childVal,
        typeof parentVal === 'function' ? parentVal.call(this, this) : parentVal
      )
    }
  } else {
    return function mergedInstanceDataFn () {
      // instance merge
      var instanceData = typeof childVal === 'function'
        ? childVal.call(vm, vm)
        : childVal;
      var defaultData = typeof parentVal === 'function'
        ? parentVal.call(vm, vm)
        : parentVal;
      if (instanceData) {
        return mergeData(instanceData, defaultData)
      } else {
        return defaultData
      }
    }
  }
}

strats.data = function (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    if (childVal && typeof childVal !== 'function') {
       true && warn(
        'The "data" option should be a function ' +
        'that returns a per-instance value in component ' +
        'definitions.',
        vm
      );

      return parentVal
    }
    return mergeDataOrFn(parentVal, childVal)
  }

  return mergeDataOrFn(parentVal, childVal, vm)
};

/**
 * Hooks and props are merged as arrays.
 */
function mergeHook (
  parentVal,
  childVal
) {
  var res = childVal
    ? parentVal
      ? parentVal.concat(childVal)
      : Array.isArray(childVal)
        ? childVal
        : [childVal]
    : parentVal;
  return res
    ? dedupeHooks(res)
    : res
}

function dedupeHooks (hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res
}

LIFECYCLE_HOOKS.forEach(function (hook) {
  strats[hook] = mergeHook;
});

/**
 * Assets
 *
 * When a vm is present (instance creation), we need to do
 * a three-way merge between constructor options, instance
 * options and parent options.
 */
function mergeAssets (
  parentVal,
  childVal,
  vm,
  key
) {
  var res = Object.create(parentVal || null);
  if (childVal) {
     true && assertObjectType(key, childVal, vm);
    return extend(res, childVal)
  } else {
    return res
  }
}

ASSET_TYPES.forEach(function (type) {
  strats[type + 's'] = mergeAssets;
});

/**
 * Watchers.
 *
 * Watchers hashes should not overwrite one
 * another, so we merge them as arrays.
 */
strats.watch = function (
  parentVal,
  childVal,
  vm,
  key
) {
  // work around Firefox's Object.prototype.watch...
  if (parentVal === nativeWatch) { parentVal = undefined; }
  if (childVal === nativeWatch) { childVal = undefined; }
  /* istanbul ignore if */
  if (!childVal) { return Object.create(parentVal || null) }
  if (true) {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = {};
  extend(ret, parentVal);
  for (var key$1 in childVal) {
    var parent = ret[key$1];
    var child = childVal[key$1];
    if (parent && !Array.isArray(parent)) {
      parent = [parent];
    }
    ret[key$1] = parent
      ? parent.concat(child)
      : Array.isArray(child) ? child : [child];
  }
  return ret
};

/**
 * Other object hashes.
 */
strats.props =
strats.methods =
strats.inject =
strats.computed = function (
  parentVal,
  childVal,
  vm,
  key
) {
  if (childVal && "development" !== 'production') {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = Object.create(null);
  extend(ret, parentVal);
  if (childVal) { extend(ret, childVal); }
  return ret
};
strats.provide = mergeDataOrFn;

/**
 * Default strategy.
 */
var defaultStrat = function (parentVal, childVal) {
  return childVal === undefined
    ? parentVal
    : childVal
};

/**
 * Validate component names
 */
function checkComponents (options) {
  for (var key in options.components) {
    validateComponentName(key);
  }
}

function validateComponentName (name) {
  if (!new RegExp(("^[a-zA-Z][\\-\\.0-9_" + (unicodeRegExp.source) + "]*$")).test(name)) {
    warn(
      'Invalid component name: "' + name + '". Component names ' +
      'should conform to valid custom element name in html5 specification.'
    );
  }
  if (isBuiltInTag(name) || config.isReservedTag(name)) {
    warn(
      'Do not use built-in or reserved HTML elements as component ' +
      'id: ' + name
    );
  }
}

/**
 * Ensure all props option syntax are normalized into the
 * Object-based format.
 */
function normalizeProps (options, vm) {
  var props = options.props;
  if (!props) { return }
  var res = {};
  var i, val, name;
  if (Array.isArray(props)) {
    i = props.length;
    while (i--) {
      val = props[i];
      if (typeof val === 'string') {
        name = camelize(val);
        res[name] = { type: null };
      } else if (true) {
        warn('props must be strings when using array syntax.');
      }
    }
  } else if (isPlainObject(props)) {
    for (var key in props) {
      val = props[key];
      name = camelize(key);
      res[name] = isPlainObject(val)
        ? val
        : { type: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"props\": expected an Array or an Object, " +
      "but got " + (toRawType(props)) + ".",
      vm
    );
  }
  options.props = res;
}

/**
 * Normalize all injections into Object-based format
 */
function normalizeInject (options, vm) {
  var inject = options.inject;
  if (!inject) { return }
  var normalized = options.inject = {};
  if (Array.isArray(inject)) {
    for (var i = 0; i < inject.length; i++) {
      normalized[inject[i]] = { from: inject[i] };
    }
  } else if (isPlainObject(inject)) {
    for (var key in inject) {
      var val = inject[key];
      normalized[key] = isPlainObject(val)
        ? extend({ from: key }, val)
        : { from: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"inject\": expected an Array or an Object, " +
      "but got " + (toRawType(inject)) + ".",
      vm
    );
  }
}

/**
 * Normalize raw function directives into object format.
 */
function normalizeDirectives (options) {
  var dirs = options.directives;
  if (dirs) {
    for (var key in dirs) {
      var def$$1 = dirs[key];
      if (typeof def$$1 === 'function') {
        dirs[key] = { bind: def$$1, update: def$$1 };
      }
    }
  }
}

function assertObjectType (name, value, vm) {
  if (!isPlainObject(value)) {
    warn(
      "Invalid value for option \"" + name + "\": expected an Object, " +
      "but got " + (toRawType(value)) + ".",
      vm
    );
  }
}

/**
 * Merge two option objects into a new one.
 * Core utility used in both instantiation and inheritance.
 */
function mergeOptions (
  parent,
  child,
  vm
) {
  if (true) {
    checkComponents(child);
  }

  if (typeof child === 'function') {
    child = child.options;
  }

  normalizeProps(child, vm);
  normalizeInject(child, vm);
  normalizeDirectives(child);

  // Apply extends and mixins on the child options,
  // but only if it is a raw options object that isn't
  // the result of another mergeOptions call.
  // Only merged options has the _base property.
  if (!child._base) {
    if (child.extends) {
      parent = mergeOptions(parent, child.extends, vm);
    }
    if (child.mixins) {
      for (var i = 0, l = child.mixins.length; i < l; i++) {
        parent = mergeOptions(parent, child.mixins[i], vm);
      }
    }
  }

  var options = {};
  var key;
  for (key in parent) {
    mergeField(key);
  }
  for (key in child) {
    if (!hasOwn(parent, key)) {
      mergeField(key);
    }
  }
  function mergeField (key) {
    var strat = strats[key] || defaultStrat;
    options[key] = strat(parent[key], child[key], vm, key);
  }
  return options
}

/**
 * Resolve an asset.
 * This function is used because child instances need access
 * to assets defined in its ancestor chain.
 */
function resolveAsset (
  options,
  type,
  id,
  warnMissing
) {
  /* istanbul ignore if */
  if (typeof id !== 'string') {
    return
  }
  var assets = options[type];
  // check local registration variations first
  if (hasOwn(assets, id)) { return assets[id] }
  var camelizedId = camelize(id);
  if (hasOwn(assets, camelizedId)) { return assets[camelizedId] }
  var PascalCaseId = capitalize(camelizedId);
  if (hasOwn(assets, PascalCaseId)) { return assets[PascalCaseId] }
  // fallback to prototype chain
  var res = assets[id] || assets[camelizedId] || assets[PascalCaseId];
  if ( true && warnMissing && !res) {
    warn(
      'Failed to resolve ' + type.slice(0, -1) + ': ' + id,
      options
    );
  }
  return res
}

/*  */



function validateProp (
  key,
  propOptions,
  propsData,
  vm
) {
  var prop = propOptions[key];
  var absent = !hasOwn(propsData, key);
  var value = propsData[key];
  // boolean casting
  var booleanIndex = getTypeIndex(Boolean, prop.type);
  if (booleanIndex > -1) {
    if (absent && !hasOwn(prop, 'default')) {
      value = false;
    } else if (value === '' || value === hyphenate(key)) {
      // only cast empty string / same name to boolean if
      // boolean has higher priority
      var stringIndex = getTypeIndex(String, prop.type);
      if (stringIndex < 0 || booleanIndex < stringIndex) {
        value = true;
      }
    }
  }
  // check default value
  if (value === undefined) {
    value = getPropDefaultValue(vm, prop, key);
    // since the default value is a fresh copy,
    // make sure to observe it.
    var prevShouldObserve = shouldObserve;
    toggleObserving(true);
    observe(value);
    toggleObserving(prevShouldObserve);
  }
  if (
    true
  ) {
    assertProp(prop, key, value, vm, absent);
  }
  return value
}

/**
 * Get the default value of a prop.
 */
function getPropDefaultValue (vm, prop, key) {
  // no default, return undefined
  if (!hasOwn(prop, 'default')) {
    return undefined
  }
  var def = prop.default;
  // warn against non-factory defaults for Object & Array
  if ( true && isObject(def)) {
    warn(
      'Invalid default value for prop "' + key + '": ' +
      'Props with type Object/Array must use a factory function ' +
      'to return the default value.',
      vm
    );
  }
  // the raw prop value was also undefined from previous render,
  // return previous default value to avoid unnecessary watcher trigger
  if (vm && vm.$options.propsData &&
    vm.$options.propsData[key] === undefined &&
    vm._props[key] !== undefined
  ) {
    return vm._props[key]
  }
  // call factory function for non-Function types
  // a value is Function if its prototype is function even across different execution context
  return typeof def === 'function' && getType(prop.type) !== 'Function'
    ? def.call(vm)
    : def
}

/**
 * Assert whether a prop is valid.
 */
function assertProp (
  prop,
  name,
  value,
  vm,
  absent
) {
  if (prop.required && absent) {
    warn(
      'Missing required prop: "' + name + '"',
      vm
    );
    return
  }
  if (value == null && !prop.required) {
    return
  }
  var type = prop.type;
  var valid = !type || type === true;
  var expectedTypes = [];
  if (type) {
    if (!Array.isArray(type)) {
      type = [type];
    }
    for (var i = 0; i < type.length && !valid; i++) {
      var assertedType = assertType(value, type[i]);
      expectedTypes.push(assertedType.expectedType || '');
      valid = assertedType.valid;
    }
  }

  if (!valid) {
    warn(
      getInvalidTypeMessage(name, value, expectedTypes),
      vm
    );
    return
  }
  var validator = prop.validator;
  if (validator) {
    if (!validator(value)) {
      warn(
        'Invalid prop: custom validator check failed for prop "' + name + '".',
        vm
      );
    }
  }
}

var simpleCheckRE = /^(String|Number|Boolean|Function|Symbol)$/;

function assertType (value, type) {
  var valid;
  var expectedType = getType(type);
  if (simpleCheckRE.test(expectedType)) {
    var t = typeof value;
    valid = t === expectedType.toLowerCase();
    // for primitive wrapper objects
    if (!valid && t === 'object') {
      valid = value instanceof type;
    }
  } else if (expectedType === 'Object') {
    valid = isPlainObject(value);
  } else if (expectedType === 'Array') {
    valid = Array.isArray(value);
  } else {
    valid = value instanceof type;
  }
  return {
    valid: valid,
    expectedType: expectedType
  }
}

/**
 * Use function string name to check built-in types,
 * because a simple equality check will fail when running
 * across different vms / iframes.
 */
function getType (fn) {
  var match = fn && fn.toString().match(/^\s*function (\w+)/);
  return match ? match[1] : ''
}

function isSameType (a, b) {
  return getType(a) === getType(b)
}

function getTypeIndex (type, expectedTypes) {
  if (!Array.isArray(expectedTypes)) {
    return isSameType(expectedTypes, type) ? 0 : -1
  }
  for (var i = 0, len = expectedTypes.length; i < len; i++) {
    if (isSameType(expectedTypes[i], type)) {
      return i
    }
  }
  return -1
}

function getInvalidTypeMessage (name, value, expectedTypes) {
  var message = "Invalid prop: type check failed for prop \"" + name + "\"." +
    " Expected " + (expectedTypes.map(capitalize).join(', '));
  var expectedType = expectedTypes[0];
  var receivedType = toRawType(value);
  var expectedValue = styleValue(value, expectedType);
  var receivedValue = styleValue(value, receivedType);
  // check if we need to specify expected value
  if (expectedTypes.length === 1 &&
      isExplicable(expectedType) &&
      !isBoolean(expectedType, receivedType)) {
    message += " with value " + expectedValue;
  }
  message += ", got " + receivedType + " ";
  // check if we need to specify received value
  if (isExplicable(receivedType)) {
    message += "with value " + receivedValue + ".";
  }
  return message
}

function styleValue (value, type) {
  if (type === 'String') {
    return ("\"" + value + "\"")
  } else if (type === 'Number') {
    return ("" + (Number(value)))
  } else {
    return ("" + value)
  }
}

function isExplicable (value) {
  var explicitTypes = ['string', 'number', 'boolean'];
  return explicitTypes.some(function (elem) { return value.toLowerCase() === elem; })
}

function isBoolean () {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  return args.some(function (elem) { return elem.toLowerCase() === 'boolean'; })
}

/*  */

function handleError (err, vm, info) {
  // Deactivate deps tracking while processing error handler to avoid possible infinite rendering.
  // See: https://github.com/vuejs/vuex/issues/1505
  pushTarget();
  try {
    if (vm) {
      var cur = vm;
      while ((cur = cur.$parent)) {
        var hooks = cur.$options.errorCaptured;
        if (hooks) {
          for (var i = 0; i < hooks.length; i++) {
            try {
              var capture = hooks[i].call(cur, err, vm, info) === false;
              if (capture) { return }
            } catch (e) {
              globalHandleError(e, cur, 'errorCaptured hook');
            }
          }
        }
      }
    }
    globalHandleError(err, vm, info);
  } finally {
    popTarget();
  }
}

function invokeWithErrorHandling (
  handler,
  context,
  args,
  vm,
  info
) {
  var res;
  try {
    res = args ? handler.apply(context, args) : handler.call(context);
    if (res && !res._isVue && isPromise(res) && !res._handled) {
      res.catch(function (e) { return handleError(e, vm, info + " (Promise/async)"); });
      // issue #9511
      // avoid catch triggering multiple times when nested calls
      res._handled = true;
    }
  } catch (e) {
    handleError(e, vm, info);
  }
  return res
}

function globalHandleError (err, vm, info) {
  if (config.errorHandler) {
    try {
      return config.errorHandler.call(null, err, vm, info)
    } catch (e) {
      // if the user intentionally throws the original error in the handler,
      // do not log it twice
      if (e !== err) {
        logError(e, null, 'config.errorHandler');
      }
    }
  }
  logError(err, vm, info);
}

function logError (err, vm, info) {
  if (true) {
    warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
  }
  /* istanbul ignore else */
  if ((inBrowser || inWeex) && typeof console !== 'undefined') {
    console.error(err);
  } else {
    throw err
  }
}

/*  */

var callbacks = [];
var pending = false;

function flushCallbacks () {
  pending = false;
  var copies = callbacks.slice(0);
  callbacks.length = 0;
  for (var i = 0; i < copies.length; i++) {
    copies[i]();
  }
}

// Here we have async deferring wrappers using microtasks.
// In 2.5 we used (macro) tasks (in combination with microtasks).
// However, it has subtle problems when state is changed right before repaint
// (e.g. #6813, out-in transitions).
// Also, using (macro) tasks in event handler would cause some weird behaviors
// that cannot be circumvented (e.g. #7109, #7153, #7546, #7834, #8109).
// So we now use microtasks everywhere, again.
// A major drawback of this tradeoff is that there are some scenarios
// where microtasks have too high a priority and fire in between supposedly
// sequential events (e.g. #4521, #6690, which have workarounds)
// or even between bubbling of the same event (#6566).
var timerFunc;

// The nextTick behavior leverages the microtask queue, which can be accessed
// via either native Promise.then or MutationObserver.
// MutationObserver has wider support, however it is seriously bugged in
// UIWebView in iOS >= 9.3.3 when triggered in touch event handlers. It
// completely stops working after triggering a few times... so, if native
// Promise is available, we will use it:
/* istanbul ignore next, $flow-disable-line */
if (typeof Promise !== 'undefined' && isNative(Promise)) {
  var p = Promise.resolve();
  timerFunc = function () {
    p.then(flushCallbacks);
    // In problematic UIWebViews, Promise.then doesn't completely break, but
    // it can get stuck in a weird state where callbacks are pushed into the
    // microtask queue but the queue isn't being flushed, until the browser
    // needs to do some other work, e.g. handle a timer. Therefore we can
    // "force" the microtask queue to be flushed by adding an empty timer.
    if (isIOS) { setTimeout(noop); }
  };
} else if (!isIE && typeof MutationObserver !== 'undefined' && (
  isNative(MutationObserver) ||
  // PhantomJS and iOS 7.x
  MutationObserver.toString() === '[object MutationObserverConstructor]'
)) {
  // Use MutationObserver where native Promise is not available,
  // e.g. PhantomJS, iOS7, Android 4.4
  // (#6466 MutationObserver is unreliable in IE11)
  var counter = 1;
  var observer = new MutationObserver(flushCallbacks);
  var textNode = document.createTextNode(String(counter));
  observer.observe(textNode, {
    characterData: true
  });
  timerFunc = function () {
    counter = (counter + 1) % 2;
    textNode.data = String(counter);
  };
} else if (typeof setImmediate !== 'undefined' && isNative(setImmediate)) {
  // Fallback to setImmediate.
  // Technically it leverages the (macro) task queue,
  // but it is still a better choice than setTimeout.
  timerFunc = function () {
    setImmediate(flushCallbacks);
  };
} else {
  // Fallback to setTimeout.
  timerFunc = function () {
    setTimeout(flushCallbacks, 0);
  };
}

function nextTick (cb, ctx) {
  var _resolve;
  callbacks.push(function () {
    if (cb) {
      try {
        cb.call(ctx);
      } catch (e) {
        handleError(e, ctx, 'nextTick');
      }
    } else if (_resolve) {
      _resolve(ctx);
    }
  });
  if (!pending) {
    pending = true;
    timerFunc();
  }
  // $flow-disable-line
  if (!cb && typeof Promise !== 'undefined') {
    return new Promise(function (resolve) {
      _resolve = resolve;
    })
  }
}

/*  */

/* not type checking this file because flow doesn't play well with Proxy */

var initProxy;

if (true) {
  var allowedGlobals = makeMap(
    'Infinity,undefined,NaN,isFinite,isNaN,' +
    'parseFloat,parseInt,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,' +
    'Math,Number,Date,Array,Object,Boolean,String,RegExp,Map,Set,JSON,Intl,' +
    'require' // for Webpack/Browserify
  );

  var warnNonPresent = function (target, key) {
    warn(
      "Property or method \"" + key + "\" is not defined on the instance but " +
      'referenced during render. Make sure that this property is reactive, ' +
      'either in the data option, or for class-based components, by ' +
      'initializing the property. ' +
      'See: https://vuejs.org/v2/guide/reactivity.html#Declaring-Reactive-Properties.',
      target
    );
  };

  var warnReservedPrefix = function (target, key) {
    warn(
      "Property \"" + key + "\" must be accessed with \"$data." + key + "\" because " +
      'properties starting with "$" or "_" are not proxied in the Vue instance to ' +
      'prevent conflicts with Vue internals. ' +
      'See: https://vuejs.org/v2/api/#data',
      target
    );
  };

  var hasProxy =
    typeof Proxy !== 'undefined' && isNative(Proxy);

  if (hasProxy) {
    var isBuiltInModifier = makeMap('stop,prevent,self,ctrl,shift,alt,meta,exact');
    config.keyCodes = new Proxy(config.keyCodes, {
      set: function set (target, key, value) {
        if (isBuiltInModifier(key)) {
          warn(("Avoid overwriting built-in modifier in config.keyCodes: ." + key));
          return false
        } else {
          target[key] = value;
          return true
        }
      }
    });
  }

  var hasHandler = {
    has: function has (target, key) {
      var has = key in target;
      var isAllowed = allowedGlobals(key) ||
        (typeof key === 'string' && key.charAt(0) === '_' && !(key in target.$data));
      if (!has && !isAllowed) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return has || !isAllowed
    }
  };

  var getHandler = {
    get: function get (target, key) {
      if (typeof key === 'string' && !(key in target)) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return target[key]
    }
  };

  initProxy = function initProxy (vm) {
    if (hasProxy) {
      // determine which proxy handler to use
      var options = vm.$options;
      var handlers = options.render && options.render._withStripped
        ? getHandler
        : hasHandler;
      vm._renderProxy = new Proxy(vm, handlers);
    } else {
      vm._renderProxy = vm;
    }
  };
}

/*  */

var seenObjects = new _Set();

/**
 * Recursively traverse an object to evoke all converted
 * getters, so that every nested property inside the object
 * is collected as a "deep" dependency.
 */
function traverse (val) {
  _traverse(val, seenObjects);
  seenObjects.clear();
}

function _traverse (val, seen) {
  var i, keys;
  var isA = Array.isArray(val);
  if ((!isA && !isObject(val)) || Object.isFrozen(val) || val instanceof VNode) {
    return
  }
  if (val.__ob__) {
    var depId = val.__ob__.dep.id;
    if (seen.has(depId)) {
      return
    }
    seen.add(depId);
  }
  if (isA) {
    i = val.length;
    while (i--) { _traverse(val[i], seen); }
  } else {
    keys = Object.keys(val);
    i = keys.length;
    while (i--) { _traverse(val[keys[i]], seen); }
  }
}

var mark;
var measure;

if (true) {
  var perf = inBrowser && window.performance;
  /* istanbul ignore if */
  if (
    perf &&
    perf.mark &&
    perf.measure &&
    perf.clearMarks &&
    perf.clearMeasures
  ) {
    mark = function (tag) { return perf.mark(tag); };
    measure = function (name, startTag, endTag) {
      perf.measure(name, startTag, endTag);
      perf.clearMarks(startTag);
      perf.clearMarks(endTag);
      // perf.clearMeasures(name)
    };
  }
}

/*  */

var normalizeEvent = cached(function (name) {
  var passive = name.charAt(0) === '&';
  name = passive ? name.slice(1) : name;
  var once$$1 = name.charAt(0) === '~'; // Prefixed last, checked first
  name = once$$1 ? name.slice(1) : name;
  var capture = name.charAt(0) === '!';
  name = capture ? name.slice(1) : name;
  return {
    name: name,
    once: once$$1,
    capture: capture,
    passive: passive
  }
});

function createFnInvoker (fns, vm) {
  function invoker () {
    var arguments$1 = arguments;

    var fns = invoker.fns;
    if (Array.isArray(fns)) {
      var cloned = fns.slice();
      for (var i = 0; i < cloned.length; i++) {
        invokeWithErrorHandling(cloned[i], null, arguments$1, vm, "v-on handler");
      }
    } else {
      // return handler return value for single handlers
      return invokeWithErrorHandling(fns, null, arguments, vm, "v-on handler")
    }
  }
  invoker.fns = fns;
  return invoker
}

function updateListeners (
  on,
  oldOn,
  add,
  remove$$1,
  createOnceHandler,
  vm
) {
  var name, def$$1, cur, old, event;
  for (name in on) {
    def$$1 = cur = on[name];
    old = oldOn[name];
    event = normalizeEvent(name);
    if (isUndef(cur)) {
       true && warn(
        "Invalid handler for event \"" + (event.name) + "\": got " + String(cur),
        vm
      );
    } else if (isUndef(old)) {
      if (isUndef(cur.fns)) {
        cur = on[name] = createFnInvoker(cur, vm);
      }
      if (isTrue(event.once)) {
        cur = on[name] = createOnceHandler(event.name, cur, event.capture);
      }
      add(event.name, cur, event.capture, event.passive, event.params);
    } else if (cur !== old) {
      old.fns = cur;
      on[name] = old;
    }
  }
  for (name in oldOn) {
    if (isUndef(on[name])) {
      event = normalizeEvent(name);
      remove$$1(event.name, oldOn[name], event.capture);
    }
  }
}

/*  */

/*  */

// fixed by xxxxxx (mp properties)
function extractPropertiesFromVNodeData(data, Ctor, res, context) {
  var propOptions = Ctor.options.mpOptions && Ctor.options.mpOptions.properties;
  if (isUndef(propOptions)) {
    return res
  }
  var externalClasses = Ctor.options.mpOptions.externalClasses || [];
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      var result = checkProp(res, props, key, altKey, true) ||
          checkProp(res, attrs, key, altKey, false);
      // externalClass
      if (
        result &&
        res[key] &&
        externalClasses.indexOf(altKey) !== -1 &&
        context[camelize(res[key])]
      ) {
        // 赋值 externalClass 真正的值(模板里 externalClass 的值可能是字符串)
        res[key] = context[camelize(res[key])];
      }
    }
  }
  return res
}

function extractPropsFromVNodeData (
  data,
  Ctor,
  tag,
  context// fixed by xxxxxx
) {
  // we are only extracting raw values here.
  // validation and default values are handled in the child
  // component itself.
  var propOptions = Ctor.options.props;
  if (isUndef(propOptions)) {
    // fixed by xxxxxx
    return extractPropertiesFromVNodeData(data, Ctor, {}, context)
  }
  var res = {};
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      if (true) {
        var keyInLowerCase = key.toLowerCase();
        if (
          key !== keyInLowerCase &&
          attrs && hasOwn(attrs, keyInLowerCase)
        ) {
          tip(
            "Prop \"" + keyInLowerCase + "\" is passed to component " +
            (formatComponentName(tag || Ctor)) + ", but the declared prop name is" +
            " \"" + key + "\". " +
            "Note that HTML attributes are case-insensitive and camelCased " +
            "props need to use their kebab-case equivalents when using in-DOM " +
            "templates. You should probably use \"" + altKey + "\" instead of \"" + key + "\"."
          );
        }
      }
      checkProp(res, props, key, altKey, true) ||
      checkProp(res, attrs, key, altKey, false);
    }
  }
  // fixed by xxxxxx
  return extractPropertiesFromVNodeData(data, Ctor, res, context)
}

function checkProp (
  res,
  hash,
  key,
  altKey,
  preserve
) {
  if (isDef(hash)) {
    if (hasOwn(hash, key)) {
      res[key] = hash[key];
      if (!preserve) {
        delete hash[key];
      }
      return true
    } else if (hasOwn(hash, altKey)) {
      res[key] = hash[altKey];
      if (!preserve) {
        delete hash[altKey];
      }
      return true
    }
  }
  return false
}

/*  */

// The template compiler attempts to minimize the need for normalization by
// statically analyzing the template at compile time.
//
// For plain HTML markup, normalization can be completely skipped because the
// generated render function is guaranteed to return Array<VNode>. There are
// two cases where extra normalization is needed:

// 1. When the children contains components - because a functional component
// may return an Array instead of a single root. In this case, just a simple
// normalization is needed - if any child is an Array, we flatten the whole
// thing with Array.prototype.concat. It is guaranteed to be only 1-level deep
// because functional components already normalize their own children.
function simpleNormalizeChildren (children) {
  for (var i = 0; i < children.length; i++) {
    if (Array.isArray(children[i])) {
      return Array.prototype.concat.apply([], children)
    }
  }
  return children
}

// 2. When the children contains constructs that always generated nested Arrays,
// e.g. <template>, <slot>, v-for, or when the children is provided by user
// with hand-written render functions / JSX. In such cases a full normalization
// is needed to cater to all possible types of children values.
function normalizeChildren (children) {
  return isPrimitive(children)
    ? [createTextVNode(children)]
    : Array.isArray(children)
      ? normalizeArrayChildren(children)
      : undefined
}

function isTextNode (node) {
  return isDef(node) && isDef(node.text) && isFalse(node.isComment)
}

function normalizeArrayChildren (children, nestedIndex) {
  var res = [];
  var i, c, lastIndex, last;
  for (i = 0; i < children.length; i++) {
    c = children[i];
    if (isUndef(c) || typeof c === 'boolean') { continue }
    lastIndex = res.length - 1;
    last = res[lastIndex];
    //  nested
    if (Array.isArray(c)) {
      if (c.length > 0) {
        c = normalizeArrayChildren(c, ((nestedIndex || '') + "_" + i));
        // merge adjacent text nodes
        if (isTextNode(c[0]) && isTextNode(last)) {
          res[lastIndex] = createTextVNode(last.text + (c[0]).text);
          c.shift();
        }
        res.push.apply(res, c);
      }
    } else if (isPrimitive(c)) {
      if (isTextNode(last)) {
        // merge adjacent text nodes
        // this is necessary for SSR hydration because text nodes are
        // essentially merged when rendered to HTML strings
        res[lastIndex] = createTextVNode(last.text + c);
      } else if (c !== '') {
        // convert primitive to vnode
        res.push(createTextVNode(c));
      }
    } else {
      if (isTextNode(c) && isTextNode(last)) {
        // merge adjacent text nodes
        res[lastIndex] = createTextVNode(last.text + c.text);
      } else {
        // default key for nested array children (likely generated by v-for)
        if (isTrue(children._isVList) &&
          isDef(c.tag) &&
          isUndef(c.key) &&
          isDef(nestedIndex)) {
          c.key = "__vlist" + nestedIndex + "_" + i + "__";
        }
        res.push(c);
      }
    }
  }
  return res
}

/*  */

function initProvide (vm) {
  var provide = vm.$options.provide;
  if (provide) {
    vm._provided = typeof provide === 'function'
      ? provide.call(vm)
      : provide;
  }
}

function initInjections (vm) {
  var result = resolveInject(vm.$options.inject, vm);
  if (result) {
    toggleObserving(false);
    Object.keys(result).forEach(function (key) {
      /* istanbul ignore else */
      if (true) {
        defineReactive$$1(vm, key, result[key], function () {
          warn(
            "Avoid mutating an injected value directly since the changes will be " +
            "overwritten whenever the provided component re-renders. " +
            "injection being mutated: \"" + key + "\"",
            vm
          );
        });
      } else {}
    });
    toggleObserving(true);
  }
}

function resolveInject (inject, vm) {
  if (inject) {
    // inject is :any because flow is not smart enough to figure out cached
    var result = Object.create(null);
    var keys = hasSymbol
      ? Reflect.ownKeys(inject)
      : Object.keys(inject);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      // #6574 in case the inject object is observed...
      if (key === '__ob__') { continue }
      var provideKey = inject[key].from;
      var source = vm;
      while (source) {
        if (source._provided && hasOwn(source._provided, provideKey)) {
          result[key] = source._provided[provideKey];
          break
        }
        source = source.$parent;
      }
      if (!source) {
        if ('default' in inject[key]) {
          var provideDefault = inject[key].default;
          result[key] = typeof provideDefault === 'function'
            ? provideDefault.call(vm)
            : provideDefault;
        } else if (true) {
          warn(("Injection \"" + key + "\" not found"), vm);
        }
      }
    }
    return result
  }
}

/*  */



/**
 * Runtime helper for resolving raw children VNodes into a slot object.
 */
function resolveSlots (
  children,
  context
) {
  if (!children || !children.length) {
    return {}
  }
  var slots = {};
  for (var i = 0, l = children.length; i < l; i++) {
    var child = children[i];
    var data = child.data;
    // remove slot attribute if the node is resolved as a Vue slot node
    if (data && data.attrs && data.attrs.slot) {
      delete data.attrs.slot;
    }
    // named slots should only be respected if the vnode was rendered in the
    // same context.
    if ((child.context === context || child.fnContext === context) &&
      data && data.slot != null
    ) {
      var name = data.slot;
      var slot = (slots[name] || (slots[name] = []));
      if (child.tag === 'template') {
        slot.push.apply(slot, child.children || []);
      } else {
        slot.push(child);
      }
    } else {
      // fixed by xxxxxx 临时 hack 掉 uni-app 中的异步 name slot page
      if(child.asyncMeta && child.asyncMeta.data && child.asyncMeta.data.slot === 'page'){
        (slots['page'] || (slots['page'] = [])).push(child);
      }else{
        (slots.default || (slots.default = [])).push(child);
      }
    }
  }
  // ignore slots that contains only whitespace
  for (var name$1 in slots) {
    if (slots[name$1].every(isWhitespace)) {
      delete slots[name$1];
    }
  }
  return slots
}

function isWhitespace (node) {
  return (node.isComment && !node.asyncFactory) || node.text === ' '
}

/*  */

function normalizeScopedSlots (
  slots,
  normalSlots,
  prevSlots
) {
  var res;
  var hasNormalSlots = Object.keys(normalSlots).length > 0;
  var isStable = slots ? !!slots.$stable : !hasNormalSlots;
  var key = slots && slots.$key;
  if (!slots) {
    res = {};
  } else if (slots._normalized) {
    // fast path 1: child component re-render only, parent did not change
    return slots._normalized
  } else if (
    isStable &&
    prevSlots &&
    prevSlots !== emptyObject &&
    key === prevSlots.$key &&
    !hasNormalSlots &&
    !prevSlots.$hasNormal
  ) {
    // fast path 2: stable scoped slots w/ no normal slots to proxy,
    // only need to normalize once
    return prevSlots
  } else {
    res = {};
    for (var key$1 in slots) {
      if (slots[key$1] && key$1[0] !== '$') {
        res[key$1] = normalizeScopedSlot(normalSlots, key$1, slots[key$1]);
      }
    }
  }
  // expose normal slots on scopedSlots
  for (var key$2 in normalSlots) {
    if (!(key$2 in res)) {
      res[key$2] = proxyNormalSlot(normalSlots, key$2);
    }
  }
  // avoriaz seems to mock a non-extensible $scopedSlots object
  // and when that is passed down this would cause an error
  if (slots && Object.isExtensible(slots)) {
    (slots)._normalized = res;
  }
  def(res, '$stable', isStable);
  def(res, '$key', key);
  def(res, '$hasNormal', hasNormalSlots);
  return res
}

function normalizeScopedSlot(normalSlots, key, fn) {
  var normalized = function () {
    var res = arguments.length ? fn.apply(null, arguments) : fn({});
    res = res && typeof res === 'object' && !Array.isArray(res)
      ? [res] // single vnode
      : normalizeChildren(res);
    return res && (
      res.length === 0 ||
      (res.length === 1 && res[0].isComment) // #9658
    ) ? undefined
      : res
  };
  // this is a slot using the new v-slot syntax without scope. although it is
  // compiled as a scoped slot, render fn users would expect it to be present
  // on this.$slots because the usage is semantically a normal slot.
  if (fn.proxy) {
    Object.defineProperty(normalSlots, key, {
      get: normalized,
      enumerable: true,
      configurable: true
    });
  }
  return normalized
}

function proxyNormalSlot(slots, key) {
  return function () { return slots[key]; }
}

/*  */

/**
 * Runtime helper for rendering v-for lists.
 */
function renderList (
  val,
  render
) {
  var ret, i, l, keys, key;
  if (Array.isArray(val) || typeof val === 'string') {
    ret = new Array(val.length);
    for (i = 0, l = val.length; i < l; i++) {
      ret[i] = render(val[i], i, i, i); // fixed by xxxxxx
    }
  } else if (typeof val === 'number') {
    ret = new Array(val);
    for (i = 0; i < val; i++) {
      ret[i] = render(i + 1, i, i, i); // fixed by xxxxxx
    }
  } else if (isObject(val)) {
    if (hasSymbol && val[Symbol.iterator]) {
      ret = [];
      var iterator = val[Symbol.iterator]();
      var result = iterator.next();
      while (!result.done) {
        ret.push(render(result.value, ret.length, i, i++)); // fixed by xxxxxx
        result = iterator.next();
      }
    } else {
      keys = Object.keys(val);
      ret = new Array(keys.length);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[i] = render(val[key], key, i, i); // fixed by xxxxxx
      }
    }
  }
  if (!isDef(ret)) {
    ret = [];
  }
  (ret)._isVList = true;
  return ret
}

/*  */

/**
 * Runtime helper for rendering <slot>
 */
function renderSlot (
  name,
  fallback,
  props,
  bindObject
) {
  var scopedSlotFn = this.$scopedSlots[name];
  var nodes;
  if (scopedSlotFn) { // scoped slot
    props = props || {};
    if (bindObject) {
      if ( true && !isObject(bindObject)) {
        warn(
          'slot v-bind without argument expects an Object',
          this
        );
      }
      props = extend(extend({}, bindObject), props);
    }
    // fixed by xxxxxx app-plus scopedSlot
    nodes = scopedSlotFn(props, this, props._i) || fallback;
  } else {
    nodes = this.$slots[name] || fallback;
  }

  var target = props && props.slot;
  if (target) {
    return this.$createElement('template', { slot: target }, nodes)
  } else {
    return nodes
  }
}

/*  */

/**
 * Runtime helper for resolving filters
 */
function resolveFilter (id) {
  return resolveAsset(this.$options, 'filters', id, true) || identity
}

/*  */

function isKeyNotMatch (expect, actual) {
  if (Array.isArray(expect)) {
    return expect.indexOf(actual) === -1
  } else {
    return expect !== actual
  }
}

/**
 * Runtime helper for checking keyCodes from config.
 * exposed as Vue.prototype._k
 * passing in eventKeyName as last argument separately for backwards compat
 */
function checkKeyCodes (
  eventKeyCode,
  key,
  builtInKeyCode,
  eventKeyName,
  builtInKeyName
) {
  var mappedKeyCode = config.keyCodes[key] || builtInKeyCode;
  if (builtInKeyName && eventKeyName && !config.keyCodes[key]) {
    return isKeyNotMatch(builtInKeyName, eventKeyName)
  } else if (mappedKeyCode) {
    return isKeyNotMatch(mappedKeyCode, eventKeyCode)
  } else if (eventKeyName) {
    return hyphenate(eventKeyName) !== key
  }
}

/*  */

/**
 * Runtime helper for merging v-bind="object" into a VNode's data.
 */
function bindObjectProps (
  data,
  tag,
  value,
  asProp,
  isSync
) {
  if (value) {
    if (!isObject(value)) {
       true && warn(
        'v-bind without argument expects an Object or Array value',
        this
      );
    } else {
      if (Array.isArray(value)) {
        value = toObject(value);
      }
      var hash;
      var loop = function ( key ) {
        if (
          key === 'class' ||
          key === 'style' ||
          isReservedAttribute(key)
        ) {
          hash = data;
        } else {
          var type = data.attrs && data.attrs.type;
          hash = asProp || config.mustUseProp(tag, type, key)
            ? data.domProps || (data.domProps = {})
            : data.attrs || (data.attrs = {});
        }
        var camelizedKey = camelize(key);
        var hyphenatedKey = hyphenate(key);
        if (!(camelizedKey in hash) && !(hyphenatedKey in hash)) {
          hash[key] = value[key];

          if (isSync) {
            var on = data.on || (data.on = {});
            on[("update:" + key)] = function ($event) {
              value[key] = $event;
            };
          }
        }
      };

      for (var key in value) loop( key );
    }
  }
  return data
}

/*  */

/**
 * Runtime helper for rendering static trees.
 */
function renderStatic (
  index,
  isInFor
) {
  var cached = this._staticTrees || (this._staticTrees = []);
  var tree = cached[index];
  // if has already-rendered static tree and not inside v-for,
  // we can reuse the same tree.
  if (tree && !isInFor) {
    return tree
  }
  // otherwise, render a fresh tree.
  tree = cached[index] = this.$options.staticRenderFns[index].call(
    this._renderProxy,
    null,
    this // for render fns generated for functional component templates
  );
  markStatic(tree, ("__static__" + index), false);
  return tree
}

/**
 * Runtime helper for v-once.
 * Effectively it means marking the node as static with a unique key.
 */
function markOnce (
  tree,
  index,
  key
) {
  markStatic(tree, ("__once__" + index + (key ? ("_" + key) : "")), true);
  return tree
}

function markStatic (
  tree,
  key,
  isOnce
) {
  if (Array.isArray(tree)) {
    for (var i = 0; i < tree.length; i++) {
      if (tree[i] && typeof tree[i] !== 'string') {
        markStaticNode(tree[i], (key + "_" + i), isOnce);
      }
    }
  } else {
    markStaticNode(tree, key, isOnce);
  }
}

function markStaticNode (node, key, isOnce) {
  node.isStatic = true;
  node.key = key;
  node.isOnce = isOnce;
}

/*  */

function bindObjectListeners (data, value) {
  if (value) {
    if (!isPlainObject(value)) {
       true && warn(
        'v-on without argument expects an Object value',
        this
      );
    } else {
      var on = data.on = data.on ? extend({}, data.on) : {};
      for (var key in value) {
        var existing = on[key];
        var ours = value[key];
        on[key] = existing ? [].concat(existing, ours) : ours;
      }
    }
  }
  return data
}

/*  */

function resolveScopedSlots (
  fns, // see flow/vnode
  res,
  // the following are added in 2.6
  hasDynamicKeys,
  contentHashKey
) {
  res = res || { $stable: !hasDynamicKeys };
  for (var i = 0; i < fns.length; i++) {
    var slot = fns[i];
    if (Array.isArray(slot)) {
      resolveScopedSlots(slot, res, hasDynamicKeys);
    } else if (slot) {
      // marker for reverse proxying v-slot without scope on this.$slots
      if (slot.proxy) {
        slot.fn.proxy = true;
      }
      res[slot.key] = slot.fn;
    }
  }
  if (contentHashKey) {
    (res).$key = contentHashKey;
  }
  return res
}

/*  */

function bindDynamicKeys (baseObj, values) {
  for (var i = 0; i < values.length; i += 2) {
    var key = values[i];
    if (typeof key === 'string' && key) {
      baseObj[values[i]] = values[i + 1];
    } else if ( true && key !== '' && key !== null) {
      // null is a special value for explicitly removing a binding
      warn(
        ("Invalid value for dynamic directive argument (expected string or null): " + key),
        this
      );
    }
  }
  return baseObj
}

// helper to dynamically append modifier runtime markers to event names.
// ensure only append when value is already string, otherwise it will be cast
// to string and cause the type check to miss.
function prependModifier (value, symbol) {
  return typeof value === 'string' ? symbol + value : value
}

/*  */

function installRenderHelpers (target) {
  target._o = markOnce;
  target._n = toNumber;
  target._s = toString;
  target._l = renderList;
  target._t = renderSlot;
  target._q = looseEqual;
  target._i = looseIndexOf;
  target._m = renderStatic;
  target._f = resolveFilter;
  target._k = checkKeyCodes;
  target._b = bindObjectProps;
  target._v = createTextVNode;
  target._e = createEmptyVNode;
  target._u = resolveScopedSlots;
  target._g = bindObjectListeners;
  target._d = bindDynamicKeys;
  target._p = prependModifier;
}

/*  */

function FunctionalRenderContext (
  data,
  props,
  children,
  parent,
  Ctor
) {
  var this$1 = this;

  var options = Ctor.options;
  // ensure the createElement function in functional components
  // gets a unique context - this is necessary for correct named slot check
  var contextVm;
  if (hasOwn(parent, '_uid')) {
    contextVm = Object.create(parent);
    // $flow-disable-line
    contextVm._original = parent;
  } else {
    // the context vm passed in is a functional context as well.
    // in this case we want to make sure we are able to get a hold to the
    // real context instance.
    contextVm = parent;
    // $flow-disable-line
    parent = parent._original;
  }
  var isCompiled = isTrue(options._compiled);
  var needNormalization = !isCompiled;

  this.data = data;
  this.props = props;
  this.children = children;
  this.parent = parent;
  this.listeners = data.on || emptyObject;
  this.injections = resolveInject(options.inject, parent);
  this.slots = function () {
    if (!this$1.$slots) {
      normalizeScopedSlots(
        data.scopedSlots,
        this$1.$slots = resolveSlots(children, parent)
      );
    }
    return this$1.$slots
  };

  Object.defineProperty(this, 'scopedSlots', ({
    enumerable: true,
    get: function get () {
      return normalizeScopedSlots(data.scopedSlots, this.slots())
    }
  }));

  // support for compiled functional template
  if (isCompiled) {
    // exposing $options for renderStatic()
    this.$options = options;
    // pre-resolve slots for renderSlot()
    this.$slots = this.slots();
    this.$scopedSlots = normalizeScopedSlots(data.scopedSlots, this.$slots);
  }

  if (options._scopeId) {
    this._c = function (a, b, c, d) {
      var vnode = createElement(contextVm, a, b, c, d, needNormalization);
      if (vnode && !Array.isArray(vnode)) {
        vnode.fnScopeId = options._scopeId;
        vnode.fnContext = parent;
      }
      return vnode
    };
  } else {
    this._c = function (a, b, c, d) { return createElement(contextVm, a, b, c, d, needNormalization); };
  }
}

installRenderHelpers(FunctionalRenderContext.prototype);

function createFunctionalComponent (
  Ctor,
  propsData,
  data,
  contextVm,
  children
) {
  var options = Ctor.options;
  var props = {};
  var propOptions = options.props;
  if (isDef(propOptions)) {
    for (var key in propOptions) {
      props[key] = validateProp(key, propOptions, propsData || emptyObject);
    }
  } else {
    if (isDef(data.attrs)) { mergeProps(props, data.attrs); }
    if (isDef(data.props)) { mergeProps(props, data.props); }
  }

  var renderContext = new FunctionalRenderContext(
    data,
    props,
    children,
    contextVm,
    Ctor
  );

  var vnode = options.render.call(null, renderContext._c, renderContext);

  if (vnode instanceof VNode) {
    return cloneAndMarkFunctionalResult(vnode, data, renderContext.parent, options, renderContext)
  } else if (Array.isArray(vnode)) {
    var vnodes = normalizeChildren(vnode) || [];
    var res = new Array(vnodes.length);
    for (var i = 0; i < vnodes.length; i++) {
      res[i] = cloneAndMarkFunctionalResult(vnodes[i], data, renderContext.parent, options, renderContext);
    }
    return res
  }
}

function cloneAndMarkFunctionalResult (vnode, data, contextVm, options, renderContext) {
  // #7817 clone node before setting fnContext, otherwise if the node is reused
  // (e.g. it was from a cached normal slot) the fnContext causes named slots
  // that should not be matched to match.
  var clone = cloneVNode(vnode);
  clone.fnContext = contextVm;
  clone.fnOptions = options;
  if (true) {
    (clone.devtoolsMeta = clone.devtoolsMeta || {}).renderContext = renderContext;
  }
  if (data.slot) {
    (clone.data || (clone.data = {})).slot = data.slot;
  }
  return clone
}

function mergeProps (to, from) {
  for (var key in from) {
    to[camelize(key)] = from[key];
  }
}

/*  */

/*  */

/*  */

/*  */

// inline hooks to be invoked on component VNodes during patch
var componentVNodeHooks = {
  init: function init (vnode, hydrating) {
    if (
      vnode.componentInstance &&
      !vnode.componentInstance._isDestroyed &&
      vnode.data.keepAlive
    ) {
      // kept-alive components, treat as a patch
      var mountedNode = vnode; // work around flow
      componentVNodeHooks.prepatch(mountedNode, mountedNode);
    } else {
      var child = vnode.componentInstance = createComponentInstanceForVnode(
        vnode,
        activeInstance
      );
      child.$mount(hydrating ? vnode.elm : undefined, hydrating);
    }
  },

  prepatch: function prepatch (oldVnode, vnode) {
    var options = vnode.componentOptions;
    var child = vnode.componentInstance = oldVnode.componentInstance;
    updateChildComponent(
      child,
      options.propsData, // updated props
      options.listeners, // updated listeners
      vnode, // new parent vnode
      options.children // new children
    );
  },

  insert: function insert (vnode) {
    var context = vnode.context;
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isMounted) {
      callHook(componentInstance, 'onServiceCreated');
      callHook(componentInstance, 'onServiceAttached');
      componentInstance._isMounted = true;
      callHook(componentInstance, 'mounted');
    }
    if (vnode.data.keepAlive) {
      if (context._isMounted) {
        // vue-router#1212
        // During updates, a kept-alive component's child components may
        // change, so directly walking the tree here may call activated hooks
        // on incorrect children. Instead we push them into a queue which will
        // be processed after the whole patch process ended.
        queueActivatedComponent(componentInstance);
      } else {
        activateChildComponent(componentInstance, true /* direct */);
      }
    }
  },

  destroy: function destroy (vnode) {
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isDestroyed) {
      if (!vnode.data.keepAlive) {
        componentInstance.$destroy();
      } else {
        deactivateChildComponent(componentInstance, true /* direct */);
      }
    }
  }
};

var hooksToMerge = Object.keys(componentVNodeHooks);

function createComponent (
  Ctor,
  data,
  context,
  children,
  tag
) {
  if (isUndef(Ctor)) {
    return
  }

  var baseCtor = context.$options._base;

  // plain options object: turn it into a constructor
  if (isObject(Ctor)) {
    Ctor = baseCtor.extend(Ctor);
  }

  // if at this stage it's not a constructor or an async component factory,
  // reject.
  if (typeof Ctor !== 'function') {
    if (true) {
      warn(("Invalid Component definition: " + (String(Ctor))), context);
    }
    return
  }

  // async component
  var asyncFactory;
  if (isUndef(Ctor.cid)) {
    asyncFactory = Ctor;
    Ctor = resolveAsyncComponent(asyncFactory, baseCtor);
    if (Ctor === undefined) {
      // return a placeholder node for async component, which is rendered
      // as a comment node but preserves all the raw information for the node.
      // the information will be used for async server-rendering and hydration.
      return createAsyncPlaceholder(
        asyncFactory,
        data,
        context,
        children,
        tag
      )
    }
  }

  data = data || {};

  // resolve constructor options in case global mixins are applied after
  // component constructor creation
  resolveConstructorOptions(Ctor);

  // transform component v-model data into props & events
  if (isDef(data.model)) {
    transformModel(Ctor.options, data);
  }

  // extract props
  var propsData = extractPropsFromVNodeData(data, Ctor, tag, context); // fixed by xxxxxx

  // functional component
  if (isTrue(Ctor.options.functional)) {
    return createFunctionalComponent(Ctor, propsData, data, context, children)
  }

  // extract listeners, since these needs to be treated as
  // child component listeners instead of DOM listeners
  var listeners = data.on;
  // replace with listeners with .native modifier
  // so it gets processed during parent component patch.
  data.on = data.nativeOn;

  if (isTrue(Ctor.options.abstract)) {
    // abstract components do not keep anything
    // other than props & listeners & slot

    // work around flow
    var slot = data.slot;
    data = {};
    if (slot) {
      data.slot = slot;
    }
  }

  // install component management hooks onto the placeholder node
  installComponentHooks(data);

  // return a placeholder vnode
  var name = Ctor.options.name || tag;
  var vnode = new VNode(
    ("vue-component-" + (Ctor.cid) + (name ? ("-" + name) : '')),
    data, undefined, undefined, undefined, context,
    { Ctor: Ctor, propsData: propsData, listeners: listeners, tag: tag, children: children },
    asyncFactory
  );

  return vnode
}

function createComponentInstanceForVnode (
  vnode, // we know it's MountedComponentVNode but flow doesn't
  parent // activeInstance in lifecycle state
) {
  var options = {
    _isComponent: true,
    _parentVnode: vnode,
    parent: parent
  };
  // check inline-template render functions
  var inlineTemplate = vnode.data.inlineTemplate;
  if (isDef(inlineTemplate)) {
    options.render = inlineTemplate.render;
    options.staticRenderFns = inlineTemplate.staticRenderFns;
  }
  return new vnode.componentOptions.Ctor(options)
}

function installComponentHooks (data) {
  var hooks = data.hook || (data.hook = {});
  for (var i = 0; i < hooksToMerge.length; i++) {
    var key = hooksToMerge[i];
    var existing = hooks[key];
    var toMerge = componentVNodeHooks[key];
    if (existing !== toMerge && !(existing && existing._merged)) {
      hooks[key] = existing ? mergeHook$1(toMerge, existing) : toMerge;
    }
  }
}

function mergeHook$1 (f1, f2) {
  var merged = function (a, b) {
    // flow complains about extra args which is why we use any
    f1(a, b);
    f2(a, b);
  };
  merged._merged = true;
  return merged
}

// transform component v-model info (value and callback) into
// prop and event handler respectively.
function transformModel (options, data) {
  var prop = (options.model && options.model.prop) || 'value';
  var event = (options.model && options.model.event) || 'input'
  ;(data.attrs || (data.attrs = {}))[prop] = data.model.value;
  var on = data.on || (data.on = {});
  var existing = on[event];
  var callback = data.model.callback;
  if (isDef(existing)) {
    if (
      Array.isArray(existing)
        ? existing.indexOf(callback) === -1
        : existing !== callback
    ) {
      on[event] = [callback].concat(existing);
    }
  } else {
    on[event] = callback;
  }
}

/*  */

var SIMPLE_NORMALIZE = 1;
var ALWAYS_NORMALIZE = 2;

// wrapper function for providing a more flexible interface
// without getting yelled at by flow
function createElement (
  context,
  tag,
  data,
  children,
  normalizationType,
  alwaysNormalize
) {
  if (Array.isArray(data) || isPrimitive(data)) {
    normalizationType = children;
    children = data;
    data = undefined;
  }
  if (isTrue(alwaysNormalize)) {
    normalizationType = ALWAYS_NORMALIZE;
  }
  return _createElement(context, tag, data, children, normalizationType)
}

function _createElement (
  context,
  tag,
  data,
  children,
  normalizationType
) {
  if (isDef(data) && isDef((data).__ob__)) {
     true && warn(
      "Avoid using observed data object as vnode data: " + (JSON.stringify(data)) + "\n" +
      'Always create fresh vnode data objects in each render!',
      context
    );
    return createEmptyVNode()
  }
  // object syntax in v-bind
  if (isDef(data) && isDef(data.is)) {
    tag = data.is;
  }
  if (!tag) {
    // in case of component :is set to falsy value
    return createEmptyVNode()
  }
  // warn against non-primitive key
  if ( true &&
    isDef(data) && isDef(data.key) && !isPrimitive(data.key)
  ) {
    {
      warn(
        'Avoid using non-primitive value as key, ' +
        'use string/number value instead.',
        context
      );
    }
  }
  // support single function children as default scoped slot
  if (Array.isArray(children) &&
    typeof children[0] === 'function'
  ) {
    data = data || {};
    data.scopedSlots = { default: children[0] };
    children.length = 0;
  }
  if (normalizationType === ALWAYS_NORMALIZE) {
    children = normalizeChildren(children);
  } else if (normalizationType === SIMPLE_NORMALIZE) {
    children = simpleNormalizeChildren(children);
  }
  var vnode, ns;
  if (typeof tag === 'string') {
    var Ctor;
    ns = (context.$vnode && context.$vnode.ns) || config.getTagNamespace(tag);
    if (config.isReservedTag(tag)) {
      // platform built-in elements
      if ( true && isDef(data) && isDef(data.nativeOn)) {
        warn(
          ("The .native modifier for v-on is only valid on components but it was used on <" + tag + ">."),
          context
        );
      }
      vnode = new VNode(
        config.parsePlatformTagName(tag), data, children,
        undefined, undefined, context
      );
    } else if ((!data || !data.pre) && isDef(Ctor = resolveAsset(context.$options, 'components', tag))) {
      // component
      vnode = createComponent(Ctor, data, context, children, tag);
    } else {
      // unknown or unlisted namespaced elements
      // check at runtime because it may get assigned a namespace when its
      // parent normalizes children
      vnode = new VNode(
        tag, data, children,
        undefined, undefined, context
      );
    }
  } else {
    // direct component options / constructor
    vnode = createComponent(tag, data, context, children);
  }
  if (Array.isArray(vnode)) {
    return vnode
  } else if (isDef(vnode)) {
    if (isDef(ns)) { applyNS(vnode, ns); }
    if (isDef(data)) { registerDeepBindings(data); }
    return vnode
  } else {
    return createEmptyVNode()
  }
}

function applyNS (vnode, ns, force) {
  vnode.ns = ns;
  if (vnode.tag === 'foreignObject') {
    // use default namespace inside foreignObject
    ns = undefined;
    force = true;
  }
  if (isDef(vnode.children)) {
    for (var i = 0, l = vnode.children.length; i < l; i++) {
      var child = vnode.children[i];
      if (isDef(child.tag) && (
        isUndef(child.ns) || (isTrue(force) && child.tag !== 'svg'))) {
        applyNS(child, ns, force);
      }
    }
  }
}

// ref #5318
// necessary to ensure parent re-render when deep bindings like :style and
// :class are used on slot nodes
function registerDeepBindings (data) {
  if (isObject(data.style)) {
    traverse(data.style);
  }
  if (isObject(data.class)) {
    traverse(data.class);
  }
}

/*  */

function initRender (vm) {
  vm._vnode = null; // the root of the child tree
  vm._staticTrees = null; // v-once cached trees
  var options = vm.$options;
  var parentVnode = vm.$vnode = options._parentVnode; // the placeholder node in parent tree
  var renderContext = parentVnode && parentVnode.context;
  vm.$slots = resolveSlots(options._renderChildren, renderContext);
  vm.$scopedSlots = emptyObject;
  // bind the createElement fn to this instance
  // so that we get proper render context inside it.
  // args order: tag, data, children, normalizationType, alwaysNormalize
  // internal version is used by render functions compiled from templates
  vm._c = function (a, b, c, d) { return createElement(vm, a, b, c, d, false); };
  // normalization is always applied for the public version, used in
  // user-written render functions.
  vm.$createElement = function (a, b, c, d) { return createElement(vm, a, b, c, d, true); };

  // $attrs & $listeners are exposed for easier HOC creation.
  // they need to be reactive so that HOCs using them are always updated
  var parentData = parentVnode && parentVnode.data;

  /* istanbul ignore else */
  if (true) {
    defineReactive$$1(vm, '$attrs', parentData && parentData.attrs || emptyObject, function () {
      !isUpdatingChildComponent && warn("$attrs is readonly.", vm);
    }, true);
    defineReactive$$1(vm, '$listeners', options._parentListeners || emptyObject, function () {
      !isUpdatingChildComponent && warn("$listeners is readonly.", vm);
    }, true);
  } else {}
}

var currentRenderingInstance = null;

function renderMixin (Vue) {
  // install runtime convenience helpers
  installRenderHelpers(Vue.prototype);

  Vue.prototype.$nextTick = function (fn) {
    return nextTick(fn, this)
  };

  Vue.prototype._render = function () {
    var vm = this;
    var ref = vm.$options;
    var render = ref.render;
    var _parentVnode = ref._parentVnode;

    if (_parentVnode) {
      vm.$scopedSlots = normalizeScopedSlots(
        _parentVnode.data.scopedSlots,
        vm.$slots,
        vm.$scopedSlots
      );
    }

    // set parent vnode. this allows render functions to have access
    // to the data on the placeholder node.
    vm.$vnode = _parentVnode;
    // render self
    var vnode;
    try {
      // There's no need to maintain a stack because all render fns are called
      // separately from one another. Nested component's render fns are called
      // when parent component is patched.
      currentRenderingInstance = vm;
      vnode = render.call(vm._renderProxy, vm.$createElement);
    } catch (e) {
      handleError(e, vm, "render");
      // return error render result,
      // or previous vnode to prevent render error causing blank component
      /* istanbul ignore else */
      if ( true && vm.$options.renderError) {
        try {
          vnode = vm.$options.renderError.call(vm._renderProxy, vm.$createElement, e);
        } catch (e) {
          handleError(e, vm, "renderError");
          vnode = vm._vnode;
        }
      } else {
        vnode = vm._vnode;
      }
    } finally {
      currentRenderingInstance = null;
    }
    // if the returned array contains only a single node, allow it
    if (Array.isArray(vnode) && vnode.length === 1) {
      vnode = vnode[0];
    }
    // return empty vnode in case the render function errored out
    if (!(vnode instanceof VNode)) {
      if ( true && Array.isArray(vnode)) {
        warn(
          'Multiple root nodes returned from render function. Render function ' +
          'should return a single root node.',
          vm
        );
      }
      vnode = createEmptyVNode();
    }
    // set parent
    vnode.parent = _parentVnode;
    return vnode
  };
}

/*  */

function ensureCtor (comp, base) {
  if (
    comp.__esModule ||
    (hasSymbol && comp[Symbol.toStringTag] === 'Module')
  ) {
    comp = comp.default;
  }
  return isObject(comp)
    ? base.extend(comp)
    : comp
}

function createAsyncPlaceholder (
  factory,
  data,
  context,
  children,
  tag
) {
  var node = createEmptyVNode();
  node.asyncFactory = factory;
  node.asyncMeta = { data: data, context: context, children: children, tag: tag };
  return node
}

function resolveAsyncComponent (
  factory,
  baseCtor
) {
  if (isTrue(factory.error) && isDef(factory.errorComp)) {
    return factory.errorComp
  }

  if (isDef(factory.resolved)) {
    return factory.resolved
  }

  var owner = currentRenderingInstance;
  if (owner && isDef(factory.owners) && factory.owners.indexOf(owner) === -1) {
    // already pending
    factory.owners.push(owner);
  }

  if (isTrue(factory.loading) && isDef(factory.loadingComp)) {
    return factory.loadingComp
  }

  if (owner && !isDef(factory.owners)) {
    var owners = factory.owners = [owner];
    var sync = true;
    var timerLoading = null;
    var timerTimeout = null

    ;(owner).$on('hook:destroyed', function () { return remove(owners, owner); });

    var forceRender = function (renderCompleted) {
      for (var i = 0, l = owners.length; i < l; i++) {
        (owners[i]).$forceUpdate();
      }

      if (renderCompleted) {
        owners.length = 0;
        if (timerLoading !== null) {
          clearTimeout(timerLoading);
          timerLoading = null;
        }
        if (timerTimeout !== null) {
          clearTimeout(timerTimeout);
          timerTimeout = null;
        }
      }
    };

    var resolve = once(function (res) {
      // cache resolved
      factory.resolved = ensureCtor(res, baseCtor);
      // invoke callbacks only if this is not a synchronous resolve
      // (async resolves are shimmed as synchronous during SSR)
      if (!sync) {
        forceRender(true);
      } else {
        owners.length = 0;
      }
    });

    var reject = once(function (reason) {
       true && warn(
        "Failed to resolve async component: " + (String(factory)) +
        (reason ? ("\nReason: " + reason) : '')
      );
      if (isDef(factory.errorComp)) {
        factory.error = true;
        forceRender(true);
      }
    });

    var res = factory(resolve, reject);

    if (isObject(res)) {
      if (isPromise(res)) {
        // () => Promise
        if (isUndef(factory.resolved)) {
          res.then(resolve, reject);
        }
      } else if (isPromise(res.component)) {
        res.component.then(resolve, reject);

        if (isDef(res.error)) {
          factory.errorComp = ensureCtor(res.error, baseCtor);
        }

        if (isDef(res.loading)) {
          factory.loadingComp = ensureCtor(res.loading, baseCtor);
          if (res.delay === 0) {
            factory.loading = true;
          } else {
            timerLoading = setTimeout(function () {
              timerLoading = null;
              if (isUndef(factory.resolved) && isUndef(factory.error)) {
                factory.loading = true;
                forceRender(false);
              }
            }, res.delay || 200);
          }
        }

        if (isDef(res.timeout)) {
          timerTimeout = setTimeout(function () {
            timerTimeout = null;
            if (isUndef(factory.resolved)) {
              reject(
                 true
                  ? ("timeout (" + (res.timeout) + "ms)")
                  : undefined
              );
            }
          }, res.timeout);
        }
      }
    }

    sync = false;
    // return in case resolved synchronously
    return factory.loading
      ? factory.loadingComp
      : factory.resolved
  }
}

/*  */

function isAsyncPlaceholder (node) {
  return node.isComment && node.asyncFactory
}

/*  */

function getFirstComponentChild (children) {
  if (Array.isArray(children)) {
    for (var i = 0; i < children.length; i++) {
      var c = children[i];
      if (isDef(c) && (isDef(c.componentOptions) || isAsyncPlaceholder(c))) {
        return c
      }
    }
  }
}

/*  */

/*  */

function initEvents (vm) {
  vm._events = Object.create(null);
  vm._hasHookEvent = false;
  // init parent attached events
  var listeners = vm.$options._parentListeners;
  if (listeners) {
    updateComponentListeners(vm, listeners);
  }
}

var target;

function add (event, fn) {
  target.$on(event, fn);
}

function remove$1 (event, fn) {
  target.$off(event, fn);
}

function createOnceHandler (event, fn) {
  var _target = target;
  return function onceHandler () {
    var res = fn.apply(null, arguments);
    if (res !== null) {
      _target.$off(event, onceHandler);
    }
  }
}

function updateComponentListeners (
  vm,
  listeners,
  oldListeners
) {
  target = vm;
  updateListeners(listeners, oldListeners || {}, add, remove$1, createOnceHandler, vm);
  target = undefined;
}

function eventsMixin (Vue) {
  var hookRE = /^hook:/;
  Vue.prototype.$on = function (event, fn) {
    var vm = this;
    if (Array.isArray(event)) {
      for (var i = 0, l = event.length; i < l; i++) {
        vm.$on(event[i], fn);
      }
    } else {
      (vm._events[event] || (vm._events[event] = [])).push(fn);
      // optimize hook:event cost by using a boolean flag marked at registration
      // instead of a hash lookup
      if (hookRE.test(event)) {
        vm._hasHookEvent = true;
      }
    }
    return vm
  };

  Vue.prototype.$once = function (event, fn) {
    var vm = this;
    function on () {
      vm.$off(event, on);
      fn.apply(vm, arguments);
    }
    on.fn = fn;
    vm.$on(event, on);
    return vm
  };

  Vue.prototype.$off = function (event, fn) {
    var vm = this;
    // all
    if (!arguments.length) {
      vm._events = Object.create(null);
      return vm
    }
    // array of events
    if (Array.isArray(event)) {
      for (var i$1 = 0, l = event.length; i$1 < l; i$1++) {
        vm.$off(event[i$1], fn);
      }
      return vm
    }
    // specific event
    var cbs = vm._events[event];
    if (!cbs) {
      return vm
    }
    if (!fn) {
      vm._events[event] = null;
      return vm
    }
    // specific handler
    var cb;
    var i = cbs.length;
    while (i--) {
      cb = cbs[i];
      if (cb === fn || cb.fn === fn) {
        cbs.splice(i, 1);
        break
      }
    }
    return vm
  };

  Vue.prototype.$emit = function (event) {
    var vm = this;
    if (true) {
      var lowerCaseEvent = event.toLowerCase();
      if (lowerCaseEvent !== event && vm._events[lowerCaseEvent]) {
        tip(
          "Event \"" + lowerCaseEvent + "\" is emitted in component " +
          (formatComponentName(vm)) + " but the handler is registered for \"" + event + "\". " +
          "Note that HTML attributes are case-insensitive and you cannot use " +
          "v-on to listen to camelCase events when using in-DOM templates. " +
          "You should probably use \"" + (hyphenate(event)) + "\" instead of \"" + event + "\"."
        );
      }
    }
    var cbs = vm._events[event];
    if (cbs) {
      cbs = cbs.length > 1 ? toArray(cbs) : cbs;
      var args = toArray(arguments, 1);
      var info = "event handler for \"" + event + "\"";
      for (var i = 0, l = cbs.length; i < l; i++) {
        invokeWithErrorHandling(cbs[i], vm, args, vm, info);
      }
    }
    return vm
  };
}

/*  */

var activeInstance = null;
var isUpdatingChildComponent = false;

function setActiveInstance(vm) {
  var prevActiveInstance = activeInstance;
  activeInstance = vm;
  return function () {
    activeInstance = prevActiveInstance;
  }
}

function initLifecycle (vm) {
  var options = vm.$options;

  // locate first non-abstract parent
  var parent = options.parent;
  if (parent && !options.abstract) {
    while (parent.$options.abstract && parent.$parent) {
      parent = parent.$parent;
    }
    parent.$children.push(vm);
  }

  vm.$parent = parent;
  vm.$root = parent ? parent.$root : vm;

  vm.$children = [];
  vm.$refs = {};

  vm._watcher = null;
  vm._inactive = null;
  vm._directInactive = false;
  vm._isMounted = false;
  vm._isDestroyed = false;
  vm._isBeingDestroyed = false;
}

function lifecycleMixin (Vue) {
  Vue.prototype._update = function (vnode, hydrating) {
    var vm = this;
    var prevEl = vm.$el;
    var prevVnode = vm._vnode;
    var restoreActiveInstance = setActiveInstance(vm);
    vm._vnode = vnode;
    // Vue.prototype.__patch__ is injected in entry points
    // based on the rendering backend used.
    if (!prevVnode) {
      // initial render
      vm.$el = vm.__patch__(vm.$el, vnode, hydrating, false /* removeOnly */);
    } else {
      // updates
      vm.$el = vm.__patch__(prevVnode, vnode);
    }
    restoreActiveInstance();
    // update __vue__ reference
    if (prevEl) {
      prevEl.__vue__ = null;
    }
    if (vm.$el) {
      vm.$el.__vue__ = vm;
    }
    // if parent is an HOC, update its $el as well
    if (vm.$vnode && vm.$parent && vm.$vnode === vm.$parent._vnode) {
      vm.$parent.$el = vm.$el;
    }
    // updated hook is called by the scheduler to ensure that children are
    // updated in a parent's updated hook.
  };

  Vue.prototype.$forceUpdate = function () {
    var vm = this;
    if (vm._watcher) {
      vm._watcher.update();
    }
  };

  Vue.prototype.$destroy = function () {
    var vm = this;
    if (vm._isBeingDestroyed) {
      return
    }
    callHook(vm, 'beforeDestroy');
    vm._isBeingDestroyed = true;
    // remove self from parent
    var parent = vm.$parent;
    if (parent && !parent._isBeingDestroyed && !vm.$options.abstract) {
      remove(parent.$children, vm);
    }
    // teardown watchers
    if (vm._watcher) {
      vm._watcher.teardown();
    }
    var i = vm._watchers.length;
    while (i--) {
      vm._watchers[i].teardown();
    }
    // remove reference from data ob
    // frozen object may not have observer.
    if (vm._data.__ob__) {
      vm._data.__ob__.vmCount--;
    }
    // call the last hook...
    vm._isDestroyed = true;
    // invoke destroy hooks on current rendered tree
    vm.__patch__(vm._vnode, null);
    // fire destroyed hook
    callHook(vm, 'destroyed');
    // turn off all instance listeners.
    vm.$off();
    // remove __vue__ reference
    if (vm.$el) {
      vm.$el.__vue__ = null;
    }
    // release circular reference (#6759)
    if (vm.$vnode) {
      vm.$vnode.parent = null;
    }
  };
}

function updateChildComponent (
  vm,
  propsData,
  listeners,
  parentVnode,
  renderChildren
) {
  if (true) {
    isUpdatingChildComponent = true;
  }

  // determine whether component has slot children
  // we need to do this before overwriting $options._renderChildren.

  // check if there are dynamic scopedSlots (hand-written or compiled but with
  // dynamic slot names). Static scoped slots compiled from template has the
  // "$stable" marker.
  var newScopedSlots = parentVnode.data.scopedSlots;
  var oldScopedSlots = vm.$scopedSlots;
  var hasDynamicScopedSlot = !!(
    (newScopedSlots && !newScopedSlots.$stable) ||
    (oldScopedSlots !== emptyObject && !oldScopedSlots.$stable) ||
    (newScopedSlots && vm.$scopedSlots.$key !== newScopedSlots.$key)
  );

  // Any static slot children from the parent may have changed during parent's
  // update. Dynamic scoped slots may also have changed. In such cases, a forced
  // update is necessary to ensure correctness.
  var needsForceUpdate = !!(
    renderChildren ||               // has new static slots
    vm.$options._renderChildren ||  // has old static slots
    hasDynamicScopedSlot
  );

  vm.$options._parentVnode = parentVnode;
  vm.$vnode = parentVnode; // update vm's placeholder node without re-render

  if (vm._vnode) { // update child tree's parent
    vm._vnode.parent = parentVnode;
  }
  vm.$options._renderChildren = renderChildren;

  // update $attrs and $listeners hash
  // these are also reactive so they may trigger child update if the child
  // used them during render
  vm.$attrs = parentVnode.data.attrs || emptyObject;
  vm.$listeners = listeners || emptyObject;

  // update props
  if (propsData && vm.$options.props) {
    toggleObserving(false);
    var props = vm._props;
    var propKeys = vm.$options._propKeys || [];
    for (var i = 0; i < propKeys.length; i++) {
      var key = propKeys[i];
      var propOptions = vm.$options.props; // wtf flow?
      props[key] = validateProp(key, propOptions, propsData, vm);
    }
    toggleObserving(true);
    // keep a copy of raw propsData
    vm.$options.propsData = propsData;
  }
  
  // fixed by xxxxxx update properties(mp runtime)
  vm._$updateProperties && vm._$updateProperties(vm);
  
  // update listeners
  listeners = listeners || emptyObject;
  var oldListeners = vm.$options._parentListeners;
  vm.$options._parentListeners = listeners;
  updateComponentListeners(vm, listeners, oldListeners);

  // resolve slots + force update if has children
  if (needsForceUpdate) {
    vm.$slots = resolveSlots(renderChildren, parentVnode.context);
    vm.$forceUpdate();
  }

  if (true) {
    isUpdatingChildComponent = false;
  }
}

function isInInactiveTree (vm) {
  while (vm && (vm = vm.$parent)) {
    if (vm._inactive) { return true }
  }
  return false
}

function activateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = false;
    if (isInInactiveTree(vm)) {
      return
    }
  } else if (vm._directInactive) {
    return
  }
  if (vm._inactive || vm._inactive === null) {
    vm._inactive = false;
    for (var i = 0; i < vm.$children.length; i++) {
      activateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'activated');
  }
}

function deactivateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = true;
    if (isInInactiveTree(vm)) {
      return
    }
  }
  if (!vm._inactive) {
    vm._inactive = true;
    for (var i = 0; i < vm.$children.length; i++) {
      deactivateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'deactivated');
  }
}

function callHook (vm, hook) {
  // #7573 disable dep collection when invoking lifecycle hooks
  pushTarget();
  var handlers = vm.$options[hook];
  var info = hook + " hook";
  if (handlers) {
    for (var i = 0, j = handlers.length; i < j; i++) {
      invokeWithErrorHandling(handlers[i], vm, null, vm, info);
    }
  }
  if (vm._hasHookEvent) {
    vm.$emit('hook:' + hook);
  }
  popTarget();
}

/*  */

var MAX_UPDATE_COUNT = 100;

var queue = [];
var activatedChildren = [];
var has = {};
var circular = {};
var waiting = false;
var flushing = false;
var index = 0;

/**
 * Reset the scheduler's state.
 */
function resetSchedulerState () {
  index = queue.length = activatedChildren.length = 0;
  has = {};
  if (true) {
    circular = {};
  }
  waiting = flushing = false;
}

// Async edge case #6566 requires saving the timestamp when event listeners are
// attached. However, calling performance.now() has a perf overhead especially
// if the page has thousands of event listeners. Instead, we take a timestamp
// every time the scheduler flushes and use that for all event listeners
// attached during that flush.
var currentFlushTimestamp = 0;

// Async edge case fix requires storing an event listener's attach timestamp.
var getNow = Date.now;

// Determine what event timestamp the browser is using. Annoyingly, the
// timestamp can either be hi-res (relative to page load) or low-res
// (relative to UNIX epoch), so in order to compare time we have to use the
// same timestamp type when saving the flush timestamp.
// All IE versions use low-res event timestamps, and have problematic clock
// implementations (#9632)
if (inBrowser && !isIE) {
  var performance = window.performance;
  if (
    performance &&
    typeof performance.now === 'function' &&
    getNow() > document.createEvent('Event').timeStamp
  ) {
    // if the event timestamp, although evaluated AFTER the Date.now(), is
    // smaller than it, it means the event is using a hi-res timestamp,
    // and we need to use the hi-res version for event listener timestamps as
    // well.
    getNow = function () { return performance.now(); };
  }
}

/**
 * Flush both queues and run the watchers.
 */
function flushSchedulerQueue () {
  currentFlushTimestamp = getNow();
  flushing = true;
  var watcher, id;

  // Sort queue before flush.
  // This ensures that:
  // 1. Components are updated from parent to child. (because parent is always
  //    created before the child)
  // 2. A component's user watchers are run before its render watcher (because
  //    user watchers are created before the render watcher)
  // 3. If a component is destroyed during a parent component's watcher run,
  //    its watchers can be skipped.
  queue.sort(function (a, b) { return a.id - b.id; });

  // do not cache length because more watchers might be pushed
  // as we run existing watchers
  for (index = 0; index < queue.length; index++) {
    watcher = queue[index];
    if (watcher.before) {
      watcher.before();
    }
    id = watcher.id;
    has[id] = null;
    watcher.run();
    // in dev build, check and stop circular updates.
    if ( true && has[id] != null) {
      circular[id] = (circular[id] || 0) + 1;
      if (circular[id] > MAX_UPDATE_COUNT) {
        warn(
          'You may have an infinite update loop ' + (
            watcher.user
              ? ("in watcher with expression \"" + (watcher.expression) + "\"")
              : "in a component render function."
          ),
          watcher.vm
        );
        break
      }
    }
  }

  // keep copies of post queues before resetting state
  var activatedQueue = activatedChildren.slice();
  var updatedQueue = queue.slice();

  resetSchedulerState();

  // call component updated and activated hooks
  callActivatedHooks(activatedQueue);
  callUpdatedHooks(updatedQueue);

  // devtool hook
  /* istanbul ignore if */
  if (devtools && config.devtools) {
    devtools.emit('flush');
  }
}

function callUpdatedHooks (queue) {
  var i = queue.length;
  while (i--) {
    var watcher = queue[i];
    var vm = watcher.vm;
    if (vm._watcher === watcher && vm._isMounted && !vm._isDestroyed) {
      callHook(vm, 'updated');
    }
  }
}

/**
 * Queue a kept-alive component that was activated during patch.
 * The queue will be processed after the entire tree has been patched.
 */
function queueActivatedComponent (vm) {
  // setting _inactive to false here so that a render function can
  // rely on checking whether it's in an inactive tree (e.g. router-view)
  vm._inactive = false;
  activatedChildren.push(vm);
}

function callActivatedHooks (queue) {
  for (var i = 0; i < queue.length; i++) {
    queue[i]._inactive = true;
    activateChildComponent(queue[i], true /* true */);
  }
}

/**
 * Push a watcher into the watcher queue.
 * Jobs with duplicate IDs will be skipped unless it's
 * pushed when the queue is being flushed.
 */
function queueWatcher (watcher) {
  var id = watcher.id;
  if (has[id] == null) {
    has[id] = true;
    if (!flushing) {
      queue.push(watcher);
    } else {
      // if already flushing, splice the watcher based on its id
      // if already past its id, it will be run next immediately.
      var i = queue.length - 1;
      while (i > index && queue[i].id > watcher.id) {
        i--;
      }
      queue.splice(i + 1, 0, watcher);
    }
    // queue the flush
    if (!waiting) {
      waiting = true;

      if ( true && !config.async) {
        flushSchedulerQueue();
        return
      }
      nextTick(flushSchedulerQueue);
    }
  }
}

/*  */



var uid$2 = 0;

/**
 * A watcher parses an expression, collects dependencies,
 * and fires callback when the expression value changes.
 * This is used for both the $watch() api and directives.
 */
var Watcher = function Watcher (
  vm,
  expOrFn,
  cb,
  options,
  isRenderWatcher
) {
  this.vm = vm;
  if (isRenderWatcher) {
    vm._watcher = this;
  }
  vm._watchers.push(this);
  // options
  if (options) {
    this.deep = !!options.deep;
    this.user = !!options.user;
    this.lazy = !!options.lazy;
    this.sync = !!options.sync;
    this.before = options.before;
  } else {
    this.deep = this.user = this.lazy = this.sync = false;
  }
  this.cb = cb;
  this.id = ++uid$2; // uid for batching
  this.active = true;
  this.dirty = this.lazy; // for lazy watchers
  this.deps = [];
  this.newDeps = [];
  this.depIds = new _Set();
  this.newDepIds = new _Set();
  this.expression =  true
    ? expOrFn.toString()
    : undefined;
  // parse expression for getter
  if (typeof expOrFn === 'function') {
    this.getter = expOrFn;
  } else {
    this.getter = parsePath(expOrFn);
    if (!this.getter) {
      this.getter = noop;
       true && warn(
        "Failed watching path: \"" + expOrFn + "\" " +
        'Watcher only accepts simple dot-delimited paths. ' +
        'For full control, use a function instead.',
        vm
      );
    }
  }
  this.value = this.lazy
    ? undefined
    : this.get();
};

/**
 * Evaluate the getter, and re-collect dependencies.
 */
Watcher.prototype.get = function get () {
  pushTarget(this);
  var value;
  var vm = this.vm;
  try {
    value = this.getter.call(vm, vm);
  } catch (e) {
    if (this.user) {
      handleError(e, vm, ("getter for watcher \"" + (this.expression) + "\""));
    } else {
      throw e
    }
  } finally {
    // "touch" every property so they are all tracked as
    // dependencies for deep watching
    if (this.deep) {
      traverse(value);
    }
    popTarget();
    this.cleanupDeps();
  }
  return value
};

/**
 * Add a dependency to this directive.
 */
Watcher.prototype.addDep = function addDep (dep) {
  var id = dep.id;
  if (!this.newDepIds.has(id)) {
    this.newDepIds.add(id);
    this.newDeps.push(dep);
    if (!this.depIds.has(id)) {
      dep.addSub(this);
    }
  }
};

/**
 * Clean up for dependency collection.
 */
Watcher.prototype.cleanupDeps = function cleanupDeps () {
  var i = this.deps.length;
  while (i--) {
    var dep = this.deps[i];
    if (!this.newDepIds.has(dep.id)) {
      dep.removeSub(this);
    }
  }
  var tmp = this.depIds;
  this.depIds = this.newDepIds;
  this.newDepIds = tmp;
  this.newDepIds.clear();
  tmp = this.deps;
  this.deps = this.newDeps;
  this.newDeps = tmp;
  this.newDeps.length = 0;
};

/**
 * Subscriber interface.
 * Will be called when a dependency changes.
 */
Watcher.prototype.update = function update () {
  /* istanbul ignore else */
  if (this.lazy) {
    this.dirty = true;
  } else if (this.sync) {
    this.run();
  } else {
    queueWatcher(this);
  }
};

/**
 * Scheduler job interface.
 * Will be called by the scheduler.
 */
Watcher.prototype.run = function run () {
  if (this.active) {
    var value = this.get();
    if (
      value !== this.value ||
      // Deep watchers and watchers on Object/Arrays should fire even
      // when the value is the same, because the value may
      // have mutated.
      isObject(value) ||
      this.deep
    ) {
      // set new value
      var oldValue = this.value;
      this.value = value;
      if (this.user) {
        try {
          this.cb.call(this.vm, value, oldValue);
        } catch (e) {
          handleError(e, this.vm, ("callback for watcher \"" + (this.expression) + "\""));
        }
      } else {
        this.cb.call(this.vm, value, oldValue);
      }
    }
  }
};

/**
 * Evaluate the value of the watcher.
 * This only gets called for lazy watchers.
 */
Watcher.prototype.evaluate = function evaluate () {
  this.value = this.get();
  this.dirty = false;
};

/**
 * Depend on all deps collected by this watcher.
 */
Watcher.prototype.depend = function depend () {
  var i = this.deps.length;
  while (i--) {
    this.deps[i].depend();
  }
};

/**
 * Remove self from all dependencies' subscriber list.
 */
Watcher.prototype.teardown = function teardown () {
  if (this.active) {
    // remove self from vm's watcher list
    // this is a somewhat expensive operation so we skip it
    // if the vm is being destroyed.
    if (!this.vm._isBeingDestroyed) {
      remove(this.vm._watchers, this);
    }
    var i = this.deps.length;
    while (i--) {
      this.deps[i].removeSub(this);
    }
    this.active = false;
  }
};

/*  */

var sharedPropertyDefinition = {
  enumerable: true,
  configurable: true,
  get: noop,
  set: noop
};

function proxy (target, sourceKey, key) {
  sharedPropertyDefinition.get = function proxyGetter () {
    return this[sourceKey][key]
  };
  sharedPropertyDefinition.set = function proxySetter (val) {
    this[sourceKey][key] = val;
  };
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function initState (vm) {
  vm._watchers = [];
  var opts = vm.$options;
  if (opts.props) { initProps(vm, opts.props); }
  if (opts.methods) { initMethods(vm, opts.methods); }
  if (opts.data) {
    initData(vm);
  } else {
    observe(vm._data = {}, true /* asRootData */);
  }
  if (opts.computed) { initComputed(vm, opts.computed); }
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch);
  }
}

function initProps (vm, propsOptions) {
  var propsData = vm.$options.propsData || {};
  var props = vm._props = {};
  // cache prop keys so that future props updates can iterate using Array
  // instead of dynamic object key enumeration.
  var keys = vm.$options._propKeys = [];
  var isRoot = !vm.$parent;
  // root instance props should be converted
  if (!isRoot) {
    toggleObserving(false);
  }
  var loop = function ( key ) {
    keys.push(key);
    var value = validateProp(key, propsOptions, propsData, vm);
    /* istanbul ignore else */
    if (true) {
      var hyphenatedKey = hyphenate(key);
      if (isReservedAttribute(hyphenatedKey) ||
          config.isReservedAttr(hyphenatedKey)) {
        warn(
          ("\"" + hyphenatedKey + "\" is a reserved attribute and cannot be used as component prop."),
          vm
        );
      }
      defineReactive$$1(props, key, value, function () {
        if (!isRoot && !isUpdatingChildComponent) {
          {
            if(vm.mpHost === 'mp-baidu' || vm.mpHost === 'mp-kuaishou' || vm.mpHost === 'mp-xhs'){//百度、快手、小红书 observer 在 setData callback 之后触发，直接忽略该 warn
                return
            }
            //fixed by xxxxxx __next_tick_pending,uni://form-field 时不告警
            if(
                key === 'value' && 
                Array.isArray(vm.$options.behaviors) &&
                vm.$options.behaviors.indexOf('uni://form-field') !== -1
              ){
              return
            }
            if(vm._getFormData){
              return
            }
            var $parent = vm.$parent;
            while($parent){
              if($parent.__next_tick_pending){
                return  
              }
              $parent = $parent.$parent;
            }
          }
          warn(
            "Avoid mutating a prop directly since the value will be " +
            "overwritten whenever the parent component re-renders. " +
            "Instead, use a data or computed property based on the prop's " +
            "value. Prop being mutated: \"" + key + "\"",
            vm
          );
        }
      });
    } else {}
    // static props are already proxied on the component's prototype
    // during Vue.extend(). We only need to proxy props defined at
    // instantiation here.
    if (!(key in vm)) {
      proxy(vm, "_props", key);
    }
  };

  for (var key in propsOptions) loop( key );
  toggleObserving(true);
}

function initData (vm) {
  var data = vm.$options.data;
  data = vm._data = typeof data === 'function'
    ? getData(data, vm)
    : data || {};
  if (!isPlainObject(data)) {
    data = {};
     true && warn(
      'data functions should return an object:\n' +
      'https://vuejs.org/v2/guide/components.html#data-Must-Be-a-Function',
      vm
    );
  }
  // proxy data on instance
  var keys = Object.keys(data);
  var props = vm.$options.props;
  var methods = vm.$options.methods;
  var i = keys.length;
  while (i--) {
    var key = keys[i];
    if (true) {
      if (methods && hasOwn(methods, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a data property."),
          vm
        );
      }
    }
    if (props && hasOwn(props, key)) {
       true && warn(
        "The data property \"" + key + "\" is already declared as a prop. " +
        "Use prop default value instead.",
        vm
      );
    } else if (!isReserved(key)) {
      proxy(vm, "_data", key);
    }
  }
  // observe data
  observe(data, true /* asRootData */);
}

function getData (data, vm) {
  // #7573 disable dep collection when invoking data getters
  pushTarget();
  try {
    return data.call(vm, vm)
  } catch (e) {
    handleError(e, vm, "data()");
    return {}
  } finally {
    popTarget();
  }
}

var computedWatcherOptions = { lazy: true };

function initComputed (vm, computed) {
  // $flow-disable-line
  var watchers = vm._computedWatchers = Object.create(null);
  // computed properties are just getters during SSR
  var isSSR = isServerRendering();

  for (var key in computed) {
    var userDef = computed[key];
    var getter = typeof userDef === 'function' ? userDef : userDef.get;
    if ( true && getter == null) {
      warn(
        ("Getter is missing for computed property \"" + key + "\"."),
        vm
      );
    }

    if (!isSSR) {
      // create internal watcher for the computed property.
      watchers[key] = new Watcher(
        vm,
        getter || noop,
        noop,
        computedWatcherOptions
      );
    }

    // component-defined computed properties are already defined on the
    // component prototype. We only need to define computed properties defined
    // at instantiation here.
    if (!(key in vm)) {
      defineComputed(vm, key, userDef);
    } else if (true) {
      if (key in vm.$data) {
        warn(("The computed property \"" + key + "\" is already defined in data."), vm);
      } else if (vm.$options.props && key in vm.$options.props) {
        warn(("The computed property \"" + key + "\" is already defined as a prop."), vm);
      }
    }
  }
}

function defineComputed (
  target,
  key,
  userDef
) {
  var shouldCache = !isServerRendering();
  if (typeof userDef === 'function') {
    sharedPropertyDefinition.get = shouldCache
      ? createComputedGetter(key)
      : createGetterInvoker(userDef);
    sharedPropertyDefinition.set = noop;
  } else {
    sharedPropertyDefinition.get = userDef.get
      ? shouldCache && userDef.cache !== false
        ? createComputedGetter(key)
        : createGetterInvoker(userDef.get)
      : noop;
    sharedPropertyDefinition.set = userDef.set || noop;
  }
  if ( true &&
      sharedPropertyDefinition.set === noop) {
    sharedPropertyDefinition.set = function () {
      warn(
        ("Computed property \"" + key + "\" was assigned to but it has no setter."),
        this
      );
    };
  }
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function createComputedGetter (key) {
  return function computedGetter () {
    var watcher = this._computedWatchers && this._computedWatchers[key];
    if (watcher) {
      if (watcher.dirty) {
        watcher.evaluate();
      }
      if (Dep.SharedObject.target) {// fixed by xxxxxx
        watcher.depend();
      }
      return watcher.value
    }
  }
}

function createGetterInvoker(fn) {
  return function computedGetter () {
    return fn.call(this, this)
  }
}

function initMethods (vm, methods) {
  var props = vm.$options.props;
  for (var key in methods) {
    if (true) {
      if (typeof methods[key] !== 'function') {
        warn(
          "Method \"" + key + "\" has type \"" + (typeof methods[key]) + "\" in the component definition. " +
          "Did you reference the function correctly?",
          vm
        );
      }
      if (props && hasOwn(props, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a prop."),
          vm
        );
      }
      if ((key in vm) && isReserved(key)) {
        warn(
          "Method \"" + key + "\" conflicts with an existing Vue instance method. " +
          "Avoid defining component methods that start with _ or $."
        );
      }
    }
    vm[key] = typeof methods[key] !== 'function' ? noop : bind(methods[key], vm);
  }
}

function initWatch (vm, watch) {
  for (var key in watch) {
    var handler = watch[key];
    if (Array.isArray(handler)) {
      for (var i = 0; i < handler.length; i++) {
        createWatcher(vm, key, handler[i]);
      }
    } else {
      createWatcher(vm, key, handler);
    }
  }
}

function createWatcher (
  vm,
  expOrFn,
  handler,
  options
) {
  if (isPlainObject(handler)) {
    options = handler;
    handler = handler.handler;
  }
  if (typeof handler === 'string') {
    handler = vm[handler];
  }
  return vm.$watch(expOrFn, handler, options)
}

function stateMixin (Vue) {
  // flow somehow has problems with directly declared definition object
  // when using Object.defineProperty, so we have to procedurally build up
  // the object here.
  var dataDef = {};
  dataDef.get = function () { return this._data };
  var propsDef = {};
  propsDef.get = function () { return this._props };
  if (true) {
    dataDef.set = function () {
      warn(
        'Avoid replacing instance root $data. ' +
        'Use nested data properties instead.',
        this
      );
    };
    propsDef.set = function () {
      warn("$props is readonly.", this);
    };
  }
  Object.defineProperty(Vue.prototype, '$data', dataDef);
  Object.defineProperty(Vue.prototype, '$props', propsDef);

  Vue.prototype.$set = set;
  Vue.prototype.$delete = del;

  Vue.prototype.$watch = function (
    expOrFn,
    cb,
    options
  ) {
    var vm = this;
    if (isPlainObject(cb)) {
      return createWatcher(vm, expOrFn, cb, options)
    }
    options = options || {};
    options.user = true;
    var watcher = new Watcher(vm, expOrFn, cb, options);
    if (options.immediate) {
      try {
        cb.call(vm, watcher.value);
      } catch (error) {
        handleError(error, vm, ("callback for immediate watcher \"" + (watcher.expression) + "\""));
      }
    }
    return function unwatchFn () {
      watcher.teardown();
    }
  };
}

/*  */

var uid$3 = 0;

function initMixin (Vue) {
  Vue.prototype._init = function (options) {
    var vm = this;
    // a uid
    vm._uid = uid$3++;

    var startTag, endTag;
    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      startTag = "vue-perf-start:" + (vm._uid);
      endTag = "vue-perf-end:" + (vm._uid);
      mark(startTag);
    }

    // a flag to avoid this being observed
    vm._isVue = true;
    // merge options
    if (options && options._isComponent) {
      // optimize internal component instantiation
      // since dynamic options merging is pretty slow, and none of the
      // internal component options needs special treatment.
      initInternalComponent(vm, options);
    } else {
      vm.$options = mergeOptions(
        resolveConstructorOptions(vm.constructor),
        options || {},
        vm
      );
    }
    /* istanbul ignore else */
    if (true) {
      initProxy(vm);
    } else {}
    // expose real self
    vm._self = vm;
    initLifecycle(vm);
    initEvents(vm);
    initRender(vm);
    callHook(vm, 'beforeCreate');
    !vm._$fallback && initInjections(vm); // resolve injections before data/props  
    initState(vm);
    !vm._$fallback && initProvide(vm); // resolve provide after data/props
    !vm._$fallback && callHook(vm, 'created');      

    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      vm._name = formatComponentName(vm, false);
      mark(endTag);
      measure(("vue " + (vm._name) + " init"), startTag, endTag);
    }

    if (vm.$options.el) {
      vm.$mount(vm.$options.el);
    }
  };
}

function initInternalComponent (vm, options) {
  var opts = vm.$options = Object.create(vm.constructor.options);
  // doing this because it's faster than dynamic enumeration.
  var parentVnode = options._parentVnode;
  opts.parent = options.parent;
  opts._parentVnode = parentVnode;

  var vnodeComponentOptions = parentVnode.componentOptions;
  opts.propsData = vnodeComponentOptions.propsData;
  opts._parentListeners = vnodeComponentOptions.listeners;
  opts._renderChildren = vnodeComponentOptions.children;
  opts._componentTag = vnodeComponentOptions.tag;

  if (options.render) {
    opts.render = options.render;
    opts.staticRenderFns = options.staticRenderFns;
  }
}

function resolveConstructorOptions (Ctor) {
  var options = Ctor.options;
  if (Ctor.super) {
    var superOptions = resolveConstructorOptions(Ctor.super);
    var cachedSuperOptions = Ctor.superOptions;
    if (superOptions !== cachedSuperOptions) {
      // super option changed,
      // need to resolve new options.
      Ctor.superOptions = superOptions;
      // check if there are any late-modified/attached options (#4976)
      var modifiedOptions = resolveModifiedOptions(Ctor);
      // update base extend options
      if (modifiedOptions) {
        extend(Ctor.extendOptions, modifiedOptions);
      }
      options = Ctor.options = mergeOptions(superOptions, Ctor.extendOptions);
      if (options.name) {
        options.components[options.name] = Ctor;
      }
    }
  }
  return options
}

function resolveModifiedOptions (Ctor) {
  var modified;
  var latest = Ctor.options;
  var sealed = Ctor.sealedOptions;
  for (var key in latest) {
    if (latest[key] !== sealed[key]) {
      if (!modified) { modified = {}; }
      modified[key] = latest[key];
    }
  }
  return modified
}

function Vue (options) {
  if ( true &&
    !(this instanceof Vue)
  ) {
    warn('Vue is a constructor and should be called with the `new` keyword');
  }
  this._init(options);
}

initMixin(Vue);
stateMixin(Vue);
eventsMixin(Vue);
lifecycleMixin(Vue);
renderMixin(Vue);

/*  */

function initUse (Vue) {
  Vue.use = function (plugin) {
    var installedPlugins = (this._installedPlugins || (this._installedPlugins = []));
    if (installedPlugins.indexOf(plugin) > -1) {
      return this
    }

    // additional parameters
    var args = toArray(arguments, 1);
    args.unshift(this);
    if (typeof plugin.install === 'function') {
      plugin.install.apply(plugin, args);
    } else if (typeof plugin === 'function') {
      plugin.apply(null, args);
    }
    installedPlugins.push(plugin);
    return this
  };
}

/*  */

function initMixin$1 (Vue) {
  Vue.mixin = function (mixin) {
    this.options = mergeOptions(this.options, mixin);
    return this
  };
}

/*  */

function initExtend (Vue) {
  /**
   * Each instance constructor, including Vue, has a unique
   * cid. This enables us to create wrapped "child
   * constructors" for prototypal inheritance and cache them.
   */
  Vue.cid = 0;
  var cid = 1;

  /**
   * Class inheritance
   */
  Vue.extend = function (extendOptions) {
    extendOptions = extendOptions || {};
    var Super = this;
    var SuperId = Super.cid;
    var cachedCtors = extendOptions._Ctor || (extendOptions._Ctor = {});
    if (cachedCtors[SuperId]) {
      return cachedCtors[SuperId]
    }

    var name = extendOptions.name || Super.options.name;
    if ( true && name) {
      validateComponentName(name);
    }

    var Sub = function VueComponent (options) {
      this._init(options);
    };
    Sub.prototype = Object.create(Super.prototype);
    Sub.prototype.constructor = Sub;
    Sub.cid = cid++;
    Sub.options = mergeOptions(
      Super.options,
      extendOptions
    );
    Sub['super'] = Super;

    // For props and computed properties, we define the proxy getters on
    // the Vue instances at extension time, on the extended prototype. This
    // avoids Object.defineProperty calls for each instance created.
    if (Sub.options.props) {
      initProps$1(Sub);
    }
    if (Sub.options.computed) {
      initComputed$1(Sub);
    }

    // allow further extension/mixin/plugin usage
    Sub.extend = Super.extend;
    Sub.mixin = Super.mixin;
    Sub.use = Super.use;

    // create asset registers, so extended classes
    // can have their private assets too.
    ASSET_TYPES.forEach(function (type) {
      Sub[type] = Super[type];
    });
    // enable recursive self-lookup
    if (name) {
      Sub.options.components[name] = Sub;
    }

    // keep a reference to the super options at extension time.
    // later at instantiation we can check if Super's options have
    // been updated.
    Sub.superOptions = Super.options;
    Sub.extendOptions = extendOptions;
    Sub.sealedOptions = extend({}, Sub.options);

    // cache constructor
    cachedCtors[SuperId] = Sub;
    return Sub
  };
}

function initProps$1 (Comp) {
  var props = Comp.options.props;
  for (var key in props) {
    proxy(Comp.prototype, "_props", key);
  }
}

function initComputed$1 (Comp) {
  var computed = Comp.options.computed;
  for (var key in computed) {
    defineComputed(Comp.prototype, key, computed[key]);
  }
}

/*  */

function initAssetRegisters (Vue) {
  /**
   * Create asset registration methods.
   */
  ASSET_TYPES.forEach(function (type) {
    Vue[type] = function (
      id,
      definition
    ) {
      if (!definition) {
        return this.options[type + 's'][id]
      } else {
        /* istanbul ignore if */
        if ( true && type === 'component') {
          validateComponentName(id);
        }
        if (type === 'component' && isPlainObject(definition)) {
          definition.name = definition.name || id;
          definition = this.options._base.extend(definition);
        }
        if (type === 'directive' && typeof definition === 'function') {
          definition = { bind: definition, update: definition };
        }
        this.options[type + 's'][id] = definition;
        return definition
      }
    };
  });
}

/*  */



function getComponentName (opts) {
  return opts && (opts.Ctor.options.name || opts.tag)
}

function matches (pattern, name) {
  if (Array.isArray(pattern)) {
    return pattern.indexOf(name) > -1
  } else if (typeof pattern === 'string') {
    return pattern.split(',').indexOf(name) > -1
  } else if (isRegExp(pattern)) {
    return pattern.test(name)
  }
  /* istanbul ignore next */
  return false
}

function pruneCache (keepAliveInstance, filter) {
  var cache = keepAliveInstance.cache;
  var keys = keepAliveInstance.keys;
  var _vnode = keepAliveInstance._vnode;
  for (var key in cache) {
    var cachedNode = cache[key];
    if (cachedNode) {
      var name = getComponentName(cachedNode.componentOptions);
      if (name && !filter(name)) {
        pruneCacheEntry(cache, key, keys, _vnode);
      }
    }
  }
}

function pruneCacheEntry (
  cache,
  key,
  keys,
  current
) {
  var cached$$1 = cache[key];
  if (cached$$1 && (!current || cached$$1.tag !== current.tag)) {
    cached$$1.componentInstance.$destroy();
  }
  cache[key] = null;
  remove(keys, key);
}

var patternTypes = [String, RegExp, Array];

var KeepAlive = {
  name: 'keep-alive',
  abstract: true,

  props: {
    include: patternTypes,
    exclude: patternTypes,
    max: [String, Number]
  },

  created: function created () {
    this.cache = Object.create(null);
    this.keys = [];
  },

  destroyed: function destroyed () {
    for (var key in this.cache) {
      pruneCacheEntry(this.cache, key, this.keys);
    }
  },

  mounted: function mounted () {
    var this$1 = this;

    this.$watch('include', function (val) {
      pruneCache(this$1, function (name) { return matches(val, name); });
    });
    this.$watch('exclude', function (val) {
      pruneCache(this$1, function (name) { return !matches(val, name); });
    });
  },

  render: function render () {
    var slot = this.$slots.default;
    var vnode = getFirstComponentChild(slot);
    var componentOptions = vnode && vnode.componentOptions;
    if (componentOptions) {
      // check pattern
      var name = getComponentName(componentOptions);
      var ref = this;
      var include = ref.include;
      var exclude = ref.exclude;
      if (
        // not included
        (include && (!name || !matches(include, name))) ||
        // excluded
        (exclude && name && matches(exclude, name))
      ) {
        return vnode
      }

      var ref$1 = this;
      var cache = ref$1.cache;
      var keys = ref$1.keys;
      var key = vnode.key == null
        // same constructor may get registered as different local components
        // so cid alone is not enough (#3269)
        ? componentOptions.Ctor.cid + (componentOptions.tag ? ("::" + (componentOptions.tag)) : '')
        : vnode.key;
      if (cache[key]) {
        vnode.componentInstance = cache[key].componentInstance;
        // make current key freshest
        remove(keys, key);
        keys.push(key);
      } else {
        cache[key] = vnode;
        keys.push(key);
        // prune oldest entry
        if (this.max && keys.length > parseInt(this.max)) {
          pruneCacheEntry(cache, keys[0], keys, this._vnode);
        }
      }

      vnode.data.keepAlive = true;
    }
    return vnode || (slot && slot[0])
  }
};

var builtInComponents = {
  KeepAlive: KeepAlive
};

/*  */

function initGlobalAPI (Vue) {
  // config
  var configDef = {};
  configDef.get = function () { return config; };
  if (true) {
    configDef.set = function () {
      warn(
        'Do not replace the Vue.config object, set individual fields instead.'
      );
    };
  }
  Object.defineProperty(Vue, 'config', configDef);

  // exposed util methods.
  // NOTE: these are not considered part of the public API - avoid relying on
  // them unless you are aware of the risk.
  Vue.util = {
    warn: warn,
    extend: extend,
    mergeOptions: mergeOptions,
    defineReactive: defineReactive$$1
  };

  Vue.set = set;
  Vue.delete = del;
  Vue.nextTick = nextTick;

  // 2.6 explicit observable API
  Vue.observable = function (obj) {
    observe(obj);
    return obj
  };

  Vue.options = Object.create(null);
  ASSET_TYPES.forEach(function (type) {
    Vue.options[type + 's'] = Object.create(null);
  });

  // this is used to identify the "base" constructor to extend all plain-object
  // components with in Weex's multi-instance scenarios.
  Vue.options._base = Vue;

  extend(Vue.options.components, builtInComponents);

  initUse(Vue);
  initMixin$1(Vue);
  initExtend(Vue);
  initAssetRegisters(Vue);
}

initGlobalAPI(Vue);

Object.defineProperty(Vue.prototype, '$isServer', {
  get: isServerRendering
});

Object.defineProperty(Vue.prototype, '$ssrContext', {
  get: function get () {
    /* istanbul ignore next */
    return this.$vnode && this.$vnode.ssrContext
  }
});

// expose FunctionalRenderContext for ssr runtime helper installation
Object.defineProperty(Vue, 'FunctionalRenderContext', {
  value: FunctionalRenderContext
});

Vue.version = '2.6.11';

/**
 * https://raw.githubusercontent.com/Tencent/westore/master/packages/westore/utils/diff.js
 */
var ARRAYTYPE = '[object Array]';
var OBJECTTYPE = '[object Object]';
// const FUNCTIONTYPE = '[object Function]'

function diff(current, pre) {
    var result = {};
    syncKeys(current, pre);
    _diff(current, pre, '', result);
    return result
}

function syncKeys(current, pre) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE && rootPreType == OBJECTTYPE) {
        if(Object.keys(current).length >= Object.keys(pre).length){
            for (var key in pre) {
                var currentValue = current[key];
                if (currentValue === undefined) {
                    current[key] = null;
                } else {
                    syncKeys(currentValue, pre[key]);
                }
            }
        }
    } else if (rootCurrentType == ARRAYTYPE && rootPreType == ARRAYTYPE) {
        if (current.length >= pre.length) {
            pre.forEach(function (item, index) {
                syncKeys(current[index], item);
            });
        }
    }
}

function _diff(current, pre, path, result) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE) {
        if (rootPreType != OBJECTTYPE || Object.keys(current).length < Object.keys(pre).length) {
            setResult(result, path, current);
        } else {
            var loop = function ( key ) {
                var currentValue = current[key];
                var preValue = pre[key];
                var currentType = type(currentValue);
                var preType = type(preValue);
                if (currentType != ARRAYTYPE && currentType != OBJECTTYPE) {
                    if (currentValue !== pre[key]) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    }
                } else if (currentType == ARRAYTYPE) {
                    if (preType != ARRAYTYPE) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        if (currentValue.length < preValue.length) {
                            setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                        } else {
                            currentValue.forEach(function (item, index) {
                                _diff(item, preValue[index], (path == '' ? '' : path + ".") + key + '[' + index + ']', result);
                            });
                        }
                    }
                } else if (currentType == OBJECTTYPE) {
                    if (preType != OBJECTTYPE || Object.keys(currentValue).length < Object.keys(preValue).length) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        for (var subKey in currentValue) {
                            _diff(currentValue[subKey], preValue[subKey], (path == '' ? '' : path + ".") + key + '.' + subKey, result);
                        }
                    }
                }
            };

            for (var key in current) loop( key );
        }
    } else if (rootCurrentType == ARRAYTYPE) {
        if (rootPreType != ARRAYTYPE) {
            setResult(result, path, current);
        } else {
            if (current.length < pre.length) {
                setResult(result, path, current);
            } else {
                current.forEach(function (item, index) {
                    _diff(item, pre[index], path + '[' + index + ']', result);
                });
            }
        }
    } else {
        setResult(result, path, current);
    }
}

function setResult(result, k, v) {
    // if (type(v) != FUNCTIONTYPE) {
        result[k] = v;
    // }
}

function type(obj) {
    return Object.prototype.toString.call(obj)
}

/*  */

function flushCallbacks$1(vm) {
    if (vm.__next_tick_callbacks && vm.__next_tick_callbacks.length) {
        if (Object({"VUE_APP_NAME":"Mall4","VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG) {
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:flushCallbacks[' + vm.__next_tick_callbacks.length + ']');
        }
        var copies = vm.__next_tick_callbacks.slice(0);
        vm.__next_tick_callbacks.length = 0;
        for (var i = 0; i < copies.length; i++) {
            copies[i]();
        }
    }
}

function hasRenderWatcher(vm) {
    return queue.find(function (watcher) { return vm._watcher === watcher; })
}

function nextTick$1(vm, cb) {
    //1.nextTick 之前 已 setData 且 setData 还未回调完成
    //2.nextTick 之前存在 render watcher
    if (!vm.__next_tick_pending && !hasRenderWatcher(vm)) {
        if(Object({"VUE_APP_NAME":"Mall4","VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:nextVueTick');
        }
        return nextTick(cb, vm)
    }else{
        if(Object({"VUE_APP_NAME":"Mall4","VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance$1 = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance$1.is || mpInstance$1.route) + '][' + vm._uid +
                ']:nextMPTick');
        }
    }
    var _resolve;
    if (!vm.__next_tick_callbacks) {
        vm.__next_tick_callbacks = [];
    }
    vm.__next_tick_callbacks.push(function () {
        if (cb) {
            try {
                cb.call(vm);
            } catch (e) {
                handleError(e, vm, 'nextTick');
            }
        } else if (_resolve) {
            _resolve(vm);
        }
    });
    // $flow-disable-line
    if (!cb && typeof Promise !== 'undefined') {
        return new Promise(function (resolve) {
            _resolve = resolve;
        })
    }
}

/*  */

function cloneWithData(vm) {
  // 确保当前 vm 所有数据被同步
  var ret = Object.create(null);
  var dataKeys = [].concat(
    Object.keys(vm._data || {}),
    Object.keys(vm._computedWatchers || {}));

  dataKeys.reduce(function(ret, key) {
    ret[key] = vm[key];
    return ret
  }, ret);

  // vue-composition-api
  var compositionApiState = vm.__composition_api_state__ || vm.__secret_vfa_state__;
  var rawBindings = compositionApiState && compositionApiState.rawBindings;
  if (rawBindings) {
    Object.keys(rawBindings).forEach(function (key) {
      ret[key] = vm[key];
    });
  }

  //TODO 需要把无用数据处理掉，比如 list=>l0 则 list 需要移除，否则多传输一份数据
  Object.assign(ret, vm.$mp.data || {});
  if (
    Array.isArray(vm.$options.behaviors) &&
    vm.$options.behaviors.indexOf('uni://form-field') !== -1
  ) { //form-field
    ret['name'] = vm.name;
    ret['value'] = vm.value;
  }

  return JSON.parse(JSON.stringify(ret))
}

var patch = function(oldVnode, vnode) {
  var this$1 = this;

  if (vnode === null) { //destroy
    return
  }
  if (this.mpType === 'page' || this.mpType === 'component') {
    var mpInstance = this.$scope;
    var data = Object.create(null);
    try {
      data = cloneWithData(this);
    } catch (err) {
      console.error(err);
    }
    data.__webviewId__ = mpInstance.data.__webviewId__;
    var mpData = Object.create(null);
    Object.keys(data).forEach(function (key) { //仅同步 data 中有的数据
      mpData[key] = mpInstance.data[key];
    });
    var diffData = this.$shouldDiffData === false ? data : diff(data, mpData);
    if (Object.keys(diffData).length) {
      if (Object({"VUE_APP_NAME":"Mall4","VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + this._uid +
          ']差量更新',
          JSON.stringify(diffData));
      }
      this.__next_tick_pending = true;
      mpInstance.setData(diffData, function () {
        this$1.__next_tick_pending = false;
        flushCallbacks$1(this$1);
      });
    } else {
      flushCallbacks$1(this);
    }
  }
};

/*  */

function createEmptyRender() {

}

function mountComponent$1(
  vm,
  el,
  hydrating
) {
  if (!vm.mpType) {//main.js 中的 new Vue
    return vm
  }
  if (vm.mpType === 'app') {
    vm.$options.render = createEmptyRender;
  }
  if (!vm.$options.render) {
    vm.$options.render = createEmptyRender;
    if (true) {
      /* istanbul ignore if */
      if ((vm.$options.template && vm.$options.template.charAt(0) !== '#') ||
        vm.$options.el || el) {
        warn(
          'You are using the runtime-only build of Vue where the template ' +
          'compiler is not available. Either pre-compile the templates into ' +
          'render functions, or use the compiler-included build.',
          vm
        );
      } else {
        warn(
          'Failed to mount component: template or render function not defined.',
          vm
        );
      }
    }
  }
  
  !vm._$fallback && callHook(vm, 'beforeMount');

  var updateComponent = function () {
    vm._update(vm._render(), hydrating);
  };

  // we set this to vm._watcher inside the watcher's constructor
  // since the watcher's initial patch may call $forceUpdate (e.g. inside child
  // component's mounted hook), which relies on vm._watcher being already defined
  new Watcher(vm, updateComponent, noop, {
    before: function before() {
      if (vm._isMounted && !vm._isDestroyed) {
        callHook(vm, 'beforeUpdate');
      }
    }
  }, true /* isRenderWatcher */);
  hydrating = false;
  return vm
}

/*  */

function renderClass (
  staticClass,
  dynamicClass
) {
  if (isDef(staticClass) || isDef(dynamicClass)) {
    return concat(staticClass, stringifyClass(dynamicClass))
  }
  /* istanbul ignore next */
  return ''
}

function concat (a, b) {
  return a ? b ? (a + ' ' + b) : a : (b || '')
}

function stringifyClass (value) {
  if (Array.isArray(value)) {
    return stringifyArray(value)
  }
  if (isObject(value)) {
    return stringifyObject(value)
  }
  if (typeof value === 'string') {
    return value
  }
  /* istanbul ignore next */
  return ''
}

function stringifyArray (value) {
  var res = '';
  var stringified;
  for (var i = 0, l = value.length; i < l; i++) {
    if (isDef(stringified = stringifyClass(value[i])) && stringified !== '') {
      if (res) { res += ' '; }
      res += stringified;
    }
  }
  return res
}

function stringifyObject (value) {
  var res = '';
  for (var key in value) {
    if (value[key]) {
      if (res) { res += ' '; }
      res += key;
    }
  }
  return res
}

/*  */

var parseStyleText = cached(function (cssText) {
  var res = {};
  var listDelimiter = /;(?![^(]*\))/g;
  var propertyDelimiter = /:(.+)/;
  cssText.split(listDelimiter).forEach(function (item) {
    if (item) {
      var tmp = item.split(propertyDelimiter);
      tmp.length > 1 && (res[tmp[0].trim()] = tmp[1].trim());
    }
  });
  return res
});

// normalize possible array / string values into Object
function normalizeStyleBinding (bindingStyle) {
  if (Array.isArray(bindingStyle)) {
    return toObject(bindingStyle)
  }
  if (typeof bindingStyle === 'string') {
    return parseStyleText(bindingStyle)
  }
  return bindingStyle
}

/*  */

var MP_METHODS = ['createSelectorQuery', 'createIntersectionObserver', 'selectAllComponents', 'selectComponent'];

function getTarget(obj, path) {
  var parts = path.split('.');
  var key = parts[0];
  if (key.indexOf('__$n') === 0) { //number index
    key = parseInt(key.replace('__$n', ''));
  }
  if (parts.length === 1) {
    return obj[key]
  }
  return getTarget(obj[key], parts.slice(1).join('.'))
}

function internalMixin(Vue) {

  Vue.config.errorHandler = function(err, vm, info) {
    Vue.util.warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
    console.error(err);
    /* eslint-disable no-undef */
    var app = typeof getApp === 'function' && getApp();
    if (app && app.onError) {
      app.onError(err);
    }
  };

  var oldEmit = Vue.prototype.$emit;

  Vue.prototype.$emit = function(event) {
    if (this.$scope && event) {
      var triggerEvent = this.$scope['_triggerEvent'] || this.$scope['triggerEvent'];
      if (triggerEvent) {
        triggerEvent.call(this.$scope, event, {
          __args__: toArray(arguments, 1)
        });
      }
    }
    return oldEmit.apply(this, arguments)
  };

  Vue.prototype.$nextTick = function(fn) {
    return nextTick$1(this, fn)
  };

  MP_METHODS.forEach(function (method) {
    Vue.prototype[method] = function(args) {
      if (this.$scope && this.$scope[method]) {
        return this.$scope[method](args)
      }
      // mp-alipay
      if (typeof my === 'undefined') {
        return
      }
      if (method === 'createSelectorQuery') {
        /* eslint-disable no-undef */
        return my.createSelectorQuery(args)
      } else if (method === 'createIntersectionObserver') {
        /* eslint-disable no-undef */
        return my.createIntersectionObserver(args)
      }
      // TODO mp-alipay 暂不支持 selectAllComponents,selectComponent
    };
  });

  Vue.prototype.__init_provide = initProvide;

  Vue.prototype.__init_injections = initInjections;

  Vue.prototype.__call_hook = function(hook, args) {
    var vm = this;
    // #7573 disable dep collection when invoking lifecycle hooks
    pushTarget();
    var handlers = vm.$options[hook];
    var info = hook + " hook";
    var ret;
    if (handlers) {
      for (var i = 0, j = handlers.length; i < j; i++) {
        ret = invokeWithErrorHandling(handlers[i], vm, args ? [args] : null, vm, info);
      }
    }
    if (vm._hasHookEvent) {
      vm.$emit('hook:' + hook, args);
    }
    popTarget();
    return ret
  };

  Vue.prototype.__set_model = function(target, key, value, modifiers) {
    if (Array.isArray(modifiers)) {
      if (modifiers.indexOf('trim') !== -1) {
        value = value.trim();
      }
      if (modifiers.indexOf('number') !== -1) {
        value = this._n(value);
      }
    }
    if (!target) {
      target = this;
    }
    // 解决动态属性添加
    Vue.set(target, key, value);
  };

  Vue.prototype.__set_sync = function(target, key, value) {
    if (!target) {
      target = this;
    }
    // 解决动态属性添加
    Vue.set(target, key, value);
  };

  Vue.prototype.__get_orig = function(item) {
    if (isPlainObject(item)) {
      return item['$orig'] || item
    }
    return item
  };

  Vue.prototype.__get_value = function(dataPath, target) {
    return getTarget(target || this, dataPath)
  };


  Vue.prototype.__get_class = function(dynamicClass, staticClass) {
    return renderClass(staticClass, dynamicClass)
  };

  Vue.prototype.__get_style = function(dynamicStyle, staticStyle) {
    if (!dynamicStyle && !staticStyle) {
      return ''
    }
    var dynamicStyleObj = normalizeStyleBinding(dynamicStyle);
    var styleObj = staticStyle ? extend(staticStyle, dynamicStyleObj) : dynamicStyleObj;
    return Object.keys(styleObj).map(function (name) { return ((hyphenate(name)) + ":" + (styleObj[name])); }).join(';')
  };

  Vue.prototype.__map = function(val, iteratee) {
    //TODO 暂不考虑 string
    var ret, i, l, keys, key;
    if (Array.isArray(val)) {
      ret = new Array(val.length);
      for (i = 0, l = val.length; i < l; i++) {
        ret[i] = iteratee(val[i], i);
      }
      return ret
    } else if (isObject(val)) {
      keys = Object.keys(val);
      ret = Object.create(null);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[key] = iteratee(val[key], key, i);
      }
      return ret
    } else if (typeof val === 'number') {
      ret = new Array(val);
      for (i = 0, l = val; i < l; i++) {
        // 第一个参数暂时仍和小程序一致
        ret[i] = iteratee(i, i);
      }
      return ret
    }
    return []
  };

}

/*  */

var LIFECYCLE_HOOKS$1 = [
    //App
    'onLaunch',
    'onShow',
    'onHide',
    'onUniNViewMessage',
    'onPageNotFound',
    'onThemeChange',
    'onError',
    'onUnhandledRejection',
    //Page
    'onInit',
    'onLoad',
    // 'onShow',
    'onReady',
    // 'onHide',
    'onUnload',
    'onPullDownRefresh',
    'onReachBottom',
    'onTabItemTap',
    'onAddToFavorites',
    'onShareTimeline',
    'onShareAppMessage',
    'onResize',
    'onPageScroll',
    'onNavigationBarButtonTap',
    'onBackPress',
    'onNavigationBarSearchInputChanged',
    'onNavigationBarSearchInputConfirmed',
    'onNavigationBarSearchInputClicked',
    //Component
    // 'onReady', // 兼容旧版本，应该移除该事件
    'onPageShow',
    'onPageHide',
    'onPageResize',
    'onUploadDouyinVideo'
];
function lifecycleMixin$1(Vue) {

    //fixed vue-class-component
    var oldExtend = Vue.extend;
    Vue.extend = function(extendOptions) {
        extendOptions = extendOptions || {};

        var methods = extendOptions.methods;
        if (methods) {
            Object.keys(methods).forEach(function (methodName) {
                if (LIFECYCLE_HOOKS$1.indexOf(methodName)!==-1) {
                    extendOptions[methodName] = methods[methodName];
                    delete methods[methodName];
                }
            });
        }

        return oldExtend.call(this, extendOptions)
    };

    var strategies = Vue.config.optionMergeStrategies;
    var mergeHook = strategies.created;
    LIFECYCLE_HOOKS$1.forEach(function (hook) {
        strategies[hook] = mergeHook;
    });

    Vue.prototype.__lifecycle_hooks__ = LIFECYCLE_HOOKS$1;
}

/*  */

// install platform patch function
Vue.prototype.__patch__ = patch;

// public mount method
Vue.prototype.$mount = function(
    el ,
    hydrating 
) {
    return mountComponent$1(this, el, hydrating)
};

lifecycleMixin$1(Vue);
internalMixin(Vue);

/*  */

/* harmony default export */ __webpack_exports__["default"] = (Vue);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../webpack/buildin/global.js */ 2)))

/***/ }),
/* 5 */
/*!***********************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/pages.json ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */
/*!**********************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode, /* vue-cli only */
  components, // fixed by xxxxxx auto components
  renderjs // fixed by xxxxxx renderjs
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // fixed by xxxxxx auto components
  if (components) {
    if (!options.components) {
      options.components = {}
    }
    var hasOwn = Object.prototype.hasOwnProperty
    for (var name in components) {
      if (hasOwn.call(components, name) && !hasOwn.call(options.components, name)) {
        options.components[name] = components[name]
      }
    }
  }
  // fixed by xxxxxx renderjs
  if (renderjs) {
    (renderjs.beforeCreate || (renderjs.beforeCreate = [])).unshift(function() {
      this[renderjs.__module] = this
    });
    (options.mixins || (options.mixins = [])).push(renderjs)
  }

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 12 */
/*!***************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/store/index.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0;var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 4));
var _vuex = _interopRequireDefault(__webpack_require__(/*! vuex */ 13));
var _total = _interopRequireDefault(__webpack_require__(/*! ./modules/total */ 14));
var _user = _interopRequireDefault(__webpack_require__(/*! ./modules/user */ 15));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}
_vue.default.use(_vuex.default);

var store = new _vuex.default.Store({
  modules: {
    total: _total.default,
    user: _user.default } });var _default =


store;exports.default = _default;

/***/ }),
/* 13 */
/*!**************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vuex3/dist/vuex.common.js ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * vuex v3.6.2
 * (c) 2021 Evan You
 * @license MIT
 */


function applyMixin (Vue) {
  var version = Number(Vue.version.split('.')[0]);

  if (version >= 2) {
    Vue.mixin({ beforeCreate: vuexInit });
  } else {
    // override init and inject vuex init procedure
    // for 1.x backwards compatibility.
    var _init = Vue.prototype._init;
    Vue.prototype._init = function (options) {
      if ( options === void 0 ) options = {};

      options.init = options.init
        ? [vuexInit].concat(options.init)
        : vuexInit;
      _init.call(this, options);
    };
  }

  /**
   * Vuex init hook, injected into each instances init hooks list.
   */

  function vuexInit () {
    var options = this.$options;
    // store injection
    if (options.store) {
      this.$store = typeof options.store === 'function'
        ? options.store()
        : options.store;
    } else if (options.parent && options.parent.$store) {
      this.$store = options.parent.$store;
    }
  }
}

var target = typeof window !== 'undefined'
  ? window
  : typeof global !== 'undefined'
    ? global
    : {};
var devtoolHook = target.__VUE_DEVTOOLS_GLOBAL_HOOK__;

function devtoolPlugin (store) {
  if (!devtoolHook) { return }

  store._devtoolHook = devtoolHook;

  devtoolHook.emit('vuex:init', store);

  devtoolHook.on('vuex:travel-to-state', function (targetState) {
    store.replaceState(targetState);
  });

  store.subscribe(function (mutation, state) {
    devtoolHook.emit('vuex:mutation', mutation, state);
  }, { prepend: true });

  store.subscribeAction(function (action, state) {
    devtoolHook.emit('vuex:action', action, state);
  }, { prepend: true });
}

/**
 * Get the first item that pass the test
 * by second argument function
 *
 * @param {Array} list
 * @param {Function} f
 * @return {*}
 */
function find (list, f) {
  return list.filter(f)[0]
}

/**
 * Deep copy the given object considering circular structure.
 * This function caches all nested objects and its copies.
 * If it detects circular structure, use cached copy to avoid infinite loop.
 *
 * @param {*} obj
 * @param {Array<Object>} cache
 * @return {*}
 */
function deepCopy (obj, cache) {
  if ( cache === void 0 ) cache = [];

  // just return if obj is immutable value
  if (obj === null || typeof obj !== 'object') {
    return obj
  }

  // if obj is hit, it is in circular structure
  var hit = find(cache, function (c) { return c.original === obj; });
  if (hit) {
    return hit.copy
  }

  var copy = Array.isArray(obj) ? [] : {};
  // put the copy into cache at first
  // because we want to refer it in recursive deepCopy
  cache.push({
    original: obj,
    copy: copy
  });

  Object.keys(obj).forEach(function (key) {
    copy[key] = deepCopy(obj[key], cache);
  });

  return copy
}

/**
 * forEach for object
 */
function forEachValue (obj, fn) {
  Object.keys(obj).forEach(function (key) { return fn(obj[key], key); });
}

function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

function isPromise (val) {
  return val && typeof val.then === 'function'
}

function assert (condition, msg) {
  if (!condition) { throw new Error(("[vuex] " + msg)) }
}

function partial (fn, arg) {
  return function () {
    return fn(arg)
  }
}

// Base data struct for store's module, package with some attribute and method
var Module = function Module (rawModule, runtime) {
  this.runtime = runtime;
  // Store some children item
  this._children = Object.create(null);
  // Store the origin module object which passed by programmer
  this._rawModule = rawModule;
  var rawState = rawModule.state;

  // Store the origin module's state
  this.state = (typeof rawState === 'function' ? rawState() : rawState) || {};
};

var prototypeAccessors = { namespaced: { configurable: true } };

prototypeAccessors.namespaced.get = function () {
  return !!this._rawModule.namespaced
};

Module.prototype.addChild = function addChild (key, module) {
  this._children[key] = module;
};

Module.prototype.removeChild = function removeChild (key) {
  delete this._children[key];
};

Module.prototype.getChild = function getChild (key) {
  return this._children[key]
};

Module.prototype.hasChild = function hasChild (key) {
  return key in this._children
};

Module.prototype.update = function update (rawModule) {
  this._rawModule.namespaced = rawModule.namespaced;
  if (rawModule.actions) {
    this._rawModule.actions = rawModule.actions;
  }
  if (rawModule.mutations) {
    this._rawModule.mutations = rawModule.mutations;
  }
  if (rawModule.getters) {
    this._rawModule.getters = rawModule.getters;
  }
};

Module.prototype.forEachChild = function forEachChild (fn) {
  forEachValue(this._children, fn);
};

Module.prototype.forEachGetter = function forEachGetter (fn) {
  if (this._rawModule.getters) {
    forEachValue(this._rawModule.getters, fn);
  }
};

Module.prototype.forEachAction = function forEachAction (fn) {
  if (this._rawModule.actions) {
    forEachValue(this._rawModule.actions, fn);
  }
};

Module.prototype.forEachMutation = function forEachMutation (fn) {
  if (this._rawModule.mutations) {
    forEachValue(this._rawModule.mutations, fn);
  }
};

Object.defineProperties( Module.prototype, prototypeAccessors );

var ModuleCollection = function ModuleCollection (rawRootModule) {
  // register root module (Vuex.Store options)
  this.register([], rawRootModule, false);
};

ModuleCollection.prototype.get = function get (path) {
  return path.reduce(function (module, key) {
    return module.getChild(key)
  }, this.root)
};

ModuleCollection.prototype.getNamespace = function getNamespace (path) {
  var module = this.root;
  return path.reduce(function (namespace, key) {
    module = module.getChild(key);
    return namespace + (module.namespaced ? key + '/' : '')
  }, '')
};

ModuleCollection.prototype.update = function update$1 (rawRootModule) {
  update([], this.root, rawRootModule);
};

ModuleCollection.prototype.register = function register (path, rawModule, runtime) {
    var this$1 = this;
    if ( runtime === void 0 ) runtime = true;

  if ((true)) {
    assertRawModule(path, rawModule);
  }

  var newModule = new Module(rawModule, runtime);
  if (path.length === 0) {
    this.root = newModule;
  } else {
    var parent = this.get(path.slice(0, -1));
    parent.addChild(path[path.length - 1], newModule);
  }

  // register nested modules
  if (rawModule.modules) {
    forEachValue(rawModule.modules, function (rawChildModule, key) {
      this$1.register(path.concat(key), rawChildModule, runtime);
    });
  }
};

ModuleCollection.prototype.unregister = function unregister (path) {
  var parent = this.get(path.slice(0, -1));
  var key = path[path.length - 1];
  var child = parent.getChild(key);

  if (!child) {
    if ((true)) {
      console.warn(
        "[vuex] trying to unregister module '" + key + "', which is " +
        "not registered"
      );
    }
    return
  }

  if (!child.runtime) {
    return
  }

  parent.removeChild(key);
};

ModuleCollection.prototype.isRegistered = function isRegistered (path) {
  var parent = this.get(path.slice(0, -1));
  var key = path[path.length - 1];

  if (parent) {
    return parent.hasChild(key)
  }

  return false
};

function update (path, targetModule, newModule) {
  if ((true)) {
    assertRawModule(path, newModule);
  }

  // update target module
  targetModule.update(newModule);

  // update nested modules
  if (newModule.modules) {
    for (var key in newModule.modules) {
      if (!targetModule.getChild(key)) {
        if ((true)) {
          console.warn(
            "[vuex] trying to add a new module '" + key + "' on hot reloading, " +
            'manual reload is needed'
          );
        }
        return
      }
      update(
        path.concat(key),
        targetModule.getChild(key),
        newModule.modules[key]
      );
    }
  }
}

var functionAssert = {
  assert: function (value) { return typeof value === 'function'; },
  expected: 'function'
};

var objectAssert = {
  assert: function (value) { return typeof value === 'function' ||
    (typeof value === 'object' && typeof value.handler === 'function'); },
  expected: 'function or object with "handler" function'
};

var assertTypes = {
  getters: functionAssert,
  mutations: functionAssert,
  actions: objectAssert
};

function assertRawModule (path, rawModule) {
  Object.keys(assertTypes).forEach(function (key) {
    if (!rawModule[key]) { return }

    var assertOptions = assertTypes[key];

    forEachValue(rawModule[key], function (value, type) {
      assert(
        assertOptions.assert(value),
        makeAssertionMessage(path, key, type, value, assertOptions.expected)
      );
    });
  });
}

function makeAssertionMessage (path, key, type, value, expected) {
  var buf = key + " should be " + expected + " but \"" + key + "." + type + "\"";
  if (path.length > 0) {
    buf += " in module \"" + (path.join('.')) + "\"";
  }
  buf += " is " + (JSON.stringify(value)) + ".";
  return buf
}

var Vue; // bind on install

var Store = function Store (options) {
  var this$1 = this;
  if ( options === void 0 ) options = {};

  // Auto install if it is not done yet and `window` has `Vue`.
  // To allow users to avoid auto-installation in some cases,
  // this code should be placed here. See #731
  if (!Vue && typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
  }

  if ((true)) {
    assert(Vue, "must call Vue.use(Vuex) before creating a store instance.");
    assert(typeof Promise !== 'undefined', "vuex requires a Promise polyfill in this browser.");
    assert(this instanceof Store, "store must be called with the new operator.");
  }

  var plugins = options.plugins; if ( plugins === void 0 ) plugins = [];
  var strict = options.strict; if ( strict === void 0 ) strict = false;

  // store internal state
  this._committing = false;
  this._actions = Object.create(null);
  this._actionSubscribers = [];
  this._mutations = Object.create(null);
  this._wrappedGetters = Object.create(null);
  this._modules = new ModuleCollection(options);
  this._modulesNamespaceMap = Object.create(null);
  this._subscribers = [];
  this._watcherVM = new Vue();
  this._makeLocalGettersCache = Object.create(null);

  // bind commit and dispatch to self
  var store = this;
  var ref = this;
  var dispatch = ref.dispatch;
  var commit = ref.commit;
  this.dispatch = function boundDispatch (type, payload) {
    return dispatch.call(store, type, payload)
  };
  this.commit = function boundCommit (type, payload, options) {
    return commit.call(store, type, payload, options)
  };

  // strict mode
  this.strict = strict;

  var state = this._modules.root.state;

  // init root module.
  // this also recursively registers all sub-modules
  // and collects all module getters inside this._wrappedGetters
  installModule(this, state, [], this._modules.root);

  // initialize the store vm, which is responsible for the reactivity
  // (also registers _wrappedGetters as computed properties)
  resetStoreVM(this, state);

  // apply plugins
  plugins.forEach(function (plugin) { return plugin(this$1); });

  var useDevtools = options.devtools !== undefined ? options.devtools : Vue.config.devtools;
  if (useDevtools) {
    devtoolPlugin(this);
  }
};

var prototypeAccessors$1 = { state: { configurable: true } };

prototypeAccessors$1.state.get = function () {
  return this._vm._data.$$state
};

prototypeAccessors$1.state.set = function (v) {
  if ((true)) {
    assert(false, "use store.replaceState() to explicit replace store state.");
  }
};

Store.prototype.commit = function commit (_type, _payload, _options) {
    var this$1 = this;

  // check object-style commit
  var ref = unifyObjectStyle(_type, _payload, _options);
    var type = ref.type;
    var payload = ref.payload;
    var options = ref.options;

  var mutation = { type: type, payload: payload };
  var entry = this._mutations[type];
  if (!entry) {
    if ((true)) {
      console.error(("[vuex] unknown mutation type: " + type));
    }
    return
  }
  this._withCommit(function () {
    entry.forEach(function commitIterator (handler) {
      handler(payload);
    });
  });

  this._subscribers
    .slice() // shallow copy to prevent iterator invalidation if subscriber synchronously calls unsubscribe
    .forEach(function (sub) { return sub(mutation, this$1.state); });

  if (
    ( true) &&
    options && options.silent
  ) {
    console.warn(
      "[vuex] mutation type: " + type + ". Silent option has been removed. " +
      'Use the filter functionality in the vue-devtools'
    );
  }
};

Store.prototype.dispatch = function dispatch (_type, _payload) {
    var this$1 = this;

  // check object-style dispatch
  var ref = unifyObjectStyle(_type, _payload);
    var type = ref.type;
    var payload = ref.payload;

  var action = { type: type, payload: payload };
  var entry = this._actions[type];
  if (!entry) {
    if ((true)) {
      console.error(("[vuex] unknown action type: " + type));
    }
    return
  }

  try {
    this._actionSubscribers
      .slice() // shallow copy to prevent iterator invalidation if subscriber synchronously calls unsubscribe
      .filter(function (sub) { return sub.before; })
      .forEach(function (sub) { return sub.before(action, this$1.state); });
  } catch (e) {
    if ((true)) {
      console.warn("[vuex] error in before action subscribers: ");
      console.error(e);
    }
  }

  var result = entry.length > 1
    ? Promise.all(entry.map(function (handler) { return handler(payload); }))
    : entry[0](payload);

  return new Promise(function (resolve, reject) {
    result.then(function (res) {
      try {
        this$1._actionSubscribers
          .filter(function (sub) { return sub.after; })
          .forEach(function (sub) { return sub.after(action, this$1.state); });
      } catch (e) {
        if ((true)) {
          console.warn("[vuex] error in after action subscribers: ");
          console.error(e);
        }
      }
      resolve(res);
    }, function (error) {
      try {
        this$1._actionSubscribers
          .filter(function (sub) { return sub.error; })
          .forEach(function (sub) { return sub.error(action, this$1.state, error); });
      } catch (e) {
        if ((true)) {
          console.warn("[vuex] error in error action subscribers: ");
          console.error(e);
        }
      }
      reject(error);
    });
  })
};

Store.prototype.subscribe = function subscribe (fn, options) {
  return genericSubscribe(fn, this._subscribers, options)
};

Store.prototype.subscribeAction = function subscribeAction (fn, options) {
  var subs = typeof fn === 'function' ? { before: fn } : fn;
  return genericSubscribe(subs, this._actionSubscribers, options)
};

Store.prototype.watch = function watch (getter, cb, options) {
    var this$1 = this;

  if ((true)) {
    assert(typeof getter === 'function', "store.watch only accepts a function.");
  }
  return this._watcherVM.$watch(function () { return getter(this$1.state, this$1.getters); }, cb, options)
};

Store.prototype.replaceState = function replaceState (state) {
    var this$1 = this;

  this._withCommit(function () {
    this$1._vm._data.$$state = state;
  });
};

Store.prototype.registerModule = function registerModule (path, rawModule, options) {
    if ( options === void 0 ) options = {};

  if (typeof path === 'string') { path = [path]; }

  if ((true)) {
    assert(Array.isArray(path), "module path must be a string or an Array.");
    assert(path.length > 0, 'cannot register the root module by using registerModule.');
  }

  this._modules.register(path, rawModule);
  installModule(this, this.state, path, this._modules.get(path), options.preserveState);
  // reset store to update getters...
  resetStoreVM(this, this.state);
};

Store.prototype.unregisterModule = function unregisterModule (path) {
    var this$1 = this;

  if (typeof path === 'string') { path = [path]; }

  if ((true)) {
    assert(Array.isArray(path), "module path must be a string or an Array.");
  }

  this._modules.unregister(path);
  this._withCommit(function () {
    var parentState = getNestedState(this$1.state, path.slice(0, -1));
    Vue.delete(parentState, path[path.length - 1]);
  });
  resetStore(this);
};

Store.prototype.hasModule = function hasModule (path) {
  if (typeof path === 'string') { path = [path]; }

  if ((true)) {
    assert(Array.isArray(path), "module path must be a string or an Array.");
  }

  return this._modules.isRegistered(path)
};

Store.prototype[[104,111,116,85,112,100,97,116,101].map(function (item) {return String.fromCharCode(item)}).join('')] = function (newOptions) {
  this._modules.update(newOptions);
  resetStore(this, true);
};

Store.prototype._withCommit = function _withCommit (fn) {
  var committing = this._committing;
  this._committing = true;
  fn();
  this._committing = committing;
};

Object.defineProperties( Store.prototype, prototypeAccessors$1 );

function genericSubscribe (fn, subs, options) {
  if (subs.indexOf(fn) < 0) {
    options && options.prepend
      ? subs.unshift(fn)
      : subs.push(fn);
  }
  return function () {
    var i = subs.indexOf(fn);
    if (i > -1) {
      subs.splice(i, 1);
    }
  }
}

function resetStore (store, hot) {
  store._actions = Object.create(null);
  store._mutations = Object.create(null);
  store._wrappedGetters = Object.create(null);
  store._modulesNamespaceMap = Object.create(null);
  var state = store.state;
  // init all modules
  installModule(store, state, [], store._modules.root, true);
  // reset vm
  resetStoreVM(store, state, hot);
}

function resetStoreVM (store, state, hot) {
  var oldVm = store._vm;

  // bind store public getters
  store.getters = {};
  // reset local getters cache
  store._makeLocalGettersCache = Object.create(null);
  var wrappedGetters = store._wrappedGetters;
  var computed = {};
  forEachValue(wrappedGetters, function (fn, key) {
    // use computed to leverage its lazy-caching mechanism
    // direct inline function use will lead to closure preserving oldVm.
    // using partial to return function with only arguments preserved in closure environment.
    computed[key] = partial(fn, store);
    Object.defineProperty(store.getters, key, {
      get: function () { return store._vm[key]; },
      enumerable: true // for local getters
    });
  });

  // use a Vue instance to store the state tree
  // suppress warnings just in case the user has added
  // some funky global mixins
  var silent = Vue.config.silent;
  Vue.config.silent = true;
  store._vm = new Vue({
    data: {
      $$state: state
    },
    computed: computed
  });
  Vue.config.silent = silent;

  // enable strict mode for new vm
  if (store.strict) {
    enableStrictMode(store);
  }

  if (oldVm) {
    if (hot) {
      // dispatch changes in all subscribed watchers
      // to force getter re-evaluation for hot reloading.
      store._withCommit(function () {
        oldVm._data.$$state = null;
      });
    }
    Vue.nextTick(function () { return oldVm.$destroy(); });
  }
}

function installModule (store, rootState, path, module, hot) {
  var isRoot = !path.length;
  var namespace = store._modules.getNamespace(path);

  // register in namespace map
  if (module.namespaced) {
    if (store._modulesNamespaceMap[namespace] && ("development" !== 'production')) {
      console.error(("[vuex] duplicate namespace " + namespace + " for the namespaced module " + (path.join('/'))));
    }
    store._modulesNamespaceMap[namespace] = module;
  }

  // set state
  if (!isRoot && !hot) {
    var parentState = getNestedState(rootState, path.slice(0, -1));
    var moduleName = path[path.length - 1];
    store._withCommit(function () {
      if ((true)) {
        if (moduleName in parentState) {
          console.warn(
            ("[vuex] state field \"" + moduleName + "\" was overridden by a module with the same name at \"" + (path.join('.')) + "\"")
          );
        }
      }
      Vue.set(parentState, moduleName, module.state);
    });
  }

  var local = module.context = makeLocalContext(store, namespace, path);

  module.forEachMutation(function (mutation, key) {
    var namespacedType = namespace + key;
    registerMutation(store, namespacedType, mutation, local);
  });

  module.forEachAction(function (action, key) {
    var type = action.root ? key : namespace + key;
    var handler = action.handler || action;
    registerAction(store, type, handler, local);
  });

  module.forEachGetter(function (getter, key) {
    var namespacedType = namespace + key;
    registerGetter(store, namespacedType, getter, local);
  });

  module.forEachChild(function (child, key) {
    installModule(store, rootState, path.concat(key), child, hot);
  });
}

/**
 * make localized dispatch, commit, getters and state
 * if there is no namespace, just use root ones
 */
function makeLocalContext (store, namespace, path) {
  var noNamespace = namespace === '';

  var local = {
    dispatch: noNamespace ? store.dispatch : function (_type, _payload, _options) {
      var args = unifyObjectStyle(_type, _payload, _options);
      var payload = args.payload;
      var options = args.options;
      var type = args.type;

      if (!options || !options.root) {
        type = namespace + type;
        if (( true) && !store._actions[type]) {
          console.error(("[vuex] unknown local action type: " + (args.type) + ", global type: " + type));
          return
        }
      }

      return store.dispatch(type, payload)
    },

    commit: noNamespace ? store.commit : function (_type, _payload, _options) {
      var args = unifyObjectStyle(_type, _payload, _options);
      var payload = args.payload;
      var options = args.options;
      var type = args.type;

      if (!options || !options.root) {
        type = namespace + type;
        if (( true) && !store._mutations[type]) {
          console.error(("[vuex] unknown local mutation type: " + (args.type) + ", global type: " + type));
          return
        }
      }

      store.commit(type, payload, options);
    }
  };

  // getters and state object must be gotten lazily
  // because they will be changed by vm update
  Object.defineProperties(local, {
    getters: {
      get: noNamespace
        ? function () { return store.getters; }
        : function () { return makeLocalGetters(store, namespace); }
    },
    state: {
      get: function () { return getNestedState(store.state, path); }
    }
  });

  return local
}

function makeLocalGetters (store, namespace) {
  if (!store._makeLocalGettersCache[namespace]) {
    var gettersProxy = {};
    var splitPos = namespace.length;
    Object.keys(store.getters).forEach(function (type) {
      // skip if the target getter is not match this namespace
      if (type.slice(0, splitPos) !== namespace) { return }

      // extract local getter type
      var localType = type.slice(splitPos);

      // Add a port to the getters proxy.
      // Define as getter property because
      // we do not want to evaluate the getters in this time.
      Object.defineProperty(gettersProxy, localType, {
        get: function () { return store.getters[type]; },
        enumerable: true
      });
    });
    store._makeLocalGettersCache[namespace] = gettersProxy;
  }

  return store._makeLocalGettersCache[namespace]
}

function registerMutation (store, type, handler, local) {
  var entry = store._mutations[type] || (store._mutations[type] = []);
  entry.push(function wrappedMutationHandler (payload) {
    handler.call(store, local.state, payload);
  });
}

function registerAction (store, type, handler, local) {
  var entry = store._actions[type] || (store._actions[type] = []);
  entry.push(function wrappedActionHandler (payload) {
    var res = handler.call(store, {
      dispatch: local.dispatch,
      commit: local.commit,
      getters: local.getters,
      state: local.state,
      rootGetters: store.getters,
      rootState: store.state
    }, payload);
    if (!isPromise(res)) {
      res = Promise.resolve(res);
    }
    if (store._devtoolHook) {
      return res.catch(function (err) {
        store._devtoolHook.emit('vuex:error', err);
        throw err
      })
    } else {
      return res
    }
  });
}

function registerGetter (store, type, rawGetter, local) {
  if (store._wrappedGetters[type]) {
    if ((true)) {
      console.error(("[vuex] duplicate getter key: " + type));
    }
    return
  }
  store._wrappedGetters[type] = function wrappedGetter (store) {
    return rawGetter(
      local.state, // local state
      local.getters, // local getters
      store.state, // root state
      store.getters // root getters
    )
  };
}

function enableStrictMode (store) {
  store._vm.$watch(function () { return this._data.$$state }, function () {
    if ((true)) {
      assert(store._committing, "do not mutate vuex store state outside mutation handlers.");
    }
  }, { deep: true, sync: true });
}

function getNestedState (state, path) {
  return path.reduce(function (state, key) { return state[key]; }, state)
}

function unifyObjectStyle (type, payload, options) {
  if (isObject(type) && type.type) {
    options = payload;
    payload = type;
    type = type.type;
  }

  if ((true)) {
    assert(typeof type === 'string', ("expects string as the type, but found " + (typeof type) + "."));
  }

  return { type: type, payload: payload, options: options }
}

function install (_Vue) {
  if (Vue && _Vue === Vue) {
    if ((true)) {
      console.error(
        '[vuex] already installed. Vue.use(Vuex) should be called only once.'
      );
    }
    return
  }
  Vue = _Vue;
  applyMixin(Vue);
}

/**
 * Reduce the code which written in Vue.js for getting the state.
 * @param {String} [namespace] - Module's namespace
 * @param {Object|Array} states # Object's item can be a function which accept state and getters for param, you can do something for state and getters in it.
 * @param {Object}
 */
var mapState = normalizeNamespace(function (namespace, states) {
  var res = {};
  if (( true) && !isValidMap(states)) {
    console.error('[vuex] mapState: mapper parameter must be either an Array or an Object');
  }
  normalizeMap(states).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    res[key] = function mappedState () {
      var state = this.$store.state;
      var getters = this.$store.getters;
      if (namespace) {
        var module = getModuleByNamespace(this.$store, 'mapState', namespace);
        if (!module) {
          return
        }
        state = module.context.state;
        getters = module.context.getters;
      }
      return typeof val === 'function'
        ? val.call(this, state, getters)
        : state[val]
    };
    // mark vuex getter for devtools
    res[key].vuex = true;
  });
  return res
});

/**
 * Reduce the code which written in Vue.js for committing the mutation
 * @param {String} [namespace] - Module's namespace
 * @param {Object|Array} mutations # Object's item can be a function which accept `commit` function as the first param, it can accept another params. You can commit mutation and do any other things in this function. specially, You need to pass anthor params from the mapped function.
 * @return {Object}
 */
var mapMutations = normalizeNamespace(function (namespace, mutations) {
  var res = {};
  if (( true) && !isValidMap(mutations)) {
    console.error('[vuex] mapMutations: mapper parameter must be either an Array or an Object');
  }
  normalizeMap(mutations).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    res[key] = function mappedMutation () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      // Get the commit method from store
      var commit = this.$store.commit;
      if (namespace) {
        var module = getModuleByNamespace(this.$store, 'mapMutations', namespace);
        if (!module) {
          return
        }
        commit = module.context.commit;
      }
      return typeof val === 'function'
        ? val.apply(this, [commit].concat(args))
        : commit.apply(this.$store, [val].concat(args))
    };
  });
  return res
});

/**
 * Reduce the code which written in Vue.js for getting the getters
 * @param {String} [namespace] - Module's namespace
 * @param {Object|Array} getters
 * @return {Object}
 */
var mapGetters = normalizeNamespace(function (namespace, getters) {
  var res = {};
  if (( true) && !isValidMap(getters)) {
    console.error('[vuex] mapGetters: mapper parameter must be either an Array or an Object');
  }
  normalizeMap(getters).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    // The namespace has been mutated by normalizeNamespace
    val = namespace + val;
    res[key] = function mappedGetter () {
      if (namespace && !getModuleByNamespace(this.$store, 'mapGetters', namespace)) {
        return
      }
      if (( true) && !(val in this.$store.getters)) {
        console.error(("[vuex] unknown getter: " + val));
        return
      }
      return this.$store.getters[val]
    };
    // mark vuex getter for devtools
    res[key].vuex = true;
  });
  return res
});

/**
 * Reduce the code which written in Vue.js for dispatch the action
 * @param {String} [namespace] - Module's namespace
 * @param {Object|Array} actions # Object's item can be a function which accept `dispatch` function as the first param, it can accept anthor params. You can dispatch action and do any other things in this function. specially, You need to pass anthor params from the mapped function.
 * @return {Object}
 */
var mapActions = normalizeNamespace(function (namespace, actions) {
  var res = {};
  if (( true) && !isValidMap(actions)) {
    console.error('[vuex] mapActions: mapper parameter must be either an Array or an Object');
  }
  normalizeMap(actions).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    res[key] = function mappedAction () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      // get dispatch function from store
      var dispatch = this.$store.dispatch;
      if (namespace) {
        var module = getModuleByNamespace(this.$store, 'mapActions', namespace);
        if (!module) {
          return
        }
        dispatch = module.context.dispatch;
      }
      return typeof val === 'function'
        ? val.apply(this, [dispatch].concat(args))
        : dispatch.apply(this.$store, [val].concat(args))
    };
  });
  return res
});

/**
 * Rebinding namespace param for mapXXX function in special scoped, and return them by simple object
 * @param {String} namespace
 * @return {Object}
 */
var createNamespacedHelpers = function (namespace) { return ({
  mapState: mapState.bind(null, namespace),
  mapGetters: mapGetters.bind(null, namespace),
  mapMutations: mapMutations.bind(null, namespace),
  mapActions: mapActions.bind(null, namespace)
}); };

/**
 * Normalize the map
 * normalizeMap([1, 2, 3]) => [ { key: 1, val: 1 }, { key: 2, val: 2 }, { key: 3, val: 3 } ]
 * normalizeMap({a: 1, b: 2, c: 3}) => [ { key: 'a', val: 1 }, { key: 'b', val: 2 }, { key: 'c', val: 3 } ]
 * @param {Array|Object} map
 * @return {Object}
 */
function normalizeMap (map) {
  if (!isValidMap(map)) {
    return []
  }
  return Array.isArray(map)
    ? map.map(function (key) { return ({ key: key, val: key }); })
    : Object.keys(map).map(function (key) { return ({ key: key, val: map[key] }); })
}

/**
 * Validate whether given map is valid or not
 * @param {*} map
 * @return {Boolean}
 */
function isValidMap (map) {
  return Array.isArray(map) || isObject(map)
}

/**
 * Return a function expect two param contains namespace and map. it will normalize the namespace and then the param's function will handle the new namespace and the map.
 * @param {Function} fn
 * @return {Function}
 */
function normalizeNamespace (fn) {
  return function (namespace, map) {
    if (typeof namespace !== 'string') {
      map = namespace;
      namespace = '';
    } else if (namespace.charAt(namespace.length - 1) !== '/') {
      namespace += '/';
    }
    return fn(namespace, map)
  }
}

/**
 * Search a special module from store by namespace. if module not exist, print error message.
 * @param {Object} store
 * @param {String} helper
 * @param {String} namespace
 * @return {Object}
 */
function getModuleByNamespace (store, helper, namespace) {
  var module = store._modulesNamespaceMap[namespace];
  if (( true) && !module) {
    console.error(("[vuex] module namespace not found in " + helper + "(): " + namespace));
  }
  return module
}

// Credits: borrowed code from fcomb/redux-logger

function createLogger (ref) {
  if ( ref === void 0 ) ref = {};
  var collapsed = ref.collapsed; if ( collapsed === void 0 ) collapsed = true;
  var filter = ref.filter; if ( filter === void 0 ) filter = function (mutation, stateBefore, stateAfter) { return true; };
  var transformer = ref.transformer; if ( transformer === void 0 ) transformer = function (state) { return state; };
  var mutationTransformer = ref.mutationTransformer; if ( mutationTransformer === void 0 ) mutationTransformer = function (mut) { return mut; };
  var actionFilter = ref.actionFilter; if ( actionFilter === void 0 ) actionFilter = function (action, state) { return true; };
  var actionTransformer = ref.actionTransformer; if ( actionTransformer === void 0 ) actionTransformer = function (act) { return act; };
  var logMutations = ref.logMutations; if ( logMutations === void 0 ) logMutations = true;
  var logActions = ref.logActions; if ( logActions === void 0 ) logActions = true;
  var logger = ref.logger; if ( logger === void 0 ) logger = console;

  return function (store) {
    var prevState = deepCopy(store.state);

    if (typeof logger === 'undefined') {
      return
    }

    if (logMutations) {
      store.subscribe(function (mutation, state) {
        var nextState = deepCopy(state);

        if (filter(mutation, prevState, nextState)) {
          var formattedTime = getFormattedTime();
          var formattedMutation = mutationTransformer(mutation);
          var message = "mutation " + (mutation.type) + formattedTime;

          startMessage(logger, message, collapsed);
          logger.log('%c prev state', 'color: #9E9E9E; font-weight: bold', transformer(prevState));
          logger.log('%c mutation', 'color: #03A9F4; font-weight: bold', formattedMutation);
          logger.log('%c next state', 'color: #4CAF50; font-weight: bold', transformer(nextState));
          endMessage(logger);
        }

        prevState = nextState;
      });
    }

    if (logActions) {
      store.subscribeAction(function (action, state) {
        if (actionFilter(action, state)) {
          var formattedTime = getFormattedTime();
          var formattedAction = actionTransformer(action);
          var message = "action " + (action.type) + formattedTime;

          startMessage(logger, message, collapsed);
          logger.log('%c action', 'color: #03A9F4; font-weight: bold', formattedAction);
          endMessage(logger);
        }
      });
    }
  }
}

function startMessage (logger, message, collapsed) {
  var startMessage = collapsed
    ? logger.groupCollapsed
    : logger.group;

  // render
  try {
    startMessage.call(logger, message);
  } catch (e) {
    logger.log(message);
  }
}

function endMessage (logger) {
  try {
    logger.groupEnd();
  } catch (e) {
    logger.log('—— log end ——');
  }
}

function getFormattedTime () {
  var time = new Date();
  return (" @ " + (pad(time.getHours(), 2)) + ":" + (pad(time.getMinutes(), 2)) + ":" + (pad(time.getSeconds(), 2)) + "." + (pad(time.getMilliseconds(), 3)))
}

function repeat (str, times) {
  return (new Array(times + 1)).join(str)
}

function pad (num, maxLength) {
  return repeat('0', maxLength - num.toString().length) + num
}

var index_cjs = {
  Store: Store,
  install: install,
  version: '3.6.2',
  mapState: mapState,
  mapMutations: mapMutations,
  mapGetters: mapGetters,
  mapActions: mapActions,
  createNamespacedHelpers: createNamespacedHelpers,
  createLogger: createLogger
};

module.exports = index_cjs;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../webpack/buildin/global.js */ 2)))

/***/ }),
/* 14 */
/*!***********************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/store/modules/total.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0;var total = {
  namespaced: true, //开启命名空间
  state: function state() {
    return {
      total: 0 };

  },
  mutations: {},


  actions: {} };var _default =




total;exports.default = _default;

/***/ }),
/* 15 */
/*!**********************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/store/modules/user.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0;var _regenerator = _interopRequireDefault(__webpack_require__(/*! ./node_modules/@babel/runtime/regenerator */ 16));var _login = __webpack_require__(/*! ../../api/login */ 19);function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {try {var info = gen[key](arg);var value = info.value;} catch (error) {reject(error);return;}if (info.done) {resolve(value);} else {Promise.resolve(value).then(_next, _throw);}}function _asyncToGenerator(fn) {return function () {var self = this,args = arguments;return new Promise(function (resolve, reject) {var gen = fn.apply(self, args);function _next(value) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);}function _throw(err) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);}_next(undefined);});};}

var user = {
  namespaced: true, //开启命名空间
  state: function state() {
    return {
      token: uni.getStorageSync('token') || '',
      userInfo: uni.getStorageSync('userInfo') || {} };

  },
  mutations: {
    changeInfo: function changeInfo(state, payload) {
      state.token = payload.token;
      state.userInfo = payload.userInfo;
      this.commit('user/savestoreage');
    },
    savestoreage: function savestoreage(state) {
      uni.setStorageSync('token', state.token);
      uni.setStorageSync('userInfo', state.userInfo);
    } },

  actions: {
    getToken: function getToken(_ref, _ref2) {return _asyncToGenerator( /*#__PURE__*/_regenerator.default.mark(function _callee() {var state, commit, code, userInfo, res;return _regenerator.default.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:state = _ref.state, commit = _ref.commit;code = _ref2.code, userInfo = _ref2.userInfo;_context.next = 4;return (
                  (0, _login.login)({
                    principal: code }));case 4:res = _context.sent;

                console.log(res);
                commit('changeInfo', { token: res.access_token, userInfo: userInfo });case 7:case "end":return _context.stop();}}}, _callee);}))();
    },
    islogin: function islogin(_ref3) {return _asyncToGenerator( /*#__PURE__*/_regenerator.default.mark(function _callee2() {var state;return _regenerator.default.wrap(function _callee2$(_context2) {while (1) {switch (_context2.prev = _context2.next) {case 0:state = _ref3.state;
                if (state.token) {

                }case 2:case "end":return _context2.stop();}}}, _callee2);}))();
    } } };var _default =



user;exports.default = _default;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"]))

/***/ }),
/* 16 */
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ 17);

/***/ }),
/* 17 */
/*!************************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime-module.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// This method of obtaining a reference to the global object needs to be
// kept identical to the way it is obtained in runtime.js
var g = (function() {
  return this || (typeof self === "object" && self);
})() || Function("return this")();

// Use `getOwnPropertyNames` because not all browsers support calling
// `hasOwnProperty` on the global `self` object in a worker. See #183.
var hadRuntime = g.regeneratorRuntime &&
  Object.getOwnPropertyNames(g).indexOf("regeneratorRuntime") >= 0;

// Save the old regeneratorRuntime in case it needs to be restored later.
var oldRuntime = hadRuntime && g.regeneratorRuntime;

// Force reevalutation of runtime.js.
g.regeneratorRuntime = undefined;

module.exports = __webpack_require__(/*! ./runtime */ 18);

if (hadRuntime) {
  // Restore the original runtime.
  g.regeneratorRuntime = oldRuntime;
} else {
  // Remove the global property added by runtime.js.
  try {
    delete g.regeneratorRuntime;
  } catch(e) {
    g.regeneratorRuntime = undefined;
  }
}


/***/ }),
/* 18 */
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

!(function(global) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  var inModule = typeof module === "object";
  var runtime = global.regeneratorRuntime;
  if (runtime) {
    if (inModule) {
      // If regeneratorRuntime is defined globally and we're in a module,
      // make the exports object identical to regeneratorRuntime.
      module.exports = runtime;
    }
    // Don't bother evaluating the rest of this file if the runtime was
    // already defined globally.
    return;
  }

  // Define the runtime globally (as expected by generated code) as either
  // module.exports (if we're in a module) or a new, empty object.
  runtime = global.regeneratorRuntime = inModule ? module.exports : {};

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  runtime.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  runtime.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  runtime.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  runtime.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return Promise.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return Promise.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new Promise(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  runtime.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  runtime.async = function(innerFn, outerFn, self, tryLocsList) {
    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList)
    );

    return runtime.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        if (delegate.iterator.return) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  runtime.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  runtime.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };
})(
  // In sloppy mode, unbound `this` refers to the global object, fallback to
  // Function constructor if we're in global strict mode. That is sadly a form
  // of indirect eval which violates Content Security Policy.
  (function() {
    return this || (typeof self === "object" && self);
  })() || Function("return this")()
);


/***/ }),
/* 19 */
/*!*************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/api/login.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.login = void 0;var _httprequest = _interopRequireDefault(__webpack_require__(/*! ../utils/httprequest */ 20));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}
//登录接口
var login = function login(params) {return _httprequest.default.post('/login?grant_type=mini_app', params);};exports.login = login;

/***/ }),
/* 20 */
/*!*********************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/utils/httprequest.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0;var _config = __webpack_require__(/*! ./config.js */ 21);function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}var

HttpRequest = /*#__PURE__*/function () {function HttpRequest() {_classCallCheck(this, HttpRequest);}_createClass(HttpRequest, [{ key: "request", value: function request(
    url, method, params) {//相当于es5 HttpRequest.prototype.request=function(){}
      uni.showLoading({
        title: '正在加载中...' });

      return new Promise(function (resolve, reject) {
        uni.request({
          url: _config.base_url + url, //仅为示例，并非真实接口地址。
          data: params,
          method: method,
          // header: {
          //     'custom-header': 'hello' //自定义请求头信息
          // },
          success: function success(res) {
            if (res.statusCode === 200) {
              resolve(res.data);
              uni.hideLoading();
            }

          },
          fail: function fail(err) {
            reject(err);
            uni.hideLoading();
            uni.showToast({
              title: '请求失败',
              duration: 2000 });

          },
          complete: function complete() {//调用成功、失败都会执行
            uni.hideLoading();
          } });

      });
    } }, { key: "get", value: function get(
    url, params) {
      console.log(params);
      return this.request(url, 'get', params);
    } }, { key: "post", value: function post(
    url, params) {
      return this.request(url, 'POST', params);
    } }]);return HttpRequest;}();var _default =


new HttpRequest();exports.default = _default;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"]))

/***/ }),
/* 21 */
/*!****************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/utils/config.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.base_url = void 0;var base_url = "https://bjwz.bwie.com/mall4j";exports.base_url = base_url;

/***/ }),
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */
/*!************************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/static/images/tabbar/basket-sel.png ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAYAAACMRWrdAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjk4RTBEQjNCOTdCRDExRTlCQ0JGRUMyNTY2NEMwRjgwIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjk4RTBEQjNDOTdCRDExRTlCQ0JGRUMyNTY2NEMwRjgwIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OThFMERCMzk5N0JEMTFFOUJDQkZFQzI1NjY0QzBGODAiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OThFMERCM0E5N0JEMTFFOUJDQkZFQzI1NjY0QzBGODAiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4bxN9IAAADa0lEQVR42uxaTWjUQBRObMDfFmS7PYiolaKC0FqrrT/gwYseBPEHPMiCHkTwB8VbPXnyJIiXXgUF6UUEQbSKCgoqbv8u9VCFQkH82d2iLlq0sdtv2Bd8LcnmJZs12ZCBj5lMJrPvm/fmzZuZ1UulkhbHtEiLaUqIJcSikpTzcIKhaQ+BKWBdvcnuRqxEOF5vxNxM8TvlzXGbYznK03EjVoirxixiqbgRy9erxgzhHNuIhlcc2kwBfaammWizBOUzQBN7n8W7B/Rje5HtCUj2l8BTv+tYL3P5lXCABM/YvFOEDXr/RdifBNMusotMUW0BJh3afFJaofJrYBRYyd4/Udqk8m3gCKBXoSk1LZaTpfiOPA6xEWqKyNx5RvLcq2aBzrFySwRIKU130eNgEO4+Ki6/jTmmoTgR62LlWBHrpnzCnD9NvBHDx3+Z92mJALFtEm1JN5r5KGjMKMvaGSSxqATCG4AVEo/olVgqQo4jGwSxrxGZYz2UfzD/bYBjpbEhSeO6IAbH0YBsi9QM60ljm4BlXogZHuZYAxqnTCK66+pAN/uxwA+ggOFXl/cV6Xk7qx8Jilh+QfRRAKkM8ls11tJ94OACYuMY2GLQpsjNMUejV8uUs3Ecb4M6GrAlBhN5BK2pI7nGGpFSodxHElDJ2CFdmEUbTWsPBMzS5u5kCB6xnW12d0tldzVFs2xyYYZVPUyLo0Edv0UhXrTm1zsM8s9aEUuFSGzQowmLkrWWdeKDE/+ZWLuXUMorMWst2wrcDCn6yHppLDVFtVjOhBhSjUkjDivplf4Ooes6V+1iZEtDIFVy2qZUlF1KzMWed9B5xA/gMQT57HE+bEa2nwZOaWaAnR5XXIN9L9AuAqXZyayF38BFISG1+F+zOZcfNgSXjb7voAVCvWDCqEv4P+z5qIDYadZ+Biiw5+dhEdvJhOhTMR2wCnhPdW8ExMao7QSwWp1EATdYvx1hEDvLBEiz+vOWBgTErBi0l9U1sn4zfolV8wcW7qlaWXkt5ZJ9kxUirWd1a1j5m39f6l9jynFM08hOApeA63TRp+r6BRrrp7az9O0FYJzqim5XVzUxRRLsnMNtY84oH3C6fd9Kbe36OBWK82DCHQNG2CjfMeabptv3bcBd4Bf1ofo6XK216ckfMRNiCbGEmJ80J8AAbGgFbuuSFqAAAAAASUVORK5CYII="

/***/ }),
/* 27 */,
/* 28 */,
/* 29 */
/*!***********************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/api/api.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.indexlastedprodpage = exports.indexnotice = exports.indexlist = exports.indexImgs = void 0;var _httprequest = _interopRequireDefault(__webpack_require__(/*! ../utils/httprequest */ 20));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

//首页轮播图接口
var indexImgs = function indexImgs(params) {return _httprequest.default.get("/indexImgs", params);};
//首页数据接口
exports.indexImgs = indexImgs;var indexlist = function indexlist(params) {return _httprequest.default.get("/prod/tagProdList", params);};
//首页更多数据接口
exports.indexlist = indexlist;var indexnotice = function indexnotice(params) {return _httprequest.default.get("/shop/notice/noticeList", params);};
//首页更多跳转
exports.indexnotice = indexnotice;var indexlastedprodpage = function indexlastedprodpage(params) {return _httprequest.default.get("/prod/prodListByTagId", params);};exports.indexlastedprodpage = indexlastedprodpage;

/***/ }),
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */
/*!****************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/api/classfiy.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.reCord = exports.classFiy = void 0;var _httprequest = _interopRequireDefault(__webpack_require__(/*! ../utils/httprequest */ 20));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}
//分类数据接口
var classFiy = function classFiy(params) {return _httprequest.default.get("/category/categoryInfo", params);};

//分类详情接口
exports.classFiy = classFiy;var reCord = function reCord(categoryId) {return _httprequest.default.get("/prod/pageProd", { categoryId: categoryId });};exports.reCord = reCord;

/***/ }),
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */
/*!*****************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/static/images/icon/toPay.png ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGCAIAAAD+THXTAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjMDBlMWI0NC0yNWE4LWExNDctYTcyNy04MmU1NmExNzQyYTEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Mzk0MkE1OEI5N0U0MTFFOUI4MEVFODkwNkRDNUEyNjMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6Mzk0MkE1OEE5N0U0MTFFOUI4MEVFODkwNkRDNUEyNjMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MGE5ZGQ2MTktOTEyZS1kNDRmLWJiZDYtNjI2OWY4ODVjZDAyIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOmMwMGUxYjQ0LTI1YTgtYTE0Ny1hNzI3LTgyZTU2YTE3NDJhMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PlfS84oAAAS7SURBVHja7JttaBtlHMCfy3vS65Km6UramtqXJb61zFUnFTs3h7ixIYN98AX9ICriYAgq4ge/iKigIOgHqYggCJbCxCEV54fNiZ17s91saffS6hqbrOmStZf33LtPdukRppfcJXeXpjx/jnAkl3ue3/N/ef7//yUYz/NgY4kBbDhBSAgJISEkhISQEBJCUiamslecmo8evRCaWlxNkYyeM7OaDHd7nfv62/f0eQ0YJv+LWIm0NUez7/84c+JSpLar3tfh+vDg1iaHRQXD+2Ad8ECZDhFvjE5SDFet4UF7O77G09/henGot7sFtxh18j2W58OrmZFzQWFNr0QSo+eDzw92VYX0/eSiyPPZsw+Y12DiWRq3mowGTGsqp9353oF+h8U49mdYmI9MJEMJdQsnLzzSI/JcXkrs//TkayMTulndyzt6hZPlRC6aJKtCEuPbHW6HcBJLkaf/inI8fzkSvxZLsZwe5bAHtzosBVMiMpQ6QVwQaM0vfX2WuxUesxT73Je/7+1re2f/fXpsnQptXK67Q9u7zX/gvlGvW60gMNz9/PpjP02HPz52CbeZvnt1B3ytbyRBLQOdzV6n/eFez7rlUYYkhIojh4ZQ2rr+tPTm6KTJWEvyDMWqjLRwM43qpXVveG/tvUd+Yq+FvPvDNCxz1ETa3pUP3LVcdYXpg6ThibmCmGLVvS8d2uX3NTfANNxpN28QX3p6eyc86jE8SGrp81+uPvHJCZjRbRykkbNBWDIdvbAos0qpA8Pj1jpHWZp1Kb8vw4MkAygOsJUWijA8mTDgMGqctpYVOP8bJAhlAUEDtYpehsdqhpRiwEwyrxy1pbA4yyTYUo0viS0U3CoLO0aBc4QWPEXZZgbMJssrXxLp8G6/v7Xx8O5Ao638vhSnwVQC6NBfuZ4DV1OVGt7BAR885AwDA8C0LjyCLGZBkxlstirX0rdnFp4aHh8+OVd2jGAG5Digp0BF8RUY3vCvc6HVzDenr8WzdIm7c7eWTWeBK3iDVG54YucxQzEl0jyCAjT//9vaPxEiR9GK5tpgs3S0uuTEbIjUatUmiMclQtzY+OzElXAFN9x5f8+ugZ6Kx1Whqs1J1GZLN5MVxrRYoppxVdCSVFx4/MEtxyfmGUZZ3LCYjTu3dWteqFcm3e3N8EDtFI2RxCe+JMPVcH4wctJMwW9kthMlL/K6Ci2UU3PRUoar8dPA+VBM2E4gj6vRXlx6KEZ6NLBZOPnqt/nJ4IrUZRXUM/IlRqTHxgtldcDXUqylBulxJX8ksJKmnvliXHwWONDpvtOD/1fzNIctkZqkd6uJrKgiowF75cBgqxsXP/XZgR9XiATlzN+xt49cpFmutu4OTezJoXu3BdqL33yoCTSalCNBmb0e/+jY7NxyslY80H/2Dd7l97UUv+mxgK1O5YZXHHNmwvGpELGSJjmJi6MkllW7umiwWds8m7ra3Lc9UIUBCarIbqwCSY7QHPiDAGlWDyOE+mku2aLH1PrpO8WBi3GQ0LJQh/rp21SGR00kId9byOQrQlaDENhiAQEc2GTsGZjqf1CARhgh890VoY9XVWqD5fcftxl4bQCXnY1i6D8XCAkhISSEhJAQEkJSJP8KMABOMcwPsVj78QAAAABJRU5ErkJggg=="

/***/ }),
/* 56 */
/*!**********************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/static/images/icon/toDelivery.png ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGCAIAAAD+THXTAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjMDBlMWI0NC0yNWE4LWExNDctYTcyNy04MmU1NmExNzQyYTEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Mzk2NDc1NUQ5N0U0MTFFOUI4MEVFODkwNkRDNUEyNjMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6Mzk2NDc1NUM5N0U0MTFFOUI4MEVFODkwNkRDNUEyNjMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MGE5ZGQ2MTktOTEyZS1kNDRmLWJiZDYtNjI2OWY4ODVjZDAyIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOmMwMGUxYjQ0LTI1YTgtYTE0Ny1hNzI3LTgyZTU2YTE3NDJhMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PnhN5xoAAAfsSURBVHja7Jt7TFtVHMfv7RtKeZRHKZT3+zmey5Y9dOi24ATGpvOxhehiojGauJgYTdRkPv8wRhOnmTFOM93mI7AncxPdMHtEGG3Hm4E8RqG0vEsLfbf+yoW7m64t95YWxtLfX7ent+ecz/2d8z3fcy6gVqsVebiChjx04UPyIfmQfEhOgvHgd1GlNda2DJ+RDqnmDB/vySuK569hpPZh1Wmp7K8OhdFswUr+7lSsSSSd0VzXoagRD3Yr1cRyNoNWnhe9xgbe3YnZM1LZxRa5Rm+y+wp4PttXkCEMWhtIZov1Ws9otVgmuTvp8AaMpzCOvwbkYVyjPysdOnt7aEKjJ5YH+7Om5wxu8KwaEjhlSAik5Vr3qIXgm5l02mMZkZtSwo/W92BIVHlWAUmjM9W22hR5cGKWWC4M9tudLypbJ9KbzK+daBqemrPjAc2QDk7lRAcHcBgPCtIdxUyNxKbI0Dm8EEWQjcnhlfmiDUlhNBQdVesc8kC8U327sX8iLTLw2IsbVhnJYLLAYgIwHXIVsTzIjwk5gcxAfrASFzy9YxrggQvljG41Bx507rR0CBb+Ga2RWJ4dHbynIKYkQwAzBy90wQMBEoJd7MiKXAUkmO43/hs7LZE19k0QN8wcJn1HlhBgUgQ8u5+45oGB+kerHLuuyBOtKNLkrOF88xA8UbvhERfKrSyIKc2JCmA7aM41DwRMvzmDbeXNjw2JDwtYIaTbsilIS32X0mS5lxg6Dd2SErG3MCY/jo86+eHErB7nYdDQDyvX3a/XZxZHXTmJFC0XCR7epbaRGrGsf1xDLA8LYEPzFfkiuHDxc1h53jglxnnAZW9KDre7p0ep7hxRYXKyLV3gRSSQICC53C7XGszE8oI4PqQFkgMpcl0D8Lx+sqlvTIPzbE4Jv/82MBbYBYxbopx4DAlM/tUuJYyxlqFpYjlMEmgSJgxMGzL1kOQBYbjcNkJeGKghKVRaGNPnm4dx64UFyBeIGEgZCBrJqkjy2AlDLLmHtTQSKDJocY1UdrNnjKjImBmrLBDBIkMpz+R5iMIAT80DHg92LOekQwAzMq11aMZgvlKdgZR4cGEAV74lNcIDSId+ERMtjJ0Zc0NRKPEQU/RELllhWAIJXCZ+DYpctTEBN2MrwANTCOSUqjAsceh1aHs6ng3QNzodXTEeiLoOBbY8wMorCvH3DBIo8uGKHMb88jIwrnnleKNscm5leIg+FeatJ48mSzIiP3+mkDtvzMC2vXy8gTgayQR4cJwHcv5+eQ4ZHmgFa4jPZW1NE3gSCaIonv/V80WgOdgRIfRP7OTEw6FmvvmbBOd5rywbdJ/MD3Fh2JUbzaChHkaCgI3kt1Xro+a1YRZ6+avkn+5RMjy4ZmI8sByT9I11HSOYxpbnidwY6qTEESbo0ar1yRE8zBO9W9N8oXnYGzxEYShKCI1yS2PJ6n0ol/31gWIwJpil+PRi+4l/BzzOs0xhoIaEedMvni18ZHG+fnO1+8iVbqtHeYjCsDklwutImLX7qDIXX/tONQx8UtuGH8QtkwcCPD6+uLshDG7ul6Cvb5Vm8gPYP1zvhY8XW+RqremD3bkGs4XI8/aT2emJwtYZRGtGSL6y19ocgwITBlG8qGEKYdIQHgMJZyHBVOwk6vYfCVSLZV/WdWEpgjmmN1lwnoPbs2NjhVTrbeyQ1d7stG1YYsIO7CwgfgVgqQFICNM7WcIDdq8hXNbhsy0mi1U6OIXn8Olt2THUeSDEXQvCUJRuLwxqEyKeRhK5SKK/p+eSvb1IF4BgcBfPfYBn76PZmYlCN6oaHlMpJm1vk3j+7NQYxw6jbxbpnfUyEnbYcGR/MdgLUI69WzOzk4Tu1dPUuZCigrRomnNh6J9DxgzeP/RKFfCqX93SrbaMmpnu1aA3mlr75oUBRQvTl1iO7miQUL6rVHjmjTqLQZ+0MLFVWKXRUZ1IzT0jRpMZE4YgLsf1zTozMqr3/tHkpBExzXOc/FPaIxtPig59bnsek0FfvjA4DECKZHs5SzOL5/hDozYd7x2e+OmSRGcwURKGQC4HskTmJyqjN+VhYTAsvMNHHi9OwS7uKqaOXbilntOTWY7uCQO5Uw29xftI+OSBkVO2OROd75lyUv39+cbJGVd7Ychke/+CMACSRzrj+T+4Aap9Jbl0mq3mKbX2u3MN8vEZ58IgN5pszzwtNnxJYVg1JIjMBEFVaSGbadOeOZ3xx9qmPrnjvXATRWFYNSSIeGHIwbLiAD82tuz8fEnc3q+0u0emnB6dsm3jgwI4yaLQBwuJ5aiaSD7vpfL1/ECbLTNbrL9facGVwC5FhWkilMpxJxP1PlKgk+UthOcHVFFhgTYJsVrBaF+V9NoJA426MAQyvY/EZzmtiMthvbCrCBZf7GO9pPfCjU4wGbgwpMaGg1Wl1Fw4y/vuAUZCJAeRO3mBDzqxf2d+TX1b27yRu9Upm9UZxqcXTHVxRgzVtoScFZGHJC7CcD7EQdOfKsndkBWLfezoV2LCACMzKZpPtSE6uiJIbBqSxUNctAVflW5Mx+3FomOgJgwCNiLyWxHFWxjibCQrEHHdwS3rEiq2ZmEYdBoIQxQlnqxAb549OLWwJqRTbdtau4juwbHrLQOwvOYmk9oywpBO5i6dH28hYTFuQJR6ZNpoOyFajurwmDZ9Az1gkB6eqO/fSHxIPiQfkg/Jh+RD8mb8L8AAmp/01vPg2kEAAAAASUVORK5CYII="

/***/ }),
/* 57 */
/*!******************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/static/images/icon/toTake.png ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGCAIAAAD+THXTAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjMDBlMWI0NC0yNWE4LWExNDctYTcyNy04MmU1NmExNzQyYTEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Mzk2NDc1NjU5N0U0MTFFOUI4MEVFODkwNkRDNUEyNjMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6Mzk2NDc1NjQ5N0U0MTFFOUI4MEVFODkwNkRDNUEyNjMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MGE5ZGQ2MTktOTEyZS1kNDRmLWJiZDYtNjI2OWY4ODVjZDAyIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOmMwMGUxYjQ0LTI1YTgtYTE0Ny1hNzI3LTgyZTU2YTE3NDJhMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PkMmLGcAAAWRSURBVHja7JtrTFtVHMDP7fv9GlWgFOQxlYZXuzoVF5ewaOZjxk3RCBq/GT84ZWqcieJitsTMTZOF7ZOJX9TBdBko8UF8LZvZMoQyWB1jwBijFFsebWnp697eekphZEza2/bcCyP3n6Y5N7095/zO+b/Ouedi0WgUrC/hgHUnLBKLxCKxSHcQEowqTb8PPHrw14M/X14PSGQ0eqDd2tI5GiGjHVZ7GjXw1hQPHiE//uHSn1cc8cv6hwrvbKQgHmls6zs3NBm/fG1ryavVRbQjwVYv3nBNeALhCEn9X4YcZXmeKvE9sMb3TvZYRmfilw2P3V9rzk9vaKgi+cPEF2eGvu+xhQgy1Ta4HKztja0aqWClG3whYk9L92W7J2bcGPb+k4anKnRpzzYlJOdscPfxLpvLn6ZXxTBItdKvbn/4zeNdw5O+WG842L5nymtKszNRYB4Vk33nW0ucB/areqO2Mk+tkvAxgFFs474chVLM///B8gYbmrtHp+dgWcjjHNhZWV2izdAmkyOdsoxdmx9C2K3DL5gMuUpU/mDc5d9zwjI+P1hSIe+T56o2FWgyrzY5Uod1Il74aEc5Qh44M281d016Q7CsEPM/QzdYyZHiU6SWCB4szkLFAy1n9zd/ewJ4vOYjdeZirQxV5ZRsCX5vkAkxRE1Cz9bQ0j0XImD5Lrmoqd6cp5YgjG9Mh9ru0Zm9J3tgFIJlnVpytN4MqdA2wSgSzAw+bO2NR7YiraypzqySCJC3whzS6QHHvrY+gozt3kBP8PmLJrmIn+B+eCePk46yM5SJ/2K1N7Yu8JgKNEde2pSY56c+e82h317/qjNCRtciUqtlbH+7lZzfXXukRHuo1igRJNGOs4NOCHPJ5p70Btec4vXccB3u6I+Xt5VmN+4o43OTj+PNzcXoGrSlKV8oXni6Urf3CQPM9+hukXakbaV3z4UMMH/bXp6LMWK3tCPBaXnWmMdkqEgfaTaAv33CMjYzl3knsuSiT5+v0iHKIdJH6rW5+ic8SDrhC/kujEzvWnUko15jzFcPOryZd0KvkW7JeJmEAEkm4h2tfwCsPWF3W1kkFmkdxSWtXARz2RyVeJWR+mxuhHHp/LWpXSb9KiNV6dWJ4xI5v+9H5YF9tlpSotfaF5cRXAzgGTznRxyXYEccITAWAB48tdqcsW3KpUs3vpDi/hsC2QBgzCDdLoEIsHpThkkswz4Q4oMyOZBR7ikyj+cjQKcbMc/NmrtSqRkNEk6CHk/smyYhouCiBwRJmhUPOvF3v7OMTsWceCQKMjm2o5CK6h43quWJnDgeBf1eYFTSiQSd+D/jaJx4MOwbHJvabEjixKfDMfVT8ul04vAz5PRmOEVQ1ArJvfmUNtwnQnQiQSd+7OWYE4e268YBM+IKM+Ie/BHm8jcqbSFAIhk80EelKapIBMnoScQImX5ASI4kFnBjiYknkMaz9LSnwjnji5clYgF6pDJd7MhCEI98fX6EGSTLFZtnLpbD5mQpBDwueqSdixuLX/41fOyPq74gsdxpoltG4gR5tnfkx3MLe+ibS5dHKi6FBBajclz3g1O9pwcWzvXwuZwirUwiWBo8XwRNKkRESMeMF19U72Ldhle2m7Bb99BVfGBWoUCCVrS/fem0EgOyUZ9VW1Mh5C8Pm8VSUChBgRSXM1edzReuW8c9JG3nsOGM5GqVD5cVlBVn365iHAxs0QABBx3SgpoFCYc3OBu4JYzDZZ8tkCmPSMhXSkVi4YoJT5EUFEkQ2RIVtwuT/+kwjaqo5gOTitLyloNKYSoUQCOgkadSSXW5jiF8QQFWdN0PRvwoUyRoP/eIQaE0he0HDPk7F2ES2INgKgy8RGxpmJ7A+CPjgSwByBUBYYqahLGvkbBILBKLxCKxSCwSnfKfAAMAPXcrDfmLkd8AAAAASUVORK5CYII="

/***/ }),
/* 58 */
/*!*********************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/static/images/icon/toComment.png ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGCAIAAAD+THXTAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjMDBlMWI0NC0yNWE4LWExNDctYTcyNy04MmU1NmExNzQyYTEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Mzk3OTkyNUQ5N0U0MTFFOUI4MEVFODkwNkRDNUEyNjMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6Mzk3OTkyNUM5N0U0MTFFOUI4MEVFODkwNkRDNUEyNjMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MGE5ZGQ2MTktOTEyZS1kNDRmLWJiZDYtNjI2OWY4ODVjZDAyIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOmMwMGUxYjQ0LTI1YTgtYTE0Ny1hNzI3LTgyZTU2YTE3NDJhMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pl/M1pEAAAelSURBVHja7Jt7TFNXGMBv372lLe9XeRZ5g1QniqJOnXMqumy6zTg0Ol3mEpO9soe6zD80mVlmsrjN6cKifyxOzXQ6Z2bmcOhQERSGQ0WgxdIC1lIepfTd29t99dYLYh+0tJ1oT27g3NPL6fmd853vdS4Um82GPFmFijxxJYQUQgohhZBCSCGkpwdJrTdvOd743O5zh2s78GC5KZTAOUS17b27fr/VpzMRt/mC8E+XFQhjuBMSyYTh31W1/dIgH9XOoFE3zMlYO1NIo1ImElKbcmjHqaaOPt2wcFMoI6UuJ4G/rawgK543AZBg3EfqZD9USyxWnGhh0alvz8+alx23p7L1oriHfBJWaX1pxrpSIazb44vUozHuPH2zUd5PtkxNjdpalp8cySFuq9t6vvrztmrIRD4A++qz5YW5ifzHEelc873dZ5u1Roy4ZTNomxdkr5yWMmrH6ExYRbXkREMnKYcgk+Ul6W/OncSkU4OKhOE2o9nq9COdGdtb1VZ1+x7ZUpQc8eHivAQ+6qq3VqVmT2XLHZWWbEmJ4sCf5CWEOzEyVITDpPsT6bJEdfBSe6tC8z+GvuEoo6woaeOcjDGyuUM6Vi+H6XxMfILMON6+tdPDWHTfkSQ9QxsO1hJCD1vCaV/wmd5k3z+wE3zWXSDVJotdqlHo5RFzZbbiZsyhP18UJW0tK/DYoUvoo1dlBM/SyYL3F+VyXUwPqAQKBRnL5LkpVtxGaHann9ZIVNt/bTJarGdu3AWTEMlh+ujjXZP2wc8IDnPL0nxXPO0qbU27avwqs6lLfaFVieHOOyrNjH29JJ0gb5QP+L5KvVq7AclN4LuSKODZePAKjKMgKbxiXYnPPFUtyu0n/4XKclHSNhdyBVqUqNxTG8bribvZIZfFKmJeb3UPEvy+lfMtSgfbbaXrYThk0mCxBjC4iOYySa+HO469lMBnE5W4B5VxFg9DUWoMWhPmdMSLCwWwB252qcH2g7vg8wjWzhLK+nSwzqCEgoEEbvXq7y+9+3zOooLEUfoINO62MajUsVjSL1+bGoyoVhDh8GgG9OYdv91456f6jl7txA7U966ZPj8nnrwFF3v9gSsHLrZPYKR4PvvzlaJvyovJ0Br0G/h7Ui/X6o+bd4/Xyx+jdIogHBVEoiPjU2/9YnBn9l8Qq/XmoCG5HB84ID/WSA/XdZAhKo/N+GBRbrwnVXuoVnryn67hUBcCE4u1vOIyOmIuEsPRL16ZwmXTg4r0ybHGBtlwiLqsKGnzgqwIT/4VEQ6CEsuIHc4EWWwgtIjdLN9XmhYLfkV8r7J9YFpGrOfxURCUhuA2fyCRPDC4j5fkky6JxwIu36vFqWVFAhiHzIB0GhAz/tADQ3oTIA0izLYx78pODcUPSKRzBarPqxwVnUaBq+LinfoujfWR2Y3koVOyBFyUKYjxIuVgfeAaG/Fxq4dwlOltzm3r0oIZmXEKE2K1OY0j8IRo3kfl86g+pfIURkRtGd8q+RJ+xvOuDSCzRRmz3QSeFB9TkzBLTRpkZiTCpAYLyYThO8/cVps9b+c5ImFsRJgPXwE7U6JD8nkB8MSdFgiEcRqDzaLDBer7uvgui2mv602WZulDsYMFs/r8LSB+JjxYq2Sw0RbOyCbq3arBpnZF2Sy7f90i6+np166YV+hthxYMvy7urrkhGyV+ShOSigYFqW/E3lWpdTwOy+eujGas7pYcLp1x2PPgP+iw3xwspCFsREZBokiJd27Nqq9LM5Oj3ehxWJx9J2oGtUayBRTvMznJoiwBcavBgiV4pFWta5a3d/e9UVbs9DGQJVDibpDUWsMonlULRblpcY9+UcCRMBsiV6qv3JSBMpiRnyIURJH+rtmC9Wv09gpmHdKZ2G7d35iIsOyU2LZOFZkVO1J5PV8Yv+LZQqanCNrfSJj14OmrTAb9hZLs0snpwx59LB+U4dc/XyJumXRaZlK0m37AbK1ZPFXS1ft34x2YI6IRpilDEDU9LyWoSHQ6bdPLM2GOYdAj27ko671Vc1VqLZHcjeJz+GGekyeZyTFwdSj6/6qXEGBag+cgxf8nVrBDRvEQhcNmpCVEpidGwTUWHrLA8wuLM/0WAnpbAngEO34k306IGYFhGnoQFMNSe5w7l0gp9w8kuwb0XmW82TT/84BeqW9xhMnRfMcxKUrzXj08mxPXUSOV9mp3nGrKEzx0Pjc9PRriQlCslc2KQYOFyJDNzbIbDS4VP9umAMPvLx7QJq3yng6FPbuPsuy70RF3MVwLv6vzJRjrmorLA87SIMIY7qG3Sg/VSvefF5ONu1aK5uXEf3tefLRWGqBNsrgkp3RyGlEX8ZFYlpeCRyRB+aiT2dDcX5mRZ+NQeu7fqseR73dfwHDPKkx1iDcViWH5ZJfyBeFHNs0+3tB5q1tN5olgYV+amgy/Vs9IA7EjTgGjwlhLChOhsq5U2Ks39xqsfoSJ4KJFmYmTRpjmSWHu1ENAXrgR6xCZPlA6GuRNxA+uqbVb/TAkgRUQnggGUujpRZ0Avukl1SNSHYL7r8NkFMkOQzzaS0pA3+aHPdVlRHpNiN6K+Pw1YIKiGHYe3tgcUkroHxRCSCGkEFIIKYQUQgohPe1I/wkwAN3FHxzBiMH5AAAAAElFTkSuQmCC"

/***/ }),
/* 59 */
/*!******************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/static/images/icon/myAddr.png ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR/mAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjMDBlMWI0NC0yNWE4LWExNDctYTcyNy04MmU1NmExNzQyYTEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDA5Q0Y1NjE5N0U2MTFFOUIwQ0JCMTRGODY3ODUzMDMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDA5Q0Y1NjA5N0U2MTFFOUIwQ0JCMTRGODY3ODUzMDMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MGE5ZGQ2MTktOTEyZS1kNDRmLWJiZDYtNjI2OWY4ODVjZDAyIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOmMwMGUxYjQ0LTI1YTgtYTE0Ny1hNzI3LTgyZTU2YTE3NDJhMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pk1aF2UAAAe1SURBVHja7FkJTFRXFP3DMMO+KCAoBREFtaAoSsSIUJeKiBWpW6MSrLihiBqtVaviUqsIbnUXt1htFNdqXVBRUzWCoFjApaJhIgLCAAPDOsPA7xk++Qz//9kUjWn68vNz8+59953/3n3n3jfDI0mS+PyaAfFZtv8urOrq6qKiopKSkrq6Oh2HPHv61EQgSDx1Sq0F+V4tLy8vLjY2aMQIB1tbQ4KgHgGP17Vz5+8mTDh6+HB5ebm6sdeTkqaHh8N+VGDg2TNnOG30hoX5xoeGAgGNhvOxNDVdtGCBWCxmewgcPpw28+zZs21gXTh/nnbat3fvBfPn79uz59zZs/juPbt2Rc2d69GjB21g167diePHGR6w3YcSEqBdumRJYWHhB8GSyWQ3rl8vKyuTSqWzZsxYuWLFq5wcdcZPMjLCJk+mV3RhdLRCoVA1KC0tnRcZ+c+LF+o8EDpiQhxggtAxY3Rf15QHD7p360Yhmz5tWmNjo+5jdYIVPnUq5T06KkrfQBwaEECN3bB+fVvC+u3YMcovJsCy6RuLoA9fHx8MFxoYpKaktBksRAacdurQ4d27d5wG9fX1+fn5CN6GhgZ1bILYhxMfb28dt1ILLPDe1vj4gwcOwDVbCzSRs2fbWFlRywnoy5YulUgkbMv9e/dSNn9cuNAGsNy7doUvIGOrMh4/drS3ZzNWDzc3kUjEMJbL5V2cnKAdHRT0obCyMjOpmU6dPMlQVVZWUtMgYsAXIDPYjBs7lrLHZrE3dG1MDGWvIQHoBAs8SaUU0BVDFb95M4UAeU21f/3atVT/6cRExpD0tDRKdeXy5Q+CNX/ePHjBPrJVAX5+UA3x92f0gzZdnZ2hAqewTwbSM1Rb4uK0wtJUQRTk5+Pt7OzMVuU3qfr178/o5/P5VOfbt28ZKkNDw06Ojpwq/Qqb2tpavM3MzNgqCwsLvN8VFrJV4BHagNHMzc3x1qX+0QTLxsaGnobR/AYPxhuRLsrNVe1HwsFDGzARN32Gra2t9opMwwbHbtyIUDAVCmtqahgqZFkjPh/ari4uSOE4d6AAhLm9jQ06wWQoE9gUqO406BfyD1NTNTjavXMnzVWAbozYaZJBAZycGbNqFbT4GDZi/WAhUbi5usLX10OHchqAHb5wcFDlUpQMt5KTWxKighTVkJkV5F8FNR3slNw7LDgkt5qU1n8Yy1P7aNgUNJwGiN9rV6/u2LYNi3fn9m26riqVk+kS8kZx8zNnTRzlJzYxiepJLSOL1ed9nuZ7Ihi5m4tLRUXFID+/O3fv6nJ9aCCJF1VEocppk4iLIga41VZVdvcesP1aiqqxrZDwsCAEBnrefKytrRctXgzh/r17ly5e1IpJ3kiklbfChHZ43Y/ABGHWui0M+xI58bCcqGlg+uFpvVXjGH7p7g7+dHJyepKdbWlpqc5SQRLp5USVolVn6vU/10z9BsKwCWFLdh/jHGhsQPi0I4wM9Lknmpqabt2xAwJqm5+WL9dg+bySiamiVLx9YQQEy/a2M1lLRbe6RiJbSpD6Xl+/HTduTEgIBCTv5Js3OW2wHUUyJiPGR4WXlxRDnh+/z8rGTsMUknqioE7/W/W+hAR7e3sI08LCiouL2Qavq5k9J+LXpidfhTAybKbf6HFap8itJhr1hWVnZ3fwyBEqFwEZIyIr6olKVkj9Hr8OgkvPXpEbdnD6LCsqVPWDrSyRccECEXh5enp7eUFgexkZFBQVHQ0B2QZ1lapKLG9lKXqetTlyCqY0s7JedfSc0NiE7S3x101TenXasyyK008rWIgbZC4UpTkvX3J+X2xc3ABfXwiAhQyjulqqa7B6cnBNpdSAz1+RcKpTl27cjNgUc9Qu001azwUr7eFDqjDy8PTk9CUUCk+fO+fg4EAFWXZWFr3+zb/eSCuASZyfB3nupl3eX41QFxXufXyUIfEmt1JS1kJGDVywkJvx7tO3r4mJiTp3HTt2BDKBQFBVVTUmOLigoACd9U2w5LK6deFjX2dlQA6dsyg4fI6GYHXr01xC5mQ+ajm8nCH/KD0db9+BAzWHPwwOHDpEMVnI6NEIRD6PUMjlv8yYmHn/Dvr9x06aEROn2Ymjq5u5dTsIr/5+pKUMRLlNV3+a29SwsJimqH+SkTE+NJSsrfo5Ynxq0iVlJT0k8IddxxBYWus8C+v2FOXSnTTRt4LlHxBANN0KcTnRimzl6tXfRygZHIXDpP5uFKae/Qfi6BkKhVpSZ13t/pULC0WvIbt6eLXU4oZcORHHcJCvL4IGMqJ+sL8/6ErzF589c+b5s2fNRXZHRyQ+gZGRpvpCoSjKEz26lSQtK1FGmFe/LZfvC4TNQ7qbE04mXEUz7sq9PTw0/9LXVo//qNALORK6JksWk7IG9fUWCvOrV65gH7F4uD3rchvAHUlgbCpr1CFd8HjWth2wSP4hE109+6hqnE0Id3OdCxsdG7ykSQip4j2HI9gHticMeW39uzwc9rLkqDN1/BW+t2ULpjb+u8CET3hb6Y3MgEf0siKsBHpWp/o2JJAsKbOgUNdQl2KNGZg+Ciwqzt7UEqKa5qTE2ZAYwAVdTJUCR0h8vH/IGklloSKWKc9BbYMSKwAY85WcaSMk7I1aBdOng/X/P2Sfqv0rwADndQ4gzKtjbwAAAABJRU5ErkJggg=="

/***/ }),
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */
/*!****************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/static/images/icon/logo.png ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAS8AAAE2CAYAAAAwBxSLAAAMbWlDQ1BJQ0MgUHJvZmlsZQAASImVVwdYU8kWnluSkJDQAghICb0J0gkgJYQWQHoRbIQkkFBiTAgqdnRRwbWLKFZ0VUSxrYCIotiVRbH3xYKKsi7qYkPlTUhA133le+f75t4/Z878p9yZ3HsA0PzAlUjyUC0A8sUF0oTwYMaYtHQGqRNgAAEocAPWXJ5MwoqLiwZQBu9/l3c3oC2Uq04Krn/O/1fR4QtkPACQcRBn8mW8fIiPA4Cv50mkBQAQFXrLKQUSBZ4Dsa4UBgjxKgXOVuKdCpypxE0DNkkJbIgvA6BG5XKl2QBo3IN6RiEvG/JofIbYRcwXiQHQHAFxAE/I5UOsiH1Efv4kBa6A2A7aSyCG8QBm5nec2X/jzxzi53Kzh7AyrwFRCxHJJHncaf9naf635OfJB33YwEEVSiMSFPnDGt7KnRSlwFSIu8WZMbGKWkP8QcRX1h0AlCKURyQr7VFjnowN6wf0IXbhc0OiIDaGOEycFxOt0mdmicI4EMPdgk4VFXCSIDaAeKFAFpqostksnZSg8oXWZUnZLJX+HFc64Ffh64E8N5ml4n8jFHBU/JhGkTApFWIKxFaFopQYiDUgdpblJkapbEYVCdkxgzZSeYIifiuIEwTi8GAlP1aYJQ1LUNmX5ssG88U2C0WcGBU+UCBMilDWBzvF4w7ED3PBLgvErORBHoFsTPRgLnxBSKgyd+y5QJycqOL5ICkITlCuxSmSvDiVPW4hyAtX6C0g9pAVJqrW4ikFcHMq+fEsSUFckjJOvCiHGxmnjAdfBqIBG4QABpDDkQkmgRwgauuu74a/lDNhgAukIBsIgJNKM7gidWBGDK+JoAj8AZEAyIbWBQ/MCkAh1H8Z0iqvTiBrYLZwYEUueApxPogCefC3fGCVeMhbCngCNaJ/eOfCwYPx5sGhmP/3+kHtNw0LaqJVGvmgR4bmoCUxlBhCjCCGEe1xIzwA98Oj4TUIDjecifsM5vHNnvCU0E54RLhO6CDcnigqlv4Q5WjQAfnDVLXI/L4WuA3k9MSDcX/IDplxfdwIOOEe0A8LD4SePaGWrYpbURXGD9x/y+C7p6GyI7uQUfIwchDZ7seVGg4ankMsilp/Xx9lrJlD9WYPzfzon/1d9fnwHvWjJbYQO4idxU5g57EmrB4wsGasAWvFjirw0O56MrC7Br0lDMSTC3lE//DHVflUVFLmUuPS5fJZOVcgmFqgOHjsSZJpUlG2sIDBgm8HAYMj5jmPYLi5uLkCoHjXKP++3sYPvEMQ/dZvunm/A+Df3N/ff+SbLrIZgP3e8Pgf/qazYwKgrQ7AucM8ubRQqcMVFwL8l9CEJ80QmAJLYAfzcQNewA8EgVAQCWJBEkgDE2CVhXCfS8EUMAPMBSWgDCwDq8E6sAlsBTvBHnAA1IMmcAKcARfBZXAd3IW7pxO8BD3gHehDEISE0BA6YoiYIdaII+KGMJEAJBSJRhKQNCQDyUbEiByZgcxDypAVyDpkC1KN7EcOIyeQ80g7cht5iHQhb5BPKIZSUV3UBLVBR6JMlIVGoUnoeDQbnYwWofPRJWgFWoXuRuvQE+hF9Dragb5EezGAqWP6mDnmhDExNhaLpWNZmBSbhZVi5VgVVos1wud8FevAurGPOBGn4wzcCe7gCDwZ5+GT8Vn4YnwdvhOvw0/hV/GHeA/+lUAjGBMcCb4EDmEMIZswhVBCKCdsJxwinIZnqZPwjkgk6hNtid7wLKYRc4jTiYuJG4h7iceJ7cTHxF4SiWRIciT5k2JJXFIBqYS0lrSb1Ey6QuokfVBTVzNTc1MLU0tXE6sVq5Wr7VI7pnZF7ZlaH1mLbE32JceS+eRp5KXkbeRG8iVyJ7mPok2xpfhTkig5lLmUCkot5TTlHuWturq6hbqPery6SH2OeoX6PvVz6g/VP1J1qA5UNnUcVU5dQt1BPU69TX1Lo9FsaEG0dFoBbQmtmnaS9oD2QYOu4azB0eBrzNao1KjTuKLxSpOsaa3J0pygWaRZrnlQ85JmtxZZy0aLrcXVmqVVqXVY66ZWrzZd21U7Vjtfe7H2Lu3z2s91SDo2OqE6fJ35Olt1Tuo8pmN0SzqbzqPPo2+jn6Z36hJ1bXU5ujm6Zbp7dNt0e/R09Dz0UvSm6lXqHdXr0Mf0bfQ5+nn6S/UP6N/Q/zTMZBhrmGDYomG1w64Me28w3CDIQGBQarDX4LrBJ0OGYahhruFyw3rD+0a4kYNRvNEUo41Gp426h+sO9xvOG146/MDwO8aosYNxgvF0463Grca9JqYm4SYSk7UmJ026TfVNg0xzTFeZHjPtMqObBZiJzFaZNZu9YOgxWIw8RgXjFKPH3Ng8wlxuvsW8zbzPwtYi2aLYYq/FfUuKJdMyy3KVZYtlj5WZ1WirGVY1VnesydZMa6H1Guuz1u9tbG1SbRbY1Ns8tzWw5dgW2dbY3rOj2QXaTbarsrtmT7Rn2ufab7C/7IA6eDoIHSodLjmijl6OIscNju0jCCN8RohHVI246UR1YjkVOtU4PXTWd452Lnaud3410mpk+sjlI8+O/Ori6ZLnss3lrquOa6RrsWuj6xs3BzeeW6XbNXeae5j7bPcG99cejh4Cj40etzzpnqM9F3i2eH7x8vaSetV6dXlbeWd4r/e+ydRlxjEXM8/5EHyCfWb7NPl89PXyLfA94Punn5Nfrt8uv+ejbEcJRm0b9djfwp/rv8W/I4ARkBGwOaAj0DyQG1gV+CjIMogftD3oGcuelcPazXoV7BIsDT4U/J7ty57JPh6ChYSHlIa0heqEJoeuC30QZhGWHVYT1hPuGT49/HgEISIqYnnETY4Jh8ep5vREekfOjDwVRY1KjFoX9SjaIVoa3TgaHR05euXoezHWMeKY+lgQy4ldGXs/zjZuctyReGJ8XHxl/NME14QZCWcT6YkTE3clvksKTlqadDfZLlme3JKimTIupTrlfWpI6orUjjEjx8wcczHNKE2U1pBOSk9J357eOzZ07OqxneM8x5WMuzHedvzU8ecnGE3Im3B0ouZE7sSDGYSM1IxdGZ+5sdwqbm8mJ3N9Zg+PzVvDe8kP4q/idwn8BSsEz7L8s1ZkPc/2z16Z3SUMFJYLu0Vs0TrR65yInE0573Njc3fk9uel5u3NV8vPyD8s1hHnik9NMp00dVK7xFFSIumY7Dt59eQeaZR0uwyRjZc1FOjCj/pWuZ38J/nDwoDCysIPU1KmHJyqPVU8tXWaw7RF054VhRX9Mh2fzpveMsN8xtwZD2eyZm6ZhczKnNUy23L2/Nmdc8Ln7JxLmZs797dil+IVxX/NS53XON9k/pz5j38K/6mmRKNEWnJzgd+CTQvxhaKFbYvcF61d9LWUX3qhzKWsvOzzYt7iCz+7/lzxc/+SrCVtS72WblxGXCZedmN54PKdK7RXFK14vHL0yrpVjFWlq/5aPXH1+XKP8k1rKGvkazoqoisa1lqtXbb28zrhuuuVwZV71xuvX7T+/Qb+hisbgzbWbjLZVLbp02bR5ltbwrfUVdlUlW8lbi3c+nRbyrazvzB/qd5utL1s+5cd4h0dOxN2nqr2rq7eZbxraQ1aI6/p2j1u9+U9IXsaap1qt+zV31u2D+yT73uxP2P/jQNRB1oOMg/W/mr96/pD9EOldUjdtLqeemF9R0NaQ/vhyMMtjX6Nh444H9nRZN5UeVTv6NJjlGPzj/U3FzX3Hpcc7z6RfeJxy8SWuyfHnLx2Kv5U2+mo0+fOhJ05eZZ1tvmc/7mm877nD19gXqi/6HWxrtWz9dBvnr8davNqq7vkfanhss/lxvZR7ceuBF45cTXk6plrnGsXr8dcb7+RfOPWzXE3O27xbz2/nXf79Z3CO31359wj3Cu9r3W//IHxg6rf7X/f2+HVcfRhyMPWR4mP7j7mPX75RPbkc+f8p7Sn5c/MnlU/d3ve1BXWdfnF2BedLyUv+7pL/tD+Y/0ru1e//hn0Z2vPmJ7O19LX/W8WvzV8u+Mvj79aeuN6H7zLf9f3vvSD4YedH5kfz35K/fSsb8pn0ueKL/ZfGr9Gfb3Xn9/fL+FKuQOfAhgcaFYWAG92AEBLA4AO+zbKWGUvOCCIsn8dQOA/YWW/OCBeANTC7/f4bvh1cxOAfdtg+wX5NWGvGkcDIMkHoO7uQ0Mlsix3NyUXFfYphAf9/W9hz0ZaCcCXZf39fVX9/V+2wmBh73hcrOxBFUKEPcPmUV8y8zPBvxFlf/pdjj/egSICD/Dj/V95rZC3htO7fgAAAHhlWElmTU0AKgAAAAgABQESAAMAAAABAAEAAAEaAAUAAAABAAAASgEbAAUAAAABAAAAUgEoAAMAAAABAAIAAIdpAAQAAAABAAAAWgAAAAAAAACQAAAAAQAAAJAAAAABAAKgAgAEAAAAAQAAAS+gAwAEAAAAAQAAATYAAAAABEGOCAAAAAlwSFlzAAAWJQAAFiUBSVIk8AAAAjppVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDYuMC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOnRpZmY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vdGlmZi8xLjAvIj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjM2ODwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj4zNDI8L2V4aWY6UGl4ZWxYRGltZW5zaW9uPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICAgICA8dGlmZjpSZXNvbHV0aW9uVW5pdD4yPC90aWZmOlJlc29sdXRpb25Vbml0PgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KZvmxXgAAQABJREFUeAHs3Wd/HD2SJfqSpSjKS4/r7pnZnd+98/Z+/8+yZnanux8nb+hEmXv+gQRZIiWRFCtLNAkpmWWzkIHAwYlAIHDpY8psKpMEJglMEjhjErh8xuo7VXeSwCSBSQIlgQm8JkWYJDBJ4ExKYAKvM9lsU6UnCUwSmMBr0oFJApMEzqQEJvA6k802VXqSwCSBCbwmHZgkMEngTEpgAq8z2WxTpScJTBKYwGvSgUkCkwTOpAQm8DqTzTZVepLAJIEJvCYdmCQwSeBMSuDqmaz1VOnPSqDWeWW114c8+JDzxzzw2Aowi8DqNc/9G16ff79/zvvD//pcf5JXq/huvTac+uv15lf+XPJe/WkPLg2P916/1N7OC97zrM7508+X673h+fD+5c+8fzkfrO+4Yvv/lZpNb51FCUzgdRZbbV+dG5g0UHofNHr37sNs532OnN/l/O59Xqvzh9l7jz+0194P772fe+77H/qRC+etXfBr4DYPiHsgB+XmMW23ih04AIwXPS+wuTTbA6I9cLoUWwAYAR/nK1cuza5euTy7kufOV/P8ynC+ejmvD+/395yvef9qO+cj7XfzN5ebyjmSwARe56QxC1hyL8DnbUBr6+27HO9n245372dvd3Le+VBn73v+1vN6D9C150AP0HVg+xCww9g+BbQA1QBwHdAAqMefKw2s5kALKwJOA0DVuT8GTnkMoAATILp+7cps5Vo7X796Jc/740/fW8l7K9d99srsxscr4Gp2bRcx1c0rUzkvEpjA64y3pC6J8zgHT4pZbQeYNrbfzTY2d2brOW/W8b6dB1Db8lqArUBuezgPYIex9QOQfQBmuTjW9iFUrNiZ1+cBLI8/B2C7wBVA8ngXqIATwBrYUz3GmDqTwpxyAK7VlavtCDDd8Hg438h59frw3kr73M13V2cfblyr37p65cPsyocr+Y054JrQ64xr/F71J/Dak8WpedTYDFAKIAGIoMJBc27v9QYm7TOAax1obeX4BLyAWAAMeAW4sLL2GHDtgZjvN+BqZyaoOjTwGh534HJO3T7umpYNwOYFyVTrZuKuSRiQAmLYVT83IBtMw4BWN/uuAyjANIDUjeEx4GpABrwacAG5mznWAl5rN/I4Z0zMtbvpuQuec0Bav53nzrtgm3ozW6dyeiVwKR1FH5nKKZGA5iigGFgNINkKoJT555yjTL4502/X5Bveb6wKSLXP9+8cMBv7NXLecd2Yjn6Pj2zXbBzq0UF0l22FgX0KXI0BFuLOyxJ4+ddBbPBpXQrj+hyQFJgVsDVwK/bFHOymYpjY9YGR9XN/j7lYJuMAbADO82u+49y/51pz5ucN3+ufzWeuzLPBCcDmW/NUPZ6Y16lqDn6j5rfqTnes6E0YVDvehk29C6uKSbi9M9vImUlYTGo4A7kGRA2Qmg+rO+z3QInTvgNUnbGrwTzsANUAS532TEKP89HhNQ9anduDzwgz788u+T4WM7A0QBa6hrF9CDjssZ0GcvOgtsuaujnJJ8a8DMB84rwfTE4+smsBpmsBKGeghKG1A0O7FkbWTE1nLO3W6t6BxV2rXhFwLcT9zD1NL50KCUzgdSqaYa8S+jrQADoc6gDq5frb2YvXW7Pnr7dnL/L41XC83Hg7e5PHzMONDWbi22JonOxmEMtHNQc8xZQKbPbAaA+IgFBez+/nNGBR+1yvnfeV+tse1rNPn9dHPvlTXwuAtYtfAmEFaMGuKsCrl/6wAC0vFn7kTz+HlB0Eu3p/AL485ujvvjTM62bA6ebNHKvXZ7dzvn3z+uzOWjvu3ro+u39rpeQVPMz3Wk38ztWPw5Neuel8qiQwgdeSmmPo92VqdWbzPi963MzEBjZlJoY9bQ+O9dcBpedvtgJe2w283mzPXgW0Xq3vFKi9yeNyzA9nJuXXfFBLut2DPzNg1wB9Azj2jxX89ScnOneQK/AL9jAZgdcaAHMGXAVe1wrAXq+vzNYL+AP+Od8KuNUEQXxs2BrTtHxiTFnmZM7dtC2G6AdThtOJ6j59+XgSmMDrePL6pk/vMZ5ZhSHwQTHvtnbe1blm/QYnevmr8nhzMAmxqv1mI5OxTMfhM4AOaGFae8C1CxPfVOez+qWCwf7nw6XMlIbFRtabQZcaKPj0PM+Exus3YbSr27OnL/bMRo7+1ZiSHP+rg3l5o4As5qczM7R8ZO2xiYACzAhscvAvV2sm8BpZ3hhXO5qvCLNiCr6JifcaMGFRGfGZglhWMal0rI34tjAqILYdkKuYrQH0emxWOdhjWpaTPc52Pqvun9pHbUa+y1N0ecA152Mjk7c72qAF774FXJHtbqxYfGPzDvuasRxYGqbGH3abiRm2xtzsZuftMObOwppxyRyeGNgyNWECryVIG3jxLXVfFubEj/U8fqxnrzLyv9qaPX2ZI2fmYZkxATXn7bCwCoUo87LN8DWz0zWb2dmZndeBlr/tzxJu7hT+BHkDMDJ4/x5wva9VB5cvvW/BsbH0usk3bwJeCYUSmsG0XIv56Hw3/rCHd2/MHty5UeeHOzeqHQHXSpiYGcxQrlkBWLMgT6FEzmeVJvBaULtWh8m1AEmftQM6WJGjhyEwATnenwWoGngBrQBYwOtJXnsVn1Y53wNcG2FnO2FWSneW15Ppz+ESaNhFcgVghWSHf6v5yCJ7rGt1FRt+WybmRtgwU9OBEddMb1gxXxq/WJ/dvGY2lG/M7GjOYtsyleCP/1NZoAQm8FqAMDvzAWBiroQviHCvc5mIlJ3vKuZiOoMO0czEPDZzmM7CCe8xXxa29S7XyYRhlQm4FtBIR7wE9kr229sNahIIGT9illzF3FzPJMmLDDSP1zbL2c+M5OAXbnFztYVdCL/gL2vhGA3YQsyaQz8PJgA7YkMc4WMTeB1BSF/7CMBqR/NpWTfIl/UyDOrFcFSIQ5TeuY3kzSHPfGwO+r3lORzvzYclDiodx8WnsjQJkDf5Z2qlzHVA1oArZn7MxJqJBFDlzOcPu1am5b3bK7tnoRf3Pqy0cI0AloiLWmmpKSf0WlhbTuC1AFHCl+BM+UKAF9OQ/+rPF5uzP59vzP58lvOzdn61vl2mYAWS1gxhWys4H1WvA2EAE24toHGOeQmyFyBs5vZyTPatrdls/fJOC49gClYQLDOxRe5jX4/ur85+6Mf2as0o86UBOtH/cYoN5iPsmtDrmE3yxY9P4PVF0Xz6RvGfAqkhLmuIRp/3aQkqZf4VYAW0/gBczwHX5uwx8MpjZmP5xASSmh2EelM5NRIwYDS2231lqhav/1DEj1UQbEAMQN0KeO0tv2ohK++iBwDQovY3malsWTFaNoxrV7LWsvxhrrMXxT9BWpfw0c8TeB1BVqXQcfw6A6vyZ3HcxjfFR1ULoMO2MC6+rB4NX+ZiTEUmJHPxbeKxAJbYowKtCbeOIP3T9xF60GZ2Z82kTNsGh6ptt7Oe1KqHZ5mA+f3p+uzu2ko59YVccO6vxTdmwfha+cWu1WxlA662QuD03e3prdEEXoe0DXwJ1BRwdX8IsHpe/qy2ZOc5f5bZw5yZhQ3MAFtz1PeAU4GpLZDU9dqVD/n56e1TJ4GmC7O4xUzUMP87m97KQCbw9dmLrYDURgHVnfi/7scfdi+hFs7tSLhFHsucYX1mOcKEdkxG5bFaewKvw8QVnYIzwMbsn9lEDEuoQ5mFMQf/cDx1rBfzEt7gc+3Mp9V8KBWvVRfzo0MnOOz3p/dPlQQ033zbiSHDpgHXq+4TK39YWxh+J2snf7x/c/bDg5uzHx+sJg3RzfKPig/Dxj5cE3M2B1yT/Xjk9p7Aa5+oKKcRlVngzG9RGUczwnLGvwyzAla/d9Aazn8Cr2frGYV3yiys75fTvbR9369MT8+yBDqAuQdMWiBsL3xiFQAbf9jlgNlrsXrRm1oVYUDL4/aduA+iH3fXhmj/IUWPCYHdwNlcK/+n8gUJTOC1TzAAaz5fFr9Wz+Lg3ANMn73crBlF5qKoeKYDptXTJk9m4T7BXpinjVHXPExAjU7QjasBM2Al7EIQMheDCRxhFbX8aFiCJASj5yWrRIoTen1Rcybw2ieaUjBO+cEhD5hqxrBmDzfLt1UBpgGy12FhmFbLq7VToOf7gMvoPHGufcK9AE+rzf2JHmRdeC0Kt8zLoCZo+XUCXQHXnQAX0Lp/+0bMytVmWuYMzDj1FT4xy5Cm8nkJTOC1Ty6deXXG9SSzRv94/Gb2f/94Pfuv319nGc9m+Tc44bcSZPr27bxPiyngggGvdtp39enpuZdAGv7jsK7SAMb3+WHjY/OJvcmGINe2ZiuZaZRux/nh3dXZv/x4qxbe05lOtJiP4sSm8mUJXFjpUCwFU+o75vBvYVE1kxjGxUTk3/rHn29m/3QExJ4HzMww9SBTI+pUJgl8IoGoRNOK5g97P+cUu3z53exqdOxqfFyYlX0FkCvpdACXiP7NOy2TCH0UXlFbuQXMKnNsMbF8sP3/5Gcv2pMLCV7YVZl10TD+rUpNE8eqWcSXprrjx6qF0xZLh2k9DsV/lmh5vgqgVRHYAb3JLrxo3eXk9wvUDJhmKJWtuCcMiFeDXO+iWxbmy2DxYMhkYdnR7QBYT8fDHwbk8r9oWp09voDlwoFXB60KfYgmiY42g/gksTmPA1RPAlICDJ/mzER8GfYlz5YgU9kEgFeFPAg0pYlTmSRwHAkY84BXAsXo4ObmbPY8CESv+FCfvlxpwHVvtaXgiVnJJ0bnZK9os5G14GgAsOP8+Pn67IUDL80HdBAnI2ADr7ez3+OQ/3tMQ1HRT/L4adjWkxxmivpmGLWrDqcWBawjf6YySeAYEqhBE+mKAn3IbKQ9MTEuGUUexzRkJj4AXGH/T3P+OfonThBwybvf/WBS7Vx09Dr34AVedmf/ojAAaD4rqQXUvz5ez8Gv9TrgtRHmlVnFMDFnjvmLVMokyZ/KAR9GUL6Y+FnKL1Pn9p7n/TNluvgOQdWfOYkBeU8j+9YWrT0MHo39iqkbHufFPZMesz2f7NZ91QAYsRhADY6z7SYzM5J2dnobP9lmQM1jecFqV6SAm5jDvqWbM79Z95dVe7TLXIi/5x+8oiiCAjk/30VROORrMwvLe2IS8mntBp0CrldhWxkF7SSNql+0ogNYMFyLhwczxcLiSptsS7Hhtb6z9W4q5HxPcGVOnxT9VAcFSuXr0Vm1xdAmLYCzbdMmCLgvaH7vcT5XHf2TK57vJ2UNJBbsTUIqlMpJkW3jyjcbvfwhbIwfrC81sk6SKdnbQ/tdlHIBwKtteqFjiHKW9YGJ+NuTsK0cnPEymlawaViYuC3AJc/WRZxJbODV0iFXNoRM1+8m10tHWc1o37IktI1b58FMBtH9m1AALWbPp2BlM1wrF9pu3S1Obm8PSoGcb9MD2+BxsQYQ92wNbKaSAuRhXiWnFugq2y7w+sujtdlWjgZU0lFfyeftDJ7TxcGu2QUAr4z0UQjAZZceAaaY1n/+9mr2v/75svJs1dZhccZLu6zjUCDAdTGZVwuOBFA3KkNo2yLMxhN8LnwytbNOJeZrm7rubvQaViYP/HyxvRt2VWs9w6R0TOEBmxkgKitH5N4mRHZmV9I2nTlgIDvJOX/RSmdeGCiXhY1YTBbZ4Wg1sv8xayTtOsXnJZi15QsLdIX1GkgkPbwo5dyBV5kpcyYKh7zGd0i3DLjEazn4uTCvGunTmQq40sHOcwEOu2vvoue1+3TW4GFNTI/rAaXKEpqOorMAK6DVd9Bp4GXn6WQVzWeN+g28LEQ+GBFuAGjgFQDr4BVZt1TZSa2cUAHO6rZzUts9abMGEoNJNiCpwaQxN+EFNbDkmv183sxK9zO/XrKvicROr0VOZGm3o36QyZ2bK9VGgM/2bNW+aWeAtm8sOVeqfe7Ai5mylxwuKZkDWvxaQiGcZTRlMkoOyLcFsNBzDX/eOsLnNJVi818BnWJXASGAJA+7PQv5UGrzieGxvQuBmdcLsFbyvfi+jPi+v2s25rq7yfXmfrjMxnQw7JcPq3xcGVDKbIzcTZ7Mm431GHhZnmX7t5wBXO1TmdeBXrVvvtdTDM393Pl7GKsZUF8KaCmsA7PhNvogS0uNHsWUrCOxYXeSP6zWRqZ9nQ1I57WcP/BKQ/OlVJLAjFZP44D/9c/1Xbb1OA3PUS+/vDAICQIBlxEMazvvBXgBLgDlkG/qQdbXPRQYeccIvjKAVQOslQGsPnXYt3TIHMXf7rBvbAobM4PGrHS2gS4AY1LaqYeZb3u4ChxOu0nsaK1g8wnJ0HC+W4xOcmFES2twde9PrmzWxMYrAdXxg/31B8uLBj2OLt9avV5CMbCcZyfYuQMvo5RRHXjZGxHjsjbxP+Pf+t//fFFBqNhWH705kjEuSnIhmFcGYowJcN0FWuUAvjn75eHa7C857gfEepZPjAtoccLvmZrNFNmdno+PpZsm3V+1H066XEvGiRGoDjnIvM9C+oyQCWZSZ2IA7FkmU357sjG7mfi7K6n3pYCvgi1vZanNeS9dLs7vc+sGWv4wkfja8cmL1UwuNcvh6pBimkwA180P5657f9LcZ/7uqlExJ0eAiIlh5tAeiGZnalYx4OUshst7Ipx1Gueczl1pYNI6OWbUMnY2nxTQAlD3Y2Lcu7uS2aubNXu1C16Zhu/moTN/1jIL09LECibhrL4tdCMsL3VhCpnx5PMRuImJ1NrUdGhnejAElp2btqXjXU93EtnanPkZNNI0Bt9a5B2Z2BTEZzFYn48bM/KI/PJA6Eux5D7SLLNRR/qtMw9elLXPXvGHMCs45f/Iphd1zogtcaC88hjXLmCdQ9CiI3QTA2rHLCbi1bZFPad7NouoGCHgFROxg1g3G3sEN39WLUP5Doqu3nJf8asp7zPL+e7das1imuWUMuZ5gNcg9PzVai3fqhRFJmTCtPnQdtt4YHl1oXPyp9S2/0nOHS4SLMzKEPfN1Bbuoy9gsfcir5odNsESJi1B4nkp5wO8MkpT3DITM3vYZxNlgrBWkW9AbnkNjXFVdLNTKcF5acp+HwNwBQC6f0v+dKmIpSF+lLVyQIt/y1ma4uasbw57/rAW9Jjtur6DnvtNv6+ov39MVCxQYOYP9wJcfJbAS5Bx2tckjIFKLB/2xbXNBG3te84a2e0k5c7HBK5SZGsiDdiAq4VV7AGXgZ1MAJhSEyxtTKjnZ/3P2Qev0OZiXOXfinM+I9Dfk3vr/yaO679+e12O3ooxotg5NLJyPoGrqSMA6D4qOaPuxbf1yw9rs3/7+c7s54c3W9YC4JXXxQpZftJDJup7uYBrfMmH1X5lnL9+M66b1D9mUBqJaViM6zbn/LBC4k0DrhcZlDAOZrFO+iI6wE8W6Bosx7T1OcMuUi/dHXKGMRFfvZY0IJNTcZMAMoyrzOfIsjAu3yFHu3sLZj0v5cyBF5vetHvFwuQsFMJMlNGXeYh18W95/vjFRrEujeV757UAmkBN/eej6snurH17mIwEvzyKXyszUn9N0jvgdS8zinfDuIzIPnOailsp0BzuqaxHfW4ofRKBn0eIB18O4DIBs5UDa7M6woYYm0kWyT9ULQ/HzpMOuJ/IpGLfwjYv7ZTACrQq9i7+r8uRRThasPxj+bswLyIoP2jeI7v9KyK6nM/C+cyBF+DCtNrxvoCrFlUHsJzt4iPwFKhRXOVcKe0+reoMSYc3E6dTc8bLCcUsFJFtOclfwrxqXVztI9jypGNZZ63obDVbmvtUOPj5Mt2JAM2Hd5IRJAykQiteXqo4sYrhi940U1KXP3+FjtMB8qgd29MHIFVfYuR1QLcRneAmMAg4X7569nSgt96ZAy+Mq8zEmAzMBHFbQiH+8cebMhd7GhtOyxp1z9No21vtkzMTbwhlyEh6I0opkd3ffri9y7T4uR7GV+RsmU8FMWYUNgKftQJwmUCZbtydVOAXwyCxSX4xmW99zmwlVlbshCeMLpxP7KpmBGB0fj0Dt3utNbrxB3vN+lL+XvIgowiw9KDFgp01LWj1PXvgVczrfQOuOGv5uDjm/+t3Pq5XGXG3q7GMNI5zX4I/GJdZJH4rzOvBndXZ3366Nft//+VeTMa1WjpyJzONgKsvHzmr0+adeTEPARYTyQG4sEyzqeQBuJ5GF2wArOjY8medY+yq+yzmlYGb6SyRpkBsA37F0+UT1poqBjCzy2e5nHrwonScj3WkEbqPC+MCXL9WAsFmKjIVzCxepAKEdGKghXWZVfwpfq2fBZ3Gz/VzOvT3jNtadFuEZFbYRGeNFcM0yKAc0nn/VdLJmH3GzHXaWiuZEILNMh3Pd0g+E/ltUPptw2zurtINkxo9wNfi+ZoICXsteZ7ROLBTD15Aaz6OC0DxbTEVMS6Bp6bLjbDv3p33cfUgFKD9d+N4Lx9XzEVMy240P8RRfydMC3B9z7itgzVe7Cu9810bNPl2lsY8iom8vnmrmPdaljdVSu/4gN5nZu4i+EHnJcxk1jfExSlm2wWiYLCYuvChsxoHdjbAay6OyyziP+Oc5+NiKnLOa5xK13wRzMRSwfaHr+tapuPuhv7/nBnFv/54uxzzouU551vQ6feN25qr7igPC7xiMqc7pkPOsq7vWvn2mE9X44wG3tfTSeXFwsYubSd8IGyM7JzPeyEH2TkUC9mBdwFX2BbTGzk4q3Fgpx+8uoM+iodhCYfAvMRyNR/XVjEujeS4KEXnUzAvi6uZif/+1zu1SLdYWGK4+Ll0Xsrq4/0750lG7g2VcL76MaEBq7OA1410zEsV18SklpUVcJmJVi4KcLlX4GRwB1xcKnxhu1lys5KiED+fO4txYKcOvAyG5VyM0Evw8VUIvDOLWM75ABcl9Jxz/qL5uGrUHJzzaD92JVoe08K4gBj2YbssHXfZaxN1mGWXArCG5eWI5vsy28iRz1n9/OV2MXTmteVD75KKAgMx2PERnefi/hwCtBXs9GmSFZi4sXekmVrxrs0HdjUz0GGw+VDpWc7GhtNaTh14Aa75fFxs9Z5AENuqXFwBrr6rz2kV7Fj1MmquxtFaTngO+oCVXE7CI3ROwOU9jIwSXrSis1WgbmbTFOzTOk4+wGcxrUWcM6N69twPpiAvUAHY+g4Ag0x2LxJWYQLEelKxcBVKE+B3Ps35wE4feGWUmM/HJTuERdYi5y354fMy41hxXBfITOz9y8hoZvFOQgLu5jCz+CgzjAJS+b4wLsDFF3YezcQuhy+dy3wMI72RKABAX+AVExrAW/uKf7xIECt/lzioZFS+UEUWCmbk7NJmu/847DtwGfSA/1nJB3bqwIupOJ+PS7R0MS+xXFmraMlPUf6B9l8ozcvNiueSHQDL+sFi6zAvObn4uTrzwrgAV5lTF0xA7hvz6h3y7VqCMsO8Hm2sztbj76k4J+w+wPV6/eIx08a8bDKTBd2JA+MLw7gAF3Nb/jbFAHja84F9d/DicTAK5n+dmYyYlQyoEgkyE/9IPBdn/Xwc1zJnioxGGar6/12fQMXNpOLB2/Ir8C2MVq/28zVDZGnHvXTInxLDJY7rYUxGvi9T3pbOXOSirQq4tVeKPPtSAZER3dJ53wbE5M2v2Kdq3FoBGAVcvORcvvuQ6Ita0Zd5naf7ymi60y5ff+moOLDuAwPy+pYAZu4IsuMn9DqfqXP+1+u9/nOX+64Pvz94peUIi1JZtwi45JqXulksl5lFM4wc87JDKMto5N4qvTNoVI2nMa9ldGKWGZ1SmTJzjWBW+LuXRRd1gJzOfpNf62FMIYusK0tEzEf555lJU/lUAmRiDd/9WzdqAghrt3eB9MnaELAUeACUIVPDp1c42TPX9zuVjiZ6Q49K18tyMCnVMp0AFW28LN32O+oCxF4nXdQfCfqm31w21kPGOxH3BPm0FEXYbOatdwNdTyaVxXz7FIDXp/sqFniFcfFxSd2MeUkkKB9XpbQpTVvMzR/tKoMJloZlslFEFFvD8j1Rgh5Hw5+w8Jzqc8BF2bAF2RTMMP4S8HLcrbzzba3f0e7p4nxKYkNM9f6HlXI+C5sAXL/zDQZMgEvhhj/t/0KFw3QHXKtJPdSZTU1IZbAz4F2Kz+090Iw3boyB72s3Q3eBub1MLz27VKYkczoiSWaStnxI3TnuFQM3gD0t5RSAVwQYxZGDyHo025NhXsBLznkR9LIGGBE61V2q8NJWRqS+dlCqXctw1kKz+Qg+thnoUgL77C26lLqoQzqBtL+YVwOvRNPH3/XLg7UKCegUf9G/f9avJ5yEOS0gEwMDEL9nQMRedUztOguhT1LSxsAWbDvSHSAJuG5l/WVNKMRsLcYX4YKtULH8dto3/WDZY7PkjUxoPjDJHcWBGZglqTTJQdcUwPUhA/dpckp8N/DqjYSt2HAB+lcgYUzEysUVGssW359zviQ58p8CigEwMB0Mq7EtjCvLbQbWpZEB6vWtNGo6h+8turimazN/nDnrK44rfhyzi3xdFEvn1CGm8qkEgAW2THYr1z6WvPjADD62c9OeNQFkcIxSdr389Crf/qy3Hz3CYAx+nb3v3GzpfKRurhRPOe/E9YARtXr087f//mHf/BCzdfst10ci8S/vFDt9/Fw6pY1a5E7v5H+DsurdQid0ju/Pwb4LeInl0jgOiG8rst8DVj2eq5mKyQiZ96zFqoasIeqwpjj5+03ZwrQCBBSegvd1g84YDqb4LpWqbexrPeV4oKE+Rm656IvKZ5bRaAg4zRIBLgfcHK8WJ5fr97wC2YS0VoRmZaMIYAkHEGoi9xW2sZnPmOluvqfxatt9litpO2tOWRTM2MpBlrMwhp5gsLkgxlX8unr/M+TEbxlqszdkgNbMP7+qPkgH1d8YWboWwdZ5PHF99cpLBy9CaEebpWP/P08EPcD6z8RxMRdtCGtfRe8BuvJFDN/76t0s4M1S9LSO6OPyL2WE/iG53+2N97cseGZqYIlihl7mjDVWC2rQEVoSiPrNSh6XulgK1MArihTl1xmrzvXjI1RgATL9npcgliaVDEipyLWsdzQAMfmlzwFeZAww+KDGLtpSPq3aJzOTLiah5B8zUMq19S5MaGfIiiqAlvqPWlw/PreeEx8DE0LxW0CKSYlcqMONrHyXNpweEiiZlVybcEet4pcuvnTwUhHCyCBXoxyFwbx+DXj9rzjozS6aDRIF3JlX/86XbmKRr8/T/OtR8rWYGOKp/u2XO7P/SH4so5G6Xk74hobVhNpvrDGog9dq2NbtAFdtmJE6MWMt6UDrm/7k/B0VaZFtsOhrMR31UYLqzEu7Yl6v42O1qavwiY0lCFCbYfAGwn/96XaZrAbJtwEKg+FmHOZxglXfYM7WyK3uI5YCyGGmFRNEHHYCoi/lBYuOW0pk8bag3vfvRf+mMhFo8Cvl+ynd0sGr4kwiIKzK8TQR9Bz0skM0X9dmc9BHUPxhY488TV9bxwdcDbDiXI1ZQcFla6g0ylIp5wAm9oZUbx3CtHc3N8aoKwVB1Tmd+Wpui+fK6GcEN1rrmFM5XAJdTMCL70loCX+hYzvAtb7RwiYOv9IxPxGloPN0ZGfQZ21nhvjHLFlS6BMGyGTUmgbuN8lJ5rHZP3rV/GAj9ge/kd+jz8xo9ZWKiu79cC8xl/dzCC9J3TFX+2Zeyvl7Lh9aOnjV7EYa6WVCH5hevz5OEGrMxOcJhxBygG2JM9HgGmz8wrfV/FvAgDlhluVRFMuGrOx9o6S1cRzlFJBSMTPKV5KGxh41+hj1BU5Ga0xLIKF8VcW6YjJOwHV87QBiNfs3+L2095v15ssxeC26RI1LN+g1fXF0RtWBVHpuDEd7moQxkEs84PmbjRYXJsRjLB375J5TX8B1qVhfWwdq0sysvwFd/8XC7J+p/vGufLeydPDSAG82pbeJQGRDjZkoCJWAjDxvM9LUSFVgML5c6CvQqlmgAMKtKI90wv8SSo/WAy/+CRHapejZYoqhhhVSOOERFdiX5xR10QXT67NTxbzS2YCX17w3leNJoMzw+cEg8nwZeZr8EJC56GJAoyt05HJ0hc7oA2iOLecwQBlB1OtWJocAw9qN1/V8e/CBAT75gYFK40eLruXe9fAFZnR6YQ3GZkKFUAAvfmDgtfMwudLUN7K78R2DJ74TeO3MHsdU/Hsclf/883WluLGJ6EZArfxckSAhjsFk9pqpPeo+rprRMxoHvH4IeP3bz7dn//GvyQGfWCpgwWxjrm2/jeIFMyhgZ14NvEZkXgFVdcD8bs35uybs2t+ahz/HZtpgcGWOyQphGIfJ0uOaDBACkYjUrZhijXnxv7Xof3UqEAsDM4ngOZcKy0QIhWJA37k0/oSCPhd3V/W999Fz+0Hqm8xH60LNsGNcgOuHgZ0dLvVxPjE6eGm8np/L2ewch2AlFozTG6I/yzpGWSIAl4YeuwCs/M/RfFwaAmg5+LUqcr3Oa+WXaPQ47CyNVmxHI6aeZmYAl1ivZuYuvubq2c0L4AlEW6qSceLKFn8Hp+uKTZ7J9RWfjQFB3NyY8gQGu2ZY2pK+CJTVLxqQAs22o9Ptm5bszGqJ3OtYIW/MZGekfJX+8ury9qBjfGDd99XPi5Xx3vVntSM3H9yVgJeeyUqRqeOBmdp7OyVDfcK99JCdxdbmy1cbHbwAlgZzoMF8WxXXkgDUJzEbn4WBmV2smYwx7K59996Y1l4cl2BFPi6mIsYFuJiMzERgATh6gjaMC21XzW7aAjGPK6Rj328t4mnVN8ph5FvJdLWO5vGyFWUR93IartHk2UJMyFK8ldm+MeXZ2IzlPy0kQ2Co1xQqBbD8vgJQzUb+y49tL0ps+88M8n8AD6CVz9C5dvhGu45HYxS6jQWuh1woq5HZsyxNe5Y6Pru7tTuw1i5OeW+ZDvzxwSs3D7jWM4rYip39zL8lKI9T8nl8X7W/XN5ja49dKIqRosdxcUJyzgMspqINLADXozTOzZVhb8BhZOHronMCZzUq05E/w+MWTLv4+jfFthD8cs0wmqlq4DUxr2/RFRCh/UueAa6S58CoAduiy66+JBThkoFuGOz29AVrUalmCQAvPlYvlasgg+sK4IqOSeljGd0y48CwRhZROGNNpKnLs5c3Zk/vbKYP3yg56kMKmTY4rqej/xkfvNJ6GBfgYsMDL8zrWZjX04AX5kVAgKs5JMe958a8MvICgx7HlVlFzvnu42rmWZaPRJGYijQJcHXdNmqq6+4ImMd9JF107Vt924LsvkdhscHd+K5F/+L5vl5jXm05FeCSv6qxay08Tol68J34UzoDiDpTbzoVkys/7hOr0UnMi+49jA/MDlCADnA9Sd8RVhGPaxs8y+e0+AEzP7Bb1LVm0/NbW5fflaz0WcwLAbHSQwFcy87/NRp4lUjzBzspP1cEz0kvLEK8iMBU5uIYi5l3JT886ApCO4GWJT/MxR7HZUbRwTkv9oYyO3Z9XHMXNJLSQ43Kd9HMxoyoeS3/Ryn8CWamrkXDKzUJppDXxmAKo9zAKbooXSjmrX2jCw5t3eQ5RkWbYljAH2Ox9IbuFCPzFqVJnWpwzMPSz1SSSXtrMC+tQHkhtChpoQySr3N+czlZYYf36xr57hgDaA3UpedNNhvxf714LW3V1ux2+rL6KlXfmLjOCjmPXUYBr2qYNJQzp7YF138mA6plEI7HYV1S35h2HbsQok5eR/i5hbhGNkt+RM5z0IvjQtXR9DYKD475zzZAY1nof2Nee2bjGOhVVah7SKfLfTAxSjE+W7expXk+rk+GAKzi+3IWIjFGmARpwabdP1k7WMAV8HKuI52kNSUdbSCmftkIKaXNMtNNy9Ow/bWk1vkzk1wCuhEDr3U3hm+PAWBq0os4xwp1ChERXuT+6KWkj8I83qcPtftor/fvjXFeOHi5mYizgIsg+busBfwzJuJ/Za/FfyQ04ln8XCKKCX/80oCLslYK5Qj3fnxalMGSH+AlALUc9AN41WfTAv4dKLnBrixM3fJh5D6b0pSqHvjKSV9QC+Bb4JX7+ELNTvozF+L7u7IMOIhV4mDW3n2AW7gQqMTc2kF6Uo73uTPQqS186qytrcBJnVJH0ewGWyBVazLDbuxDaeCsVDYxJ5eZD0yf1XdFC7Ba3Z5BX4gH95B7K9xN/evWCXyksnDwUuPUvzpzWG0tNiXkBl6va5frvnsLZ/fohV4MwGWatzMvbMtaReDV4qekSPm8j2u+jmpc9xdlEvPSEsh1sJ7/5IIeq38Bl/uYe0zDp3J8CZQ8Aw6R3+7KigKv41/qqN+gLwVO+sUcU2rsC+urD4CuAi702lJDzwGDxJP8nXLxy0kmTozlYmMa+e/CAqKT+Y5ru9SIpcArvwm4hHLkF4txSUeOqLgn9S8AG1lFFw5ekFfck8PMiD0X92YYW36undykQ+cfu5CfaWiBiT0QlSK0WbsWpc58FWcmALXMh8i/dKDO9WhXKSSyk0vf6NNzL7mHseg6ZbC+TazNi+xTaU2ZtXgb1sNtXN/1MYwtx/NyfVkcTB45Nrd3avLI4MopXR1vrBsddErnX0/gpz1Hf0sIhFJMX4evQSoaO3R645M6VQqmfM8gRofpbk+RRJcloQBaHyBe09y67hh/Sh/13fyUEArg+izhT/q4+E0EwGoF6X4upZ4GibHKwsELvaUYwEAHt/THrAS/11aQGmgZOXxuZDmXzCgGp6xZkZWEPmhsylDrKqM8Zm+6fImZEjRqn3M+5/HeazYw3arsF9afARVlLOBybQBZWTeyBlR5Gl+DmShK46jZ0Hpn+nMUCUg5o9MBK2dhO/ywzpjD2MXvMrlkCVb+fvdNgZcBdtddEUVkLTRTdqiR1/JQ5onK+hAwo8smnrY2m65aEzx2AY8AjNmqWGXyOqBV+05kYNeXLGMTyOp+rlweb/Hj4sErN4UVmCHBUAq8EkFvAw0zi41x8RWNT3EJlwKYETFSyYNV4JU6UoIrAS/ZIYCpEdHBDKzGiZKXTysN5XkBWs6C9Wy/VuAVZR8TuNQfU8BeFXI1U2qhttHNKNyDG+sD059DJaAttTO2zW1RuhodeB4mJJ/W2AVg0h/ldTJHAJ8CrvjeejB0Z2IAjE+O/tYsc84mwPQtlk2BV3TaVDcd3u6j8Jg3YXDP74Wnlu6zAhATg+qvTxtgqZt7MsjOWhbpUWq0cPDCaoRGPH/TFl7/FuYlrssOJVvpfDW6FbsZl7F0aUWGZTLydTXwStrf1BHzskgW+7N+rNYphklRDiv4jSx7YNaArDKn5v35kbv/zlhniiA5nDVmRuym4G0mh5IbjadydAlgDp1Nc3EYrOgk4DKwjl1kJtWOTNV/Pn4dYJLaiP/NDHcGo93HzSdnRq+x7LbhizZXX+DbBmTAJWPFMEM+8g20QbyEmMF9YF65F/ekj3XgZT5KujhmWQh4UYjuJDICMMX4Z8R0ObqPCDBgM8suTVkbe9pJbt33Wx8KsABbG33j/0idmbt9fSWlbuCVOgfsWmhEu8Yy68+8fh+QLaBd5g9PvzWKBAyKG44M8ArLAGBdLvBqILbLxPKaIFqbd5SrwIRS2Nd86RMAQeTRrYD+u2VtpBvryQB/PQkV+LwALZZokbmUOQZeA0QbYJnB/QqLOZ8YvNzA/EiGlUht42bEo0jpjDkwGZfhoN8vFsBDwH17smvZLIMQOUedgVRnUjWiEbhZRIA1d/B7TWWSwBgSoFt0rUqiWT9mVL2cmDC62cApehq9lC4KQ5svXbfpbr/E/PtjP9ZPWC4Wj2NdHPUWba/fa5MgyExZCPpcYGyRAHZi8AK/XfhSZmBXHPUcoH8+W69gOpH0fAsaY9kF8mtYhSAFoRoK8j+CbEGDGFYdeV9jdGq8x9gaQLexpi41/ZkksCAJ0LdcKu42uvqxdLLpZulndBNw7QS4tgZ2Nv/DmBz9Hn22dP5H5x538Loc8NKHrH38McvtavY+9WLZcNkX5up0Aw+bu8Q3Pzw5eAW9yncQwQMn4REqDryYjAAMoBkhvg/zamlIagTICEEhevGQ4hRYDedSpHzAa0p/nkdzj+ut6c8kgRNLoOnXnm5dqnCHdlm6+i7W5duc6SprYX/pututhP3vj/0ceFZ22PR9prCJpBePWigRQG2raHCu1D/M7OAdfHsNTwxeqCrbFkAxv8w8CKCrcx4Dsr1Zu2+v6Ld+U+PaQirzgt96iel7kwRGlUAHMD+yN1jWs1F/dxEX17/0/yIuCTgzEeHQ/63HrGyrmem3OqCc+fHvLaqcGLwgL5vXLizWPImk5+/i98K2Crhyg43JTACyqIabrjNJ4NRIIN26enaYjNCe5vNulhcH/t3EfNlwpC2CX1ytTw5eg8POhhpipsSwCKLj46rZhsGHZESZoGtxDTddaZLAaZDAHlMMQUmFuGcsG7KCQIwn8GLSAq4KVVpg3NfiwCuAJVvEn8mO+iLLBSyBEB3eZlHajU3odRrUbarDJIHFSqBZVblm0Ath2WNeSeaZ8AkBtjYXWbTP+5vBqyOuyla+roAX4PozACbfz0bWjXlv98YWK6/papMEJgmcIgl0PMC8xEy+yIqa69c3ax3mbXFfyYyxhwfxe7X/J7qDbwKvmtJlBg5IW6ER4rqyyt1hOcv3ius6kTSmL08SmCRwIgn00AlxX4Jte0qf2pM1lpjVLXm5zTqaRT3Brx0bvABWO4RISHmTAFBIGxu3dryOnWu2kRP/e8R1nUAW01cnCUwSOKEEgJft3cw2eiyz6k9J+llupGCF1yDWbtjECdDr2ODl3oCXOvBnoYJv4t+qiHrMK7ONIoHFeFRFTyiM6euTBCYJnB0J9Lgv/i3xX8CLJYbgCKcqQiNqNfiBgZ0kaPXY4MVkFEkLtCoPVkIi5JpqsV3JS5/HbkAlfXYqkwQmCVwcCejysAFxsdgdHlhhI97TXpR3MhNpCZHMKJcDbCcJ+zo+eKVSglE56TnlMa4yE/Pc7CLgEttlTdY0u3hxlHa600kCTQJiOptllsjwwgQJDyqUKmlzVpNXTzZY2Yut0+QX+9bybeAVk1BAqrQyMkawb3dzdQ3ANQWlfmuTTN+bJHB2JVC2lj8hL1lbXul6YAOMEAdqh3LuJnnKpPo5STn2ty2+3tpJRL3F15X+Nbm6Qg2xMAtEMa/GuBoCn6Ry03cnCUwSOGMSYHBZn+l/DiYkK63AK3n9AJacdM4t7uvbM60eG7ygJmc8W1YULeZlLRPnHOAqc/GMyXuq7iSBSQILlEDDrlyQfzxkJxN488wLcN1du37iCb1jgxdH3GbAy64laODTAJjHm5kenWYXF6gA06UmCZwDCfRZR9YZoiPDKuACaCfFi28ArzYF2mlgba6Rii2iMuegraZbmCQwSWBOAiw1WYARnKtlNl7LBs87FQvmvZOU44NX0cD38XM15vWkm42p4EmR9CQ3Mn13ksAkgdMnAZgAvLiWsrVkpbTGworsBEtOUo4EXmYOOd/8FAec6Hk+L1tyvZL+Jg65SkN7QiQ9yY1M350kMEng9EkAu4INglThx6ubiQXNZB8MsTpHzKiki/Is1vkYt3AoeAEu6FnH4Hwz9Wnl+HoAzNn2R0BtctYfQ/LTRycJXAAJID0AysY2yibgylFxosER/vOrifWSnv1KJh7nMx0fJp4jgFeykAa8gFNjXe+DmvnxVOBN9p3bCIB5/Z3Eg2o6lUkCkwQmCQwSKPITfHib51gY3MDC2o7lycsfx73U0cplG9QeI2b1cPDKRS31AVAi6/1YoWYqUcwrvi+Mq2WamMCrWmH6M0lgkkBJAHhhXkWALrVdvOxB2rcaxLwUC7WvFfk5OnodCl7Q0hrGAq3ydTXWxXS03b1Uz2eisKlTUbTURgaVTzsCqy3QsqHQ7muM7/pc++yuKIcHJY8sg5LuFqBrlDGLejZKPZzzvPsIakOG3QqOWYtzdO00lz4SL26dtZ8cVDqYQ/uOWaqTZl2fzKLXwjg8V3oz+vXuY/Y6UqBOXlM1j+uYe93n3NNpLOql7iSu8H+JCeX3ehWr7U6Ojx/bruEWcR+nHAm8Kig1P2Y5kK3G/TAWNnZDH+dGvvbZhkfNKWg91fUIqY5sCmCBKEXyvOXYbuutrLmyhGHPiRj1yn+AbaLCSnn7UdoQdswCuKTP7Yct3ns9C9QG5R+zDufp2sCAJQG0WgxSC7g2A2bx8FvbQI9YtN2d5HO/e+v67E42ZtWegKsPSI2p9PrNJUEYBkuLnfmPgIAD4A64MIDEiJVfwKXJ3QqdngOQXqu/bKurK7mX7O541HIoeFkOBKj8mNz0Ou6b2KxeG5t1HPUmvvq5GtL2ZjPsTrySxaE3AUIWiNqNWMSvBaN2Je4gBtQ64ynlomL5b5b118fr9ZPo79g7WauvBn5w98bs4b3VthtLgNZW70Yq70/l6BIw4GLMjp714HEyACvyUAGEMQvwAlw/PVyb/fxobXb75rVd4AJg6geQ1K3qmPpsZsBUN+fyF0Xv+I6KgeXzjdMMf9tpzFs40bXVua3Qadsjrq1mF/AAl77nveOUQ8GrfiwCbHmpt4t51Qxj0P+4P3acii3qswU8cCeKcSmgjnndiKBuZUcTI9+dRPvanun26vVSJEI0GgIx4DDPvlzDfpQKJXqSlNdjl868ANcvP6wViN1cyfbviVS+GeDV8FM5ugSwLZ1nKzrtLOpbAQ4ypIxdmIr0DnD997/enT1IeuQs9Sv95AZACBADIKp+HNuIg0QIzq83mJrbZU763CXMqwPY8fr+2Lf62evDDLJ/nft5FnnfTN9DHvTD4+LJ0cErPi5mI3NJVkSCO42zi3wITL6ebkPnt4NJmVo56/B3sx35nRx3C7xWGngVgGFi2WNuAC4A5vvRqXaEeslF9DSpPVzHAtOxi3sBpgAW+/oxWSmlE2nH9dm11GcqR5cA/xa2LM4Ik6EvYhVvhHmfJD3LUWvgNww896J/Pz24Ofvh/mrVwesdvAq40r908oqpLPACYPETpQ8aaF+uZk/E6AEG1phkTEi+2IBZmcVlGo/vwzvqfffPAWf3x2qRJuf2m2uz+zGjvXZcS+5w8IrZ6MICUQmOk60ySMQGPy5S9hsY44wVKcCmACho7gxkNPJaHje/UWNaa2FeAMH7nzUbcx2AR7mLtbl4foKzsczLes+L4xa/rR4AFeNyL7fVfTgD06kcXQI6+rWr74pRY9WsiO5HTFOPXuiT9qRzTEbr/C5nEAReDj6vMhcDQrtmYwFtn+k3yy8BaM6pez127kcsApNpLAPnnY/8SN1pPvrtHfoDCI+Mqhhl26C2hU14Lbh2rHI4eEHKNDjAKvBKbFeLqIfqx/qt0T7cgcsZ4+LHwq74Fu7dvhFqvjJ7MJx1fGYj5XHuiivWhFIxw4q9RbH7aFg6Pfwx0vmuz9ds32h31S4MkwEyH5cRG2g50Ow7uRfscCpHl4BZYkycTMufuNF2c24Me3z00p5Mx9W0Z2vHlQZcg765EwykMhHnjEl1ZuUxU7LM3nT+WnYTMsH0fS7DixRVOdt60O9IDiqrA0DUN9qs39FlNcYnhVUV8yoy1GYchV6Jtj8uGTocvAbm5QdeRlBMx90fy3unpXQAo5jYFFr+KLT8pwers58frM1+yeOf4yQ10rUZxj5rxxc2hE+kgfOwMa06u7s8mCtvNq4XeDHXgNzYBUAWeGFexSL55hpwATCMbCpHl4CBuIBLm4e0GhD6ALas9qR/JoiYf/QRiF7FvnKmUrpVO4bwiDwBPH2mFIhVeMe7j+mT2dz16UZt8Lr2bD3bjWX2MtcAeG823iU2IX6xUwJcWsk9qJtA96sbl4t9MY0B7XHB9avgBZqajTqsZwxwmW3rP3ZcpDy6in39kxqnQCVn7Aj7KOaU852aybk5+xFwBax+ehjQAlw5fgmI3Vlrvqqi6aUsdbGv/+Dcu5z42JnvH++bcxc5zsPhHim23wWalQM89SjfXJR1KkeXgIHqXUyGFtf1vgYy5uMyWLRaApIyHdOW16st26QQi+Eo9QBq+iQQ0P9ubWRjV9/FJgdGCQjrfrIS2tZjWA1wcK6ceypSAKmHL7ekygVeJkguX2nmLjxpdQPSrT6tj3+9bp8Fr34Bk7AEhWrz9bCrmU0VnBr0JMBlFzdFAeqIIq6mcZqJuDK7Fwe8zS0fxQn6KLNzZugexsl9nwmZEc4Iy8lOeTTukuBn2SKafu+cS0AfKE9n9NhgiokDYwMqsLI7NZeCmUwmpXhEE20vQj70Y6BXq2Li9jku2zmpaP0u1sj01Yc57q2N5vMyqMAb95f/hw4onwUvFXRToMlmGi7spjc4CcO82KzvAmjfAbtSowZcBUAZMW+koR7cTRhBpp4dPwSw7sfHdT8+LmemFUc3h33FRXXgIh3HVCYJnCEJ9I5NeQEYE5TjX3/gizUJdbeAayUz06uzxwnn+e3J+uzq08uxIMWQ8acFJGaFXMXAlnn7CA+gvRTwAqIWaWNhVuqom/fbFFR8dKmY+/1SOQBevgC2Crjyp5hXLtyYV/xd+bE2FfudpmFzM+WjCnD1mC0hBH/78dbs3/92d/ZLTEWAhWk5a1BsCyVvrEuzu0j9/5JcptcnCZxaCbAa9M/S4TAvjIuev4/L5MHbG7PXt0UFrFZkwP2n67MrAbi3AYwXEimEhCjlQ0s8bl2nXlnOH4QH8wJcO+8uf8K8gKrXQ7kagH0FuNT2AHihUwCrjiCheK46hlkOrMvsa6Oby7n1hr7tTvh9boRFmSl08G0J+PtLAjj/GgDj20KbKywiZyPTVCYJnDcJdEZyJQ+uJBvD9eEGb1z/UL40YUJYGF9pj2tb52fK5yuMggsofZ3zvCFYCMsSunOB5kD63r/fC8atGdRgjLqa9W/+PzGWX0awA+AF+CCjWRmgxc+FdaF1bT/GBlxudAn32uzf3EDzc82ytOdq2fJMRYzr5zjk//bT7ZpJvB+fF+Bi91fIw5fv+7zp8nQ/kwRKAvo63eciUe4lAFQw7LvMTHLs34s18jSmpNUhLRtMywrTgGs5AKZeQEyf/hA/F2e92UeTgQJx9V8HRhn48vHPls+AV9A49K1nkVgX7JZIZGBmaYUf7Te6FPRK5Qu43EgObOs+M/GnWwVavwS8+LlEKnPMY1w1Kxf0rhmXz9729OIkgfMpAToPpG6EimFdHz6slC/Ja/oGV8rfAwzMsyImWSQdIpTnAIVMxqckHbicyy0VbDHjaDtFy6ACvwVch8UwHgAvaIxKom/8W415NYcaX1eB13Lu0a9UMZqUg34AL7MofFz/8a/3Zn+JyWhmpcU+XatgznysgOtrlLNfezpPEjhPEqDzBm+sRfiQsAnAJSDWzLtQIX1cv+bMv5JYq0DXYDkuyZzKL8IRBYhyRQGvtoZzZ7fu6vm1cgC8fN5sABt0PRd0MB+xMTMBw29+7ZoLfU8jVAaFjBZMRiEQPYYLcAk8FS5xIxHLzhpqKpMELqoEDPQArJtbZbXkNTFlMjhAKZlhLEK3dSEnObdQOzITeAhgLFquMKVWDgRjKmNGguHFL+7caHjztd87AF4Q0ZTldtmhWbw6gFdbZvC1S43znsA7U8H3wrYcwiH+8ujW7FFGERHKAKuNMM28HKcW01UnCZxNCQAzs+zXhp7eGNjq7K8/vC2Cov/U8qJsSwbI+LuXWcp0RJaKfTUGth2QVZfDiNJnwGtImD93sYrrqot9ncaNcdOY1O3Y6djWLwEts4oYFwoM1DCunsBfQ01lksAkgT0JFHjF9xVHigiE8nvpOywrhWPc4P8+Dn2LvbeSqrmbdPWBkf8AKMRILCnfevnXBawOLqqv/fwB8OpBZNsuFtZlJ2yPXcx7yy6mTTkZf0oqmH/75XZ8XbfjoE8UfWYb+bmMHJ0aO09lksAkgT0J1KRVPCnlyP/I9/Vx9igWjJ5SwBUTDcsx0/f4WXO56EfLArCON2Ycm+m6lyH2MA54APt/8hUAAEAASURBVLzgE8d8OdF2kXBYE7UE7CK47pw3Ush4Ku/WwwCWAFRxXLJFAC7TwZOPa09Rp0eTBD4ngQKwYVzXZyTi1JW5ZEQQWD705PlmxYUJYhW+UIHo8X+NDWKuz8fenfY9ssFSocN++yB45bZ8UWiEGUdo6PFRaNznBHfc1wBXLVhFZyNoaxUdZhgt9wFc0jf7jM9OZZLAJIGjSwCQtawWreuvhxjIwCL86EEmw3YCWPq89cvbsbr0+zFLNxvL0gvedB97i2z4+i8fBK/UVYUhYdG4XLB8XoWEX7/YIt4FSBZQY1wOQt0FsKxXtPi6r8Y3EzmVSQKTBI4uAf1LZpKbWVzDagEW1gH3PRK2Q1TeZAG3IhNFFkGOWrAr7G+XeSFLwR5s7PjMy8WKxnGgBQl7gOoRLraIu4RHGFeZi8DqrlHBQut2WLOoASr6dvJxLULk0zUukAQa82rxXyvXkZT0K4kMWDdxzejv4ilkktnYHJ8ctNCstgzxoI/96w1zgHn1i5UDLTdixXdDQg77r19sEe/2xdY2KZDS5od7N0uw8nSJU+FknMokgUkC3yYB431bD9mAyRpI/mNuGfsjVDpmpqNMpzVL+W2/c9Rv7fq8upuqyNJJmFfROMFrbaFkLQ1aEvOyINMyBmmb//LoZoVFeMzPJVp4KpMEJgksTgL6FFIgVfrW27WyunZicTEdlzEZxufFv9VW9bTU1sxVy4a897VygHn5Ap9XJSDEvHJUrp0jXOxrP3TU9wTUAaqHGQnMLjo89hpWNpVJApMEFieBIgvpW5hXA5Lm83r6omVoHTtsYt7nVROEfOwxWVuQ6tfR6zPgtefzarONR7/YIkTahNlGgr8EuBzivCQT9N5UJglMElicBBACeyNwCZmFBBqyTrT+Nn7c1x5ZaksSe5bmFt3w9fv8DHgNCQhzE6Jwa11j7NGicV+/1je9ywaH7u2IAGuXHKvfmxORo16iNTOQU2jEN4l4+tIkgS9KgAO/bZ+Xnauvtq3g+MBMmPWNSQSSxpNUzOywGcAv/tAX3sCtWmLCtkRIyAQT0muH/dZB8MpMgy+aqnQRBxRsF/tCDU7wMtCC/tJ3CJqrnaCFSYRprQW0Crgytcv+rmC7E/zW9NVJApMEPpUA8jC/9lHKqeqDYWNrWX63mr5ovaO077AAU1pkaWZjs/b4urirdgNkD/mhA+Al9BZQoY8NvJr96TXvLbo08Erg3BCUSlhts9i2YawtogCXg6CnMklgksDiJFDgVe6YtvbRfpLV/+LEv5nEBzdXLeBuqaOBSuBrcT+eKwHDRpaa0x6AIU7Y3mFAeQC8VK2tNxouNs+8FlxxUiA8jEtsl800gBcbvB2NeWFcPgfopjJJYJLA4iRQ1kxcW85Xs/YR82LtMBsxL2cFcL3dSf8bIWi1gVc25QjWtNU8zWw8jC0dBK9CwgBYKgsBe7L8Il6LBd0SSgktrEqurhLYwLxWwrhE0kusNpVJApMExpNAAdjACzjtZWqpDY4DXgDMWkcRB2MsaGE2Np9arL2AF4sPmDmOzbyg3e4FP7nQCMiV9rATUEXUB7zMKjrY3NJ0lFDHa7PpypMEJgnskwDrxvIh7EsqKs77dzHlBK2O1h8BWAFWgGvwrx+KXKn3QVoTjIJ4LlY7CB0RBffJ4MhP23Kg5qiXn4vAoP608PrIIpw+OElgYRIwo484IBDSq0sbXWQibh1EY4wCb4owDQAm/XOxrkP40gHw8vl+ofL67zrPGiNbdOW7sFYDWMW8IjCoX8xrJGEt+h6m600SOC8S2CUTxbwGMqE/ho2Nwbz2AxcTtTnrgzeH+NgP+Lw0wq7Z6EK7zOsQGPzG1iMQCQclFay9Fvm84u/y2hjC+sZqTl+bJHAhJIBM6Hs30geljK6tBPXH+J7HWp1XAAZnQuzK11XM63C8OQBeLpTv7pmN3e+V1w6/3PHbl40tDKL7vW5y1Nv1JNO3eWsqkwQmCSxRAr0/Slp4c6WZjJX0M4A2zmx/MxFhjpnMIksBocbIvn7jB8ALRGFe7M4PA3B9DAPz2hjohaYCKlG+FWMSiurxFJT69Yab3p0kMIYE9EcsayU7dlSwagCsIu0xrxHYRBEif+AN5hWHffN5ebFh0ZdA8zPgla8EqLq5WAAGzIDXCKUjPbbFXARgWNgUlDqCsKdLThI4RAK9P1ZC0F3m1RIXjoBdRYg+Xgq2+J+j447HHXJgz+cA7CB45UvtQgOA5YsfZcL3evvjwcIKgUgsyM4msL6eymv+TWWSwCSB5Umg90fxXtUfQyQ8rv44Cnrl3gBV3WIjSUWe4E4deQMMfAbADoDXgF3DFxtw9Yu0H6hfWdgf9eIkxLTQVUeZjMBrwq6FyXm60CSBo0gAYdAfi1DEnYNUeOy1ZXTHwp/8wcB6qBYc6CbrPCYcCJWoGxwQr9ueeZqSP3U+igiO8ZnURsX4va7FdCQs+zD2yh7jStNHJwlMEjihBDpQACzL9hAJSRP0R++NXgBXHdZXA7AWOiF8Yn/5LHj52C7bAmT+Hfzu/mt903PyaEhvV992yDJRSL8UaX1TtacvTRI4txIQEqEPAi4A1pnXMm64Y08wK7uY2cmM+6qsxsKh+Tp8Frw6w+oA1p/Pf3Fhj4Neu2ifByWonCfcWpiEpwtNEjiyBIpcpfOFeFVfDOlKX2wm4+ec5ke+8DE+2AGs8KeAC306WD4PXgc/N70ySWCSwCSBUyWBCbxOVXNMlZkkMEngqBKYwOuokpo+N0lgksCpksDnwYudm2qWrVs274h1jjFrMsBswnsHB13OXpvKJIFJAsuVQHW7dD4zfhWqkHP5nlIN52WUPeyJry0I1X1u+3/7QJyXD9SXxXUMR/eWjVF54mgxHT3t9F5CsjF+b78ApueTBCYJfCoBs3uSAvZkpEUooNkSSgcuM55CpipsqgAMLnl3rxwAr/5laCdcQXxHq/bwd9H3UCgva2vbK7I2/BjY1141p0eTBCYJLEMCyFVZQemDtfFGQKxy1+eNpRCvAFBgp3BHfJnNQQJB9Xz//R8Ar0a7ULXhcKXcSGHWooErtXFJzKt2K4qgdgbE99pShLVfItPzSQIXWAJ6eo9u3ylC0bY9rP64BLkEbRpYBXeETTlgUn99vgoHwau+HIJWzKsFi4ZFzi4lXUXDrsUiGIBCSzGu2icye0V67LUBMufrOz2eJDBJYEQJHOiPyV2/2x/HYhMDOIGo5mfvEf2et5v1+v7yefDKp8pkDOo5Q61a+b3/2wt4zq+Fddnie3M7R7ZZehuBeW0sWS2g2tMlJgmcSwn0/miz6c3td9UnPR6tPxZwNZDadVUFc7rlR8ifAy6vH5htzNf20K/AqyUha4joK4stIVgRzMfszP2hgIvAPCasz61nWuyvT1ebJDBJYF4CCEPrj+9nG+mLjgZe3Dh5c8EFnyqgGojS5fi5aqKwmFbDoi/95AHwKvsSGg7AVWsMByT80kVO8npHemyrCet9sTACHEFWJ6nq9N1JAudeAghDWUK7zKuDFzIxxu03gDIxCLj6JOE88/rSrx4EL590IUfebRdrr33pIid5nbDY1Jvxda1v7tTBfPTaxLxOItnpu5MEji8BhKH6Y/rgm/RHx9bghx6DealhAVVnXohSkSWc7Ovls+DVUa+Zig0ZD7/U13/oS++axSh/19bO7NX629mrjbdla3vNe1OZJDBJYHkS6P1xY3voj+vbs430zbH6Y8OagXGFLV125MXCnppj/PK9H3DYlw2az/tysa7A2zyY9TnHL1/yeO/Ap7fxcW1kU8trGzuz1wGwElZem8DreLKcPj1J4KQSKPDiwkl/LDJR/dEk2niW0H4Aa8wrd3IIYzoAXvWFQr4OWg0FC8FOKpnPfF/Cw+7vuoR5dWFhXpPT6zMSm16aJDCeBPS5TiY+Ba8RLaFdohTWNfi9joI3B8Er6BWTs7InVibFJCOzCcfld4fA4DfKk7A4CLczq4Ht8XttBvW3Y3MToqDVRiMbGxynFt9Y+elrkwTOgQT0QTzB8Ta+5q2EK7F+3sSFsx5raCt9s/mgF3+zZeEN4HVF4sMhEWlNFB7S2Q+AV67TsigmJbP92hzAy1IB7y26EJhrv72cKNiUzYAXwbVDnMm7ltGxMjs2AFt0HabrTRK4qBIAXLUAO7P7Ui4DKn1uI/0QcDlvB8z0UVbSGAVQydpqow9H38PiMLvx8+CVi9XebcNuPiq+E3CBkosuZjCsnXqbGQ32NvDCvNjcAMzM48q1JrQrl68cdj+Lrt50vUkC51oCyIP+x8LZESgeoKr+twnA4n9Of9T/343kxgEpDbzaZh/X5vZsPQxuDoJXzEbrieSTv56NJ+2h+Pbq+/qBwy72La3cwItz/lIJqUArAnNeH0aBrC+v36+dTA7z4n1LJabvTBK4oBIo8ArjAlyCUTGvThwwL2QCO0MsfHbRBSGqNYxhXoDLJjxtQXZ87Yf82EHwgoTDhfo+itvbjcqNw7zY2qTSJPN2sLdfZYr2+aut2dqNa0Vr+eGYsLP8n8okgUkCi5EAYLKi5U2Y1nqY1vPX27PX8XU1c7HFW/ql0WK8cm3MCzFpbqq2CU/zeX0dvj4DXi2HDgS8EbPxRnaw3ry6U+j49UstRpgi69fDup692p79+nS9gJRDHzrfDJBlE9+pTBKYJLAgCcjbxT3z/PXW7OnLrdlvTzfS97aqD+p3yljA5dqsubbtYd90+moBmdcOs/Q+A15tppHjDHhhX9eLyrU1jn5wzMJpuB5hPo0AV59ercmDBlxXZw/er4z509O1JwlcOAk0srBTgAW4fgthQBz0QeA1JnARdjMbuag6WTo63nwGvGKDMhvRuGJebSNYAHIYEi6i5UuYsbOfZSS4nDqIuL15I8B1ZyV7uLWRYBG/M11jksAkAXsjhizEZARYgKuYV/qekKXOvMaUE0zh44I3rDyEie/rKHhzALz4lqRexbbqYkHE6zUD0OK/xrwR10ZjOQ1fvdkuVF65fnn2MMD16u5qCfnmSps8cHM9/mvsOk3XnyRwXiTAvdwd8M5mFvm4mIp/Pt+YPc7xMn1PuIRZyLEL5tXDJLqbCvZ47TAf+wHwKho3OOz7xWoGIAzosIst4kaFklTEfZBfublydfb8ZZz393LEmYheAtMeg3ZlGXRwETc2XWOSwCmQAMCyTnEnTnoBqS8CVM/DukyOPXuxVY/frIvtGjGifk4ORZbCvPTn1fT11RWxpSYIDydLB8ErF8Zqus8LgHmM2i0DJ0zJEpwivuRGwjUItoQbOsuUNQOpQGd1ncokgUkCR5OA/gW4pJ/Crgq80q+exVn/9MXm7EX6GstHYKrPjl2KLIUYISUNvFp41jczr0bjYjYGODp4XcW8Do28OPmtEphlQXxfKC3WV8BVALYdJtaAi428en184Z78jqYrTBI4PRJozCspqAJQMrgwEV/Eonke8HoW8HL+ED8Yk1Hk/dgFIcKysK3VEBMAhoUdhSwdZF7d55WL3QiFu5GLQcWjTF0u4kbNbpTgGvmqeBMCfhpK+9ut9ZpMMCOJcQHWVq/GCqH4xMMW0QrTNc6TBABW/tchENWaRT6uHhrxBONKH3szrGN072PPMnb56rNFljrzitO+lggdwU11ALw4wV0M+vE3OTz2mveWXSwItbr9jzgSBc96vpMGUBXgVUsLAmTqd8Xqoe9Qx2XLZPq9SQJHlQDgml+7KLngkwDXr4/XZ78+eVPn3xMi8fLN21oi5LrLAi6/1fGmJgiDNQhTI0vBGx/4SjkAXvq+bBKfA6/vAQyma+X4unJloxyMnI1u2E3eXVvZjUNzj5entY9faerprYsoAYxrfu2iEAiMC3D973++mv2WMxZmRYslQssELu1ReBOz0STcaliXo0c3HIY3nwEvLGbw/ie+qtmgzftv6+1lF07716GzZkZeBsQ48zGuO7euz364vzpbu9l8YLXEQEtNhuOym2j6vVMsgQKvuFn62kXM6+nLzdk/w7z+968vZ78/WS8HPSc9q2bZBUCJK0WWzDQW3uwGxX+9NgfAy+RdmY0BiLV3VzOzF7MxaMhBjvFAynLj1Z+vX3wR76K8Zj6sejdDom5/PluZ3b+9MrsbAFPu3Lw+u51j9jHLh1JX99Dq2uq7iHpM15gkcBYkAKywp+7nKh9XAAtovc7aRYGozMQ/n4np2izWJbZyWQ76/TLUTyuDDTdVoggEpIso8Jr3vlYOgJcUrC3aNXm8AgZrq9fKRLuWi7cZgAG9Lo2zyvxzldUgfdrWCIHm/uPPN8U5mZQ/3FstFvb+w2pA7GPduHsggMOo5+d+b3ptksBZlQDg6rvOO3PCP4mZ+CRs64lJr4CXvsN01JcA14fMLFauriURErLt/ZLF1MMkbgVrHNiX/guLvlYOgFdHwo9hMH5ATFXZobnYld0ZAMCVCwfAhmQQX/uNE743gGQYrdGEwE3nmleUukMkvoXcGFqfVGBWhj/WjKQAj6lMErgoEtBH+Int+IN1mUX8/dn67J9/BrQevynWJZ4LAdjSbwbgar6u5aBXBy5nUQOACmDdLvC6Xm4hrx2CXenh+4ovcNhDPWDQmFeQMMyrbQiZLyT3VqHWErAL6/Jb7TyrdNEEz4Q0mmicdwGua/HT8X+hnAo7+vrHKX9OCWP6c2EkoJ9gXIDL4uoXccT/ERPx//z+avY/46D/MyZjJfmMGbmZHYIwr+4H6n1sGcLqAGbtspnG1cRv3orrB/MCXPzuxzYbObXS7+uLzhxpdQQUAAOK1+Kw2Mluc3y07gDm1zTMh4wY3Zb3nhteq2nWq0WB76xdz0zk9dnbtfeF6BgjYTjnf7E2hGziZCQ6lbMogQ40FQqRPmEAB0T2P+VKEYDq+D1mIuf8b3HMc85jXT0zqvNYqZ2/JFOgxVRsR4ushysOFlMPfwJcHeC+dK0DzKt16MassLCidQEswaprq9dnNwMU1h7uvA1sfWyOvi9dfJTXA1YE3uZF2iJuSxsALNPxRdZp3c9C7vu3b9QZkInKN/HAISgAzn1FNhB6ArBRGmm66JgS6M54AGYWkRXCdYJRASx9QH4uaxYfB6wAF9CSFRVglZ8rfWV82nFQCvody87KGeebq3HQB1uER+xuvtH758Gvf/LKAfDq7zbU+1imogsDr5sxy9ZC7S5HSMGt7zK1iulptMz+lvC3UpdnMR81yJuMOE8yg/IoIRSP4sR/uJ7jbkAsM5PvP6yUcIDxx7CvK/iX1gNiU5kkcEYkQPfbYUax7fYjlKjAKst85MHTB8wkipznYnmZ161SAW6Vix5w+TL9X3LBqLijrmNZwZTVkCExm9xS3VQEcP34WvU+C16+2Ep3qHXmlanM/JiZv3dZf7i998H+hdHPGq4DmFbc3G4RxFbC//lss+K/fnp9s3xhr9n1CbPgwARaGJg1VKFc8ek13Jq41+hNNv3AgiWgD8Ae/ZD7REobM4q/x7fFTPwjfi0m4u9PNmpCay+LxPsa5IculO5TjxZcu69frnzpYVzMxNWwrnnmZf20fqocBVo+C179513GxSr6NShZDrUwr/ehnnLNs1u/R+kAZuRotn5mIS+1xZD2nPM+E3LHEcdcUeWiyx9ndzeul+mItnbHIIEaEdpBcOztLsBP75Gy8Lu5/lKafriX3Shpg0YOpvtWjqkcTwKCnQU6a8eSY55LAsAUW0apGKzSS23YHOvvMgF2NXV4V07qzqxqiC5dBlLq1/xbQyjEuzarKHDbbKLYLWfOeU55cVwGc+sYFb/7PcBqv0xBhn6HbZkMdJhp5LQ/ymLs+et9Fbx8UEfW0WsqM/6j2zEda5OMzRheevgpKRpGfQAVevzi9eVqrJ0o6kZo9YuMTH9kVBLMWnQ1AnM2AhAm4TlXcFwkTJCAuxyH7rFu9VKNclbkSyvSY8/GFAGFxRx1Nvf1JoGGe8A6m22Fbk/l6BLoPqLaISezbc7ArDZVDUiMXbQnALWln6BR4EPPur5RMwOjg34ZKGs9b3xbu8Cb+hqkt3KNtsh6uzIPMxH5uV7E39W2LGuD22kArS5X/Uk/M7uoL94OplRcVyyi4+LJoeDVgshaKuY7AS4OcI6/16mAmbvTUDpwOdeMSzq5xyLzBenJUfTHjfUB6TMdO/junDnxCc/SBIvQzaYWmMUud3b/hDpgVy1VAl6oOEUcu/iJAq/ci44mUro3sveaGTx2Lc7P9QFBz2WlHTm6xUSR8TI6uTYDoNwZ2tKaQuECBWDRNXUAWAbhnbAxg5Z61jHowHrYlDWKdUS/XcfRX2v7nn6ag34Z93YULWHl6GMi6e+sBU8CYB4jD8e15A4Hr3RcF9axCynzY69WWkZTKHpaSm8cSti3beLA5xxssxvtDKzuxoF/p5YX5ZzF3UAMoxQktxpB3ohwzV4Ssu+7zToCYbUtVIEX5jX+3ZcyR5GZiBthCpRU8bqOIN/3VI4uAaAArObBYJd5RZ5jl2JTqYPf52i/sfK2Om1n+cXMuklbbd5Ajt7x4QqD4Hx/FSf8q2SCwMZr9j+WgGwr5dII6LXlPssB5OPIDGZwQxWesOQCYI00tCiA41zrUPAiVKO7SPt7t1aKpr58xfEdGzXvnbZS/gF0OwqiNOAZ2FP6ubWPb9LgpQhRnjtrbxt4JQwEgBEkUxJwuUczIJ15OfesrpRGXrGxC/NB58IghYQobzYwRqEfb8vMHbsO5+n62ow8DQbOTC0pl5hgZD128RsYtASA/FP0tGIPo1s6Nv3dZiKmfnxiWCLTsPQ1ZmbFcAW0rCwBXlacWN7T9b4P4mPfx7deH2YU84qzXlaYu8EUTnuvHRdPDgUvVE6Hx07u76wUNX26ulkd+7g071tv+CTfK3Xsf+IYZVZqcHU3Xfw2CmxnYGyyjQDdByawVexJMxnrb7CaIsmFRPn4vcYumGSlMcm0t0J5ixmmTWoASf2mcnQJdOYDNPiQtKdNJ5yxsrELkxHwmA1UnuS3a3Asdj+YjamHdldHkwqdJdY5wGfgtLSnwh4AV7FwSj4++FalT/BHv6O/t0MWHsT6cXjstePiyaHgZYOL/mManknGvBIJe1ykPME9f/tXtWctItfJG53ezihbcWpGttxP+bgiPOcCrAjYvc2zrvLY5xL8aIALdaeIYxf0H3gpmEKfYChzGLimnlM5ugR09GZS5RwW1p3ezDKAMXYBSBL/KcBIe2rBDmCYE3bWj/J95TsNbJvTvrGymIjRX32yuxBOP3QlSCn66p6LDN0Rg3mjVsh4beHg1X/sdjJM6CgFXvF7fcuPVYt9hz98Q30ROea1nQbnH9jcGmYTY06i7O61+/HydNdcrCoPGFEjd5QGcFGosUtnXmaXAKY26IpedZ2w63hNEF2gDwJdnIEEgCDnZYEXJ711hTI79A4734wFRrmrqmf+AFweinbuz73W7qHdjS8cTxTf49P0194YlvQJHnd43MjQ8fy3hzIvwrWk5mMWOessYr34W8zQraQSImP30L8L83uI5ZDfLIWlEGl0rTw+aTqkQkd7m2zffkhlG/k62pemT51aCWhPDNpxERp1lwTkgcd8W0KUAFbPw2fjDeFYgZpjlUPBy/UAmPgnBUKakavEYRzccR6zvbEQIxhwmMokgUkCkwRIgJVQrpjBJWOFDgf9aiYA+ZgBl2gGG1377HHK4eCV65VfZYh5sot2xUWlAlLQqMz2th/tO+xO4HWcBpg+O0ngPEsAIF0JdmBc1jMWeHXgCnjBE8AF4I6JXQfzee0XZP344BOSHquYV35UJSzSXl1tzke+g8vxBbU0OfuvMj2fJDBJ4CJKACABJsCFbVmI7WihPo15wRifWzjzIvB+cY/NyHWbVYyGWZpLMRWZjlstDMnHpjJJYJLAJIFyOVVcV4DrVvacuBPM4O9ivZW5OLijvkVUh5qN+y9aswWhetYkSTezmZiT3Xipy7zKZ8QTvv/GpueTBCYJLFwCDS8S1xW8eJDQiIc5an1xMEQM5UnKscGL896KcDMFKlPLEwR6JniyT/uepELTdycJTBI4PxIovIjJaF00svPwriV5LYfXSfHi2OAllTInG/TszGs9ubSeLwBJz0+TTXcySWCSAAkUXsREhBfIjmOXeYUInaR8A3iFeQVJrQN8uNPMRmlnhU/UjIEK1YTjKY75OonEpu9OEpgk8HUJcL7XJ9oqFWRHNhom457ZePIVOscGr6KBmfaEngLuRNw/XtvcXRcoDzUfWAv36uev3+v07iSBSQLnRAIFXHuzhyb4zCwWeJXZeKNMyG+JqN8voWODFwechdq3A1AeWyGvYqZB+8a0Fs1YztABbP+PTs8nCUwSOJ8SwLgq7CHYYEXO1QSgssrmmZcMNdjY0n1e3QHXQcziUhUTNSu3FOYV6Bosx9iPU8zq+dTS6a4mCXxWAi1SHnDZ5xUmiKSX/ubh3dX4vFazrFDm4pMnFTg28yo0DUABMZRQzIb1jsxIif0kWJMm2tot5mP+fvYWpxcnCUwSOH8SwLrgAtBiidW+FyE3lfAzWGELwp7yOhByonJs8PJrKlgrHQcAU6F7WR3+Q7Yckx5ZBlN5p2zUMUXcn6h9pi9PEjhTEmB5CWKvbRIDVrYgrISDMRXb3oxZLoSVAZHBrf+tN3hs8PKbDTBTgfyq1eB2ACnwenCzErypnPQiAlinbAjf2jTT9yYJnD0JVFCqONCwrfuZXbSHqrTra9xKwQrvA64FYNfhaxs/Jz4/XsZgUIztap0j8PoR80qeLIxL8KoYj6lMEpgkcHEkAJws/bEM6GFYF+Z1L8uCYATmZRF20Z9dEvTtsjk28+o/VciZJ2K71lJZ+e037rf98DCul2+u1cYXbbFlQiZ8cXJ/dfFN50kC50oCu3gQMiPyAGAhMz/EGoMNMIIv7LiLr78mpG8Gr35RzjdIq4I275Sh9HUyfj7NduMqy7FfIRMBro9JxzwBWJfcdJ4kcD4ksJu4IWxKn+dGkt75pwDXzwEwVplYr0VbYicHr4Em3g3SAirgZVMBN2DGwWuZdJTDtoDLw6lMEpgkcD4kgHG1o/mxbDN4K0D14M7K7OeAFwC7m0iEm9lk1p4QiywnBq+r8WuJ4yhbN4FnwOu3JzcaeMVBJ9ZDools3DMFrS6y5aZrTRI4LRLoABai0ifw9pjXzQpIbRH1pwy8LsUn381DDjmzDLtH2Jjt6e0/B9Qcyw6dQGnNG5hkwALnbW4jBpO28toP57YqoL1GN/rzPJp7fFq0ZqrHeZAAPdydw5/r390c6+cWXvDpHXfdtVTPsew07Oqm/4ukd5b6xgED7iXuU/ynFPJYl/63yHJi5hU4CDCka9fE4uXaS1DgqmnSH0MZ+cHEfdkXz+aY75eMXoCrUtAmYE7QnAkGsR7ESPAavO8cIwe/jLClELW8qT1uAEfsE4AtUvmma9HBpocdoD49JyYqnZ7O6vzO+wHAblhm+AWFIwm2dVtm6XFd3ERmFMV63pvbzgxwuYciDguu2InBq4AgDWD3OSwMUMhtfz82708P1gq8TI8CCZu9Ljvuy2ilTpV6lh8uIwSF8bqzxt7ebfy2kciHLMwEYn00c86Kp4F5LVc5Ftze0+VOnQSaNXApfUQH7xZCdfY8x2bor60GLatpy+/2boI1IzmCgigsmRs0d1HcRkIj7g1xXffS90XU9x3n6150tqIMVdWF/DkxeKkS6mUi0WOCLuYV9P3xYZYKZWQwOlgDeeV10bOFVPyoFyE4jMuoQMArCaBrwNUYowa3uPxqwjsuZSORSxnB3r/Pe3m97eNnmbn5hgm0jirz6XPHk0ANpoArB3CqQM6wFY+5YnZ32okjHJjNFxsoK6yGzejxsst8XJeYrkf3ExpRQaltb1cO/MII5KY9WFgVTwxealJ1yp9Ur4Rr1Tj0/SmjQu30G+BiOmqIcuAHB5opthxAaFSc8FK/K8MoViNZdhRJFQArVriZNZlFvWtZU9uEFDPDxMrkzfldqHlnas5jb1RKOSgsxlgL32MHf+LDW5gqXIwL0ThuAfrXd8+uHajT5uWTxbJHLAAJg8JKsCm+olrrl9cvp2H3HjeT0cBbu3Vl0LXsBsC1HbOHXbSjj1gbZKDfyyj1O9Xf4+9O/dZWE00fwDKz6OCsbxH1uacR67QQ8JoXGBNRTMf9WzeyMDtmWZRiPf6u50lYeMP6poBEmWQFCk2R5r+/6Md00e7WwIni8B1Yi3k3DkVZHSnR/I7JDaxst94Aq5uOzQ/2MfeS/GUvNmaPE8f2+MXm7N1mY2aLrne/HuCyvML6sBYvExnmNZsXNDBbjsL2+pz1s/Y04OzkaKw7WYBfbZV+Pn+9PXsfPRmzAC4M5Yd7N+vMxbLLtGpgagysTK30pfJ1Gbzobs4G12evW335kddjMmJfdLjcG2NWPtcGXL1uzgCVg949/fJobfaXh2uzB5hX+rpIhDHLwsELQNiQNn2/BM5kBFyPb21WXp9rG/E5RdDvpM0ZRsAxb9BvaNjtsCqjkylbjk+xJwQNxPrgAAaMyAVUzlH0Xsf22sfZ82zR/r/++TKfvDR7FTbJ5PTeWAXjAlx/+eFWjrWaCKnNOqM0RuS+GfBYv3/ervsuijnPnAHXP/58U7cJCLa2x71jgyXg+u9/vTP797/emz1Igj4gUACWc/fFYlOAousm84Z+vgxg0bc3GUQ56Vud35WFsxTXhnqob/pQscjooJlF6W70p18e3qwZRuCFRY5ZFg9euTGdS6dCHZlcf4aluMEbbPY0nqIB4loavRiNmK6zjE4YH38cdgi0CPunB6vNLEt9AUUNFoCrKuncgKnj0+9P1gsEAVdT+u1SsrEADLvCuADX//Mv92c/D8ohDbdpaJ1hKkeXABa+EXbl2NzeSUziRn0ZCPz5bPPoF/rGTzIVsRTA9f/9x6N09rUCLwBWznpoNfQLD4spDmzxXc5/JgD8TfaM+P3pRgOvgFiFIUXHu45+Y9WO9DVVK7BNf8EEi3llcH0EvMK83A+rAMOsmf0jXfXbPrRw8NIATB10V8fauPWu7GGhE9B5KyBidsRRJtrI0yOgB2jFRigfx3aUtk0r88fltbwHaM2OADaCb6Pc3Ki3T7YPfn9dnwUszc/gVyyDakC37+MnekpRZK6VF4kfsRyiAV5sDABjklM5ugQqZVN0D1i9ySTNdswwZg9QIevRSi7t6jq0QV0Euo7+tx9v14BZ5lj6DsAqLcofA6eBdz31XA/D38l5J/VtoRHR4/Qluvw+eszNMYb+7ZdH6WP6NReQDKmY44P4uPi8LBG0q5jP7ALx/gss8PnCwat1fDVkG38s/wzlsEjzX39KQ6Vxnr3Ymj3LWdLCnTbLu8Bb2ncpShCAap6pD+X7ejaYCmjvmyix2JQfMxoWZV9Vb8LPCJN7oEzzxVOvFXUeHKywsZY/5bRw/FL/XNRv8CHuPl74D83f5Tl+XPLMgBb5GTy5NQxuY4qz6VDTpTIH6dfuQZ8GPYteUaW+BwQz0GTSM26X+FefvNyc/Rrmj/E/i/tCAoQGXKn/yPfQNQL4GuiRkXvJivq3H29V/2FZiTTofWd/v+nfX+R54eDV+rrerRlajBU07ulygIJ8X4DrZRyk45emmHxwakQZ+K0ww7dGtYAXH4g6CbQzAl8RcYuAuY38+6R4rSternE5jcnBV+DiB+pXPvnGiZ+UQkeRKTOm2P6d+LIX8gK7sow+MMM47Uumke0ozKXUZwCuKNQewzJAtuddz3w01agDuKrXVphVDbaP38z+64/XBV5/xmR8CryiyyYfanCmgEsoBV7pJ81Bf2v214CXgd++jCuxuNo9DX1nX9dZdPUWDl4qqDF6t+9Lhn7cubnbcG/TIK8CXMtwNlMGgFLn/DHrSBm2Q7Wfv0nkf0YvowWTEQPjS1IuJeIWoO3HrrqvvA60mtPSzfbMGTSvvr6wP0P1q/4FXn7Ciwv+nYVV+AxcSD8HDMW8cjawOcYopR3UKDojiJuu7Q5+BsFBx3xOMTD1QYopCKCeRl//GfD6H39/GfB6k6wt0dv4XBvz2vN1jQK+rVq7f69lwL6V0Ag+LqyrMa/sCDQwLxMPitsau4wCXipddc8fSC10gtNZo5n5e5HGePoiUbi5YbFVPXSi0ffF98oOYDq835co0VQ5py3lsQaLvS4zhqK+VsHXxENGEzcTlasG0SjahxL2mBzX//gh/rN8d/G1Z5GGIUSRd8IS2hQ/pjcSUygJnN8/2gpwVbhEdIFPyeMmzzHum/LsA6zoDj3qR+lPwdYQbJrBvcUdvp/98Wxj9keY1m91rGdSIY766C1/F/+XexmzFFOk6zno/E2+19vA68aQNUKyQSlvrF7hAx6zNp9eezTw6j8DHNBJQKBsbq00J1+o5sOAGMDaCoBprIpXSQcdsxid+Dk0iiIGTdwWwPKSDUTsBP4oNv0so8vNSy0wMO1WPjDf8V2N2aeLi7YP1/P+Ikurbw/3aA5anQ1rGFdSi7yL03OtkucAXj3Ys+SZ18aSZ+nOnL4AgWJcEUsBV/508NwczETWwdP4un57ul7m4pOAFrYFuIAWHV4G01JXrpTrOUzAlYM+/cJZPwFcmJj3fHaZZXzwyg1x5CmmVsVbtZnHlt8ak9AoSoU0tNUO9XyMP5SlRt6YjRpf1osnCeWgTFvxfb0KmP1LJhYUsynM3k98YGmfTvuxSr6zD4n58FoDxMV2AfVlPpBN62xt1cKyZpfGaIPveU2to/1LntEBMqWDXhsDDIxpXV+4GuqgK3P6Arj8voF8z0xsjvlfA15/5hAU3cDL8rUOXuNL0iANuETRC6h9sAtcIR8BL9H0gKtma0cawL90l+ODV24IeOnoN9JIzLZC7ders+dGkowiitGkLzD9UmUX8XqNvPKLUZaB5T25tFnA9SwZYF8HvBRM0dbkfGHKrg8s5mPaczAbm9/r8uUPuyNpfXiBf6q+qavOtr0Tdqqz5XEzsRf4QxfkUk2ezVSsYNUAGJ0cU57N9KIrja1bBtSZPzBtA9QeeGFdf8+M4v/4x4vZr4/XZ6+il3zEnXm5B98Zx0nxqSIU80r/BVwWXt+PxdQBzAoVIRKdSXZ/16dXGO/Z6OAFjDm++42V/ytU02zFm8SuGEXw9Z3Q5Z77qzeO8xilX98Pvw1Wvbm05wPze7UHZWz77rwXyQ7M1B3oRfPKZMTKAPOH3IMO4F4XXYiAWaOjiebnp/PYa2PJZ9H3cJqu1+TZMolgORYzjynP7mJgddAVBxCjKxiXgajqoS7xY/2RIFSBs7/FMf9rnPT8XRzzW+rJP6y/jFzUuXQ5dRRULsYQUMlHL4Je3+XDNrB/zzjD0cFrv5z7VOsPoZ9Gu7RjjSIa5lX8TdZuNVaEGntvHADr9XJ9CrGzA3naAvLH8S/cSKP56ZeZ2cHAUGQHEwPYFpMMoAE24IU59tG0X3sRZwpOJhRcTJr1bJvbKxXmAUencjwJNMB4X0DBv/k6bgOgUc7vEXQNCNQymgx0otFvZIE1IFOEaQhAFcdlRlHs1u/Rvb8nJIKj/lV0D3CZnW+D1fHu9Vs+TYeLSWGJMTFuJqAW4/pZQG3cKX/NSg+hEbcDXP0+vuV3FvGd7wRe12fv7yWynT8pwhIlbJNasSvYxbtQeeVDZvBG0KdP5Ob6fTQDmqi5GR1Kri4vQtlNB4u7Ydub8fOPr8uowy9mecb2FTOXn1x6IU/UyUoA4MWk1eE87qsDFvIjF+gi5Cm+r8kz4DXIU+T9GOESwICuYFx0pa+tNYNtBlnkvADUf4RlCT79ffBv8cMyFTGumlBIncceyKkBsJX5pfy5AVnpeJiGlqX9t59v1zI1TvqeIfV7qs7SwUtsF9SWGUHwqpXnOuWTRN3X3m6D856SNSYzNr3AvBpQMv0oSAeupy+3Zy/Wt4sJAq77aTSjkhoZTY2kmJcZoM08H415zXe2YgotdY96TuV4EiAyLKsxWRl++2AwDjgY0AABp3djXlcHxtI2ZgZMIucFoP6Pv7+Iubhe7Nr6xfJxxSJRZ3q5DPACqhgXVsVk1CdNsAGvfwt4yRwBgFkmQPl7lqWDl85vFCIcHZ9JJOCtlui8lDa6zT6+yXpZ5lgfDcfqp+26ew5QPjBFvQSwAohbGX1E37PxNShTkp9E01FMje2+jFqLLsHw8ovwdV3DvALuGKFZMoxR/drPjvP7i76f73W9rj/0S9thPBbX98wg5GnAXHiJUtANOiLAk474rZcZFCUsMGBa8tOP+Tgug2K3ChZer/0XTL3oUXeHcNDfzAxj7b2Y4G19VGyXAbwmHoZ72n+ZZT5fOni5uaKmHkQAplgra0IQnVJBdT6nx8/bNmrdx+Tjyxh5/AaF6Syq4sDiRMUUvQ54hVO8jOKL0m9GJDfYCIqfey4zJ3LZjL8r4pq9CltVJ8yB+WPGtORJ86J+Hk/lUwkAeODlAF5iCsnQ8rSXcQvwO9E9roGxizYzm6iZNjab+c9ctORH5Pyy47jcb9OZpjv0m1n4CGDlqFRM6ZtMxxtJEFDAlS+0OLXvq2xLBy+CarccAURw3RzbjoCMUMDrWs78SGx+s0FNpYa/I+sX/QUY/G6ArJIPJlUK5yr/iPrVmri8J9aqKqdOOcbQ/Q5ewFTnuq2zBch0QOaPzmhUJ8zG4r+vQukMp6lok3ZwB2Cxbb0gc/EF8OJXMhBE30ZhXvuE4XckPQRcWJbnHPX8vd8jjqt1xgZcdMwMuuSXzMN/iZn41+SRYzICr1r3G10DXPrx0JH33eHyni4dvNyam9ff3TwwwLw4CfvUK+AyIv7+NIkLdcxoXX2+/vjieKUxrwZgWE0BWQCCX46y8VvIg99njlrNxquY+tRM2AczsQH01Q5eCbAMwJqxVSieRxN0lTg++QO8iKm3JbASltOZF9PtXWS7DPASGtEZl3Y1k2hGEZN29po2V+c2Mn5yKwt/UhiUP/Snb6BzL8vkANa//+VOLbzuy+f0VbnwSst8Z+G1Od4Fvwt4qWIhd86cfuKnJAGE7JhEH4meSLMcANHAOm7lLsr7Y5d55WFyAAhLmK5sJPd45daKDyw+gfcfrmVNY/NhMCl9b9HFNV37Q3btJRtMVMgEMGW63r75tuRGsVYuZYlGo1+LrsaZvZ72MzPLXOPH5OMiO4zaGldAZn8CwDVC85VOaD8AyTTdjetKO1aMWXRbWhttK2X6MgB0vjEFzGJbfLlMRjn3+J/lohce8VM21OhxjibbgNxpKd8NvLoACAOaVxLAvCgw9NG9GzUlS7HY2VgYiv/hQ3KMp5GXWoJHtXaxflQ8VxbNbqUBgUqUDbhYGQBcBxK00OrtDcB5FACj/MxGfhPr3vgg7mZhuVX9Nb095Sb8RP70xWRHxXQFtH5/tp4c8IlW720GuNKGbeAZYfCJUmBTWJVC34FYDcgDcFV0/0jg+YkwPvOkZv+jO5Jb2l1LEOpfHt2qQFTJLgEX1w7dKtL1mWt8r5dOAXi1GQ4C4PMSP2JmQx4jIyGmI/WyEQlIbL+VfmbxSvblBmgjcndvzT6KQWtZCTBBleyscJRR060mN9rHj0a8tryqgZdFuxsVssG8xWBtLELRprInAf5Js7P8TE9ebVYQKOAnw2aON+CiUmNoFWCkHwr2BbywrDJV0261RjV1HEV39sTw2UdFHIaQJRtEY1p8XfxcPyQQ1YBoZQngOm2syw2dAvCy1AZwxVGfhv4Q8NoOeKU9K6c8Kov5ULanMSMVQl8WgFFqal1n9YseGs3fXkqohKHIa/kM5RtLAeu3JXfMf8wLa3iaHWRWnybKOYrlEMrxIJ1hKp9KALCbTXz+Zqui1kWw23SDDPcCfdO+vjaC+OiE36EzWxl4DUH0hf5q136un6+G9mj8og8p0tjcDnNnHv7bL/FxBbiERyAQd24m1U1SQ1Fzny99H79qR/6F7w9eqWoJsmSZ0ImAlbTRZtZ0Su2JcfFVMB1R7Bq10ol7NoAj3+03frDpFE1r+j0WSH21esNvG7Ur8j9LSmwY0uPlUPyNO8zslpqETHdnhb564fP1prYCCM0UTAqm+Cr5uMzm2bTCsps227dTejT2IKg+AFRQzWkorJsa8AJazgJQrVX8qcxFzCubxjIhA2hMRuB2Wst3B6/9giFcDkRbJyn8A6/Xb1aHNYKtZqaPMopAdn6LCl2gwrnM0csPaBgX+HgnrMsMkf3yxKNRuNqkNufKBnuB5AOMDGpA3llQr633LLfhfrDQ+UWeGwBMBl20ArDkoF+LhWN2n7n41x/torXWFlwHuL5Xfq7jtsWpAy8xS91voxNiObU4NUyLYjIj5Taafcxq+zhiuz/huDd+Vj/PR2IiA1E1eQCc7oV12Y3mZUbQvmTJ/bXlG0Vpz+rtHrveGBfg4jM18Akofh4T28w18BIMiskbADCii1bKNxrQYhY+jHlYDvqYipgXP9f3zM913LY4deDF1OmzG6vXP5a9Xb6J6BlWdiWApujA6P9FKzpcgXk6Jr8NaHpoS7QoHjbG96WYwV25FqFdMP89Mw3jAlx8XQVeYVoGvD+Sakas3rssZuV6qCDjktbF+dOYVzaJzYy+zTP4uOy3KK7LLL+Za/1MP6zg51MsmlMHXpFZsYkuuA+JpTKNK5uD9xSMA+taT6AhL9S7RE3XDE6U9rv4o6pWy/mDWXyo+8Uawizi4+pbY91L6ATFk3tJJ6aonKzd91VO1+VUc6m/0v1b7hloVfS88JqsH6wsDWFdZhiBu5RCChY/tr9rqUL4wo/VgB8doAuSCYieB1zl40p6m19yYFwtnfP3zc/1hVv44sunDrz211QcE8chh7QCnJiKGAjlk7LDTCSlpLQXxQfm3oGR2UeZNnVSyslc6v6cMhsD+BS3HcPkyH4hn+HngMskTjnFY1Jjo0845xPPxTn/z2QilalBNL0VCcpFAK3epEgANi47hDOG3thWGFdCI+TmAlxCIq6IFD9D5fSDV4S/mlgvhQ8sT6Oozf+FiYkD489Q7MZ9kXxgOiGTWqT9lZhEUmpjpDoyIBP3xXFf5mPkcyXhKGVnlrTOx5+IoO6XqUgWDbw2Z//8c332f35/VcAlxAbr6ktv3PlFATCDFuCSer2ntmEu/gXrirko0SbgQhAQhbNUzgZ4ZYbRDKTUNPxhFJY5xP/Vsznygb2IY/aiFcxLx2QuPY+ZhHUBLltRMR+dw73KnAT+WS16rkRU4BXGBbjIQGZUueEk9/uf/3xVTnoZOSwFwrwuCmj1Rq5BLOAFuJiIskQUeAW4fgnzMkuNcVW2CMzgDJVTD15MI+ZPCESoFw/XLJ0yDteYCt0JxvkqjXRbgtH2hrwoPrC27rLtgNP3oeQjZGZbaqWzipSW+JHJbblV94N0X9gZ0tcauMrvl3txLh9XwKlcBzlbMuUQ01U7S4d1tbjAFih6lu71W+qqbZuLoJ35uJiKHPKV3iYA9mMCUg1sdKSHJH3Lb33v75x68NovIDa8WC/hAVWiwG2NYfPnrK1mi6jB/+W8fd7jwILh82svMdBnWQbzjz+uVkcX3FvT4nHSOgM07LWOOPvPWhwYwOIaaAutm5koAPVpspHKAw+05MdiKmJbNas4TOQY+M57AVwVxxW2ZeOMmlUc2BbGBbiktwFafVLsrMrk7IHX4MAncDFfQgJ0XgytO/crDuzSRpzXLWHf+TYV2jKTvvZSznNZOVBUoQJy8P/l0dts7dbSCWNfa9lUQWFynzUFrgmbMG3AhGmaRbQ92HwmUmDmdeaivGy+Q0dOSZB7yX6MP2WlpE0FoPYBq0XONx+XkAiMC3DJ5ALoznI5c+AFrDgXZaG4tcoMihcnpjrbHphJ7aEwIy0nUjTqeQWwEJGUBmCoVjGvdF6+LxMZz+IHJAsf47zvTlmKS45nrXTmBbiwSjOL/8xs4n/++jLHq4rnIgMgvrk9LAFyk7CrCeus3fKR6kvHlWJeYV0CULtj3qwi5zwfF1PRgOVzXReO9AOn8ENnTns1kiUxA0aV+5nfw8DK3leYkUwLs4+ckZy5HNsOo3B1dh/08ByUDmDuZycJxj58bGmNX0cefIM3rtn5O8AehTVTu5k1kDawJQ8szCYowJ9vkQy7L4ysl+3CrdbJDbknYNPDIOy0YxYVKL/IImsByi+SNlmIiP0NfxtCIiy6do8VOpFzMa5z0MYHbiENo23k49KuWLQJGY55S35EzguJ4KQXDmFWkXP+LPu49svgzIHX/hvopmKPA6sRNh1WJ9RhsQ8jdO06nLM0NtXZdY4hU8P+a5715+6vgXQYaFiIWdieg58cKLjlRM53BoWm1JVSJ0zWqNyOxlqXKY8CrIBUTbikHbUX89eGGUxFbVnAFZBy1r6i5oHW/9/eubY3cSRRWPJNtvEFYzvJPptNPuz//10JDwkGEuMYA97znuoajQaxMqA4mpkeGPfcNJfq7tNVp6urc8IKxn8O2UwMJUsNi9CLXngCGdAhQ3QI+3FprGLGnk8H1D76ca0qd/0HL4FU2w8MrUyNkYELNwECrD1X6wyYoaFZ+6J5RwOL/6tk1LPzxYRUzEb3xqnyvyraCB0Y8GGA1lkBrzP5/zw71oS6OvZB65Fk8k/6hQG8AJcJeWnM+G0xggAOK1OAKlecTwE0BuljLqJxAVxhIupmQ1usCgdw2QpRA03vMtoWYW3guBhkTUoPYzqg9tGPa1XWDQO8Wn5gtDDwYXje0yLhVZzABXkNV6JRNQQlDQ1sKLZjyWlrlfqmSEPzwoxi9MFvOzfNPHyA1tPT2eTyDyJnqtKrwpsHEcj/k35hgA6mLj5ZNwJeZlF/oZ7EX+SEiwsEnTEvtX8lEL5SSjhnR5GAFsBcLI0SLVPKYFUl6NN5cgeNyyZ9aaRpoAkkSDwuSHm0LzQuxioSOaKvflyr8qX34EUmtv3AACrK75ZMH7zv2aciQFrT+wjhjwaGOXIrToxWngpDQY/WepXINv98VNrQOgj7wjfmgukFB0jvI0B++w4eEI5IqVZMtCdqANBa6ZHCLIEDQ45etU3laXgxMS/ss5AXy5aUq2VcQAWtkH1S9wY61XwAejd4LUYKuDdRJm8CF5FjMRPRJF+zSutC2xr6YqBCtFppYKBDyBdSYs5DxANa5rhKPC6iQzDImg6toS69B69uxtCTMlPGwt+wUCkh78l7Jvh4carCD9lbViozlSdI3uEAmD9+yR+AHXkQFoYFYPkoreXmrbgxaTMEoQO0YpVPGFqtNFlXFqWQwtFYBC+GvBPUSLv4ZYDSQxOk0IzMZylfAFa0JSbGAFBJaWQAL4ALp2NMwjQXiT3PaAK0LXfS8DEDXwAuy1d0CLIm5BGzbWEOYi4yWYbHKtr5NAdY79mPj98NeRkceJFhtEgstFKRf/S4bdvT/NlJmB+7Arg7aRyYKPgCsXyUQyuVbcgLQO3Y+/pIHDg99ZaADG3mV4NWDOI9KNuRhiYGb4KDMGBGA4GcA8wEZBI0PZZoZO0F0KJhsJyVwjkCnozDRMvKgeRoWta20Lr0Pvbj0tyGpA1hr+NcA3Dx3jYR2w8b4DbiZEYod6DYhysGV9v1QdoWPBdmoldRATQ+6YDczYuhiWd44KXcJvPoPt5XPDDUZlZUaFopyGp8wQixA59yI9ORhUodZs+w0YvvRMNB6/E0apIBwAUvgka1J1MbretAXAkrnCHDi7JHyxEKBGAGMl2LNousATFWtIP2AsAAWF4FXgAnoZnDLIyeRKKdMrsPvBydCgYvAEwr1xN3i/e1dqz7WVMuaftZg9yWOJGpXSFUbg8P5IAqwPpJE8L+99+nJuWJNe8hYMqnHP4FcHXzYmjyGRx40VIx5IWM85Bk9z4K0FTJmGsR7YDWPldaNCoJY+PealBvtuh0t1Pxkq//os7KAAAPOElEQVQZSsbzPeEDlV8U4J17VJJ9gZcn1YX3SvCS7KggAV4CNwEXWtgcvEIL6w43Yi6CAK8Yf2nwUoMBgGEaIveYlkzAJRBjH3PRTqZK0Q7HtKSZSPlNK+JAvnhwkDQmGbYZB1QP95HWdaBJMtzgSDOmIRnLMjjw6mYchQGTBlOHhQrIhJq03mholyI8CRNM1zsaCK0/xD5EcBD6w9bEuvLCbLY5uRWgBtjdY+6hLcmkO1AjgLlos7HhwEJrQ3vrmio2G4vmBNdls1Hmos1GmX9dsxG504BER0r37Ya/D2DRIOTK+MSnuLaI58JqoBfRkU/lDoE1AXBxbVAki1rv0KU1AvAK7ouMpGCcHMpEPAvggvT87UzhgeXkyJRY9E7ualAzGgALFQ23ijEtqZkBIAA8QHanbbShvZ13c46r8DCpIQBayBfNt70AhtznU8I+iPsAMz0DHkvP4nkAV07o277XGLaRJ2DkCTKeRFgj/Lbw2fpBja6dTgEyk/aaV1Fasvkw5UdX9kOX1wjACyIZ4JJZo5qEWo3GRY/N7fkHRyJgolsKjDO/VD4C12WP3NALQfv7Arxwn4jQQoDSzdZ7d3wATuzbtJGcSJGZRcY2N/Kf1h0FXtZd0eB0GDDjGcKzksJhle0CcnkN141tSc3rSMCFxnUp0Prp++PJz+K4flaKawQcLmWYFDM/8iTyY0zyGj54KTepZFmzoARoqWix0AhwAfC4OVUU0VzmxDCJADlaQbQwO0FKGzBp7Jo2r4RDKywJHPrCoX3axnxPlkdwHrCirAFClEs4xWcyDQllQxpxuMKPix7GM1kL/Iayyep7bcyXPe6LDB68lokTLDOtqcynBWNc5J16cCgMRwK1M+0zZOalWj78wTxPZCGW8UOCyIfQT41h2TPqsSqBZRIAbAAfT4xSyh8x1uC2SPHdImSzAUwpmhYzWENx4O7Db/17CvEnau6yJw732OjAq1HClPEAWIDXzMBFhIWcvPXiVGPpVHgIufJCXt0MS0Ezg7uZwstM5p75wy0e9cvWLgGMAAEQvd6s9CCeCaDgslitcanRNIAphd441jVJbaS2VbFLFtTaM6cHN4QjsFGkggR40ZLhkX9+8jF6H08iYgFRC56KyN8Tt0CX/3VxkOQTzQ3R+NWlSuALJECRobwBXOGWAngRX/5o8p/vj0zKEzCQqB8OHCjgslnJ9SLlAS9rXErGXvxGCV6UNbdcSgmpsw2jX2ZnxfmSQkXPI4O72XZPmbWuifyftosPEgHvNKmDiH2cKO07VQjnwlCbnOZZdRmZBBpgwcRTydIfQIeyhoP0TGUMPzpSzEKG9/wov60fvzuWG49MRLlAnCpUkU1FNZx1WS6B0YLXcnGoVRSqzQRYRKdgyfAq+DbhI4Y/WMQHk1e4ZushZIuHtpTBxHT9x2/86wpgluKI/hi4Si+syhKjDhhi5REJSp/IQ96jFVSWSBlADadFbHlGgABc0BdpEYxIcl/8qRW8OiJDpW/GRqrg0QMUwDXzODJMSU/4IH8wYmNdKdYUg4Vfb98WDU1Evu4ZZD43r712HREPehdTDq0+ZyrflebEyA4PpMY/SyYhsdOeSeM6l7lI72GCGQEF6QUHuFjDRBy0uL7p4yp4dcSH5gVYAVr74rngwuiNZDAxTpQAFQ6tv76cTX45lGPr7K3nj4QTI/4U1wi6iuUo4KrY1ZHw0HfDfYFyxIBqAgDQkwivdUnAQK2EsMHhNGLKz+yuk5E7KHfmxNSIco+6fF4CFbw6sqG85NhITlGoGOBtx0rhEi0jccPdWyTNjPDKgJ1TXftGpiTe6QY7AR7OrpUT6wi557uBKQEsCTTJaQE+8FqAFhOeEGbbJqHA6jvxWYDXD3jMOz1UT+KeeTG7UOjGFa8eXjgqeK2SFSaAr4F8lVOrCiTRKe8+7Ms0mHisH5rZpVrWVxcRktgToCo6AinhXBgTmOFeKie2SuCbfR5wAWhijRmZ6MSJcEExgBpH01h3HIbcQ3lkKnp8IqajeC7KEHwYhH7ea/Tdh1+Y9RW8VggsgWvCZB0qXRQ4JvWk0DFI2aF2TiMqBZNEQOYz4UUM9r71JBGYmoSgtnOrzMvKia0Q+kafLsCFWacVLQuzEA0LB1P8sgKkYp4AygdABlgloBEhgmi1NIQJXLprxa4vzPcKXg8QWKjyUbgMXlsKTwJwabYKR2AQz5XTqwFez6801bzW53Js3Ve6LcADuIgdVjmxBwh8wy+hPABcrPgAMogaAv7ijJ7DMhGGzEJMRLRyaAUIeIai7cotB5eJheCNaHMb/s2b+HoVvB6YKy5c+tPwYQIkFilSc41KAEUrjG8YIEernOGT4cTgQODE4MEcRUGcWAbYA9ysmSnF1QLfMng27l+Xx5NA2yw0QJHnUrPZNujAaZW8JX/xfL+QiwNuDrl6Fh9ATOB1Ii297edVSfj15WUFrzXI0i0x96ElVsHGRPggTgwSFw2NLvALcR7/Oo9xkubAZGK+1YzOxK9CI3MMsUL0w4u9h+xXOsbICmvIkq++BWZcNj47amxofIhOGhF55/5adNwQS+tY0R/SXMwU9wd8AsPdIYAP0KqG4Vdny9IfVvBaKpaHH3RL7ctlRigFvBiLZuASrwHncX6iKK3nihSqmOyEOWagN+YlKSGQCUf9pmzDmzF56l+6F177xeni4S9Ur/wmCQBeaFaMsMADHpDKENhoWYBSruzDfzJh76EcS53qehyc01+LcmDgQnW3+v5Nr1d/3JJABa+WML52k8Jp606FcypTIoELMxDtiZXJJkjxyMfRlUlUIfVJf5ej60wOr1OZJJPprV8Dc/KdCv7YgiF+bR6s63eYh2hcABeDpiHiw6k0nEtxLCXiCOMOOQ4Jb0qguEYw6bHNRPNaW+6RtsZF2VjXS9b7WAIVvNZUENDAWIITm49H63JixG2nVWZlGIhjkxOfnMqiY69UGa6lhf15veeUEDzEHUs+LCOS4sHfbBdujOvwijWQjp0ra8ACYl3AoQyyBmRNqBDuHNO+tSNS7ScBj1c8RDymoAELsBJwGcjUowh44fKAE3NwY3GfLAcuDPXP3yqBCl5/q3jj5hRo0/uqIEQIOBRXAtBQ6OFFaL0ZPkJPFWYl/mFEcWWFH/MkueLAmLzC035Ji2NAuLU5yH9pdEzfRgqoGb3k2jFWsj8ARPoOcpeM2xwWJmF6s9u5GE5LeeCY8UppRJh0JFf4y7bZSMwtzMWYmT35LMDxEQpSfcSCBCp4LYhj/TsU6ijXauV1eyoPWhetPRUGfuzpsQh7eC6B03yQt8ALUl8Ahqn5p5xd7fTKXIYi+uHGcmZpzwYuaxPF6/4+0Ov+ngdzYP3ftNF3tLADuNC2GAlhVwWBEvI2OKHpKg8AoPTBAqTsi6XjRBPhXA6oNmEvR9SGuDfYab4D5WXyWc7nyOiNFs+QXq6C1yPk5gInJrd8gIvW/ug+NLBwk4iwOmhP9DxmDySzGV3h5CqejBSHV8j9N9d3k9ci+7e1jXaBJofPmagyoRi1KIBrjNgVQCIAU2vh8YWAFmAlgMIUpPf3RL2EEO90qDSe79J+IeBtwtPDKACDz2rMStmf8+0wOclblpJ4u/55HAlU8HocOTeFu/ET+8xzAbI0DzERGV4E/+JeLUwWVTiH5BFwHZsbK2amQO5a807y25hzMsAw/MXQyMKMXNzHn2x+ztfovWxuGvsWTc902zAgNqj4QI5NdbxU8/jyZp+KH2c4kZsc4yj73i6pcHphn/Oh/RQzUQfwx4rxp2GW2wSU7JiwNc2+7DEkbhauDQlgaF+Y8rEyM48eUJeNlEAFrw3LFqqKg9bdmyUz6KBVpbaG1nB9HKZjmo02OTE7tQJeOY0YaUwewkSzMYEI04q91/3a+94ux5qOAe3zXP1vgC8AsH2Mk3pjgOz/cGwAjC4IYNJ2gJEAprXdBikT4DoJKPHdDHZOB1FHE9X+jrSgZpuePY4phVPEGTgHRmMqegJdaVExBlEghmmofXeayERMkxEuEtDi9/QYxnvz7nXZRAlU8NqwXKHCUGFVO115qchUJPgWiGIT9gIlhhlF5IoYmgSBD1hx3o6vMj3/kvNrXu9JXpPo57rWNvcKwAvS37MkFbAzgDVAFoA21f69egYMbJipQq/Pcmz6lAQugxamnD5yqm9qg1Sz7e8N7Qm+Kgl2T3IrYGkieLAtGcXQm0xDY7JDKWAl4DIYld/5Xt5eJO3bhD3XpKtDBS/ybnOXqVrTxgDY3Ncc15uFhhMKTYIH8cJyu+0iEXyZzsmh1SangCmjWbjXUtqYOwFKZwCAdmNOTcd1LDsKYpbwADt4txyrCY/Gc/0cAZq3S/qRFHNUAJZaWbc0AQCpaZmD0oEtgZJBuZUml2QNSgDiHkKdx3XhgKgNAm+0JWtP8FFFo4rjQcRbm9I1OIviwoAbCppXo70JGBuQ5D3Kvp+tfdK5iRogO66S16+vreDVr/z65G1peRLs0ITQxuDJWO1uYfBKwIphSAtgVjS0HJ5ks9PgBYBhagqgtAZ4AaCxbeDSAwEzP99pvEv7JRswKMCQgAGABWCFSehtHcP0SwBLTckEukCJoVaYgJkCYAlqBjidcw8ivYUFwHCLkJJnUApurP12dbvPEqhmY59zT+9uq8x/5TemHbghTCUAhcoKf3MwQ6PaDTNTgATA2Zws2zY5ixmZE+wmJ+b5KZdofR48nsCF9iXg5JnLlgbA9D5sA2A5byHv2AAa23p/gMzBHguH1TYNF3y0umajvhVNK8j2MP8SuHTrFNayV6zHeiiBCl49zLRlrwwoCD5c8eGCplN6yuDKtmOmb2lPcFnMDj7ntMp26xga1qJ5OtfsQsPT+QJUBiu2eSEAzCl/WoteizfTC5WUzQJinCvbvL63CydmQNPBJOoBtCTlATZraEKmPJ/nSOczUMdzfG893e/RerW62W8JVLOx3/m38PYJIoBLcFFtoAFkOvsCnDkQtUAKGIr/1qhyx/fXE0PBKnvluoUX+cxOgFieBFhie368AIwONIDj7bgWcEO7DMBTyj/tpznYPm/tjmuBrPifD67pQCRQwWsgGVk/o0pgbBIIZ6KxfXX93iqBKoHeS6CCV++zsH5AlcA4JVDBa5z5Xr+6SqD3Eqjg1fssrB9QJTBOCVTwGme+16+uEui9BCp49T4L6wdUCYxTAv8DEjrQaSitTukAAAAASUVORK5CYII="

/***/ }),
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */
/*!********************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/static/images/icon/prod-col.png ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QkRCRkE3QjcwMDBGMTFFODkyRkRGREU3OUZGMkMwRDAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QkRCRkE3QjgwMDBGMTFFODkyRkRGREU3OUZGMkMwRDAiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpCREJGQTdCNTAwMEYxMUU4OTJGREZERTc5RkYyQzBEMCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpCREJGQTdCNjAwMEYxMUU4OTJGREZERTc5RkYyQzBEMCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PmHFvkoAAAZCSURBVHjazJlvaB1FEMDv7l0SBdNvoiit/eOfWC3kQ4qKmCaafJGUUqxalWKCGtuoRVFQGtGKQZGKosWWfohWhLZIlagpYoy29EMLbTGtJlFsmkZixVZoKonk37t7/uZu9nF5eS+5l5cPXZg3u7OzM7Ozs7N7++xUKmVdzsVtaGhIN8TYoqIiy7Zty3YcIVwBrYb2SrqXAlfRHqT9h+/7neBTwut5nuUIf6TQH9LoT/l+OfX7GHsD/AvpHgH6aR+n3QnPGDyB/snJyUB/2sAchifgfhX8hApMFzNYDerE/2+Df8whpwbuLUy2Ojo2Q9YgulrBLYA3zYPRhi53Bco/o14W6TpL+zR4FKHXgsuBEjWgBv4d1J+JyoH2EbxNERnjwEn6/gZfSd9N4CWAOGAr/OtZiQ30n8jqQRGaSCQqYTwkk1PaV8B2BhymOWlmzXIspFJH8yVZejzUBPPN8Naqd79X4y2m3A96l/52+AZNzIOLgFXIexZYA6mMccewoYrwOGyMtOvr68M1TSSWuYnEafX7JLNZj6gv7YylkaU18YWCYmAX/fWqdJew0H5K47AVvibaE2aM4KhTBOhfh/49kIqEkPS8G9HfP8WDCdc9gBm25/sePHcwqsuaZYfDJ4obUDyMkueApyN92+nbHGy4jNjLUvaLQfAdTyAMY9tpLw8cojNrRMQtMhvaj/qe12XHTAOinPGbqR6NkI8ILXNnz1TQ+xPwmHr0VsY+GdgmacV13RYrjJdOiJ+L35Jsd2f2mRsDBT8UoT2otFnHC5+XTFrqnH3oPqgh1xLYRkVSwNXK0Cx5yw61WHgywBpvmcsbbhhwEgWUP6mvhOaD/zKxZniyTczX3Gerp1OhJ5vRd4T+a8DVLpXV6uKzKDpmGMUwE4Mx40jKiTyWNEjwVoaupO8fxXMDGLcYnvslBlfomIN4U5Y7BK0LLVuSLaQYWWl9Eb2q75Auf7l4cJEuV58xJseOnTcDNZHPxPK76lvkBrknHDQWzVG5Zl6IoSbvzSYD48fUy0VioLGqOJ+0MFcDY8ZzsQlVV85GBiwDFsc1MHJcxY659NEVI5ZZyaVaPScxeIrK3UCV7KqUOYhjeiMOn4ROXF6dTKVe47rEwG/pkFtHmR4vvfl4xTZpKRuP9hkjY5bl7OYy1fGdwwVRDPw3OJhd9xVZ5jhgEu1Man3Nd+a0iQNigy7zEA7rkJuHB3ygS7AB4hK51crpEAe88BTJWryYMgREp+gWG3R1PhTb3OLiYlmCbVi8JXSi20rHvXETc64NI+PN50MeKajV3JLw5jbJy87o6KgFjGD9C2p5NZ1r4wqOXBamBHrcy0Ik94nOat0cL2LTf2Kb3djYmM7s4G54b4NpGJdfB4wYRanIuRz1mt6GwqXOuNjqsk1JMZlyJEaRUQqco11KVw99t6djV4TLMgfudJx1OrBULo0lJSUBPZpg9dMgbZihpSI72xghRgqfgLkRZcpRHe2iUz0vt+vAJpHvTExMWOPj45ZgZvybuFeFr4Lh/WzHmygTITOd3embuvJlLrcxVnVUBpsK3WJD1CZJM5YBIeDa9zCgTYP2ecZtzCY8zpka5c1xo9moOmTSbaJbHZUGN/ODSGeyFvQzsILZ74T+D/UvhDept9+5nMPmeqXjH0DeTp3AL6pz2uZyst1yx5kFhlRBOq/0/aC6ebw31KlMKedF13i4etNCwZnhQnmRmd0JvqS0b0C1Bd1mQlSrsqRcUh0Xc6Wk2a4vA/KdIQLUyA7Q6jnfpC1rjcowDhDZAzPmxxhy+xB0D3hYd+XXwMP5xKHG3yN8e7cpaVhl9s2awGPq6EXgXcCQBvE+aJvycN4mxu1xwpQ1JLLi3pqcPLzQw7av0Oc3MXIHP6/FGLpVeDWfDqqMnthHYB6fYrLL5E2vgla3FT7CvIHij3NtCLz2Cf2vK6lbxooMK58zeg6fixc0uH9QmryASuCXRlhL4ewwj0rCq2Mu5Pv5OtevJPkCrAF2q8JavCXvetcLSF3TiXh9t/DKmDk9AReYcBvwzBnwmxgl74O96tUFGrfNoLcKeqMu6IUgvJrLw9MZ0sin4AXaNemnUo/Tv7fgR/R5+t7di6G/4rmXtf0OPyeteXgusS/3vyH+F2AAqMH0EhkSXAgAAAAASUVORK5CYII="

/***/ }),
/* 94 */
/*!****************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/static/images/icon/more.png ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QkU2QUYyNzk1RTU5MTFFODhDMUI4Qjk4ODgyRDhCOTkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QkU2QUYyN0E1RTU5MTFFODhDMUI4Qjk4ODgyRDhCOTkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpCRTZBRjI3NzVFNTkxMUU4OEMxQjhCOTg4ODJEOEI5OSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpCRTZBRjI3ODVFNTkxMUU4OEMxQjhCOTg4ODJEOEI5OSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgMp+AwAAAIzSURBVHjatJY9SFtRFMdvXj7AQOugTooI4lKsVKmTg4OL4iIogoga0KFCEav1YyhUioKiREpxEI0iGEwHxbbi1sGhVNwKpZtDhw4d6uAQ8h1/R25AqKU+wznw59z7wn2/9z+5X558Pm/cRCQSqc7lcrOMW6f7TZ4V3uHxeK5zPB43gUDABINB4/V6TTabvVYqlTKOcRnAFgGMog265W7HuwYmk8kffr8/h5tmuseoUhWYyWTm0RBOsxZ6hGrUgBLpdHqXkvajFN0n6AOqVQM6jiN6DzAkcwSnDeiQdpMK8EbsoQF0ierR9v+gxQIlDlAfuhCn5JjNakBjZ2svJf5DrmMNfmJSNWoCJT6jbtz99vl81Sz4j/zPTwubgQZQ4gSX3bj7BbSKNXvMEmqRSaYFlPiCegD/ZDurSCQSh3xAa8GpBlDiVP5TnJ3L9sceus8HdAhUCyhxBqQLyFfaZZQ2Rn/Quc+ilxOAMv31W6Fskm37O/nUPnvI2Faf0Y3X6IU9vvbIU5rAZVy9lAYViTKBhuWwUQHiZJU0LmUHtgM4pLUs5H1vAYxbZ1ukkNrCx9kasDF7M1inPaK1tcl7NgE8kw7r7h3t5/INGsASeywN236YCTIhlwONzfuBXOTQoC3pEmkad5l/DShmlpby4ii50/bfoLnbylg0ECcCi9Fst49eoQWVOw2wRxw9+zdgM3eF3cshp/kK0DbZU8mTOA27Ge8ayCU4ys7/GJBMlrDb8VcCDACd9eAwREUiFwAAAABJRU5ErkJggg=="

/***/ }),
/* 95 */,
/* 96 */,
/* 97 */
/*!**************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/api/detail.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.detail = void 0;var _httprequest = _interopRequireDefault(__webpack_require__(/*! ../utils/httprequest */ 20));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

//详情接口
var detail = function detail(params) {return _httprequest.default.get("/prod/prodInfo", params);};exports.detail = detail;

/***/ }),
/* 98 */,
/* 99 */,
/* 100 */,
/* 101 */,
/* 102 */,
/* 103 */,
/* 104 */,
/* 105 */,
/* 106 */
/*!**************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/api/search.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.seAechCc = exports.seArch = void 0;var _httprequest = _interopRequireDefault(__webpack_require__(/*! ../utils/httprequest */ 20));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

//搜索界面接口
var seArch = function seArch(params) {return _httprequest.default.get("/search/hotSearchByShopId", params);};
//搜索结果
exports.seArch = seArch;var seAechCc = function seAechCc(params) {return _httprequest.default.get("/search/searchProdPage", params);};exports.seAechCc = seAechCc;

/***/ }),
/* 107 */,
/* 108 */,
/* 109 */,
/* 110 */,
/* 111 */,
/* 112 */,
/* 113 */,
/* 114 */,
/* 115 */,
/* 116 */,
/* 117 */,
/* 118 */,
/* 119 */,
/* 120 */,
/* 121 */,
/* 122 */,
/* 123 */,
/* 124 */,
/* 125 */,
/* 126 */,
/* 127 */,
/* 128 */,
/* 129 */
/*!*****************************************************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/uni_modules/uni-goods-nav/components/uni-goods-nav/i18n/index.js ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0;var _en = _interopRequireDefault(__webpack_require__(/*! ./en.json */ 130));
var _zhHans = _interopRequireDefault(__webpack_require__(/*! ./zh-Hans.json */ 131));
var _zhHant = _interopRequireDefault(__webpack_require__(/*! ./zh-Hant.json */ 132));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}var _default =
{
  en: _en.default,
  'zh-Hans': _zhHans.default,
  'zh-Hant': _zhHant.default };exports.default = _default;

/***/ }),
/* 130 */
/*!****************************************************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/uni_modules/uni-goods-nav/components/uni-goods-nav/i18n/en.json ***!
  \****************************************************************************************************/
/*! exports provided: uni-goods-nav.options.shop, uni-goods-nav.options.cart, uni-goods-nav.buttonGroup.addToCart, uni-goods-nav.buttonGroup.buyNow, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"uni-goods-nav.options.shop\":\"shop\",\"uni-goods-nav.options.cart\":\"cart\",\"uni-goods-nav.buttonGroup.addToCart\":\"add to cart\",\"uni-goods-nav.buttonGroup.buyNow\":\"buy now\"}");

/***/ }),
/* 131 */
/*!*********************************************************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/uni_modules/uni-goods-nav/components/uni-goods-nav/i18n/zh-Hans.json ***!
  \*********************************************************************************************************/
/*! exports provided: uni-goods-nav.options.shop, uni-goods-nav.options.cart, uni-goods-nav.buttonGroup.addToCart, uni-goods-nav.buttonGroup.buyNow, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"uni-goods-nav.options.shop\":\"店铺\",\"uni-goods-nav.options.cart\":\"购物车\",\"uni-goods-nav.buttonGroup.addToCart\":\"加入购物车\",\"uni-goods-nav.buttonGroup.buyNow\":\"立即购买\"}");

/***/ }),
/* 132 */
/*!*********************************************************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/uni_modules/uni-goods-nav/components/uni-goods-nav/i18n/zh-Hant.json ***!
  \*********************************************************************************************************/
/*! exports provided: uni-goods-nav.options.shop, uni-goods-nav.options.cart, uni-goods-nav.buttonGroup.addToCart, uni-goods-nav.buttonGroup.buyNow, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"uni-goods-nav.options.shop\":\"店鋪\",\"uni-goods-nav.options.cart\":\"購物車\",\"uni-goods-nav.buttonGroup.addToCart\":\"加入購物車\",\"uni-goods-nav.buttonGroup.buyNow\":\"立即購買\"}");

/***/ }),
/* 133 */,
/* 134 */,
/* 135 */,
/* 136 */,
/* 137 */,
/* 138 */,
/* 139 */,
/* 140 */
/*!*******************************************************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/uni_modules/uni-search-bar/components/uni-search-bar/i18n/index.js ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0;var _en = _interopRequireDefault(__webpack_require__(/*! ./en.json */ 141));
var _zhHans = _interopRequireDefault(__webpack_require__(/*! ./zh-Hans.json */ 142));
var _zhHant = _interopRequireDefault(__webpack_require__(/*! ./zh-Hant.json */ 143));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}var _default =
{
  en: _en.default,
  'zh-Hans': _zhHans.default,
  'zh-Hant': _zhHant.default };exports.default = _default;

/***/ }),
/* 141 */
/*!******************************************************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/uni_modules/uni-search-bar/components/uni-search-bar/i18n/en.json ***!
  \******************************************************************************************************/
/*! exports provided: uni-search-bar.cancel, uni-search-bar.placeholder, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"uni-search-bar.cancel\":\"cancel\",\"uni-search-bar.placeholder\":\"Search enter content\"}");

/***/ }),
/* 142 */
/*!***********************************************************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/uni_modules/uni-search-bar/components/uni-search-bar/i18n/zh-Hans.json ***!
  \***********************************************************************************************************/
/*! exports provided: uni-search-bar.cancel, uni-search-bar.placeholder, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"uni-search-bar.cancel\":\"cancel\",\"uni-search-bar.placeholder\":\"请输入搜索内容\"}");

/***/ }),
/* 143 */
/*!***********************************************************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/uni_modules/uni-search-bar/components/uni-search-bar/i18n/zh-Hant.json ***!
  \***********************************************************************************************************/
/*! exports provided: uni-search-bar.cancel, uni-search-bar.placeholder, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"uni-search-bar.cancel\":\"cancel\",\"uni-search-bar.placeholder\":\"請輸入搜索內容\"}");

/***/ }),
/* 144 */,
/* 145 */,
/* 146 */,
/* 147 */,
/* 148 */,
/* 149 */,
/* 150 */,
/* 151 */
/*!****************************************************************************************!*\
  !*** C:/Users/86131/Desktop/Mall4/uni_modules/uni-icons/components/uni-icons/icons.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0;var _default = {
  "id": "2852637",
  "name": "uniui图标库",
  "font_family": "uniicons",
  "css_prefix_text": "uniui-",
  "description": "",
  "glyphs": [
  {
    "icon_id": "25027049",
    "name": "yanse",
    "font_class": "color",
    "unicode": "e6cf",
    "unicode_decimal": 59087 },

  {
    "icon_id": "25027048",
    "name": "wallet",
    "font_class": "wallet",
    "unicode": "e6b1",
    "unicode_decimal": 59057 },

  {
    "icon_id": "25015720",
    "name": "settings-filled",
    "font_class": "settings-filled",
    "unicode": "e6ce",
    "unicode_decimal": 59086 },

  {
    "icon_id": "25015434",
    "name": "shimingrenzheng-filled",
    "font_class": "auth-filled",
    "unicode": "e6cc",
    "unicode_decimal": 59084 },

  {
    "icon_id": "24934246",
    "name": "shop-filled",
    "font_class": "shop-filled",
    "unicode": "e6cd",
    "unicode_decimal": 59085 },

  {
    "icon_id": "24934159",
    "name": "staff-filled-01",
    "font_class": "staff-filled",
    "unicode": "e6cb",
    "unicode_decimal": 59083 },

  {
    "icon_id": "24932461",
    "name": "VIP-filled",
    "font_class": "vip-filled",
    "unicode": "e6c6",
    "unicode_decimal": 59078 },

  {
    "icon_id": "24932462",
    "name": "plus_circle_fill",
    "font_class": "plus-filled",
    "unicode": "e6c7",
    "unicode_decimal": 59079 },

  {
    "icon_id": "24932463",
    "name": "folder_add-filled",
    "font_class": "folder-add-filled",
    "unicode": "e6c8",
    "unicode_decimal": 59080 },

  {
    "icon_id": "24932464",
    "name": "yanse-filled",
    "font_class": "color-filled",
    "unicode": "e6c9",
    "unicode_decimal": 59081 },

  {
    "icon_id": "24932465",
    "name": "tune-filled",
    "font_class": "tune-filled",
    "unicode": "e6ca",
    "unicode_decimal": 59082 },

  {
    "icon_id": "24932455",
    "name": "a-rilidaka-filled",
    "font_class": "calendar-filled",
    "unicode": "e6c0",
    "unicode_decimal": 59072 },

  {
    "icon_id": "24932456",
    "name": "notification-filled",
    "font_class": "notification-filled",
    "unicode": "e6c1",
    "unicode_decimal": 59073 },

  {
    "icon_id": "24932457",
    "name": "wallet-filled",
    "font_class": "wallet-filled",
    "unicode": "e6c2",
    "unicode_decimal": 59074 },

  {
    "icon_id": "24932458",
    "name": "paihangbang-filled",
    "font_class": "medal-filled",
    "unicode": "e6c3",
    "unicode_decimal": 59075 },

  {
    "icon_id": "24932459",
    "name": "gift-filled",
    "font_class": "gift-filled",
    "unicode": "e6c4",
    "unicode_decimal": 59076 },

  {
    "icon_id": "24932460",
    "name": "fire-filled",
    "font_class": "fire-filled",
    "unicode": "e6c5",
    "unicode_decimal": 59077 },

  {
    "icon_id": "24928001",
    "name": "refreshempty",
    "font_class": "refreshempty",
    "unicode": "e6bf",
    "unicode_decimal": 59071 },

  {
    "icon_id": "24926853",
    "name": "location-ellipse",
    "font_class": "location-filled",
    "unicode": "e6af",
    "unicode_decimal": 59055 },

  {
    "icon_id": "24926735",
    "name": "person-filled",
    "font_class": "person-filled",
    "unicode": "e69d",
    "unicode_decimal": 59037 },

  {
    "icon_id": "24926703",
    "name": "personadd-filled",
    "font_class": "personadd-filled",
    "unicode": "e698",
    "unicode_decimal": 59032 },

  {
    "icon_id": "24923351",
    "name": "back",
    "font_class": "back",
    "unicode": "e6b9",
    "unicode_decimal": 59065 },

  {
    "icon_id": "24923352",
    "name": "forward",
    "font_class": "forward",
    "unicode": "e6ba",
    "unicode_decimal": 59066 },

  {
    "icon_id": "24923353",
    "name": "arrowthinright",
    "font_class": "arrow-right",
    "unicode": "e6bb",
    "unicode_decimal": 59067 },

  {
    "icon_id": "24923353",
    "name": "arrowthinright",
    "font_class": "arrowthinright",
    "unicode": "e6bb",
    "unicode_decimal": 59067 },

  {
    "icon_id": "24923354",
    "name": "arrowthinleft",
    "font_class": "arrow-left",
    "unicode": "e6bc",
    "unicode_decimal": 59068 },

  {
    "icon_id": "24923354",
    "name": "arrowthinleft",
    "font_class": "arrowthinleft",
    "unicode": "e6bc",
    "unicode_decimal": 59068 },

  {
    "icon_id": "24923355",
    "name": "arrowthinup",
    "font_class": "arrow-up",
    "unicode": "e6bd",
    "unicode_decimal": 59069 },

  {
    "icon_id": "24923355",
    "name": "arrowthinup",
    "font_class": "arrowthinup",
    "unicode": "e6bd",
    "unicode_decimal": 59069 },

  {
    "icon_id": "24923356",
    "name": "arrowthindown",
    "font_class": "arrow-down",
    "unicode": "e6be",
    "unicode_decimal": 59070 },
  {
    "icon_id": "24923356",
    "name": "arrowthindown",
    "font_class": "arrowthindown",
    "unicode": "e6be",
    "unicode_decimal": 59070 },

  {
    "icon_id": "24923349",
    "name": "arrowdown",
    "font_class": "bottom",
    "unicode": "e6b8",
    "unicode_decimal": 59064 },
  {
    "icon_id": "24923349",
    "name": "arrowdown",
    "font_class": "arrowdown",
    "unicode": "e6b8",
    "unicode_decimal": 59064 },

  {
    "icon_id": "24923346",
    "name": "arrowright",
    "font_class": "right",
    "unicode": "e6b5",
    "unicode_decimal": 59061 },

  {
    "icon_id": "24923346",
    "name": "arrowright",
    "font_class": "arrowright",
    "unicode": "e6b5",
    "unicode_decimal": 59061 },

  {
    "icon_id": "24923347",
    "name": "arrowup",
    "font_class": "top",
    "unicode": "e6b6",
    "unicode_decimal": 59062 },

  {
    "icon_id": "24923347",
    "name": "arrowup",
    "font_class": "arrowup",
    "unicode": "e6b6",
    "unicode_decimal": 59062 },

  {
    "icon_id": "24923348",
    "name": "arrowleft",
    "font_class": "left",
    "unicode": "e6b7",
    "unicode_decimal": 59063 },

  {
    "icon_id": "24923348",
    "name": "arrowleft",
    "font_class": "arrowleft",
    "unicode": "e6b7",
    "unicode_decimal": 59063 },

  {
    "icon_id": "24923334",
    "name": "eye",
    "font_class": "eye",
    "unicode": "e651",
    "unicode_decimal": 58961 },

  {
    "icon_id": "24923335",
    "name": "eye-filled",
    "font_class": "eye-filled",
    "unicode": "e66a",
    "unicode_decimal": 58986 },

  {
    "icon_id": "24923336",
    "name": "eye-slash",
    "font_class": "eye-slash",
    "unicode": "e6b3",
    "unicode_decimal": 59059 },

  {
    "icon_id": "24923337",
    "name": "eye-slash-filled",
    "font_class": "eye-slash-filled",
    "unicode": "e6b4",
    "unicode_decimal": 59060 },

  {
    "icon_id": "24923305",
    "name": "info-filled",
    "font_class": "info-filled",
    "unicode": "e649",
    "unicode_decimal": 58953 },

  {
    "icon_id": "24923299",
    "name": "reload-01",
    "font_class": "reload",
    "unicode": "e6b2",
    "unicode_decimal": 59058 },

  {
    "icon_id": "24923195",
    "name": "mic_slash_fill",
    "font_class": "micoff-filled",
    "unicode": "e6b0",
    "unicode_decimal": 59056 },

  {
    "icon_id": "24923165",
    "name": "map-pin-ellipse",
    "font_class": "map-pin-ellipse",
    "unicode": "e6ac",
    "unicode_decimal": 59052 },

  {
    "icon_id": "24923166",
    "name": "map-pin",
    "font_class": "map-pin",
    "unicode": "e6ad",
    "unicode_decimal": 59053 },

  {
    "icon_id": "24923167",
    "name": "location",
    "font_class": "location",
    "unicode": "e6ae",
    "unicode_decimal": 59054 },

  {
    "icon_id": "24923064",
    "name": "starhalf",
    "font_class": "starhalf",
    "unicode": "e683",
    "unicode_decimal": 59011 },

  {
    "icon_id": "24923065",
    "name": "star",
    "font_class": "star",
    "unicode": "e688",
    "unicode_decimal": 59016 },

  {
    "icon_id": "24923066",
    "name": "star-filled",
    "font_class": "star-filled",
    "unicode": "e68f",
    "unicode_decimal": 59023 },

  {
    "icon_id": "24899646",
    "name": "a-rilidaka",
    "font_class": "calendar",
    "unicode": "e6a0",
    "unicode_decimal": 59040 },

  {
    "icon_id": "24899647",
    "name": "fire",
    "font_class": "fire",
    "unicode": "e6a1",
    "unicode_decimal": 59041 },

  {
    "icon_id": "24899648",
    "name": "paihangbang",
    "font_class": "medal",
    "unicode": "e6a2",
    "unicode_decimal": 59042 },

  {
    "icon_id": "24899649",
    "name": "font",
    "font_class": "font",
    "unicode": "e6a3",
    "unicode_decimal": 59043 },

  {
    "icon_id": "24899650",
    "name": "gift",
    "font_class": "gift",
    "unicode": "e6a4",
    "unicode_decimal": 59044 },

  {
    "icon_id": "24899651",
    "name": "link",
    "font_class": "link",
    "unicode": "e6a5",
    "unicode_decimal": 59045 },

  {
    "icon_id": "24899652",
    "name": "notification",
    "font_class": "notification",
    "unicode": "e6a6",
    "unicode_decimal": 59046 },

  {
    "icon_id": "24899653",
    "name": "staff",
    "font_class": "staff",
    "unicode": "e6a7",
    "unicode_decimal": 59047 },

  {
    "icon_id": "24899654",
    "name": "VIP",
    "font_class": "vip",
    "unicode": "e6a8",
    "unicode_decimal": 59048 },

  {
    "icon_id": "24899655",
    "name": "folder_add",
    "font_class": "folder-add",
    "unicode": "e6a9",
    "unicode_decimal": 59049 },

  {
    "icon_id": "24899656",
    "name": "tune",
    "font_class": "tune",
    "unicode": "e6aa",
    "unicode_decimal": 59050 },

  {
    "icon_id": "24899657",
    "name": "shimingrenzheng",
    "font_class": "auth",
    "unicode": "e6ab",
    "unicode_decimal": 59051 },

  {
    "icon_id": "24899565",
    "name": "person",
    "font_class": "person",
    "unicode": "e699",
    "unicode_decimal": 59033 },

  {
    "icon_id": "24899566",
    "name": "email-filled",
    "font_class": "email-filled",
    "unicode": "e69a",
    "unicode_decimal": 59034 },

  {
    "icon_id": "24899567",
    "name": "phone-filled",
    "font_class": "phone-filled",
    "unicode": "e69b",
    "unicode_decimal": 59035 },

  {
    "icon_id": "24899568",
    "name": "phone",
    "font_class": "phone",
    "unicode": "e69c",
    "unicode_decimal": 59036 },

  {
    "icon_id": "24899570",
    "name": "email",
    "font_class": "email",
    "unicode": "e69e",
    "unicode_decimal": 59038 },

  {
    "icon_id": "24899571",
    "name": "personadd",
    "font_class": "personadd",
    "unicode": "e69f",
    "unicode_decimal": 59039 },

  {
    "icon_id": "24899558",
    "name": "chatboxes-filled",
    "font_class": "chatboxes-filled",
    "unicode": "e692",
    "unicode_decimal": 59026 },

  {
    "icon_id": "24899559",
    "name": "contact",
    "font_class": "contact",
    "unicode": "e693",
    "unicode_decimal": 59027 },

  {
    "icon_id": "24899560",
    "name": "chatbubble-filled",
    "font_class": "chatbubble-filled",
    "unicode": "e694",
    "unicode_decimal": 59028 },

  {
    "icon_id": "24899561",
    "name": "contact-filled",
    "font_class": "contact-filled",
    "unicode": "e695",
    "unicode_decimal": 59029 },

  {
    "icon_id": "24899562",
    "name": "chatboxes",
    "font_class": "chatboxes",
    "unicode": "e696",
    "unicode_decimal": 59030 },

  {
    "icon_id": "24899563",
    "name": "chatbubble",
    "font_class": "chatbubble",
    "unicode": "e697",
    "unicode_decimal": 59031 },

  {
    "icon_id": "24881290",
    "name": "upload-filled",
    "font_class": "upload-filled",
    "unicode": "e68e",
    "unicode_decimal": 59022 },

  {
    "icon_id": "24881292",
    "name": "upload",
    "font_class": "upload",
    "unicode": "e690",
    "unicode_decimal": 59024 },

  {
    "icon_id": "24881293",
    "name": "weixin",
    "font_class": "weixin",
    "unicode": "e691",
    "unicode_decimal": 59025 },

  {
    "icon_id": "24881274",
    "name": "compose",
    "font_class": "compose",
    "unicode": "e67f",
    "unicode_decimal": 59007 },

  {
    "icon_id": "24881275",
    "name": "qq",
    "font_class": "qq",
    "unicode": "e680",
    "unicode_decimal": 59008 },

  {
    "icon_id": "24881276",
    "name": "download-filled",
    "font_class": "download-filled",
    "unicode": "e681",
    "unicode_decimal": 59009 },

  {
    "icon_id": "24881277",
    "name": "pengyouquan",
    "font_class": "pyq",
    "unicode": "e682",
    "unicode_decimal": 59010 },

  {
    "icon_id": "24881279",
    "name": "sound",
    "font_class": "sound",
    "unicode": "e684",
    "unicode_decimal": 59012 },

  {
    "icon_id": "24881280",
    "name": "trash-filled",
    "font_class": "trash-filled",
    "unicode": "e685",
    "unicode_decimal": 59013 },

  {
    "icon_id": "24881281",
    "name": "sound-filled",
    "font_class": "sound-filled",
    "unicode": "e686",
    "unicode_decimal": 59014 },

  {
    "icon_id": "24881282",
    "name": "trash",
    "font_class": "trash",
    "unicode": "e687",
    "unicode_decimal": 59015 },

  {
    "icon_id": "24881284",
    "name": "videocam-filled",
    "font_class": "videocam-filled",
    "unicode": "e689",
    "unicode_decimal": 59017 },

  {
    "icon_id": "24881285",
    "name": "spinner-cycle",
    "font_class": "spinner-cycle",
    "unicode": "e68a",
    "unicode_decimal": 59018 },

  {
    "icon_id": "24881286",
    "name": "weibo",
    "font_class": "weibo",
    "unicode": "e68b",
    "unicode_decimal": 59019 },

  {
    "icon_id": "24881288",
    "name": "videocam",
    "font_class": "videocam",
    "unicode": "e68c",
    "unicode_decimal": 59020 },

  {
    "icon_id": "24881289",
    "name": "download",
    "font_class": "download",
    "unicode": "e68d",
    "unicode_decimal": 59021 },

  {
    "icon_id": "24879601",
    "name": "help",
    "font_class": "help",
    "unicode": "e679",
    "unicode_decimal": 59001 },

  {
    "icon_id": "24879602",
    "name": "navigate-filled",
    "font_class": "navigate-filled",
    "unicode": "e67a",
    "unicode_decimal": 59002 },

  {
    "icon_id": "24879603",
    "name": "plusempty",
    "font_class": "plusempty",
    "unicode": "e67b",
    "unicode_decimal": 59003 },

  {
    "icon_id": "24879604",
    "name": "smallcircle",
    "font_class": "smallcircle",
    "unicode": "e67c",
    "unicode_decimal": 59004 },

  {
    "icon_id": "24879605",
    "name": "minus-filled",
    "font_class": "minus-filled",
    "unicode": "e67d",
    "unicode_decimal": 59005 },

  {
    "icon_id": "24879606",
    "name": "micoff",
    "font_class": "micoff",
    "unicode": "e67e",
    "unicode_decimal": 59006 },

  {
    "icon_id": "24879588",
    "name": "closeempty",
    "font_class": "closeempty",
    "unicode": "e66c",
    "unicode_decimal": 58988 },

  {
    "icon_id": "24879589",
    "name": "clear",
    "font_class": "clear",
    "unicode": "e66d",
    "unicode_decimal": 58989 },

  {
    "icon_id": "24879590",
    "name": "navigate",
    "font_class": "navigate",
    "unicode": "e66e",
    "unicode_decimal": 58990 },

  {
    "icon_id": "24879591",
    "name": "minus",
    "font_class": "minus",
    "unicode": "e66f",
    "unicode_decimal": 58991 },

  {
    "icon_id": "24879592",
    "name": "image",
    "font_class": "image",
    "unicode": "e670",
    "unicode_decimal": 58992 },

  {
    "icon_id": "24879593",
    "name": "mic",
    "font_class": "mic",
    "unicode": "e671",
    "unicode_decimal": 58993 },

  {
    "icon_id": "24879594",
    "name": "paperplane",
    "font_class": "paperplane",
    "unicode": "e672",
    "unicode_decimal": 58994 },

  {
    "icon_id": "24879595",
    "name": "close",
    "font_class": "close",
    "unicode": "e673",
    "unicode_decimal": 58995 },

  {
    "icon_id": "24879596",
    "name": "help-filled",
    "font_class": "help-filled",
    "unicode": "e674",
    "unicode_decimal": 58996 },

  {
    "icon_id": "24879597",
    "name": "plus-filled",
    "font_class": "paperplane-filled",
    "unicode": "e675",
    "unicode_decimal": 58997 },

  {
    "icon_id": "24879598",
    "name": "plus",
    "font_class": "plus",
    "unicode": "e676",
    "unicode_decimal": 58998 },

  {
    "icon_id": "24879599",
    "name": "mic-filled",
    "font_class": "mic-filled",
    "unicode": "e677",
    "unicode_decimal": 58999 },

  {
    "icon_id": "24879600",
    "name": "image-filled",
    "font_class": "image-filled",
    "unicode": "e678",
    "unicode_decimal": 59000 },

  {
    "icon_id": "24855900",
    "name": "locked-filled",
    "font_class": "locked-filled",
    "unicode": "e668",
    "unicode_decimal": 58984 },

  {
    "icon_id": "24855901",
    "name": "info",
    "font_class": "info",
    "unicode": "e669",
    "unicode_decimal": 58985 },

  {
    "icon_id": "24855903",
    "name": "locked",
    "font_class": "locked",
    "unicode": "e66b",
    "unicode_decimal": 58987 },

  {
    "icon_id": "24855884",
    "name": "camera-filled",
    "font_class": "camera-filled",
    "unicode": "e658",
    "unicode_decimal": 58968 },

  {
    "icon_id": "24855885",
    "name": "chat-filled",
    "font_class": "chat-filled",
    "unicode": "e659",
    "unicode_decimal": 58969 },

  {
    "icon_id": "24855886",
    "name": "camera",
    "font_class": "camera",
    "unicode": "e65a",
    "unicode_decimal": 58970 },

  {
    "icon_id": "24855887",
    "name": "circle",
    "font_class": "circle",
    "unicode": "e65b",
    "unicode_decimal": 58971 },

  {
    "icon_id": "24855888",
    "name": "checkmarkempty",
    "font_class": "checkmarkempty",
    "unicode": "e65c",
    "unicode_decimal": 58972 },

  {
    "icon_id": "24855889",
    "name": "chat",
    "font_class": "chat",
    "unicode": "e65d",
    "unicode_decimal": 58973 },

  {
    "icon_id": "24855890",
    "name": "circle-filled",
    "font_class": "circle-filled",
    "unicode": "e65e",
    "unicode_decimal": 58974 },

  {
    "icon_id": "24855891",
    "name": "flag",
    "font_class": "flag",
    "unicode": "e65f",
    "unicode_decimal": 58975 },

  {
    "icon_id": "24855892",
    "name": "flag-filled",
    "font_class": "flag-filled",
    "unicode": "e660",
    "unicode_decimal": 58976 },

  {
    "icon_id": "24855893",
    "name": "gear-filled",
    "font_class": "gear-filled",
    "unicode": "e661",
    "unicode_decimal": 58977 },

  {
    "icon_id": "24855894",
    "name": "home",
    "font_class": "home",
    "unicode": "e662",
    "unicode_decimal": 58978 },

  {
    "icon_id": "24855895",
    "name": "home-filled",
    "font_class": "home-filled",
    "unicode": "e663",
    "unicode_decimal": 58979 },

  {
    "icon_id": "24855896",
    "name": "gear",
    "font_class": "gear",
    "unicode": "e664",
    "unicode_decimal": 58980 },

  {
    "icon_id": "24855897",
    "name": "smallcircle-filled",
    "font_class": "smallcircle-filled",
    "unicode": "e665",
    "unicode_decimal": 58981 },

  {
    "icon_id": "24855898",
    "name": "map-filled",
    "font_class": "map-filled",
    "unicode": "e666",
    "unicode_decimal": 58982 },

  {
    "icon_id": "24855899",
    "name": "map",
    "font_class": "map",
    "unicode": "e667",
    "unicode_decimal": 58983 },

  {
    "icon_id": "24855825",
    "name": "refresh-filled",
    "font_class": "refresh-filled",
    "unicode": "e656",
    "unicode_decimal": 58966 },

  {
    "icon_id": "24855826",
    "name": "refresh",
    "font_class": "refresh",
    "unicode": "e657",
    "unicode_decimal": 58967 },

  {
    "icon_id": "24855808",
    "name": "cloud-upload",
    "font_class": "cloud-upload",
    "unicode": "e645",
    "unicode_decimal": 58949 },

  {
    "icon_id": "24855809",
    "name": "cloud-download-filled",
    "font_class": "cloud-download-filled",
    "unicode": "e646",
    "unicode_decimal": 58950 },

  {
    "icon_id": "24855810",
    "name": "cloud-download",
    "font_class": "cloud-download",
    "unicode": "e647",
    "unicode_decimal": 58951 },

  {
    "icon_id": "24855811",
    "name": "cloud-upload-filled",
    "font_class": "cloud-upload-filled",
    "unicode": "e648",
    "unicode_decimal": 58952 },

  {
    "icon_id": "24855813",
    "name": "redo",
    "font_class": "redo",
    "unicode": "e64a",
    "unicode_decimal": 58954 },

  {
    "icon_id": "24855814",
    "name": "images-filled",
    "font_class": "images-filled",
    "unicode": "e64b",
    "unicode_decimal": 58955 },

  {
    "icon_id": "24855815",
    "name": "undo-filled",
    "font_class": "undo-filled",
    "unicode": "e64c",
    "unicode_decimal": 58956 },

  {
    "icon_id": "24855816",
    "name": "more",
    "font_class": "more",
    "unicode": "e64d",
    "unicode_decimal": 58957 },

  {
    "icon_id": "24855817",
    "name": "more-filled",
    "font_class": "more-filled",
    "unicode": "e64e",
    "unicode_decimal": 58958 },

  {
    "icon_id": "24855818",
    "name": "undo",
    "font_class": "undo",
    "unicode": "e64f",
    "unicode_decimal": 58959 },

  {
    "icon_id": "24855819",
    "name": "images",
    "font_class": "images",
    "unicode": "e650",
    "unicode_decimal": 58960 },

  {
    "icon_id": "24855821",
    "name": "paperclip",
    "font_class": "paperclip",
    "unicode": "e652",
    "unicode_decimal": 58962 },

  {
    "icon_id": "24855822",
    "name": "settings",
    "font_class": "settings",
    "unicode": "e653",
    "unicode_decimal": 58963 },

  {
    "icon_id": "24855823",
    "name": "search",
    "font_class": "search",
    "unicode": "e654",
    "unicode_decimal": 58964 },

  {
    "icon_id": "24855824",
    "name": "redo-filled",
    "font_class": "redo-filled",
    "unicode": "e655",
    "unicode_decimal": 58965 },

  {
    "icon_id": "24841702",
    "name": "list",
    "font_class": "list",
    "unicode": "e644",
    "unicode_decimal": 58948 },

  {
    "icon_id": "24841489",
    "name": "mail-open-filled",
    "font_class": "mail-open-filled",
    "unicode": "e63a",
    "unicode_decimal": 58938 },

  {
    "icon_id": "24841491",
    "name": "hand-thumbsdown-filled",
    "font_class": "hand-down-filled",
    "unicode": "e63c",
    "unicode_decimal": 58940 },

  {
    "icon_id": "24841492",
    "name": "hand-thumbsdown",
    "font_class": "hand-down",
    "unicode": "e63d",
    "unicode_decimal": 58941 },

  {
    "icon_id": "24841493",
    "name": "hand-thumbsup-filled",
    "font_class": "hand-up-filled",
    "unicode": "e63e",
    "unicode_decimal": 58942 },

  {
    "icon_id": "24841494",
    "name": "hand-thumbsup",
    "font_class": "hand-up",
    "unicode": "e63f",
    "unicode_decimal": 58943 },

  {
    "icon_id": "24841496",
    "name": "heart-filled",
    "font_class": "heart-filled",
    "unicode": "e641",
    "unicode_decimal": 58945 },

  {
    "icon_id": "24841498",
    "name": "mail-open",
    "font_class": "mail-open",
    "unicode": "e643",
    "unicode_decimal": 58947 },

  {
    "icon_id": "24841488",
    "name": "heart",
    "font_class": "heart",
    "unicode": "e639",
    "unicode_decimal": 58937 },

  {
    "icon_id": "24839963",
    "name": "loop",
    "font_class": "loop",
    "unicode": "e633",
    "unicode_decimal": 58931 },

  {
    "icon_id": "24839866",
    "name": "pulldown",
    "font_class": "pulldown",
    "unicode": "e632",
    "unicode_decimal": 58930 },

  {
    "icon_id": "24813798",
    "name": "scan",
    "font_class": "scan",
    "unicode": "e62a",
    "unicode_decimal": 58922 },

  {
    "icon_id": "24813786",
    "name": "bars",
    "font_class": "bars",
    "unicode": "e627",
    "unicode_decimal": 58919 },

  {
    "icon_id": "24813788",
    "name": "cart-filled",
    "font_class": "cart-filled",
    "unicode": "e629",
    "unicode_decimal": 58921 },

  {
    "icon_id": "24813790",
    "name": "checkbox",
    "font_class": "checkbox",
    "unicode": "e62b",
    "unicode_decimal": 58923 },

  {
    "icon_id": "24813791",
    "name": "checkbox-filled",
    "font_class": "checkbox-filled",
    "unicode": "e62c",
    "unicode_decimal": 58924 },

  {
    "icon_id": "24813794",
    "name": "shop",
    "font_class": "shop",
    "unicode": "e62f",
    "unicode_decimal": 58927 },

  {
    "icon_id": "24813795",
    "name": "headphones",
    "font_class": "headphones",
    "unicode": "e630",
    "unicode_decimal": 58928 },

  {
    "icon_id": "24813796",
    "name": "cart",
    "font_class": "cart",
    "unicode": "e631",
    "unicode_decimal": 58929 }] };exports.default = _default;

/***/ })
]]);
//# sourceMappingURL=../../.sourcemap/mp-weixin/common/vendor.js.map